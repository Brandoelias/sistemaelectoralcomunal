<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-4">
		<div class="jumbotron p-2 d-block mx-auto w-25 text-center mb-3 titleBox">
			<h2 class="m-0">Unidades</h2>
		</div>


		<table class="table table-hover table-stripped">
			
			<th>Nombre</th>
			<th>Editar</th>
			<th>Borrar</th>

			<?php foreach($unidades as $unidad){ ?>

				<tr>
					<td class="idUnidad d-none">
						<?php echo $unidad->id ?>
					</td>
					<td class="nombreUnidad">
						<?php echo $unidad->nombre_unidad ?>
					</td>
					<td>
						<img class="editarImg editarUnidad" src="<?php echo SERVERURL ?>/views/img/editar.svg">
					</td>
					<td>
						<img class="borrarImg borrarUnidad" src="<?php echo SERVERURL ?>/views/img/borrar.svg">
					</td>
				</tr>

			<?php } ?>

			<?php if(count($unidades) < 1){ ?>

				<tr>
					<td colspan="8">
						No hay unidaes registrados.
					</td>
				</tr>

			<?php } ?>

		</table>

		<button class="btn btn-success d-block mx-auto mt-5 agregarUnidad" type="button">Agregar +</button>

		<a class="btn btn-dark d-block mx-auto mt-3" style="width: 100px;" href="<?php echo SERVERURL ?>/menu/consejoscomunales/">
			Regresar
		</a>

	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>