<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main>
		<div class="jumbotron pr-5 pl-5 pt-3 pb-3 d-block mx-auto text-center loginBox" style="width: 345px">
			<span class="text-primary loginText">Iniciar Sesión</span>
			<div id="loginErrorAlert" class="alert alert-danger text-center mx-auto w-100 mb-4 mt-2 rebote d-none">¡Datos incorrectos!</div>
			<form method="POST" autocomplete="off">
				<div class="form-group">
					<span class="form-label">Usuario...</span>
					<input id="usuario" class="form-control mt-3" type="text" />
				</div>
				<div class="form-group">
					<span class="form-label">Contraseña...</span>
					<input id="pass" class="form-control mt-2" type="password" autocomplete="new-password" />
				</div>
				<div>
					<img id="imgCaptcha" class="d-inline-block mx-auto mt-3 mb-1" style="border-radius: 8px; height: 60px; width: 200px" src="<?php echo SERVERURL . '/login/getCaptcha?rand=' . rand() ?>" />
					<img id="recargarCaptcha" class="btn btn-primary mt-3 d-inline-block" style="height: 40px; width: 40px" type="button" src="<?php echo SERVERURL . '/views/img/recargar.svg' ?>" />
					
				</div>
				<div class="form-group mt-3">
					<span class="form-label">Imágen de Seguridad...</span>
					<input id="captcha" class="form-control mt-2" type="text" />
				</div>
				<input id="loginButton" class="btn btn-primary mt-2" type="submit" value="Ingresar" />
			</form>
		</div>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>

<script>
	$(document).ready(function(){
		$("form").keypress(function(e) {
		  //Enter key
		  if (e.which == 13) {
		  	$('#loginButton').trigger('click');
		    return false;
		  }
		});
	});
</script>