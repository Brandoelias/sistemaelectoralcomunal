<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-4">
		<input id="tipoUsuario" class="d-none" type="hidden" value="<?php echo $tipoUsuario ?>" />
		<div class="jumbotron p-2 d-block mx-auto w-50 text-center mb-4 titleBox">
			<h2 class="m-0">Lista de Usuarios</h2>
			<h5 class="m-0">Total: <?php echo $totalUsuarios ?></h5>
			<?php if($tipoUsuario == 2){ ?>
				<span class="mt-3 d-block" style="font-size: 20px">Aquí puedes asignar a los Administradores de Elección</span>
			<?php }?>
		</div>

		<?php if($tipoUsuario == 1){ ?>
			<select id="filtrarPorConsejoUsuarios" class="form-control mx-auto d-block text-center w-25 mb-3 mt-4 pl-1" style="<?php !$filtrarConsejo ? print('color: grey') : '' ?>">
				<option disabled <?php !$filtrarConsejo ? print('selected') : '' ?> hidden>Buscar por Consejo Comunal...</option>
				<?php if($filtrarConsejo){ ?>
					<option value="" style="color: red">Cancelar</option>
				<?php } ?>
				<?php foreach ($consejoscomunales as $consejocomunal) { ?>	
					<option class="text-center" value="<?php echo $consejocomunal->cod ?>" <?php $filtrarConsejo == $consejocomunal->cod ? print('selected') : '' ?>><?php echo $consejocomunal->nombre ?></option>
				<?php } ?>
			</select>
		<?php } ?>

		<?php if(count($usuarios) > 0){ ?>

			<input id="nombreConsejo" class="d-none" type="hidden" value="<?php echo $usuarios[0]->nombre_consejo ?>" />

			<div class="form-group w-25 mx-auto mt-3 mb-0">
				<span class="form-label">Buscar por Cédula, Nombre o Apellido...</span>
				<input class="mx-auto form-control d-block pr-5" id="buscarUsuarioAjax" type="text" autocomplete="new-password" oninput="validarBusquedaNombreCedula(this)" />
			</div>
		<?php } ?>

		<div id="tablaDatosAjaxContenedor"></div>


		<table id="tablaDatos" class="table table-hover table-stripped mt-1">
			<th>Cédula</th>
			<th>Nombre y Apellido</th>
			<th>Consejo Comunal</th>
			<th>Nombre de Usuario</th>
			<th class="<?php $tipoUsuario != 1 ? print('d-none') : '' ?>">Tipo de Usuario</th>
			<th>Editar</th>
			<th>Borrar</th>

			<?php foreach($usuarios as $usuario){ ?>

				<tr>
					<td>
						<?php isset($usuario->cedula) ? print($usuario->cedula) : print(' - ') ?>
					</td>
					<td>
						<?php isset($usuario->pnombre) ? print($usuario->pnombre . ' ' . $usuario->papellido) : print(' - ') ?>
					</td>
					<td>
						<?php isset($usuario->nombre_consejo) ? print($usuario->nombre_consejo) : print(' - ') ?>
					</td>
					<td>
						<?php echo $usuario->nombre_usuario ?>
					</td>
					<td class="<?php $tipoUsuario != 1 ? print('d-none') : '' ?>">
						<?php echo $usuario->tipo ?>
					</td>
					<td idUsuario="<?php echo $usuario->id ?>" nombreUsuario="<?php echo $usuario->nombre_usuario ?>" tipoUsuario="<?php echo $usuario->tipo_id ?>">
						<?php if($usuario->id != 1 || $_SESSION['id'] == 1){ ?>
							<img class="editarImg editarUsuario" src="<?php echo SERVERURL ?>/views/img/editar.svg">
						<?php }else{ print(' - '); } ?>
					</td>
					<td>
						<?php if($usuario->id != 1){ ?>
							<img idUsuario="<?php echo $usuario->id ?>" nombreUsuario="<?php echo $usuario->nombre_usuario ?>" class="borrarImg borrarUsuario" src="<?php echo SERVERURL ?>/views/img/borrar.svg">
						<?php }else{ print(' - '); } ?>
					</td>
				</tr>

			<?php } ?>

			<?php if(count($usuarios) < 1){ ?>

				<tr>
					<td colspan="10">
						<?php if($tipoUsuario == 1){ ?>
							No hay usuarios registrados.
						<?php }else{ ?>
							No hay administradores de elección registrados.
						<?php } ?>
					</td>
				</tr>

			<?php } ?>

		</table>

		<div id="paginacionAjax" class="mx-auto w-100 d-flex justify-content-center"></div>

		<?php $this->model->paginar($paginaActual, $totalUsuarios, $limite, 'usuarios', $filtrarConsejo ? '?consejo=' . $filtrarConsejo : '') ?>

		<button id="agregarUsuario" class="btn btn-success d-block mx-auto mt-5 w-13 agregarUsuario" type="button">Agregar +</button>

		<a class="d-block mx-auto mt-3 w-13" href="<?php echo SERVERURL ?>/menu/">
			<button class="btn btn-dark d-block mx-auto w-100" type="button">Regresar</button>
		</a>

	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>