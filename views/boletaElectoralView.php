<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-4">
		<div class="jumbotron p-2 d-block mx-auto w-50 text-center mb-4 titleBox">
			<h2 class="mb-2">Boleta Electoral</h2>
			<h3><?php echo $eleccion->nombre ?></h3>
			<h4><?php echo $eleccion->fecha_formato ?></h4>
			<?php $sinCandidatos = true; ?>
		</div>

		<div class="boletaElectoral">
			<?php foreach($unidades as $unidad){ ?>
				<?php if(count($unidad->comites) > 0) { ?>
					<?php foreach($unidad->comites as $comite){ ?>
						<?php if(count($comite->candidatos) > 0) { ?>
							<?php $sinCandidatos = false; ?>
							<div class="tarjetaElectoralBoleta">
								<div class="nombreUnidadElectoral" style="border-bottom: none!important;">
									<b><?php echo $unidad->nombre_unidad ?></b>
								</div>
								<div class="nombreComiteElectoral">
									<b><?php echo $comite->nombre_comite ?></b>
								</div>
								<?php foreach($comite->candidatos as $candidato){ ?>
									<div class="candidatoElectoralBoleta">
										<div class="fotoCandidato">
											<img src="<?php echo SERVERURL ?>/views/img/fotos_personas/<?php echo $candidato->ci_persona ?>.jpg" onerror="this.onerror=null; this.src='<?php echo SERVERURL ?>/views/img/sin_foto.png'" />
										</div>
										<div class="nombreCandidato" style="width:310px">
											<?php echo "$candidato->pnombre $candidato->snombre $candidato->papellido $candidato->sapellido" ?>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>	
					<?php } ?>
				<?php }else{ ?>
					<?php if(count($unidad->candidatos) > 0){ ?>
						<?php $sinCandidatos = false; ?>
						<div class="tarjetaElectoralBoleta">
							<div class="nombreUnidadElectoral">
								<b><?php echo $unidad->nombre_unidad ?></b>
							</div>
							<?php foreach($unidad->candidatos as $candidato){ ?>
								<div class="candidatoElectoralBoleta">
									<div class="fotoCandidato">
										<img src="<?php echo SERVERURL ?>/views/img/fotos_personas/<?php echo $candidato->ci_persona ?>.jpg" onerror="this.onerror=null; this.src='<?php echo SERVERURL ?>/views/img/sin_foto.png'" />
									</div>
									<div class="nombreCandidato" style="width:310px">
										<?php echo "$candidato->pnombre $candidato->snombre $candidato->papellido $candidato->sapellido" ?>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php } ?>
				<?php } ?>
			<?php } ?>
			<?php if($sinCandidatos){ ?>
				<div class="w-100 text-center font-weight-bold">NO HAY CANDIDATOS POSTULADOS</div>
			<?php } ?>
		</div>

		
		<button class="btn btn-primary d-block mx-auto mt-5 no-print" style="width:100px" type="button" onclick="window.print()">Imprimir</button>
		<a class="btn btn-dark d-block mx-auto mt-3 no-print" style="width:100px" href="<?php echo SERVERURL ?>/elecciones/ver/<?php echo $url[2] ?>/">
			Regresar
		</a>

	</main>

	<div class="no-print">
		<?php include_once('./views/partials/footer.php') ?>
	</div>

</body>
</html>
<script>
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();
	});
</script>