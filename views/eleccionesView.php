<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="pb-1">
		<input id="tipoUsuario" class="d-none" type="hidden" value="<?php echo $tipoUsuario ?>" />
		<div class="jumbotron p-2 d-block mx-auto w-25 text-center menuBox">
			<span class="mb-3">Elecciones</span>
		</div>
		<div id="menuContenedor" class="mt-2">
			<a class="w-75 d-block mx-auto mt-3 btn btn-primary w-100 d-block mx-auto" href="<?php echo SERVERURL ?>/elecciones/finalizadas/"><img src="<?php echo SERVERURL ?>/views/img/eleccionesfinalizadas.svg">Finalizadas</a>
			<a class="w-75 d-block mx-auto mt-3 btn btn-primary w-100 d-block mx-auto" href="<?php echo SERVERURL ?>/elecciones/activas/"><img src="<?php echo SERVERURL ?>/views/img/eleccionesactivas.svg">Activas</a>
			<?php if($tipoUsuario == 1 || $tipoUsuario == 2){ ?>
				<a class="w-75 d-block mx-auto mt-3 crearEleccion btn btn-primary w-100 d-block mx-auto" href="#"><img src="<?php echo SERVERURL ?>/views/img/creareleccion.svg">Crear</a>
			<?php } ?>
		</div>
		<a class="btn btn-dark d-block mx-auto mt-2" style="width: 100px;" class="d-block mx-auto mt-3" href="<?php echo SERVERURL ?>/menu/">
			Regresar
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>