<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<main class="pt-0 pb-2 p-5" style="height: 100vh">

		<input id="idEleccion" type="hidden" value="<?php echo $eleccion->id ?>">

		<div class="jumbotron text-center graciasParticipacion">
			¡GRACIAS POR SU PARTICIPACIÓN!
		</div>

	</main>

</body>
</html>
<script>
	$(document).ready(function(){
	  	$(document).on("contextmenu",function(){
       		return false;
    	}); 
    	$(document).on("keydown", function(){
       		return false;
    	});

    	var timeout;

    	function esperar(){
    		var idEleccion = $('#idEleccion').val().trim();
    		$.post(SERVERURL + '/elecciones/esperando', {idEleccion : idEleccion}, function(res){
    			try{
	    			if(res){
	    				window.location.href = SERVERURL + '/elecciones/votar/' + idEleccion + '/';
	    			}else if(res == 'quitar'){
	                    window.location.href = SERVERURL + '/elecciones/autenticar/' + idEleccion + '/';
	                }
	                clearTimeout(timeout);
	    			timeout = setTimeout(function(){esperar()}, 3000);
	    		}catch(err){
	    			clearTimeout(timeout);
	    			timeout = setTimeout(function(){esperar()}, 3000);
	    		}

    		});
    	}

    	esperar();

      $(function() {
        $.ajaxSetup({
            error: function(jqXHR, exception) {
            	clearTimeout(timeout);
                timeout = setTimeout(function(){esperar()},3000);
            }
        });
      });


	});
</script>