<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="pr-4 pl-4">
		<div class="jumbotron p-2 d-block mx-auto w-25 text-center menuBox">
			<span class="mb-3">Menú Principal</span>
		</div>
		<div id="menuContenedor" class="mt-2">
			<?php if($tipoUsuario == 1){ ?>
				<a class="w-50 d-block mx-auto mt-3 btn btn-primary d-block mx-auto mt-3" href="<?php echo SERVERURL ?>/menu/consejoscomunales/">
					<img src="<?php echo SERVERURL ?>/views/img/consejoscomunales.svg">Consejos Comunales
				</a>
			<?php } ?>
			<a class="w-50 d-block mx-auto mt-3 btn btn-primary d-block mx-auto mt-3" href="<?php echo SERVERURL ?>/personas/"><img src="<?php echo SERVERURL ?>/views/img/habitantes.svg">Habitantes</a>
			<a class="w-50 d-block mx-auto mt-3 btn btn-primary d-block mx-auto mt-3" href="<?php echo SERVERURL ?>/elecciones/"><img src="<?php echo SERVERURL ?>/views/img/elecciones.svg">Elecciones</a>
			<?php if($tipoUsuario == 1 || $tipoUsuario == 2){ ?>
				<a class="w-50 d-block mx-auto mt-3 btn btn-primary d-block mx-auto mt-3" href="<?php echo SERVERURL ?>/usuarios/"><img src="<?php echo SERVERURL ?>/views/img/usuarios.svg">Usuarios</a>
			<?php } ?>
		</div>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>