<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-4">
		<input id="tipoUsuario" class="d-none" type="hidden" value="<?php echo $tipoUsuario ?>" />
		<div class="jumbotron p-2 d-block mx-auto w-50 text-center mb-3 titleBox">
			<h2 class="mb-2">Administrar Votaciones</h2>
			<h3><?php echo $eleccion->nombre ?></h3>
			<h4><?php echo $eleccion->fecha_formato ?></h4>
			<input id="idEleccion" type="hidden" value="<?php echo $eleccion->id ?>">
		</div>

		
		<div class="maquinas-votantes-container mt-5 p-1 text-center">
			<h4 class="text-center text-primary mt-2 mb-3 font-weight-bold">Máquinas de Votación</h4>
			<?php if($tipoUsuario == 1 || $tipoUsuario == 2){ ?>
				<h6 class="text-center text-primary mt-2 mb-3 font-weight-bold" style="font-size: 18px">Insertar máquinas de votación a través del siguiente enlace:<br><span class=""><?php echo SERVERURL . '/elecciones/autenticar/' . $eleccion->id .'/' ?></span></h6>
			<?php } ?>
			<div class="maquinas-votantes-ajax">
				<span class="text-secondary">Obteniendo...</span>
			</div>
		</div>
		<?php if($tipoUsuario == 1 || $tipoUsuario == 2){ ?>
			<button class="btn btn-info d-block mx-auto mt-5 cambiarPassVotacion" id="<?php echo $eleccion->id ?>">Cambiar Contraseña</button>

			<button class="btn btn-danger d-block mx-auto mt-3 finalizarVotaciones">Finalizar Votaciones</button>
		<?php } ?>
		<a class="btn btn-dark d-block mx-auto mt-5" style="width: 150px" href="<?php echo SERVERURL . '/elecciones/ver/' . $eleccion->id . '/' ?>">Regresar</a>

		<input id="idEleccion" type="hidden" value="<?php echo $eleccion->id ?>">
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>
<script>
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();

	  var tipoUsuario = $('#tipoUsuario').val();

	  var timeout;

	  var ultimasObtenidas = '';
	  function getMaquinasVotantes(){
	  	var idEleccion = $('#idEleccion').val().trim();
	  	$.post(SERVERURL + '/elecciones/getMaquinasVotantes', {idEleccion : idEleccion},function(res){
	  		try{
	  			if(res === ultimasObtenidas){
	  				clearTimeout(timeout);
					timeout = setTimeout(function(){getMaquinasVotantes()},3000);
	  				return;
	  			}
	  			$('.tooltip').remove();
	  			ultimasObtenidas = res;
		  		res = JSON.parse(res);
				if(typeof res == 'object' && res.length > 0){
					$('.maquinas-votantes-ajax').html('');
					res.forEach((r)=>{
						var maquina = `
						<div id="maquinaDeVotacion_${r.id}" class="maquina-votante" ${tipoUsuario != 1 && tipoUsuario != 2 ? 'style="height: 141px"' : ''}>
							<span class="maquina-votante-ip">${r.ip}</span>
							<div class="maquina-votante-img">
								<img class="maquinaVotanteConfiguracion ${tipoUsuario != 1 && tipoUsuario != 2 ? 'd-none' : ''}" src="${SERVERURL}/views/img/configuracion.svg" ip="${r.ip}" idMaquina="${r.id}" />
							</div>`;

						if(r.activado){
							maquina +=	`
								<button data-html="true" data-toggle="tooltip" title="Se está realizando una votación.<br>${r.pnombre} ${r.papellido} (${r.cedula})" data-placement="bottom" id="${r.id}" type="button" class="btn btn-success w-100 p-0 pt-1 pb-2 d-block mx-auto text-center disabled cursor-pointer maquinaVotanteId_${r.id}" style="color: #dddddd!important">ACTIVADA</button>
							`;
						}else{
							maquina +=	`<button id="${r.id}" type="button" class="btn btn-success w-100 d-block mx-auto text-center activarMaquinaVotante maquinaVotanteId_${r.id}">Activar</button>`;
						}
						if(tipoUsuario == 1 || tipoUsuario == 2){
							maquina += `<button id="${r.id}" type="button" class="btn btn-danger w-100 d-block mx-auto text-center quitarMaquinaVotante quitarMaquinaVotanteId_${r.id}">Quitar</button>`;
						}
						maquina += `</div>`;
						$('.maquinas-votantes-ajax').append(maquina);
					})
				}else if(typeof res == 'object' && res.length < 1){
					var mensajeSinMaquina = '<span class="text-secondary font-weight-bold">NO SE HAN INSERTADO MÁQUINAS DE VOTACIÓN</span>';
					if(tipoUsuario != 1 && tipoUsuario != 2){
						mensajeSinMaquina = '<span class="text-secondary font-weight-bold d-block" style="margin-top:30px">NO TE HAN HABILITADO MÁQUINAS DE VOTACIÓN</span>';
					}
					$('.maquinas-votantes-ajax').html(`${mensajeSinMaquina}`);
				}
				clearTimeout(timeout);
				timeout = setTimeout(function(){getMaquinasVotantes()},3000);
				$('[data-toggle="tooltip"]').tooltip();
			}catch(err){
				console.log(err);
				clearTimeout(timeout);
				timeout = setTimeout(function(){getMaquinasVotantes()},3000);
			}
	  	});
	  }


	  getMaquinasVotantes();

	  $(function() {
	    $.ajaxSetup({
	        error: function(jqXHR, exception) {
	        	clearTimeout(timeout);
	            timeout = setTimeout(function(){getMaquinasVotantes()},3000);
	        }
	    });
	  });


	$(document).on('click', '.maquinaVotanteConfiguracion', function(){
		var idMaquina = $(this).attr('idMaquina');
		var ip = $(this).attr('ip');

		alerta.fire({
			title: 'Administradores de Elección',
			html: `
				<h4>Máquina de Votación</h4>
				<h5>${ip}</h5>
				<span class="d-block mb-3">Selecciona los administradores de elección que podrán activar esta máquina de votación</span>
				<div id="contenido" class="mt-3 mb-3">
					<div class="spinner-border text-primary"></div><span class="cargandoText text-primary">Cargando...</span>
				</div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: () => {
				return new Promise(function(resolve){
					var idsUsuarios = [];
					var checkboxs = $('.habilitarCheckbox');
					$(checkboxs).each((check) => {
						if($(checkboxs).get(check).checked){
							idsUsuarios.push($($(checkboxs).get(check)).attr('idUsuario'));
						}
					});

					idsUsuarios = JSON.stringify(idsUsuarios);
					
					$.post(SERVERURL + '/elecciones/habilitarAdministradoresEleccion', {idsUsuarios: idsUsuarios, idMaquina: idMaquina}, function(res){
						ajaxRespuesta(res, '¡Administradores de elección Guardados!', '¡No se pudo guardar los administradores de elección!', false);
					});

				});
			}
		});
		$('.swal2-confirm').addClass('d-none');

		$.post(SERVERURL + '/elecciones/getAdministradoresEleccion', {idMaquina: idMaquina}, function(res){
			var tabla = `
				<table class="table table-hover table-stripped">
					<th>Cédula</th>
					<th>Nombre y Apellido</th>
					<th>Permitir</th>
			`;

			if(res == 'no_maquina'){
				alerta.fire({
					icon: 'error',
					title: '¡Esa máquina de votación no está disponible!'
				});
				return;
			}

			try{
				res = JSON.parse(res);
		
				res.forEach((u)=>{
					tabla += `
						<tr>
							<td>${u.cedula}</td>
							<td>${u.pnombre} ${u.papellido}</td>
							<td>
								<input class="habilitarCheckbox" style="width:25px;height:25px;cursor:pointer" idUsuario="${u.id}" type="checkbox" ${u.habilitado == idMaquina ? 'checked' : ''} />
							</td>
						</tr>
					`;
				});

				tabla += '</table>';
			}catch(err){
				console.log(err);
				alerta.fire({
					icon: 'error',
					title: '¡Error al obtener administradores de elección!'
				})
			}

			if(res.length < 1){
				tabla = '<span class="text-secondary font-weight-bold mt-3">No hay administradores de elección asignados a este consejo comunal<br>Usted puede asignarlos en el módulo de usuarios</span>';
			}else{
				$('.swal2-confirm').removeClass('d-none');
			}

			$('#contenido').removeClass('mt-3 mb-3');
			$('#contenido').html(tabla);
		});

	});


	});





</script>