<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="pr-4 pl-4 pb-1">
		<div class="jumbotron p-2 d-block mx-auto w-25 text-center menuBox">
			<span class="mb-3">Consejos Comunales</span>
		</div>
		<div id="menuContenedor">
			<a class="w-75 d-block mx-auto mt-3 btn btn-primary w-100 d-block mx-auto mt-3" href="<?php echo SERVERURL ?>/consejoscomunales/">
				<img src="<?php echo SERVERURL ?>/views/img/mostrartodos.svg">Mostrar Todos
			</a>
			<a class="w-75 d-block mx-auto mt-3 btn btn-primary w-100 d-block mx-auto mt-3" href="<?php echo SERVERURL ?>/unidades/">
				<img src="<?php echo SERVERURL ?>/views/img/unidades.svg">Unidades
			</a>
			<a class="w-75 d-block mx-auto mt-3 btn btn-primary w-100 d-block mx-auto mt-3" href="<?php echo SERVERURL ?>/comites/">
				<img src="<?php echo SERVERURL ?>/views/img/comites.svg">Comités
			</a>
		</div>
		<a class="btn btn-dark d-block mx-auto mt-3" style="width: 100px;" href="<?php echo SERVERURL ?>/menu/">
			Regresar
		</a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>