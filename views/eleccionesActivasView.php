<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main>
		<div class="jumbotron p-2 d-block mx-auto w-75 text-center menuBox">
			<span class="mb-3">Elecciones Activas</span>

			<?php foreach($elecciones as $eleccion){ ?>
				<div class="d-block mx-auto mt-3">
					<a class="w-75 d-inline-block btn btn-primary" href="<?php echo SERVERURL ?>/elecciones/resultados/<?php echo $eleccion->id ?>/">
						<?php echo $eleccion->fecha_formato . ' ( ' . $eleccion->nombre . ' ) ' ?>
						
					</a>
					<?php if($tipoUsuario == 1 || $tipoUsuario == 2){ ?>
						<button class="eliminarEleccion btn btn-danger d-inline-block" type="button" idEleccion="<?php echo $eleccion->id ?>" nombre="<?php echo $eleccion->fecha_formato . ' ( ' . $eleccion->nombre . ' ) ' ?>">
							<b>X</b>
						</button>
					<?php } ?>
				</div>
			<?php } ?>
			<?php if(count($elecciones) < 1) { ?>
				<h4 class="mt-3 mb-3">No hay elecciones activas.</h4>
			<?php } ?>
			<a class="btn btn-dark d-block mx-auto mt-3" style="width: 130px;" href="<?php echo SERVERURL ?>/elecciones/">
				Regresar
			</a>
		</div>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>