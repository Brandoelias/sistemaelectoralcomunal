
const alerta = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success ml-2 mr-2',
    cancelButton: 'btn btn-danger ml-2 mr-2'
  },
  buttonsStyling: false,
  cancelButtonText: 'Cancelar',
  confirmButtonText: 'Cerrar',
  allowOutsideClick: () => !Swal.isLoading(),
  width: 'auto'
})

//funciones
function actualizarFechaActual(fecha){
	fecha = new Date(fecha);
	var h = null;
	var m = null;
	var s = null;
	var me = null;

	setInterval(()=>{
		h = fecha.getHours();
		m = agregarCero(fecha.getMinutes());
		s = agregarCero(fecha.getSeconds());
		me = 'AM';

		if(h > 12){
			h = agregarCero(h - 12);
			me = 'PM';
		}else if (h == `00`){
			h = '12';
			me = 'AM';
		}else{
			h = agregarCero(h);
		}
		fecha.setSeconds( fecha.getSeconds() + 1);
		$('#fechaActual').text(`${h}:${m}:${s} ${me}`);
	},1000);
}

function agregarCero(n){
	if (n < 10){
		return `0${n}`;
	}else{
		return n;
	}
}

function ajaxRespuesta(res, exito, fallo, reload = true, then = false){

		if(res && res == '1' || res == 1){
			alerta.fire({
			icon: 'success',
			title: exito,
			showConfirmButton: exito != '¡Votación Guardada!',
			allowOutsideClick: exito != '¡Votación Guardada!'
		})
		.then(()=>{
			if(reload){
				window.location.reload();
			}else if(then){
				then(1);
			}
		})
		}else{
			alerta.fire({
			icon: 'error',
			title: fallo,
			showConfirmButton: exito != '¡Votación Guardada!',
			allowOutsideClick: exito != '¡Votación Guardada!'
			})
			.then(()=>{

				if(then){
					then(0);
				}

			})
		}

}


function calendario(startDate = '01/01/1995', minYear = '1901', maxYear = null, minDate = null, maxDate = null){

	if(maxYear == null){
		maxYear = parseInt(moment().format('YYYY'),10);
	}
	if(minDate == null){
		minDate = '01/01/1901';
	}

	$(function() {
  		$('input[name="fecha"]').daterangepicker({
    		singleDatePicker: true,
	 		showDropdowns: true,
			startDate: startDate,
    		minYear: minYear,
    		maxYear: maxYear,
    		minDate: minDate,
    		maxDate: maxDate,
    		autoUpdateInput: false,
    		parentEl: '.swal-overlay',
    		locale: {
		        separator: " - ",
		        applyLabel: "Ok",
		        cancelLabel: "Cancelar",
		        fromLabel: "Desde",
		        toLabel: "Hasta",
		        customRangeLabel: "Elegir",
		        daysOfWeek: [
		            "Dom",
		            "Lun",
		            "Mar",
		            "Mié",
		            "Jue",
		            "Vie",
		            "Sáb"
		        ],
		        monthNames: [
		            "Enero",
		            "Febrero",
		            "Marzo",
		            "Abril",
		            "Mayo",
		            "Junio",
		            "Julio",
		            "Agosto",
		            "Septiembre",
		            "Octubre",
		            "Noviembre",
		            "Deciembre"
		        ],
		        firstDay: 1,
		        format: 'DD/MM/Y'
		    }
  		});
	});


	$('input[name="fecha"]').on('apply.daterangepicker', function(ev, picker) {
    	$(this).val(picker.startDate.format('DD/MM/YYYY'));
    	$(this).parents('.form-group').addClass('focused');
  	});
	

}

function validarLongitudMaxima(input, longitud){
	if(input.value.length > longitud){
		input.value = input.value.slice(0, -1);
	}
}

function validarLetras(input){
	input.value = input.value.replace(/[^a-zA-Zá-źÁ-Źñ]+/g, '').toLowerCase();
}

function validarLetrasConEspacio(input){
	input.value = input.value.replace(/[^a-zA-Zá-źÁ-Źñ ]+/g, '').toLowerCase();
}

function validarAlfanumerico(input){
	input.value = input.value.replace(/[^a-zA-Zá-źÁ-Ź1-9ñ ]/g, '');
}

function validarNumerico(input){
	input.value = input.value.replace(/[^0-9]/g, '');
}

function validarTelefono(input){
	//input.value = input.value.replace(/^0(416|426|412|414|424|212)\d{7}/, '');
	/*if(!/^0(416|426|412|414|424|212)\d{7}/.test(input.value)){
		input.value = input.value.slice(0, -1);
	}*/
	/*
	if(input.value.length < 1){
		return false;
	}
	if(input.value[0] != '0'){
		input.value = input.value.slice(0, -1);
		return false;
	}
	if(input.value.length == 2 && input.value[1] != '4' && input.value[1] != '2'){
		input.value = input.value.slice(0, -1);
		return false;
	}
	if(input.value.length == 3 && input.value[2] != '1' && input.value[2] != '2'){
		input.value = input.value.slice(0, -1);
		return false;
	}
	if(input.value.length == 4){
		if(input.value.slice(0, 3) == '021' && input.value[3] != '2'){
			input.value = input.value.slice(0, -1);
			return false;
		}else if(input.value[3] != '6' && input.value[3] != '4' && input.value[3] != '2'){
			input.value = input.value.slice(0, -1);
			return false;
		}
	}
	*/
	validarNumerico(input);
	validarLongitudMaxima(input, 11);
}

function validarRif(input){
	if(input.value == '' || input.value == 'C-'){
		input.value = '';
		return;
	}
	validarNumerico(input);
	validarLongitudMaxima(input, 9);
	input.value = 'C-' + input.value; 
}

function validarCorreo(input){
	input.value = input.value.replace(/[^a-z1-9ñ@.]/g, '');
	validarLongitudMaxima(input, 50);
}

function validarNumeroBanco(input){
	validarNumerico(input);
	validarLongitudMaxima(input, 20);
}

function validarCodigoSitur(input){
	input.value = input.value.replace(/[^a-zA-Z0-9-]/g, '');
	input.value = input.value.toUpperCase();
	validarLongitudMaxima(input, 20);
}

function validarUnidad(input){
	const ignorar = ['de', 'del', 'y', 'o', 'para', 'por', 'la', 'las', 'el', 'los', 'en', 'lo'];
	input.value = input.value.replace(/[^a-zA-Zá-źÁ-Źñ ]/g, '');
	input.value = input.value.toLowerCase().split(' ').map(word => word.length > 1 && !ignorar.includes(word) ? word.charAt(0).toUpperCase() + word.substring(1) : word).join(' ');
	validarLongitudMaxima(input, 50);
}

function validarCedula(input){
	if(input.value.length > 0 && input.value[0] == '0'){
		input.value = input.value.slice(1, input.length);
	}
	input.value = input.value.replace(/[^0-9]/g, '');
}
function validarVotosMaximos(input){
	if(input.value.length > 0 && input.value[0] == '0'){
		input.value = input.value.slice(1, input.length);
	}
	validarNumerico(input);
	validarLongitudMaxima(input, 2);
}

function validarBusquedaNombreCedula(input){
	if(input.value.length < 1){
		return;
	}
	if(!isNaN(input.value[0])){
		validarCedula(input);
		validarLongitudMaxima(input, 9);
	}else{
		validarLetrasConEspacio(input);
		validarLongitudMaxima(input, 30);
	}
}

// events listeners
$(document).ready(()=>{


	$('input').on('focus', function(){
		$(this).parent('.form-group').addClass('focused');
	})
	$('input').focusout(function(){
		if($(this).val() == ''){
			$(this).parent('.form-group').removeClass('focused');
		}
	})
	$('select').on('change', function(){
		$(this).parent('.form-group').addClass('focused');
	})
	$('select').focusout(function(){
		if($(this).val() == ''){
			$(this).parent('.form-group').removeClass('focused');
		}
	})

	function focusedVerificar(){
		var inputs = $(document).find('input');
		$(inputs).each((i)=>{
			var inputValue = $(inputs).get(i).value;
			if(inputValue != ''){
				$($(inputs).get(i)).parents('.form-group').addClass('focused');
			}

		})

		var inputs = $(document).find('select');
		$(inputs).each((i)=>{
			var inputValue = $(inputs).get(i).value;
			if(inputValue != ''){
				$($(inputs).get(i)).parents('.form-group').addClass('focused');
			}

		})
	}

	$(document).on('DOMNodeInserted', '.swal2-content', function(){
		var inputs = $(this).find('input');
		$(inputs).each((i)=>{
			var inputValue = $(inputs).get(i).value;
			if(inputValue != ''){
				$($(inputs).get(i)).parents('.form-group').addClass('focused');
			}

		})

		var inputs = $(this).find('select');
		$(inputs).each((i)=>{
			var inputValue = $(inputs).get(i).value;
			if(inputValue != ''){
				$($(inputs).get(i)).parents('.form-group').addClass('focused');
			}

		})

		$('input').on('focus', function(){
			$(this).parent('.form-group').addClass('focused');
		})
		$('input').focusout(function(){
			if($(this).val() == ''){
				$(this).parent('.form-group').removeClass('focused');
			}
		})
		$('select').on('change', function(){
			$(this).parent('.form-group').addClass('focused');
		})
		$('select').focusout(function(){
			if($(this).val() == ''){
				$(this).parent('.form-group').removeClass('focused');
			}
		})

	})



	$('#loginButton').click(function(e){
		e.preventDefault();

		var usuario = $('#usuario').val();
		var contrasena = $('#pass').val();
		var captcha = $('#captcha').val();

		if(usuario == ''){
			alerta.fire({
				icon: 'error',
				title: '¡Debe ingresar el usuario!'
			});
			return;
		}
		if(contrasena == ''){
			alerta.fire({
				icon: 'error',
				title: '¡Debe ingresar la contraseña!'
			});
			return;
		}
		if(captcha == ''){
			alerta.fire({
				icon: 'error',
				title: '¡Debe ingresar la imágen de seguridad!'
			});
			return;
		}

		$(this).attr('disabled','disabled');
		$(this).removeClass('btn btn-primary');
		$(this).val('');
		$(this).addClass('spinner-border text-primary');
		$(this).attr('style','background:transparent!important');

		$.post(SERVERURL + '/login/check', {usuario : usuario, contrasena : contrasena, captcha: captcha}, function(res){

			if(res && res != 'es_maquina_votante' & res != 'captcha_incorrecto'){
				window.location.href = SERVERURL + '/menu/';
			}else{
				if(res == 'es_maquina_votante'){
					$('#loginErrorAlert').text('¡Esta es una máquina de votación!');
				}else if(res == 'captcha_incorrecto'){
					$('#loginErrorAlert').text('¡Imágen de seguridad Incorrecta!');
				}else{
					$('#loginErrorAlert').text('¡Datos Incorrectos!');
				}
				$('#loginErrorAlert').removeClass('d-none');
				$('#loginButton').removeAttr('disabled');
				$('#loginButton').removeClass('spinner-border text-primary');
				$('#loginButton').removeAttr('style');
				$('#loginButton').addClass('btn btn-primary');
				$('#loginButton').val('Ingresar');
				$('#recargarCaptcha').trigger('click');
				$('#captcha').val('');
			}

		});
	});



	$('.editarConsejoComunal').click(function(e){

		var boton = $(this);
		var bancos = [].slice.call($('.banco'));

		var cod = $(this).parent().parent().find('.codConsejoComunal').text().trim();
		var nombre = $(this).parent().parent().find('.nombreConsejoComunal').text().trim();
		var telefono = $(this).parent().parent().find('.telefonoConsejoComunal').text().trim();
		var rif = $(this).parent().parent().find('.rifConsejoComunal').text().trim();
		var correo = $(this).parent().parent().find('.correoConsejoComunal').text().trim();
		var id_banco = $(this).parent().parent().find('.idbancoConsejoComunal').text().trim();
		var numerodecuenta = $(this).parent().parent().find('.numerodecuentaConsejoComunal').text().trim();

		if(telefono == '-'){
			telefono = '';
		}
		if(rif == '-'){
			rif = '';
		}
		if(correo == '-'){
			correo = '';
		}
		if(numerodecuenta == '-'){
			numerodecuenta = '';
		}

		var html = `

				<h4>${nombre}</h4><br>
					<div class="form-group">
						<span class="form-label">Código SITUR...</span>
						<input id="cod" class="form-control" type="text" value="${cod}" oninput="validarCodigoSitur(this)" />
					</div>
					<div class="form-group">
						<span class="form-label">Nombre...</span>
						<input id="nombre" class="form-control" type="text" value="${nombre}" oninput="validarAlfanumerico(this)" />
					</div>
					<div class="form-group">
						<span class="form-label">Teléfono...</span>
						<input id="telefono" class="form-control" type="text" value="${telefono}" oninput="validarTelefono(this); validarLongitudMaxima(this, 11)" />
					</div>
					<div class="form-group">
						<span class="form-label">Rif...</span>
						<input id="rif" class="form-control" type="text" value="${rif}" oninput="validarRif(this)" />
					</div>
					<div class="form-group">
						<span class="form-label">Correo...</span>
						<input id="correo" class="form-control" type="text" value="${correo}" oninput="validarCorreo(this)" />
					</div>

					<div class="form-group">
						<span class="form-label">Nombre del banco...</span>
						<select id="id_banco" class="form-control">
							<option disabled value="" selected hidden></option>
						`;

						bancos.forEach((banco)=>{
							html += `<option value="${banco.value}">${banco.name}</option>`;
						})

		html+=		`	</select>
					</div>
					<div class="form-group numcuenta">
						<span class="form-label">Número de cuenta bancaria...</span>
						<input id="numerodecuenta" class="form-control" type="text" value="${numerodecuenta}" oninput="validarNumeroBanco(this)" />
					</div>

		`;

		alerta.fire({
			title: 'Editar Consejo Comunal',
			html: html,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			cancelButtonText: 'Cancelar',
			showLoaderOnConfirm: true,
			preConfirm: () => {
				return new Promise(function(resolve){

					var codM = $('#cod').val().trim();
					var nombreM = $('#nombre').val().trim();
					var telefonoM = $('#telefono').val().trim();
					var rifM = $('#rif').val().trim();
					var correoM = $('#correo').val().trim();
					var numerodecuentaM = $('#numerodecuenta').val().trim();
					var id_bancoM = $('#id_banco').val().trim();

					if(cod == codM && nombre == nombreM && telefono == telefonoM && rif == rifM && correo == correoM && numerodecuenta == numerodecuentaM && id_banco == id_bancoM){
						alerta.fire({
							icon: 'error',
							title: '¡No se ha cambiado ningún dato!'
						})
						.then(()=>{
							$(boton).trigger('click');
						})
						return;
					}

					const alertaError = (title = '¡Debes llenar los datos obligatorios!') => {
						alerta.fire({
							icon: 'error',
							title: title
						})
						.then(()=>{
							$(boton).trigger('click');
							$('#cod').val(codM);
							$('#nombre').val(nombreM);
							$('#telefono').val(telefonoM);
							$('#rif').val(rifM);
							$('#correo').val(correoM);
							$('#id_banco').val(id_bancoM);
							$('#numerodecuenta').val(numerodecuentaM);
							if(id_bancoM != null && id_bancoM != 0 && id_bancoM != '' && id_bancoM != '0'){
								$('.numcuenta').removeClass('d-none');
							}else{
								$('.numcuenta').addClass('d-none');
							}
							focusedVerificar();
						})
						return;
					}

					if(codM == '' || nombreM == ''){
						alertaError();
						return;
					}
					if(telefonoM != '' && !/^0(416|426|412|414|424|212)\d{7}/.test(telefonoM)){
						alertaError('Número de teléfono Incorrecto');
						return;
					}
					if(rifM != '' && rifM.length < 10){
						alertaError('Rif Incorrecto');
						return;
					}
					if(correoM != '' && !/^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/.test(correoM)){
						alertaError('Correo Incorrecto');
						return;
					}

					if(!(id_bancoM != null && id_bancoM != 0 && id_bancoM != '' && id_bancoM != '0')){
						numerodecuentaM = '';
					}else if(numerodecuentaM.length < 16){
						alertaError('Número de cuenta bancaria Incorrecto');
						return;
					}

					var datos = { cod : cod, codM : codM ,nombre : nombreM, rif : rifM, telefono : telefonoM ,correo : correoM, numerodecuenta : numerodecuentaM, id_banco : id_bancoM };

					$.post(SERVERURL + '/consejoscomunales/editarConsejoComunal', datos, function(res){

						if(res && res != 'codigo_existe' && res != 'eleccion_comenzada'){
							alerta.fire({
								icon: 'success',
								title: '¡Consejo comunal modificado!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							var errorMsj = '¡No se pudo editar el consejo comunal!';
							if(res == 'codigo_existe'){
								errorMsj = 'Ya existe un Consejo Comunal con ese Código SITUR';
							}else if(res == 'eleccion_comenzada'){
								errorMsj = '¡Este Consejo Comunal está realizando actualmente una Elección!'
							}
							alerta.fire({
								icon: 'error',
								title: errorMsj
							})
						}

					});


				});
			},
			allowOutsideClick: () => !Swal.isLoading(),
		})

		$('#id_banco').val(id_banco);
		if(id_banco == '' || id_banco == 0 || id_banco == '0'){
			$('#numerodecuenta').parent('.form-group').addClass('d-none');
		}
		focusedVerificar();

		$('#id_banco').on('change', function(){
			if ($(this).val() != 0){
				$('#numerodecuenta').parent('.form-group').removeClass('d-none');
			}else{
				$('#numerodecuenta').parent('.form-group').addClass('d-none');
			}
		});

	});



	$('.borrarConsejoComunal').click(function(){

		var cod = $(this).parent().parent().find('.codConsejoComunal').text().trim();
		var nombre = $(this).parent().parent().find('.nombreConsejoComunal').text().trim();

		alerta.fire({
			icon: 'warning',
			title: '¿Está seguro de borrar el consejo comunal?',
			html: `
					<h3 class="text-dark">${nombre}</h3><br>
					<h5 class="text-danger">También se eliminarán sus habitantes y elecciones realizadas</h5>
			`,
			showCancelButton: true,
			confirmButtonText: 'Borrar',
			showLoaderOnConfirm: true,
			preConfirm: () => {
				return new Promise(function(resolve){

					var datos = {cod : cod};

					$.post(SERVERURL + '/consejoscomunales/borrarConsejoComunal', datos, function(res){

						if(res && res != 'eleccion_comenzada'){
							alerta.fire({
								icon: 'success',
								title: '¡Consejo comunal borrado!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							var errorMsj = '¡No se pudo borrar el consejo comunal!';
							if(res == 'eleccion_comenzada'){
								errorMsj = '¡Este Consejo Comunal está realizando actualmente una Elección!'
							}
							alerta.fire({
								icon: 'error',
								title: errorMsj
							})
						}

					});

				});
			}
		})

	});


	$('.agregarConsejoComunal').click(function(){

		var boton = $(this);
		var bancos = [].slice.call($('.banco'));

		var html = `

					<div class="form-group mt-3">
						<span class="form-label">Código SITUR (*)...</span>
						<input id="cod" class="form-control" type="text" value="" oninput="validarCodigoSitur(this)" />
					</div>
					<div class="form-group">
						<span class="form-label">Nombre (*)...</span>
						<input id="nombre" class="form-control" type="text" value="" oninput="validarAlfanumerico(this); validarLongitudMaxima(this, 50)" />
					</div>
					<div class="form-group">
						<span class="form-label">Teléfono...</span>
						<input id="telefono" class="form-control" type="text" value="" oninput="validarTelefono(this); validarLongitudMaxima(this, 11)" />
					</div>
					<div class="form-group">
						<span class="form-label">Rif...</span>
						<input id="rif" class="form-control" type="text" value="" oninput="validarRif(this)" />
					</div>
					<div class="form-group">
						<span class="form-label">Correo...</span>
						<input id="correo" class="form-control" type="text" value="" oninput="validarCorreo(this)" />
					</div>
					<div class="form-group">
						<span class="form-label">Nombre del banco...</span>
						<select id="id_banco" class="form-control">
							<option disabled value="" selected hidden></option>
						`;

						bancos.forEach((banco)=>{
							html += `<option value="${banco.value}">${banco.name}</option>`;
						})

			html+=`		</select>
					</div>
					<div class="form-group numcuenta d-none">
						<span class="form-label">Número de cuenta bancaria...</span>
						<input id="numerodecuenta" class="form-control" type="text" value="" oninput="validarNumeroBanco(this)" />
					</div>
			`;

		$.post(SERVERURL + '/banco/getTodos', function(res){

			res = JSON.parse(res);


		});

		alerta.fire({
			title: 'Agregar consejo comunal',
			html: html,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: () => {
				return new Promise(function(resolve){
					var cod = $('#cod').val();
					var nombre = $('#nombre').val();
					var telefono = $('#telefono').val();
					var rif = $('#rif').val();
					var correo = $('#correo').val();
					var id_banco = $('#id_banco').val();
					var numerodecuenta = $('#numerodecuenta').val();

					const alertaError = (title = '¡Debes llenar los datos obligatorios!') => {
						alerta.fire({
							icon: 'error',
							title: title
						})
						.then(()=>{
							$(boton).trigger('click');
							$('#cod').val(cod);
							$('#nombre').val(nombre);
							$('#telefono').val(telefono);
							$('#rif').val(rif);
							$('#correo').val(correo);
							$('#id_banco').val(id_banco);
							$('#numerodecuenta').val(numerodecuenta);
							if(id_banco != null && id_banco != 0 && id_banco != '' && id_banco != '0'){
								$('.numcuenta').removeClass('d-none');
							}else{
								$('.numcuenta').addClass('d-none');
							}
							focusedVerificar();
						})
						return;
					}

					if(cod == '' || nombre == ''){
						alertaError();
						return;
					}
					if(telefono != '' && !/^0(416|426|412|414|424|212)\d{7}/.test(telefono)){
						alertaError('Número de teléfono Incorrecto');
						return;
					}
					if(rif != '' && rif.length < 10){
						alertaError('Rif Incorrecto');
						return;
					}
					if(correo != '' && !/^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/.test(correo)){
						alertaError('Correo Incorrecto');
						return;
					}

					if(!(id_banco != null && id_banco != 0 && id_banco != '' && id_banco != '0')){
						numerodecuenta = '';
					}else if(numerodecuenta.length < 16){
						alertaError('Número de cuenta bancaria Incorrecto');
						return;
					}

					var datos = {cod : cod, nombre : nombre, telefono : telefono, rif : rif, correo : correo, id_banco : id_banco ,numerodecuenta : numerodecuenta};

					$.post(SERVERURL + '/consejoscomunales/agregarConsejoComunal', datos, function(res){

						if(res && res != 'codigo_existe'){
							alerta.fire({
								icon: 'success',
								title: '¡Consejo comunal agregado!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							var errorMsj = '¡No se pudo agregar el consejo comunal!';
							if(res == 'codigo_existe'){
								errorMsj = 'Ya existe un Consejo Comunal con ese Código SITUR';
							}
							alerta.fire({
								icon: 'error',
								title: errorMsj
							})
						}

					})
				});
			}
		})

		$('#id_banco').on('change', function(){
			if ($(this).val() != 0){
				$('.numcuenta').removeClass('d-none');
			}else{
				$('.numcuenta').addClass('d-none');
			}
		});

	});



	$('.editarUnidad').click(function(){

		var boton = $(this);
		var id = $(this).parent().parent().find('.idUnidad').text().trim();
		var nombre = $(this).parent().parent().find('.nombreUnidad').text().trim();

		alerta.fire({
			title: `Editar unidad`,
			html: `

					<h5>${nombre}</h5><br>

					<div class="form-group">
						<span class="form-label">Nombre...</span>
						<input id="nombre" class="form-control" type="text" value="${nombre}" oninput="validarUnidad(this)" />
					</div>

			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{

				return new Promise(function(resolve){

					var nombre = $('#nombre').val().trim();

					if(nombre == ''){
						alerta.fire({
							icon: 'error',
							title: '¡El nombre no puede estar vacío!'
						})
						.then(()=>{
							$(boton).trigger('click');
						})
						return;
					}

					var datos = {id : id, nombre : nombre};

					$.post(SERVERURL + '/unidades/editarUnidad', datos, function(res){

						var errorMsj = '¡No se pudo editar la unidad!';
						if(res == 'eleccion_comenzada'){
							errorMsj = '¡No se puede editar una unidad que está siendo usada en una elección en curso!'
						}

						if(res && res != 'eleccion_comenzada'){
							alerta.fire({
								icon: 'success',
								title: '¡Unidad Modificada!'
							})
							.then(()=>{
								window.location.reload();
							})
						}else{
							alerta.fire({
								icon: 'error',
								title: errorMsj
							})
						}

					});

				})

			}
		})

	});


	$('.borrarUnidad').click(function(){

		var boton = $(this);
		var id = $(this).parent().parent().find('.idUnidad').text().trim();
		var nombre = $(this).parent().parent().find('.nombreUnidad').text().trim();

		alerta.fire({
			icon: 'warning',
			title: '¿Seguro que desea borrar la unidad?',
			html: `<h5>${nombre}</h5><br>`,
			showCancelButton: true,
			confirmButtonText: 'Borrar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					$.post(SERVERURL + '/unidades/borrarUnidad', {id : id}, function(res){
						var errorMsj = '¡No se pudo borrar la unidad!';
						if(res == 'eleccion_comenzada'){
							errorMsj = '¡No se puede borrar la unidad porque está siendo usada en una elección en curso!';
						}
						ajaxRespuesta(res, '¡Unidad borrada!', errorMsj);

					});

				})
			}

		})

	});



	$('.agregarUnidad').click(function(){

		alerta.fire({
			title: 'Agregar Unidad',
			html: `

				<div class="form-group">
					<span class="form-label">Nombre...</span>
					<input id="nombre" class="form-control" type="text" oninput="validarUnidad(this)" />
				</div>

			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{

				return new Promise(function(resolve){

					var nombre = $('#nombre').val().trim();

					if(nombre == ''){
						alerta.fire({
							icon: 'error',
							title: '¡El nombre no puede estar vacío!'
						}).then(()=>{
							$('.agregarUnidad').trigger('click');
						});
						return;
					}

					$.post(SERVERURL + '/unidades/agregarUnidad', {nombre : nombre}, function(res){

						ajaxRespuesta(res, '¡Unidad agregada!', '¡No se pudo agregar la unidad!');

					});

				});

			}
		})

	});



	$('.verUnidades').click(function(){

		var boton = $(this);
		var cod = $(this).parent().parent().find('.codConsejoComunal').text().trim();
		var nombre = $(this).parent().parent().find('.nombreConsejoComunal').text().trim();

		alerta.fire({
			title: 'Unidades',
			html: `

				<h5 id="nombreConsejo">${nombre}</h5><br><br>
				<div id="contenido">
					<div class="spinner-border text-primary"></div><span class="cargandoText text-primary">Cargando...</span>
				</div>
				<br>

			`,
			showConfirmButton: false,
			width: 'auto',
			showCloseButton: true
		})

		$.post(SERVERURL + '/consejoscomunales/verUnidades', {cod: cod}, function(res){

			var unidadesJSON = res;

			res = JSON.parse(res);

			if(res.length < 1){
				$('#contenido').html(`<input id="codConsejo" type="hidden" value="${cod}" /><h6>No hay unidades en este consejo comunal.</h6><br><button class="btn btn-success agregarUnidadConsejo" type="button">Agregar +</button>`);
				return;
			}

			var table = `
					<input id="codConsejo" type="hidden" value="${cod}" />
					<table class="table table-hover table-stripped">
						<th>Nombre</th>
						<th>Votos Máximos</th>
						<th>Comités</th>
						<th>Quitar</th>
			`;

			var cambiarVotosMaximosHabilitado = false;
			res.forEach((r)=>{
				if(r.comites == 'Ninguno'){
					cambiarVotosMaximosHabilitado = true;
				}
				table += `
					<tr>
						<input id="idUnidad" type="hidden" value="${r.id}" />
						<td class="nombreU">${r.nombre_unidad}</td>
						<td>
							<span>${r.comites == 'Ninguno' ? r.votos_maximos : '-'}</span>
						</td>
						<td><span class="verComites">${r.comites}</span></td>
						<td><img class="borrarImg borrarUnidadConsejo" idUnidad="${r.id}" src="${SERVERURL}/views/img/borrar.svg" height=25 width=25 /></td>
					</tr>
				`;
			});

			table += '</table>';

			if(cambiarVotosMaximosHabilitado){
				table += `<button id="cambiarVotosMaximos" class="btn btn-secondary d-block mb-4 mx-auto">Cambiar Votos Máximos</button>`;
			}

			table += `<button class="btn btn-success agregarUnidadConsejo" type="button">Agregar +</button>`;

			$('#contenido').html(table);

			$('.borrarUnidadConsejo').click(function(){
				var unidades = JSON.parse(unidadesJSON);
				var table = `
					<table class="table table-hover table-stripped">
						<th>Nombre</th>
						<th>Quitar</th>
				`;

				unidades.forEach((r)=>{
					table += `
						<tr>
							<td class="nombreU">${r.nombre_unidad}</td>
							<td>
								<input id="unidad${r.id}" type="checkbox" idUnidad="${r.id}" class="form-control text-center d-block mx-auto m-0 p-0 cursor-pointer quitarUnidadInput" style="width:25px;height:25px;cursor:pointer" />
							</td>
						</tr>
					`;
				});

				table += '</table>';

				alerta.fire({
					title: 'Quitar Unidad de Consejo Comunal',
					html: `
						<h5>${nombre}</h5>
						<br>
						<h6>Seleccione las unidades que desea quitar</h6>
						<div>
							${table}
						</div>
					`,
					showCancelButton: true,
					confirmButtonText: 'Quitar',
					showLoaderOnConfirm: true,
					width: 'auto',
					preConfirm: ()=>{
						return new Promise(function(resolve){
							var unidades = [];
							var quitarUnidadInput = $('.quitarUnidadInput');
							$('.quitarUnidadInput').each((i)=>{
								i = $(quitarUnidadInput).get(i);
								if(i.checked){
									unidades.push($(i).attr('idUnidad'));
								}
							});
							if(unidades.length < 1){
								alerta.fire({
									icon: 'error',
									title: 'No seleccionó ninguna unidad'
								});
								return;
							}
							unidades = JSON.stringify(unidades);

							$.post(SERVERURL + '/unidades/borrarUnidadConsejo', {codConsejo: cod, unidades: unidades}, function(res){
								var errorMsj = '¡No se pudo quitar la unidad!';
								if(res == 'eleccion_comenzada'){
									errorMsj = 'No se puede quitar unidades de este consejo comunal porque hay una elección en curso';
								}
								ajaxRespuesta(res, '¡Unidades Quitadas!', errorMsj);
							});
						});
					}
				});

				$(`#unidad${$(this).attr('idUnidad')}`).attr('checked', true);

			});

			$('#cambiarVotosMaximos').click(function(){
				var unidades = JSON.parse(unidadesJSON);
				var table = `
					<table class="table table-hover table-stripped">
						<th>Nombre</th>
						<th>Votos Máximos</th>
				`;

				unidades.forEach((r)=>{
					if(r.comites == 'Ninguno'){
						table += `
							<tr>
								<td class="nombreU">${r.nombre_unidad}</td>
								<td>
									<input idUnidad="${r.id}" class="form-control text-center d-block mx-auto votosMaximosInput" style="width:50px" value="${r.votos_maximos}" oninput="validarVotosMaximos(this)" />
								</td>
							</tr>
						`;
					}
				});

				table += '</table>';

				alerta.fire({
					title: 'Cambiar Votos Máximos',
					html: `
						<h5 id="nombreConsejo">${nombre}</h5><br><br>
						<div>
							${table}
						</div>
					`,
					showCancelButton: true,
					confirmButtonText: 'Guardar',
					showLoaderOnConfirm: true,
					preConfirm: ()=>{
						return new Promise(function(resolve){
							var votosMaximos = [];
							var votosMaximosInput = $('.votosMaximosInput');
							var inputVacio = false;
							$('.votosMaximosInput').each((i)=>{
								i = $(votosMaximosInput).get(i);
								if($(i).val() == ''){
									$(i).addClass('border-danger');
									inputVacio = true;
								}
								votosMaximos.push({idUnidad: $(i).attr('idUnidad'), votosMaximos: $(i).val()});
							});
							if(inputVacio){
								Swal.disableLoading();
								return;
							}
							votosMaximos = JSON.stringify(votosMaximos);

							$.post(SERVERURL + '/consejoscomunales/cambiarVotosMaximos', {codConsejo: cod, votosMaximos: votosMaximos}, function(res){
								var errorMsj = 'Error al cambiar Votos Máximos';
								if(res == 'eleccion_comenzada'){
									errorMsj = 'No se pueden cambiar los votos máximos porque hay una elección en curso';
								}
								ajaxRespuesta(res, 'Votos Máximos Cambiado', errorMsj, false);
							});
						});
					}
				});

			});

		});

	});



	$(document).on('click', '.agregarUnidadConsejo', function(){

		var cod = $('#codConsejo').val().trim();
		var nombre = $('#nombreConsejo').text().trim();
		var unidades = [];
		var res = null;
		
		alerta.fire({
			title: 'Agregar unidad',
			html: `
				<h5>${nombre}</h5><br><br>
				<div id="contenido">
					<div class="spinner-border text-primary"></div><span class="cargandoText text-primary">Cargando...</span>
				</div>
				<br>
			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{

				return new Promise(function(resolve){

					if(unidades.length < 1){
						alerta.fire({
							icon: 'error',
							title: '¡No se seleccionó ninguna unidad!'
						})
						return;
					}

					unidades = JSON.stringify(unidades);

					$.post(SERVERURL + '/consejoscomunales/agregarUnidadConsejo', {cod : cod, unidades : unidades}, function(res){

						ajaxRespuesta(res, '¡Unidad(es) agregada(s)!', '¡No se pudo agregar la(s) unidad(es)!');

					});

				});

			}
		})
		.then(()=>{
			unidades = [];
			$('.swal2-content').unbind();
		})


		$.post(SERVERURL + '/consejoscomunales/getUnidadesNotIn', {cod : cod}, function(r){
			$('.swal2-confirm').removeAttr('disabled');

			res = JSON.parse(r);

			if(res.length < 1){
				alerta.fire({
					title: 'No hay unidades para agregar.'
				})
				return;
			}

			var select = `<div class="form-group">
						   <select id="unidades" class="form-control">
							<option hidden selected value="">Seleccionar...</option>
						 `;

			res.forEach((r)=>{
				select += `<option class="unidadesOption" value="${r.id}">${r.nombre_unidad}</option>`;
			})

			select +=  ` </select>
						</div>`;

			select += `<table id="unidadesTable" class="table table-hover table-stripped d-none">
						<th>Nombre</th>
						<th>Quitar</th>
					   </table>
			`;

			$('#contenido').html(select);

			$('#unidades').on('change', function(){

				var value = $(this).val();
				var nombre = $(this).find(`.unidadesOption[value="${value}"]`).text().trim();

				unidades.push(value);

				if(unidades.length == res.length){
					$('#unidades').parent('.form-group').addClass('d-none');
				}

				$(this).find(`.unidadesOption[value="${value}"]`).addClass('d-none');
				$('#unidadesTable').removeClass('d-none');
				$(this).val('');
				$(this).focus();
				$(':focus').blur();
				focusedVerificar();

				$('#unidadesTable').append(`<tr><input class="valueUnidad" type="hidden" value="${value}" /><td>${nombre}</td><td><img class="borrarImg quitarUnidad" src="${SERVERURL}/views/img/borrar.svg" height=25 width=25 /></td></tr>`);


			});


		});

		$('.swal2-confirm').attr('disabled','disabled');

		$('.swal2-content').on('click', '.quitarUnidad', function(){

			var value = $(this).parent().parent().find('.valueUnidad').val();
			$('#unidades').find(`.unidadesOption[value="${value}"]`).removeClass('d-none');

			const index = unidades.indexOf(value);
			if (index > -1) {
			  unidades.splice(index, 1);
			}
			$(this).parent().parent().remove();
			if(unidades.length < 1){
				$('#unidadesTable').addClass('d-none');
			}

			if(unidades.length != res.length){
				$('#unidades').parent('.form-group').removeClass('d-none');
			}

		});

	});



	// Agregar comites
	$('.agregarComite').click(function(){

		alerta.fire({
			title: 'Agregar comité',
			html: `
					<div class="form-group mt-3">
						<span class="form-label">Nombre...</span>
						<input id="nombre" class="form-control" type="text" oninput="validarUnidad(this)" />
					</div>

				`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{

				return new Promise(function(resolve){

					var nombre = $('#nombre').val().trim();

					if(nombre == ''){
						alerta.fire({
							icon: 'error',
							title: '¡El nombre no puede estar vacío!'
						})
						.then(()=>{
							$('.agregarComite').trigger('click');
						})
						return;
					}

					$.post(SERVERURL + '/comites/agregarComite', {nombre : nombre}, function(res){
						ajaxRespuesta(res, '¡Comité agregado!', '¡El comité no se pudo agregar!');
					})

				});

			}
		})

	});


	//Editar comite
	$('.editarComite').click(function(){
		var boton = $(this);
		var id = $(this).parent().parent().find('.idComite').text().trim();
		var nombre = $(this).parent().parent().find('.nombreComite').text().trim();

		alerta.fire({
			title: 'Editar comité',
			html: `
				<h5>${nombre}</h5><br><br>
				<div class="form-group">
					<span class="form-label">Nombre...</span>
					<input id="nombre" class="form-control" type="text" value="${nombre}" oninput="validarUnidad(this)" />
				</div>
				`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					var nombre = $('#nombre').val().trim();

					if(nombre == ''){
						alerta.fire({
							icon: 'error',
							title: '¡El nombre no puede estar vacío!'
						}).then(()=>{
							$(boton).trigger('click');
						});
						return;
					}

					$.post(SERVERURL + '/comites/editarComite', {id : id, nombre : nombre}, function(res){
						var errorMsj = '¡El comité no se pudo editar!';
						if(res == 'eleccion_comenzada'){
							errorMsj = '¡No se puede borrar el comité porque está siendo usado en una elección en curso!';
						}
						ajaxRespuesta(res, '¡Comité Modificado!', errorMsj);
					})

				})
			}

		})

	});


	//Borrar comite
	$('.borrarComite').click(function(){

		var id = $(this).parent().parent().find('.idComite').text().trim();
		var nombre = $(this).parent().parent().find('.nombreComite').text().trim();

		alerta.fire({
			icon: 'warning',
			title: '¿Seguro de borrar el comité?',
			html: `
					<h5>${nombre}</h5><br><br>
				`,
			showCancelButton: true,
			confirmButtonText: 'Borrar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					$.post(SERVERURL + '/comites/borrarComite', {id : id}, function(res){
						var errorMsj = '¡El comité no se pudo borrar!';
						if(res == 'eleccion_comenzada'){
							errorMsj = '¡No se puede borrar el comité porque está siendo usado en una elección en curso!';
						}
						ajaxRespuesta(res, '¡Comité Borrado!', errorMsj);
					});

				});
			}
		})

	});


	//Ver comites
	$(document).on('click', '.verComites', function(){

		var cod = $('#codConsejo').val().trim();
		var idUnidad = $(this).parent().parent().find('#idUnidad').val().trim();
		var nombreU = $(this).parent().parent().find('.nombreU').text().trim();
		var nombreC = $('#nombreConsejo').text().trim();

		alerta.fire({
			title: 'Comités',
			html: `
					<h4 id="nombreC">${nombreC}</h4>
					<h5 id="nombreU">${nombreU}</h5>
					<br>
					<div id="contenido">
						<div class="spinner-border text-primary"></div><span class="cargandoText text-primary">Cargando...</span>
					</div><br><br>

			`,
			showConfirmButton: false,
			showCloseButton: true,
			width: 'auto'
		})



		$.post(SERVERURL + '/comites/getPorConsejoUnidad', {cod : cod, idUnidad : idUnidad}, function(res){

			var comitesJSON = res;

			res = JSON.parse(res);

			var table = `<input id="codConsejo" type="hidden" value="${cod}" />
						 <input id="idUnidad" type="hidden" value="${idUnidad}" />
						<table class="table table-hover table-stripped">
							<th>Nombre</th>
							<th>Votos Máximos</th>
							<th>Quitar</th>
			`;

			if(res.length < 1){
				table += `<tr><td colspan="3">No hay comités registrados.</td></tr>`;
			}else{

				res.forEach((r)=>{

				table += `<tr>
							<input id="idComite" type="hidden" value="${r.id_comite}" />
							<td class="nombreComite">${r.nombre_comite}</td>
							<td>${r.votos_maximos}</td>
							<td><img idComite="${r.id_comite}" idUnidad="${idUnidad}" class="borrarImg borrarComiteUnidad" src="${SERVERURL}/views/img/borrar.svg" height="25" width="25" /></td>
						  </tr>`;

				});

			}

			table += '</table>';

			if(res.length > 0){
				table += `<button id="cambiarVotosMaximos" class="btn btn-secondary d-block mb-4 mx-auto">Cambiar Votos Máximos</button>`;
			}

			table += `
					<button class="btn btn-success agregarComiteUnidad" type="button">Agregar +</button>
			`;

			$('#contenido').html(table);

			$('.borrarComiteUnidad').click(function(){
				var comites = JSON.parse(comitesJSON);
				var idUnidad = $(this).attr('idUnidad');
				var table = `
					<table class="table table-hover table-stripped">
						<th>Nombre</th>
						<th>Quitar</th>
				`;

				comites.forEach((r)=>{
					table += `
						<tr>
							<td class="nombreU">${r.nombre_comite}</td>
							<td>
								<input id="comite${r.id_comite}" type="checkbox" idUnidad="${r.id_unidad}" idComite="${r.id_comite}" class="form-control text-center d-block mx-auto m-0 p-0 cursor-pointer quitarComiteUnidadInput" style="width:25px;height:25px;cursor:pointer" />
							</td>
						</tr>
					`;
				});

				table += '</table>';

				alerta.fire({
					title: 'Quitar Comité de Unidad',
					html: `
						<h5 id="nombreU">${nombreU}</h5>
						<br>
						<h6>Seleccione los comités que desea quitar</h6>
						<div>
							${table}
						</div>
					`,
					showCancelButton: true,
					confirmButtonText: 'Quitar',
					showLoaderOnConfirm: true,
					preConfirm: ()=>{
						return new Promise(function(resolve){
							var comites = [];
							var quitarComiteUnidadInput = $('.quitarComiteUnidadInput');
							$('.quitarComiteUnidadInput').each((i)=>{
								i = $(quitarComiteUnidadInput).get(i);
								if(i.checked){
									comites.push($(i).attr('idComite'));
								}
							});
							if(comites.length < 1){
								alerta.fire({
									icon: 'error',
									title: 'No seleccionó ningún comité'
								});
								return;
							}
							comites = JSON.stringify(comites);

							$.post(SERVERURL + '/unidades/borrarComiteUnidad', {codConsejo: cod, idUnidad: idUnidad, comites: comites}, function(res){
								var errorMsj = '¡No se pudo quitar el comité!';
								if(res == 'eleccion_comenzada'){
									errorMsj = 'No se puede quitar comités de este consejo comunal porque hay una elección en curso';
								}
								ajaxRespuesta(res, 'Comités Quitados', errorMsj, false);
							});
						});
					}
				});

				$(`#comite${$(this).attr('idComite')}`).attr('checked', true);

			});

			$('#cambiarVotosMaximos').click(function(){
				var comites = JSON.parse(comitesJSON);
				var table = `
					<table class="table table-hover table-stripped">
						<th>Nombre</th>
						<th>Votos Máximos</th>
				`;

				comites.forEach((r)=>{
					table += `
						<tr>
							<td class="nombreU">${r.nombre_comite}</td>
							<td>
								<input idUnidad="${r.id_unidad}" idCOmite="${r.id_comite}" class="form-control text-center d-block mx-auto votosMaximosInput" style="width:50px" value="${r.votos_maximos}" oninput="validarVotosMaximos(this)" />
							</td>
						</tr>
					`;
				});

				table += '</table>';

				alerta.fire({
					title: 'Cambiar Votos Máximos',
					html: `
						<h4 id="nombreC">${nombreC}</h4>
						<h5 id="nombreU">${nombreU}</h5>
						<br>
						<div>
							${table}
						</div>
					`,
					showCancelButton: true,
					confirmButtonText: 'Guardar',
					showLoaderOnConfirm: true,
					preConfirm: ()=>{
						return new Promise(function(resolve){
							var votosMaximos = [];
							var votosMaximosInput = $('.votosMaximosInput');
							var inputVacio = false;
							$('.votosMaximosInput').each((i)=>{
								i = $(votosMaximosInput).get(i);
								if($(i).val() == ''){
									$(i).addClass('border-danger');
									inputVacio = true;
								}
								votosMaximos.push({idUnidad: $(i).attr('idUnidad'), idComite: $(i).attr('idComite'), votosMaximos: $(i).val()});
							});
							if(inputVacio){
								Swal.disableLoading();
								return;
							}
							votosMaximos = JSON.stringify(votosMaximos);

							$.post(SERVERURL + '/consejoscomunales/cambiarVotosMaximos', {codConsejo: cod, votosMaximos: votosMaximos}, function(res){
								var errorMsj = 'Error al cambiar Votos Máximos';
								if(res == 'eleccion_comenzada'){
									errorMsj = 'No se pueden cambiar los votos máximos porque hay una elección en curso';
								}
								ajaxRespuesta(res, 'Votos Máximos Cambiado', errorMsj, false);
							});
						});
					}
				});

			});


		});



	});



	//Agregar comite a unidad
	$(document).on('click', '.agregarComiteUnidad', function(){

		var cod = $('#codConsejo').val().trim();
		var idUnidad = $('#idUnidad').val().trim();
		var nombreC = $('#nombreC').text().trim();
		var nombreU = $('#nombreU').text().trim();
		var comites = [];
		var res = null;

		alerta.fire({
			title: 'Agregar Comité',
			html: `

				<h4>${nombreC}</h4>
				<h5>${nombreU}</h5>
				<br><br>
				<div id="contenido">
					<div class="spinner-border text-primary"></div><span class="cargandoText text-primary">Cargando...</span>
				</div>
				<br>

			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{

				return new Promise(function(resolve){

					if(comites.length < 1){
						alerta.fire({
							icon: 'error',
							title: '¡No se seleccionó ningún comité!'
						})
						return;
					}

					comites = JSON.stringify(comites);

					$.post(SERVERURL + '/comites/agregarComiteUnidad', {cod : cod, idUnidad : idUnidad, comites : comites}, function(res){

						ajaxRespuesta(res, '¡Comité(s) agregado(s)!', '¡No se pudo agregar el comité!', false);

					});

				});

			}
		})
		.then(()=>{
			comites = [];
			$('.swal2-content').unbind();
		})


		$.post(SERVERURL + '/comites/getComitesNotIn', {cod : cod, idUnidad : idUnidad}, function(r){
			$('.swal2-confirm').removeAttr('disabled');

			res = JSON.parse(r);

			if(res.length < 1){
				alerta.fire({
					title: 'No hay comités para agregar.'
				})
				return;
			}

			var select = `<div class="form-group">
						   <select id="comites" class="form-control">
							<option hidden selected value="">Seleccionar...</option>
						 `;

			res.forEach((r)=>{
				select += `<option class="comitesOption" value="${r.id}">${r.nombre_comite}</option>`;
			})

			select +=  ` </select>
						</div>`;

			select += `<table id="comitesTable" class="table table-hover table-stripped d-none">
						<th>Nombre</th>
						<th>Quitar</th>
					   </table>
			`;

			$('#contenido').html(select);

			$('#comites').on('change', function(){

				var value = $(this).val();
				var nombre = $(this).find(`.comitesOption[value="${value}"]`).text().trim();

				comites.push(value);

				if(comites.length == res.length){
					$('#comites').parent('.form-group').addClass('d-none');
				}

				$(this).find(`.comitesOption[value="${value}"]`).addClass('d-none');
				$('#comitesTable').removeClass('d-none');
				$(this).val('');
				$(this).focus();
				$(':focus').blur();
				focusedVerificar();

				$('#comitesTable').append(`<tr><input class="valueComite" type="hidden" value="${value}" /><td>${nombre}</td><td><img class="borrarImg quitarComite" src="${SERVERURL}/views/img/borrar.svg" height=25 width=25 /></td></tr>`);


			});


		});

		$('.swal2-confirm').attr('disabled','disabled');

		$('.swal2-content').on('click', '.quitarComite', function(){

			var value = $(this).parent().parent().find('.valueComite').val();
			$('#comites').find(`.comitesOption[value="${value}"]`).removeClass('d-none');

			const index = comites.indexOf(value);
			if (index > -1) {
			  comites.splice(index, 1);
			}
			$(this).parent().parent().remove();
			if(comites.length < 1){
				$('#comitesTable').addClass('d-none');
			}

			if(comites.length != res.length){
				$('#comites').parent('.form-group').removeClass('d-none');
			}

		});

	});



	//Borrar unidad de consejo comunal
	/*$(document).on('click', '.borrarUnidadConsejo', function(){

		var id = $(this).parent().parent().find('#idUnidad').val().trim();
		var nombre = $(this).parent().parent().find('.nombreU').text().trim();
		var cod = $('#codConsejo').val().trim();
		var nombreC = $('#nombreConsejo').text().trim();

		alerta.fire({
			icon: 'warning',
			title: '¿Estás seguro de quitar la Unidad del Consejo Comunal?',
			html: `
				<h4 class="m-0 p-0">${nombreC}</h4>
				<h5 class="mt-0 pt-0">${nombre}</h5>
			`,
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					$.post(SERVERURL + '/unidades/borrarUnidadConsejo', {id : id, cod : cod}, function(res){
						var errorMsj = '¡No se pudo quitar la unidad!';
						if(res == 'eleccion_comenzada'){
							errorMsj = 'No se puede quitar la unidad de este consejo comunal porque hay una elección en curso';
						}
						ajaxRespuesta(res, '¡Unidad Quitada!', errorMsj);
					});

				});
			}
		})

	});*/



	//Borrar comite de unidad
	/*$(document).on('click', '.borrarComiteUnidad', function(){

		var idUnidad = $('#idUnidad').val().trim();
		var cod = $('#codConsejo').val().trim();
		var idComite = $(this).parent().parent().find('#idComite').val().trim();
		var nombre =  $(this).parent().parent().find('.nombreComite').text().trim();

		alerta.fire({
			icon: 'warning',
			title: '¿Estás seguro de quitar el comité?',
			html: `
				<h5>${nombre}</h5>
			`,
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					$.post(SERVERURL + '/unidades/borrarComiteUnidad', {idUnidad : idUnidad, cod : cod, idComite : idComite}, function(res){
						var errorMsj = '¡No se pudo quitar el comité!';
						if(res == 'eleccion_comenzada'){
							errorMsj = 'No se puede quitar el comité de este consejo comunal porque hay una elección en curso';
						}
						ajaxRespuesta(res, '¡Comité Quitado!', errorMsj, false);
					});
				});
			}
		})

	});*/


	var fotoPersonaFile = null;
	var TODOSLOSCONSEJOS = [];
	//Agregar persona
	$('.agregarPersona').click(function(){

		var boton = $(this);

		var tipoUsuario = $('#tipoUsuario').val();

		alerta.fire({
			title: 'Agregar persona',
			html: `
				<div class="form-group fotoGroup">
					<label for="fotoPersona" class="d-block m-0">
						<div class="fotoPersona">
							<span>Subir Foto</span>
							<img id="fotoPersonaPreview" src="" class="d-none" />
						</div>
					</label>
					<input type="file" id="fotoPersona" accept="image/*" class="d-none" />
					<div class="form-group mt-3">
						<span class="form-label">Cédula...</span>
						<input id="cedula" class="form-control" type="text" oninput="validarCedula(this); javascript:if(this.value.length>9) this.value = this.value.slice(0, this.maxLength);" />
					</div>
					<div class="form-inline formAgregarPersona mt-1">
						<div class="form-group">
							<span class="form-label">Primer nombre...</span>
							<input id="pnombre" class="form-control" type="text" oninput="validarLetras(this)" style="text-transform: capitalize;" maxlength="15" />
						</div>
						<div class="form-group">
							<span class="form-label ml">Segundo nombre...</span>
							<input id="snombre" class="form-control ml" type="text" oninput="validarLetras(this)" style="text-transform: capitalize;" maxlength="15" />
						</div>
					</div>
				</div>
				<div class="form-inline formAgregarPersona mt-1 mb-3 text-center">
					<div class="form-group">
						<span class="form-label">Primer apellido...</span>
						<input id="papellido" class="form-control" type="text" oninput="validarLetras(this)" style="text-transform: capitalize;" maxlength="15" />
					</div>
					<div class="form-group">
						<span class="form-label ml">Segundo apellido...</span>
						<input id="sapellido" class="form-control ml" type="text" oninput="validarLetras(this)" style="text-transform: capitalize;" maxlength="15" />
					</div>
					<div class="form-group">
					<span class="form-label spanGenero">Género...</span>
					<select id="sexo" class="form-control">
						<option hidden value=""></option>
						<option value="M">Masculino</option>
						<option value="F">Femenino</option>
					</select>
				</div>
				</div>
				<div class="form-group mt-1">
					<span class="form-label">Fecha de nacimiento...</span>
					<input id="fechanacimiento" class="form-control fechanacimiento" type="text" name="fecha" autocomplete="off" readonly/>
				</div>
				<div class="form-group mt-1">
					<span class="form-label">Consejo comunal...</span>
					<select id="consejocomunal" class="form-control">
						<option disabled value="">Cargando...</option>
					</select>
				</div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					var cedula = $('#cedula').val();
					var pnombre = $('#pnombre').val();
					var snombre = $('#snombre').val();
					var papellido = $('#papellido').val();
					var sapellido = $('#sapellido').val();
					var sexo = $('#sexo').val();
					var fecha_nacimiento = $('#fechanacimiento').val();
					var consejocomunal = $('#consejocomunal').val();
					var fotoPersonaPreview = $('#fotoPersonaPreview').attr('src');
					var patron = /^[a-zA-Zá-Źñ]+$/;
					var soloLetras = new RegExp(patron);

					var imgFoto = $('#fotoPersonaPreview').attr('src');

					var datosError = ()=>{
							$(boton).trigger('click');
							$('#cedula').val(cedula);
							$('#pnombre').val(pnombre);
							$('#snombre').val(snombre);
							$('#papellido').val(papellido);
							$('#sapellido').val(sapellido);
							$('#sexo').val(sexo);
							$('.fechanacimiento').val(fecha_nacimiento);
							$('#consejocomunal').on('listo', function(){
								$(this).val(consejocomunal);
								focusedVerificar();
							})
							focusedVerificar();
							if(imgFoto != ''){
								$('#fotoPersonaPreview').attr('src', imgFoto);
								$('#fotoPersonaPreview').removeClass('d-none');
								var file = new File([fotoPersonaFile], "img.jpg",{type:"image/jpeg", lastModified:new Date().getTime()});
								var container = new DataTransfer();
								container.items.add(file);
								document.getElementById('fotoPersona').files = container.files;
							}
						};

					if(cedula == '' || pnombre == '' || papellido == '' || sexo == '' ||fecha_nacimiento == '' || (tipoUsuario == '1' && (consejocomunal == '' || consejocomunal == null))){
						alerta.fire({
							icon: 'error',
							title: '¡Faltó datos por llenar!'
						})
						.then(datosError);
						return;
					}
					if(!$.isNumeric(cedula) || cedula.length < 7){
						alerta.fire({
							icon: 'error',
							title: '¡Cédula incorrecta!'
						})
						.then(datosError);
						return;
					}
					if(!soloLetras.test(pnombre)){
						alerta.fire({
							icon: 'error',
							title: '¡Primer nombre incorrecto!'
						})
						.then(datosError);
						return;
					}
					if(snombre != "" && !soloLetras.test(snombre)){
						alerta.fire({
							icon: 'error',
							title: '¡Segundo nombre incorrecto!'
						})
						.then(datosError);
						return;
					}
					if(!soloLetras.test(papellido)){
						alerta.fire({
							icon: 'error',
							title: '¡Primer apellido incorrecto!'
						})
						.then(datosError);
						return;
					}
					if(sapellido != "" && !soloLetras.test(sapellido)){
						alerta.fire({
							icon: 'error',
							title: '¡Segundo apellido incorrecto!'
						})
						.then(datosError);
						return;
					}
		
					var datos = {cedula : cedula, pnombre : pnombre, snombre : snombre, papellido : papellido, sapellido : sapellido, sexo : sexo, fecha_nacimiento : fecha_nacimiento, consejocomunal : consejocomunal};

					datos = JSON.stringify(datos);

					var fd = new FormData();
			        var files = $('#fotoPersona')[0].files[0];
			        fd.append('file',files);
			        fd.foto = fd;
			        fd.append('datos', datos);

					$.ajax({

						url: SERVERURL + '/personas/agregar',
						data: fd,
						processData: false,
  						contentType: false,
  						type: 'POST',
  						success: function(res){

  						var errorMsj = '¡No se pudo agregar la persona!';
  						if(res == 'cedula_existe'){
  							errorMsj = 'La persona con esa cédula ya está registrada';
  						}

							ajaxRespuesta(res, '¡Persona agregada!', errorMsj, true, function(){

								if(!res || res == 'cedula_existe'){
									$(boton).trigger('click');
									$('#cedula').val(cedula);
									$('#pnombre').val(pnombre);
									$('#snombre').val(snombre);
									$('#papellido').val(papellido);
									$('#sapellido').val(sapellido);
									$('#sexo').val(sexo);
									$('.fechanacimiento').val(fecha_nacimiento);
									$('#consejocomunal').on('listo', function(){
										$(this).val(consejocomunal);
										focusedVerificar();
									})
								}

							})

						}

				});
				});
			}
		})

		if(tipoUsuario != '1'){
			$('#consejocomunal').parent().addClass('d-none');
		}

		if(TODOSLOSCONSEJOS.length < 1){
			$.post(SERVERURL + '/consejoscomunales/consultarTodos', function(res){

				TODOSLOSCONSEJOS = JSON.parse(res);

				TODOSLOSCONSEJOS.forEach((r)=>{
					$('#consejocomunal').append(`<option value="${r.cod}">${r.nombre}</option>`);
				})

				$('#consejocomunal').find('option[disabled]').text('');
				$('#consejocomunal').find('option[disabled]').attr('selected','selected');
				$('#consejocomunal').find('option[disabled]').attr('hidden','hidden');
				$('#consejocomunal').parent().removeClass('focused');

				$('#consejocomunal').trigger('listo');

			});
		}else{
				TODOSLOSCONSEJOS.forEach((r)=>{
					$('#consejocomunal').append(`<option value="${r.cod}">${r.nombre}</option>`);
				})

				$('#consejocomunal').find('option[disabled]').text('');
				$('#consejocomunal').find('option[disabled]').attr('selected','selected');
				$('#consejocomunal').find('option[disabled]').attr('hidden','hidden');
				$('#consejocomunal').parent().removeClass('focused');

				$('#consejocomunal').trigger('listo');
		}

		calendario(startDate = '01/01/1995', minYear = '1901', maxYear = new Date().getFullYear(), '01/01/1900', $('#fechaActualServidor').text());

	});


	
	function onEditarPersona(){
		//Editar persona
	$('.editarPersona').click(function(){

		var boton = $(this);

		var tipoUsuario = $('#tipoUsuario').val();

		var cedulaOriginal = $(this).parent().parent().find('.cedulaPersona').text().trim();
		var pnombre = $(this).parent().parent().find('.pnombrePersona').text().trim();
		var snombre = $(this).parent().parent().find('.snombrePersona').text().trim();
		var papellido = $(this).parent().parent().find('.papellidoPersona').text().trim();
		var sapellido = $(this).parent().parent().find('.sapellidoPersona').text().trim();
		var sexo = $(this).parent().parent().find('.sexoPersona').text().trim().charAt(0);
		var fecha_nacimiento = $(this).parent().parent().find('#fecha_nacimiento').val().trim();
		var consejocomunal = $(this).parent().parent().find('.codConsejocomunalPersona').val().trim();

		$.get(`${SERVERURL}/views/img/fotos_personas/${cedulaOriginal}.jpg`)
			.done(()=>{
				$('#fotoPersonaPreview').removeClass('d-none');
			})
			.fail(()=>{
				if($('#fotoPersonaPreview').attr('src') == ''){
					$('#fotoPersonaPreview').addClass('d-none');
				}
			})

		alerta.fire({
			title: `Editar persona`,
			html: `
				<h5 class="mb-4">${pnombre} ${snombre} ${papellido} ${sapellido}</h5>
				<div class="form-group fotoGroup">
					<label for="fotoPersona" class="d-block m-0">
						<div class="fotoPersona">
							<span>Subir Foto</span>
							<img id="fotoPersonaPreview" src="${SERVERURL}/views/img/fotos_personas/${cedulaOriginal}.jpg" class="d-none" />
						</div>
					</label>
					<input type="file" id="fotoPersona" accept="image/*" class="d-none" />
					<div class="form-group mt-3">
						<span class="form-label">Cédula...</span>
						<input id="cedula" class="form-control" type="text" oninput="validarCedula(this); javascript:if(this.value.length>9) this.value = this.value.slice(0, this.maxLength);" />
					</div>
					<div class="form-inline formAgregarPersona mt-1">
						<div class="form-group">
							<span class="form-label">Primer nombre...</span>
							<input id="pnombre" class="form-control" type="text" oninput="validarLetras(this)" style="text-transform: capitalize;" />
						</div>
						<div class="form-group">
							<span class="form-label ml">Segundo nombre...</span>
							<input id="snombre" class="form-control ml" type="text" oninput="validarLetras(this)" style="text-transform: capitalize;" />
						</div>
					</div>
				</div>
				<div class="form-inline formAgregarPersona mt-1 mb-3 text-center">
					<div class="form-group">
						<span class="form-label">Primer apellido...</span>
						<input id="papellido" class="form-control" type="text" oninput="validarLetras(this)" style="text-transform: capitalize;" />
					</div>
					<div class="form-group">
						<span class="form-label ml">Segundo apellido...</span>
						<input id="sapellido" class="form-control ml" type="text" oninput="validarLetras(this)" style="text-transform: capitalize;" />
					</div>
					<div class="form-group">
					<span class="form-label spanGenero">Género...</span>
					<select id="sexo" class="form-control">
						<option hidden value=""></option>
						<option value="M">Masculino</option>
						<option value="F">Femenino</option>
					</select>
				</div>
				</div>
				<div class="form-group mt-1">
					<span class="form-label">Fecha de nacimiento...</span>
					<input id="fechanacimiento" class="form-control fecha_nacimiento" type="text" name="fecha" autocomplete="off" readonly />
				</div>
				<div class="form-group mt-1">
					<span class="form-label">Consejo comunal...</span>
					<select id="consejocomunal" class="form-control">
						<option disabled value="">Cargando...</option>
					</select>
				</div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					var cedulaModificar = $('#cedula').val();
					var pnombre = $('#pnombre').val();
					var snombre = $('#snombre').val();
					var papellido = $('#papellido').val();
					var sapellido = $('#sapellido').val();
					var sexo = $('#sexo').val();
					var fecha_nacimiento = $('#fechanacimiento').val();
					var consejocomunal = $('#consejocomunal').val();
					var patron = /^[a-zA-Zá-Źñ]+$/;
					var soloLetras = new RegExp(patron);

					var imgFoto = $('#fotoPersonaPreview').attr('src');

					var datosError = ()=>{
							$(boton).trigger('click');
							$('#cedula').val(cedulaModificar);
							$('#pnombre').val(pnombre);
							$('#snombre').val(snombre);
							$('#papellido').val(papellido);
							$('#sapellido').val(sapellido);
							$('#sexo').val(sexo);
							$('.fechanacimiento').val(fecha_nacimiento);
							$('#consejocomunal').on('listo', function(){
								$(this).val(consejocomunal);
								focusedVerificar();
							})
							if(imgFoto != ''){
								$('#fotoPersonaPreview').attr('src', imgFoto);
								$('#fotoPersonaPreview').removeClass('d-none');
								var file = new File([fotoPersonaFile], "img.jpg",{type:"image/jpeg", lastModified:new Date().getTime()});
								var container = new DataTransfer();
								container.items.add(file);
								document.getElementById('fotoPersona').files = container.files;
							}
						};

					if(cedulaModificar == '' || pnombre == '' || papellido == '' || sexo == '' ||fecha_nacimiento == '' || (tipoUsuario == '1' && (consejocomunal == '' || consejocomunal == null))){
						alerta.fire({
							icon: 'error',
							title: '¡Faltó datos por llenar!'
						})
						.then(datosError);
						return;
					}
					if(!$.isNumeric(cedulaModificar)){
						alerta.fire({
							icon: 'error',
							title: '¡Cédula incorrecta!'
						})
						.then(datosError);
						return;
					}
					if(!soloLetras.test(pnombre)){
						alerta.fire({
							icon: 'error',
							title: '¡Primer nombre incorrecto!'
						})
						.then(datosError);
						return;
					}
					if(snombre != "" && !soloLetras.test(snombre)){
						alerta.fire({
							icon: 'error',
							title: '¡Segundo nombre incorrecto!'
						})
						.then(datosError);
						return;
					}
					if(!soloLetras.test(papellido)){
						alerta.fire({
							icon: 'error',
							title: '¡Primer apellido incorrecto!'
						})
						.then(datosError);
						return;
					}
					if(sapellido != "" && !soloLetras.test(sapellido)){
						alerta.fire({
							icon: 'error',
							title: '¡Segundo apellido incorrecto!'
						})
						.then(datosError);
						return;
					}
					

					var datos = {cedulaOriginal : cedulaOriginal, cedulaModificar : cedulaModificar, pnombre : pnombre, snombre : snombre, papellido : papellido, sapellido : sapellido, sexo : sexo, fecha_nacimiento : fecha_nacimiento, consejocomunal : consejocomunal};

					datos = JSON.stringify(datos);

					var fd = new FormData();
			        var files = $('#fotoPersona')[0].files[0];
			        fd.append('file',files);
			        fd.foto = fd;
			        fd.append('datos', datos);

					$('input, select').attr('disabled','disabled');



					$.ajax({

						url: SERVERURL + '/personas/editar',
						data: fd,
						processData: false,
  						contentType: false,
  						type: 'POST',
  						success: function(res){

  						var errorMsj = '¡No se pudo editar la persona!';
  						if(res == 'cedula_existe'){
  							errorMsj = 'La persona con esa cédula ya está registrada';
  						}else if(res == 'pertenece_eleccion'){
  							errorMsj = 'No se puede cambiar el Consejo Comunal porque la persona se encuentra postulada a una Elección';
  						}else if(res == 'no_puedes_editar'){
  							errorMsj = 'No tienes permitido modificar a esta persona.';
  						}else if(res == 'no_permitido'){
								errorMsj = '¡No tienes permitido modificar a esta persona!';
							}else if(res == 'participa_eleccion'){
								errorMsj = 'No puedes modificar los datos de una persona que participó en una elección.';
							}

							ajaxRespuesta(res, '¡Persona Modificada!', errorMsj, true, function(){
								if(!res || res == 'cedula_existe' || res == 'pertenece_eleccion' || res == 'no_puedes_editar' || res == 'no_permitido'){
									$(boton).trigger('click');
									$('#cedula').val(cedulaModificar);
									$('#pnombre').val(pnombre);
									$('#snombre').val(snombre);
									$('#papellido').val(papellido);
									$('#sapellido').val(sapellido);
									$('#sexo').val(sexo);
									$('.fechanacimiento').val(fecha_nacimiento);
									$('#consejocomunal').on('listo', function(){
										$(this).val(consejocomunal);
										focusedVerificar();
									})
								}
							});

							$('input, select').removeAttr('disabled');

						}

					});

				 	});

				}
			});

		if(tipoUsuario != '1'){
			$('#consejocomunal').parent().addClass('d-none');
		}

		$('#consejocomunal').on('listo', function(){
			$('#consejocomunal').val(consejocomunal);
			focusedVerificar();
		});

		if(TODOSLOSCONSEJOS.length < 1){
			$.post(SERVERURL + '/consejoscomunales/consultarTodos', function(res){

				TODOSLOSCONSEJOS = JSON.parse(res);

				TODOSLOSCONSEJOS.forEach((r)=>{
					$('#consejocomunal').append(`<option value="${r.cod}">${r.nombre}</option>`);
				})

				$('#consejocomunal').find('option[disabled]').text('');
				$('#consejocomunal').find('option[disabled]').attr('selected','selected');
				$('#consejocomunal').find('option[disabled]').attr('hidden','hidden');
				$('#consejocomunal').parent().removeClass('focused');

				$('#consejocomunal').trigger('listo');

			});
		}else{
				TODOSLOSCONSEJOS.forEach((r)=>{
					$('#consejocomunal').append(`<option value="${r.cod}">${r.nombre}</option>`);
				})

				$('#consejocomunal').find('option[disabled]').text('');
				$('#consejocomunal').find('option[disabled]').attr('selected','selected');
				$('#consejocomunal').find('option[disabled]').attr('hidden','hidden');
				$('#consejocomunal').parent().removeClass('focused');

				$('#consejocomunal').trigger('listo');
		}

		$('#cedula').val(cedulaOriginal);
		$('#pnombre').val(pnombre);
		$('#snombre').val(snombre);
		$('#papellido').val(papellido);
		$('#sapellido').val(sapellido);
		$('#sexo').val(sexo);
		$('.fecha_nacimiento').val(fecha_nacimiento);

		calendario(startDate = '01/01/1995', minYear = '1901', maxYear = new Date().getFullYear(), '01/01/1900', $('#fechaActualServidor').text());
		focusedVerificar();

	});
	}

	onEditarPersona();



	//Borrar persona
	function onBorrarPersona(){
			$('.borrarPersona').click(function(){

		var cedula = $(this).parent().parent().find('.cedulaPersona').text().trim();
		var pnombre = $(this).parent().parent().find('.pnombrePersona').text().trim();
		var snombre = $(this).parent().parent().find('.snombrePersona').text().trim();
		var papellido = $(this).parent().parent().find('.papellidoPersona').text().trim();
		var sapellido = $(this).parent().parent().find('.sapellidoPersona').text().trim();
	 
	 	alerta.fire({
	 		icon: 'warning',
	 		title: '¿Estás seguro de borrar al Habitante?',
	 		html: `
	 			<h5>${pnombre} ${snombre} ${papellido} ${sapellido}</h5>
	 		`,
	 		showCancelButton: true,
	 		confirmButtonText: 'Borrar',
	 		showLoaderOnConfirm: true,
	 		preConfirm: ()=>{
	 			return new Promise(function(resolve){

	 				$.post(SERVERURL + '/personas/borrar', {cedula : cedula}, function(res){
	 					var errorMsj = '¡No se pudo borrar la persona!';
	 					if(res == 'candidato_participando'){
	 						errorMsj = 'La persona está participando actualmente en una elección que ya comenzó';
	 					}else if(res == 'no_permitido'){
	 						errorMsj = '¡No tienes permitido borrar a esta persona!';
	 					}else if(res == 'participa_eleccion'){
	 						errorMsj = '¡No puedes borrar una persona que  en una elección!';
	 					}
	 					ajaxRespuesta(res, '¡Persona Borrada!', errorMsj);
	 				});

	 			});
	 		}
	 	})

	});
	}

	onBorrarPersona()



	//Crear elección
	$('.crearEleccion').click(function(){

		var tipoUsuario = $('#tipoUsuario').val();

		alerta.fire({
			title: 'Crear nueva elección',
			html: `
				<div id="contenido" class="mt-3 mb-3">
					<div class="spinner-border text-primary"></div><span class="cargandoText text-primary">Cargando...</span>
				</div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Crear',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					var consejocomunal = $('#consejocomunal').val();
					var fecha = $('#fecha').val();

					if(tipoUsuario == '2'){
						consejocomunal = '-';
					}

					if(consejocomunal == '' || fecha == ''){
						alerta.fire({
							title: '¡Debe llenar todos los datos!',
							icon: 'error'
						}).then(()=>{
							$('.crearEleccion').trigger('click');
						})
						return;
					}

					$.post(SERVERURL + '/elecciones/crear', {consejocomunal : consejocomunal, fecha : fecha}, function(res){
						var r = false;
						if(!isNaN(res) && res > 0){
							r = 1;
						}
						ajaxRespuesta(r, '¡Elección Creada!', '¡No se pudo crear la elección!', false, (re)=>{re == 1 ? window.location.href = SERVERURL + '/elecciones/ver/' + res + '/' : ''});
					});

				});
			}
		});

		$('.swal2-confirm').attr('disabled','disabled');

		$.post(SERVERURL + '/consejoscomunales/consultarTodos', function(res){
			res = JSON.parse(res);

			var contenido = `<div class="form-group mb-3 ${tipoUsuario == '2' ? 'd-none' : ''}">
							<span class="form-label">Consejo comunal...</span>
							<select id="consejocomunal" class="form-control">
								<option hidden value=""></option>`;
			
			res.forEach((r)=>{
				 contenido += `<option value="${r.cod}">${r.nombre}</option>`;
			})

			contenido += `</select></div>`;

			contenido += `
				<div class="form-group">
					<span class="form-label">Fecha de realización...</span>
					<input id="fecha" class="form-control" name="fecha" type="text" autocomplete="off" readonly />
			`;

			$('#contenido').html(contenido);
			calendario(moment(), '14/01/2020', parseInt(moment().format('YYYY'),10) + 10, moment());

			$('.swal2-confirm').removeAttr('disabled');

		});

	});


	//Ver candidatos de unidad
	$('.verCandidatosUnidad').click(function(){

		var boton = $(this);
		var nombre = $(this).parent().parent().find('.nombreUnidad').text().trim();
		var idUnidad = $(this).parent().parent().find('.idUnidad').text().trim();
		var cod_consejocomunal = $('#consejocomunal').val().trim();
		var idEleccion = $('#idEleccion').val().trim();

		alerta.fire({
			title: 'Candidatos',
			html: `
				<h4 class="nombreU">${nombre}</h4>
				<input class="idUnidad" type="hidden" value="${idUnidad}" />
				<h5 id="seleccionarComite" class="d-none" name="${idUnidad}">Seleccionar comité</h5>
				<div id="contenido" class="mt-3 mb-3">
					<div class="spinner-border text-primary"></div><span class="cargandoText text-primary">Cargando...</span>
				</div>
			`,
			confirmButtonText: 'Cerrar',
			confirmButtonColor: '#cccccc',
			width: 'auto'
		})

		$.post(SERVERURL + '/elecciones/verCandidatosUnidad', {idUnidad : idUnidad, idEleccion : idEleccion, cod_consejocomunal : cod_consejocomunal}, function(res){

			res = JSON.parse(res);

			var tabla = `<table class="table table-hover table-stripped">`;

			var ninguno = false;
			if (res.comites){
				$('#seleccionarComite').removeClass('d-none');

				tabla += `<th>Nombre</th>
							<th>Votos Máximos</th>
						  <th>Candidatos</th>`;

				res.comites.forEach((c)=>{

					tabla += `<tr class="comiteEleccion">
								<input class="idComite" type="hidden" value="${c.id}"/>
								<td class="nombreComite">${c.nombre_comite}</td>
								<td>${c.votos_maximos}</td>
								<td>${c.candidatos}</td>
							  </tr>`;

				});

			}else{

				tabla += `<th>CI</th>
						  <th>Nombres</th>
						  <th>Apellidos</th>
						  <th>Borrar</th>`;

				res.candidatos.forEach((c)=>{
					tabla += `<tr>
								<td class="ciCandidato">${c.cedula}</td>
								<td class="nombresCandidato">${c.pnombre} ${c.snombre}</td>
								<td class="apellidosCandidato">${c.papellido} ${c.sapellido}</td>
								<td><img cedula="${c.cedula}" class="borrarImg borrarCandidato" src="${SERVERURL}/views/img/borrar.svg" /></td>
							  </tr>`;

				});

				if (res.candidatos.length < 1){
					ninguno = true;
				}

			}

			tabla += `</table>`;

			var sePuedeModificar = $('#sePuedeModificar').val();

			if(!res.comites && sePuedeModificar == 'si'){
				tabla += `<button class="btn btn-success mt-4 postularCandidato">Postular +</button>`;
			}

			if(ninguno){
				var h = `<span>No hay candidatos postulados.</span><br>`;
				if(sePuedeModificar == 'si'){
					h += `<button class="btn btn-success mt-4 postularCandidato">Postular +</button>`;
				}
				$('#contenido').html(h);
			}else{
				$('#contenido').html(tabla);
				if(sePuedeModificar == 'no'){
					$('.borrarCandidato').parent().html('-');
				}
			}


			$('.borrarCandidato').click(function(){
				var tabla = `<table class="table table-hover table-stripped">`;
				tabla += `<th>CI</th>
						  <th>Nombres</th>
						  <th>Apellidos</th>
						  <th>Borrar</th>`;

				res.candidatos.forEach((c)=>{
					tabla += `<tr>
								<td class="ciCandidato">${c.cedula}</td>
								<td class="nombresCandidato">${c.pnombre} ${c.snombre}</td>
								<td class="apellidosCandidato">${c.papellido} ${c.sapellido}</td>
								<td><input id="candidato${c.cedula}" class="borrarCandidatoInput" cedula="${c.cedula}" type="checkbox" style="width:25px;height:25px;cursor:pointer" /></td>
							  </tr>`;
				});

				tabla += `</table>`;

				alerta.fire({
					title: 'Borrar Candidatos',
					html: `
						<h4>${nombre}</h4>
						<br>
						<h5>Selecciona los candidatos a borrar</h5>
						${tabla}
					`,
					confirmButtonText: 'Borrar',
					showCancelButton: true,
					width: 'auto',
					showLoaderOnConfirm: true,
					preConfirm: () => {
						return new Promise(function(resolve){
							var candidatos = [];
							var borrarCandidatoInput = $('.borrarCandidatoInput');
							$('.borrarCandidatoInput').each((i)=>{
								i = $(borrarCandidatoInput).get(i);
								if(i.checked){
									candidatos.push($(i).attr('cedula'));
								}
							});
							if(candidatos.length < 1){
								alerta.fire({
									icon: 'error',
									title: 'No seleccionó ningún candidato'
								});
								return;
							}
							candidatos = JSON.stringify(candidatos);

							$.post(SERVERURL + '/elecciones/borrarCandidato', {idEleccion: idEleccion, idUnidad: idUnidad, cedulas: candidatos}, function(res){
								ajaxRespuesta(res, '¡Candidatos Borrados!', '¡No se pudo borrar los candidatos!');
							});

						});
					}
				});

				$(`#candidato${$(this).attr('cedula')}`).attr('checked', true);

			});


		});

	});



	//Ver candidatos de comite
	$(document).on('click', '.comiteEleccion', function(){

		var boton = $(this);
		var nombre = $(this).find('.nombreComite').text().trim();
		var nombreU = $('.nombreU').text().trim();
		var idComite = $(this).find('.idComite').val().trim();
		var idUnidad = $('#seleccionarComite').attr('name').trim();
		var cod_consejocomunal = $('#consejocomunal').val().trim();
		var idEleccion = $('#idEleccion').val().trim();

		alerta.fire({
			title: 'Candidatos',
			html: `
				<h5 class="nombreU">${nombreU}</h5>
				<h6 class="nombreC">${nombre}</h6>
				<input class="idUnidad" type="hidden" value="${idUnidad}" />
				<input class="idComite" type="hidden" value="${idComite}" />
				<div id="contenido" class="mt-3 mb-3">
					<div class="spinner-border text-primary"></div><span class="cargandoText text-primary">Cargando...</span>
				</div>
			`,
			confirmButtonText: 'Cerrar',
			width: 'auto'
		})

		$.post(SERVERURL + '/elecciones/verCandidatosComite', {idUnidad : idUnidad, idEleccion : idEleccion, idComite : idComite}, function(res){

			res = JSON.parse(res);

			var sePuedeModificar = $('#sePuedeModificar').val();

			if(res.length < 1 || typeof res != 'object'){
				var h = `<span>No hay candidatos postulados.</span><br>`;
				if(sePuedeModificar == 'si'){
					h += `<button class="btn btn-success mt-4 postularCandidato">Postular +</button>`;
				}
				$('#contenido').html(h);
			}else{
				var tabla = `<table class="table table-hover table-stripped">`;

				console.log(res);

				tabla += `<th>CI</th>
						  <th>Nombres</th>
						  <th>Apellidos</th>
						  <th>Borrar</th>`;

				res.forEach((c)=>{
				tabla +=`<tr>
							<td class="ciCandidato">${c.cedula}</td>
							<td class="nombresCandidato">${c.pnombre} ${c.snombre}</td>
							<td class="apellidosCandidato">${c.papellido} ${c.sapellido}</td>
							<td><img cedula="${c.cedula}" class="borrarImg borrarCandidato" src="${SERVERURL}/views/img/borrar.svg" /></td>
						 </tr>`;
					});


				tabla += `</table>`;

				if(sePuedeModificar == 'si'){
					$('#contenido').html(tabla + `<button class="btn btn-success mt-4 postularCandidato">Postular +</button>`);
				}else{
					$('#contenido').html(tabla);
					$('.borrarCandidato').parent().html('-');
				}
			}

			$('.borrarCandidato').click(function(){
				var tabla = `<table class="table table-hover table-stripped">`;
				tabla += `<th>CI</th>
						  <th>Nombres</th>
						  <th>Apellidos</th>
						  <th>Borrar</th>`;

				res.forEach((c)=>{
					tabla += `<tr>
								<td class="ciCandidato">${c.cedula}</td>
								<td class="nombresCandidato">${c.pnombre} ${c.snombre}</td>
								<td class="apellidosCandidato">${c.papellido} ${c.sapellido}</td>
								<td><input id="candidato${c.cedula}" class="borrarCandidatoInput" cedula="${c.cedula}" type="checkbox" style="width:25px;height:25px;cursor:pointer" /></td>
							  </tr>`;
				});

				tabla += `</table>`;

				alerta.fire({
					title: 'Borrar Candidatos',
					html: `
						<h5 class="nombreU">${nombreU}</h5>
						<h6 class="nombreC">${nombre}</h6>
						<br>
						<h5>Selecciona los candidatos a borrar</h5>
						${tabla}
					`,
					confirmButtonText: 'Borrar',
					showCancelButton: true,
					width: 'auto',
					showLoaderOnConfirm: true,
					preConfirm: () => {
						return new Promise(function(resolve){
							var candidatos = [];
							var borrarCandidatoInput = $('.borrarCandidatoInput');
							$('.borrarCandidatoInput').each((i)=>{
								i = $(borrarCandidatoInput).get(i);
								if(i.checked){
									candidatos.push($(i).attr('cedula'));
								}
							});
							if(candidatos.length < 1){
								alerta.fire({
									icon: 'error',
									title: 'No seleccionó ningún candidato'
								});
								return;
							}
							candidatos = JSON.stringify(candidatos);

							$.post(SERVERURL + '/elecciones/borrarCandidato', {idEleccion: idEleccion, idUnidad: idUnidad, idComite: idComite, cedulas: candidatos}, function(res){
								ajaxRespuesta(res, '¡Candidatos Borrados!', '¡No se pudo borrar los candidatos!');
							});

						});
					}
				});

				$(`#candidato${$(this).attr('cedula')}`).attr('checked', true);

			});

		});

	});



	//Postular candidato a unidad o comite
	$(document).on('click', '.postularCandidato', function(){

		var boton = $(this);

		var nombreU = $('.nombreU').text().trim();
		var nombreC = $('.nombreC').text().trim();
		var idComite = $('.idComite').val();
		if(typeof idComite != 'undefined'){
			idComite = idComite.trim();
		}
		var idUnidad = $(this).parent().parent().find('.idUnidad').val().trim();
		var idEleccion = $('#idEleccion').val().trim();

		if(nombreC == ''){

		}

		alerta.fire({
			title: 'Postular candidato/s',
			html:`
				<h5>${nombreU}</h5>
				<h6>${nombreC}</h6>
				<input class="idUnidad" type="hidden" value="${idUnidad}" />
				<input class="idComite" type="hidden" value="${idComite}" />
				<input class="idEleccion" type="hidden" value="${idEleccion}" />
				<div class="form-group mt-5">
					<span class="form-label">Buscar por cédula o nombre y apellido...</span>
					<input id="buscarPersonaCandidato" class="form-control" type="text" autocomplete="off" maxlength="30" oninput="validarBusquedaNombreCedula(this)" />
					<div id="listaPersonas">
					</div>
				</div>
				<table id="tablaPersonas" class="table table-hover table-stripped d-none">
					<th>CI</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Quitar</th>
				</table>
			`,
			confirmButtonText: 'Guardar',
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					$('#buscarPersonaCandidato').attr('disabled','disabled');

					if(LISTAPERSONAS.length < 1){
						alerta.fire({
							icon: 'error',
							title: '¡No se eligió a ninguna persona!'
						})
						.then(()=>{
							$(boton).trigger('click');
						})
						return;
					}

					var listaPersonas = JSON.stringify(LISTAPERSONAS);

					$.post(SERVERURL + '/elecciones/guardarCandidatoPostulado', {listaPersonas : listaPersonas, idUnidad : idUnidad, idComite : idComite, idEleccion : idEleccion}, function(res){

						res = JSON.parse(res);

						if(res.err != '0'){
							LISTAPERSONAS = [];
							alerta.fire({
								icon: 'error',
								title: res.msj
							})
						}else{
							alerta.fire({
								icon: 'success',
								title: res.msj
							})
							.then(()=>{
								window.location.reload();
							})
						}

					});

				});
			}
		})

	});



	//Busqueda de personas por AJAX para postular candidatos
	var PERSONAS=[];
	var LISTAPERSONAS=[];
	var buscarPersonaCandidatoTimeout;
	$(document).on('keyup', '#buscarPersonaCandidato' ,function(){

		var self = this;
		clearTimeout(buscarPersonaCandidatoTimeout);
		buscarPersonaCandidatoTimeout = setTimeout(function(){

			PERSONAS.forEach((a, i)=>{
	 			a.abort();
	 			PERSONAS.splice(i, 1);
	 		})

	 		var dato = $(self).val();
			if(dato == ''){
				$('#listaPersonas').html('');
				return;
			}else if(!isNaN(dato) && (dato < 1000000)){
				return;
			}

			$('.spinner-input-ajax').remove();
			$('#buscarPersonaCandidato').parent().append(`<div class="spinner-border text-primary spinner-input-ajax"></div>`);

			var idComite = $('.idComite').val().trim();
			var idUnidad = $(self).parent().parent().find('.idUnidad').val().trim();
			var idEleccion = $('#idEleccion').val().trim();

			PERSONAS.push($.post(SERVERURL + '/elecciones/getPersonasAjax', {idComite : idComite, idUnidad : idUnidad, idEleccion : idEleccion, dato : dato}, function(res){

				$('.spinner-input-ajax').remove();
				$('.swal2-actions').css('z-index','0');

				res = JSON.parse(res);

				$('#listaPersonas').html('');

				if(typeof res.errorInfo == 'object'){
					$('#listaPersonas').prepend(`<div class="d-block text-danger">¡Ha ocurrido un error!</div>`);
					return;
				}

				res.forEach((r)=>{
					if(!LISTAPERSONAS.includes(`${r.cedula}`)){
						$('#listaPersonas').prepend(`<div class="persona personaListaCandidatoAjax" ci="${r.cedula}" nombres="${r.pnombre} ${r.snombre}" apellidos="${r.papellido} ${r.sapellido}">${r.pnombre} ${r.snombre} ${r.papellido} ${r.sapellido} (${r.cedula})</div>`);
					}
				});

			}));

		},350)

	});



	//Agregar persona a la lista para ser postulado
	$(document).on('click', '.personaListaCandidatoAjax', function(){

		$('#listaPersonas').html('');
		$('#buscarPersonaCandidato').val('');

		var cedula = $(this).attr('ci');
		var nombres = $(this).attr('nombres');
		var apellidos = $(this).attr('apellidos');

		LISTAPERSONAS.push(cedula);

		$('#tablaPersonas').removeClass('d-none');

		$('#tablaPersonas').append(`<tr><td class="ciPostulado">${cedula}</td><td>${nombres}</td><td>${apellidos}</td><td><img class="borrarImg quitarPersonaListaCandidato" src="${SERVERURL}/views/img/borrar.svg" /></td></tr>`);

	});


	//Quitar persona de la lista de postulados
	$(document).on('click', '.quitarPersonaListaCandidato', function(){

		var cedula = $(this).parent().parent().find('.ciPostulado').text().trim();
		
		var index = LISTAPERSONAS.indexOf(cedula);

		if (index > -1) {
  			LISTAPERSONAS.splice(index, 1);
		}

		$(this).parent().parent().remove();
		if(LISTAPERSONAS.length < 1){
			$('#tablaPersonas').addClass('d-none');
		}

	});


	//Borrar candidato de una unidad o comite
	/*$(document).on('click', '.borrarCandidato', function(){

		var cedula = $(this).parent().parent().find('.ciCandidato').text().trim();
		var nombres = $(this).parent().parent().find('.nombresCandidato').text().trim();
		var apellidos = $(this).parent().parent().find('.apellidosCandidato').text().trim();
		var idEleccion = $('#idEleccion').val().trim();
		var idUnidad = $('#swal2-content').find('.idUnidad').val().trim();
		var idComite = $('#swal2-content').find('.idComite').val();

		var datos = {cedula : cedula, idEleccion : idEleccion, idUnidad : idUnidad};

		if(idComite != undefined){
			idComite = idComite.trim();
			datos.idComite = idComite;
		}

		alerta.fire({
			title: '¿Estás se guro de borrar el candidato?',
			html: `
				<h5>${nombres} ${apellidos}</ht>
			`,
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					$.post(SERVERURL + '/elecciones/borrarCandidato', datos, function(res){
						ajaxRespuesta(res, '¡Candidato borrado!', '¡No se pudo borrar el candidato!');
					});

				});
			}
		});

	});*/


	//Preview de foto persona
	$(document).on('change', '#fotoPersona', function(){

		fotoPersonaFile = this.files[0];

		var imagen = window.URL.createObjectURL(this.files[0]);

		$('#fotoPersonaPreview').attr('src',imagen);
		$('#fotoPersonaPreview').removeClass('d-none');

	});



	//Seleccionar candidato electoral
	$('.candidatoElectoral').click(function(){

		var ciPersona = $(this).find('.ciPersona').val().trim();
		var checksMaximos = $(this).parent().find('.votosMaximos').val().trim();
		var cantidadCheckeds = $(this).parent().find('input[type=checkbox]');
		var cont = 0;
		$(cantidadCheckeds).each((c)=>{
			if ($(cantidadCheckeds).get(c).checked){
				cont++;
			}
		})

		var checkbox = $(this).find('.checkCandidato').find('input').get(0);

		if(cont >= checksMaximos && !checkbox.checked){
			alerta.fire({
				icon: 'warning',
				title: '¡Alcanzó el máximo de votos!',
				html: `
					<span>Desmarque una opción para poder seleccionar otra.</span>
				`
			})
			return;
		}
		
		if(checkbox.checked){
			checkbox.checked = false;
		}else if(cont < checksMaximos){
			checkbox.checked = true;
		}

	});


	//Guardar votación
	$('.guardarVotacion').click(function(){
		alerta.fire({
			title: 'CONFIRMAR VOTACIÓN',
			html: '<div id="swalConfirmarCandidatos"></div>',
			showCancelButton: true,
			confirmButtonText: 'CONFIRMAR',
			cancelButtonText: 'CANCELAR',
			showLoaderOnConfirm: true,
			showClass: {popup: 'guardarVotacionSwal'},
			customClass: {
    		confirmButton: 'btn btn-success ml-5 mr-5',
    		cancelButton: 'btn btn-danger ml-5 mr-5'
  		},
			preConfirm: ()=>{
				return new Promise(function(resolve){

					var idEleccion = $('#idEleccion').val().trim();
					var votos = [];
					//var votosComprobante = [];

					var checkboxs = $(document).find('input[type=checkbox]');

					$(checkboxs).each((check)=>{

						if($(checkboxs).get(check).checked){
							var check = $(checkboxs).get(check);
							var ciPersona = $(check).parent().parent().parent().find('.ciPersona').val().trim();
							var idUnidad = $(check).parent().parent().parent().parent().find('.idUnidad').val().trim();
							var idComite = $(check).parent().parent().parent().parent().find('.idComite').val();
							var nombreCandidato = $(check).parent().parent().parent().find('.nombreCandidato').text();
							if (idComite != undefined){
								idComite = idComite.trim();
							}else{
								idComite = '';
							}
							votos.push({ciPersona : ciPersona, idEleccion : idEleccion, idUnidad : idUnidad, idComite : idComite});
							//votosComprobante.push({ciPersona : ciPersona, nombreCandidato : nombreCandidato, idUnidad : idUnidad, idComite : idComite});
						}

					});

					votos = JSON.stringify(votos);

					$('.guardarVotacion').attr('disabled', 'disabled');
					$.post(SERVERURL + '/elecciones/guardarVotacion', {votos : votos, idEleccion: idEleccion}, function(res){
						console.log(res);
						ajaxRespuesta(res, '¡Votación Guardada!', '¡Error al guardar la votación!', false, function(r){
								if(r && r == '1' || r == 1){
									window.location.href = SERVERURL + '/elecciones/esperar/' + idEleccion + '/';
								}else{
									window.location.reload();
								}
						});

						//GENERAR COMPROBANTE DE VOTACION DONDE SE MUESTRA LOS CANDIDATOS SELECCIONADOS
						//DE CADA UNIDAD Y COMITÉ EL CUAL SE DESCARGARÁ AUTOMÁTICAMENTE COMO UNA IMÁGEN
						/*
						var htmlVotos = `
							<h1>VOTACIÓN</h1>
							<h2>Consejo Comunal</h2>
							<br>
						`;
						var tarjetasElectorales = $('.boletaElectoral').find('.tarjetaElectoral');
						$(tarjetasElectorales).each((tarjeta)=>{
							var tarjeta = $(tarjetasElectorales).get(tarjeta);
							var idUnidad = $(tarjeta).find('.idUnidad').val();
							var idComite = $(tarjeta).find('.idComite').val();
							var nombreUnidad = $(tarjeta).find('.nombreUnidad').val();
							htmlVotos += `<b>${nombreUnidad}</b><br>`;
							if (idComite != undefined){
								var nombreComite = $(tarjeta).find('.nombreComite').val();
								htmlVotos += `<b>${nombreComite}</b><br><br>`;
								votosComprobante.forEach(v=>{
									if(v.idUnidad == idUnidad && v.idComite == idComite){
										htmlVotos += `<p>${v.nombreCandidato} (${v.ciPersona})</p>`;
									}
								});
							}else{
								htmlVotos += '<br>';
								votosComprobante.forEach(v=>{
									if(v.idUnidad == idUnidad && v.idComite == ''){
										htmlVotos += `<p>${v.nombreCandidato} (${v.ciPersona})</p>`;
									}
								});
							}
							htmlVotos += '<br>';
						});
						$('#comprobante').html(htmlVotos);
						if(res && res == '1' || res == 1){
							html2canvas(document.getElementById('comprobante')).then(canvas => {
					      canvas.style.display = 'none'
					      document.body.appendChild(canvas)
					      return canvas
					    })
					    .then(canvas => {
					      const image = canvas.toDataURL('image/png').replace('image/png', 'image/octet-stream')
					      const a = document.createElement('a')
					      a.setAttribute('download', 'votacion.png')
					      a.setAttribute('href', image)
					      a.click()
					      canvas.remove()
					    })
						}
						*/

						setTimeout(function(){
							if(res && res == '1' || res == 1){
								window.location.href = SERVERURL + '/elecciones/esperar/' + idEleccion + '/';
							}else{
								window.location.reload();
							}
						},3000);

					});


				});
			}
		});

		$('.swal2-content').attr('style','min-height:75vh;display:flex;align-items:center');
		$('.swal2-confirm').attr('style', 'font-size: 26px');
		$('.swal2-cancel').attr('style', 'font-size: 26px');

		var confirmarCandidatos = '<h4>CONFIRME LOS CANDIDATOS QUE HA SELECCIONADO</h4><div class="boletaElectoral" style="box-shadow:none!important;overflow-y:auto;">';
		var tarjetasElectorales = $('.boletaElectoral').find('.tarjetaElectoral');
		$(tarjetasElectorales).each((tarjeta)=>{
			var tarjeta = $(tarjetasElectorales).get(tarjeta);
			confirmarCandidatos += '<div class="tarjetaElectoralBoleta" style="align-self:stretch!important">';
			var nombreUnidad = $(tarjeta).find('.nombreUnidad').val();
			var idComite = $(tarjeta).find('.idComite').val();
			if (idComite != undefined){
				var nombreComite = $(tarjeta).find('.nombreComite').val();
				confirmarCandidatos += `<div class="nombreUnidadElectoral font-weight-bold" style="border-bottom:none">${nombreUnidad}</div>`;
				confirmarCandidatos += `<div class="nombreComiteElectoral font-weight-bold">${nombreComite}</div>`;
			}else{
				confirmarCandidatos += `<div class="nombreUnidadElectoral font-weight-bold">${nombreUnidad}</div>`;
			}
			var checkboxs = $(tarjeta).find('input[type=checkbox]');
			var sinElegir = true;
			$(checkboxs).each((check)=>{
				if($(checkboxs).get(check).checked){
					sinElegir = false;
					var check = $(checkboxs).get(check);
					var fotoCandidato = $(check).parent().parent().parent().find('.fotoCandidato').html();
					var nombreCandidato = $(check).parent().parent().parent().find('.nombreCandidato').text();
					confirmarCandidatos += `
						<div class="candidatoElectoralBoleta">
							<div class="fotoCandidato" style="position:absolute;float:left">
								${fotoCandidato}
							</div>
							<div class="nombreCandidato" style="width: 300px!important;">
								${nombreCandidato}
							</div>
						</div>`;
				}
			});
			if(sinElegir){
				confirmarCandidatos += '<div style="width:100%;border-bottom:0px solid black">SIN ELEGIR</div>';
			}
			confirmarCandidatos += '</div>';
		});
		confirmarCandidatos += '</div>';
		$('#swalConfirmarCandidatos').html(confirmarCandidatos);

	})



	//Cambiar fecha de elección
	$('.fechaEleccion').click(function(){

		var fecha = $('.fechaEleccion').attr('fecha').trim();
		var idEleccion = $('#idEleccion').val().trim();

		alerta.fire({
			title: 'Cambiar fecha de elección',
			html: `<input id="nuevaFecha" class="text-center form-control w-25 d-block mx-auto" type="text" name="fecha" autocomplete="off" readonly />`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					var fecha = $('#nuevaFecha').val();
					var fecha = moment(fecha, 'DD/MM/YYYY').format('YYYY-MM-DD');

					$.post(SERVERURL + '/elecciones/cambiarFecha', {idEleccion : idEleccion, fecha: fecha}, function(res){
						ajaxRespuesta(res, '¡Fecha de elección cambiada!', '¡No se pudo cambiar la fecha de elección!');
					});
				});
			}
		})

		$('#nuevaFecha').val(moment(fecha, 'YYYY-MM-DD').format('DD/MM/YYYY'));
		calendario(moment(fecha, 'YYYY-MM-DD').format('DD/MM/YYYY'), moment().format('YYYY'),parseInt(moment().format('YYYY'),10) + 10, moment().format('DD/MM/YYYY'));

	});


	//Autenticar maquina votante
	$('#autenticarseButton').click(function(e){
		e.preventDefault();

		var pass = $('#pass').val().trim();

		if(pass == ''){
			alerta.fire({
				icon: 'error',
				title: '¡Debe ingresar la contraseña!'
			});
			return;
		}

		$(this).attr('disabled','disabled');
		$(this).removeClass('btn btn-primary');
		$(this).val('');
		$(this).addClass('spinner-border text-primary');
		$(this).attr('style','background:transparent!important');

		var idEleccion = $('#idEleccion').val().trim();
		var usuario = $('#usuario').val().trim();
		var captcha = $('#captcha').val();


		$.post(SERVERURL + '/elecciones/autenticarse', {idEleccion : idEleccion, usuario : usuario, pass : pass, captcha: captcha}, function(res){
			console.log(res);
			if(res === 'ip_existente'){
				$('#loginErrorAlert').text('La IP de esta máquina ya está en uso.');
				$('#loginErrorAlert').removeClass('d-none');
				$('#autenticarseButton').removeAttr('disabled');
				$('#autenticarseButton').removeClass('spinner-border text-primary');
				$('#autenticarseButton').addClass('btn btn-primary');
				$('#autenticarseButton').val('Ingresar');
				$('#recargarCaptcha').trigger('click');
				$('#captcha').val('');
				$('#autenticarseButton').removeAttr('style');
			}else if(res == 'captcha_incorrecto'){
				$('#loginErrorAlert').text('¡Imágen de seguridad Incorrecta!');
				$('#loginErrorAlert').removeClass('d-none');
				$('#autenticarseButton').removeAttr('disabled');
				$('#autenticarseButton').removeClass('spinner-border text-primary');
				$('#autenticarseButton').addClass('btn btn-primary');
				$('#autenticarseButton').val('Ingresar');
				$('#recargarCaptcha').trigger('click');
				$('#captcha').val('');
				$('#autenticarseButton').removeAttr('style');
			}else if(res === 'true'){
				window.location.href = SERVERURL + '/elecciones/esperar/' + idEleccion + '/';
			}else{
				$('#loginErrorAlert').text('¡Datos incorrectos!');
				$('#loginErrorAlert').removeClass('d-none');
				$('#autenticarseButton').removeAttr('disabled');
				$('#autenticarseButton').removeClass('spinner-border text-primary');
				$('#autenticarseButton').addClass('btn btn-primary');
				$('#autenticarseButton').val('Ingresar');
				$('#recargarCaptcha').trigger('click');
				$('#captcha').val('');
				$('#autenticarseButton').removeAttr('style');
			}
		})

	});




	//Activar maquina votante
	$(document).on('click', '.activarMaquinaVotante', function(){

		var ip = $(this).parent().find('.maquina-votante-ip').text().trim();
		var cedula = '';
		var idEleccion = $('#idEleccion').val().trim();
		var boton = $(this);
		var idMaquina = $(this).attr('id');

		alerta.fire({
			icon: 'info',
			title: 'Activar máquina de votación<br>IP: ' + ip,
			html: `
				<div class="form-group mt-2" style="z-index:9999">
					<span class="form-label">Buscar por cédula o nombre y apellido</span>
					<input id="dato" class="form-control" type="text" autocomplete="off" oninput="validarBusquedaNombreCedula(this)" />
					<div id="datos-ajax"></div>
				</div>
				<div id="votanteTable"></div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Activar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					$('.cancelarVotanteTh').remove();
					$('.cancelarVotante').parent().remove();
					$(`.maquinaVotanteId_${idMaquina}`).addClass('disabled');
					$(`.maquinaVotanteId_${idMaquina}`).attr('disabled', 'disabled');
					var id = $(boton).attr('id');
					
					$.post(SERVERURL + '/elecciones/activarMaquinaVotante', {idEleccion : idEleccion, id : id, cedula : cedula, ip: ip}, function(res){
						if(res == 'ok'){
							$(`.maquinaVotanteId_${idMaquina}`).addClass('disabled');
							$(`.maquinaVotanteId_${idMaquina}`).attr('disabled', 'disabled');
							Swal.close();
						}else{
							alerta.fire({
								icon: 'error',
								title: 'No se pudo activar la maquina de votación'
							})
						}
					})

				});
			}
		})

		$('.swal2-confirm').attr('disabled', 'disabled');
		$('.swal2-confirm').addClass('disabled');
		$('.swal2-actions').css('z-index', '0');

		var AJAX = [];
		$('#dato').on('keyup', function(){

			AJAX.forEach((a, i)=>{
	 			a.abort();
	 			PERSONAS.splice(i, 1);
 			})

 			var dato = $(this).val().trim();
 			if(dato == ''){
				$('#datos-ajax').html('');
				return;
			}else if(!isNaN(dato) && (dato < 1000000 || dato > 99999999)){
				return;
			}

			$('.spinner-input-ajax').remove();
			$('#dato').parent().append(`<div class="spinner-border text-primary spinner-input-ajax"></div>`);

			AJAX.push($.post(SERVERURL + '/personas/getAjax', {dato : dato, id_eleccion : idEleccion}, function(res){


				$('.spinner-input-ajax').remove();
				$('.swal2-actions').css('z-index','0');

				res = JSON.parse(res);
				$('#datos-ajax').html('');

				if(!res || res == 'false'){
					$('#datos-ajax').prepend(`<div class="d-block text-danger">¡Ha ocurrido un error!</div>`);
					return;
				}

				res.forEach((r)=>{
					$('#datos-ajax').prepend(`<div class="persona personaVotante" ci="${r.cedula}" nombres="${r.pnombre} ${r.snombre}" apellidos="${r.papellido} ${r.sapellido}">${r.pnombre} ${r.snombre} ${r.papellido} ${r.sapellido} (${r.cedula})</div>`);
				})

				$('.personaVotante').click(function(){

					cedula = $(this).attr('ci');
					var nombres = $(this).attr('nombres');
					var apellidos = $(this).attr('apellidos');

					$('#datos-ajax').html('');
					$('.form-group').addClass('d-none');

					$('#votanteTable').html(`

						<table class="table table-stripped">
							<th>Cédula</th>
							<th>Nombres y Apellidos</th>
							<th class="cancelarVotanteTh">Cancelar</th>
							<tr>
								<td>${cedula}</td>
								<td>${nombres} ${apellidos}</td>
								<td>
									<img class="borrarImg cancelarVotante" src="${SERVERURL}/views/img/borrar.svg" height=25 width=25 />
								</td>
							</tr>
						</table>

					`);

					$('.swal2-confirm').removeAttr('disabled');
					$('.swal2-confirm').removeClass('disabled');

					$('.cancelarVotante').click(function(){
						cedula = '';

						$('.swal2-confirm').attr('disabled', 'disabled');
						$('.swal2-confirm').addClass('disabled');
						$('#votanteTable').html('');
						$('.form-group').removeClass('d-none');


					});

				});
				

			}));

		})

	});


	//Quitar maquina votante
	$(document).on('click', '.quitarMaquinaVotante', function(){

		var boton = $(this);

		var ip = $(this).parent().find('.maquina-votante-ip').text().trim();
		var idMaquina = $(this).attr('id');

		alerta.fire({
			icon: 'question',
			title: '¿Quitar máquina de votación IP: ' + ip + '?',
			showCancelButton: true,
			confirmButtonText: 'Quitar'
		})
		.then((res)=>{
			if(res.value){
				$(boton).addClass('disabled');
				$(boton).attr('disabled', 'disabled');
				$(`.maquinaVotanteId_${idMaquina}`).addClass('disabled');
				$(`.maquinaVotanteId_${idMaquina}`).attr('disabled', 'disabled');
				
				var id = $(this).attr('id');
				var idEleccion = $('#idEleccion').val().trim();

				$(`#maquinaDeVotacion_${id}`).find('.maquinaVotanteConfiguracion').remove();

				$.post(SERVERURL + '/elecciones/quitarMaquinaVotante', {idEleccion : idEleccion, id : id}, function(res){
					if(res == false || res == 0){
						alerta.fire({
							icon: 'error',
							title: '¡No se pudo quitar la máquina de votación!'
						});
					}
				})
			}
		});

	});


	//Finalizar votaciones
	$('.finalizarVotaciones').click(function(){

		alerta.fire({
			icon: 'question',
			title: '¿Estás seguro de finalizar las votaciones?',
			html: `
				<div class="form-group w-75 mx-auto mt-2">
					<span class="form-label">Ingresa tu Contraseña...</span>
					<input id="claveUsuario" class="form-control" type="password" autocomplete="new-password" autofocus/>
				</div>
			`,
			showCancelButton: true,
			showLoaderOnConfirm: true,
			confirmButtonText: 'Finalizar',
			preConfirm: ()=>{
				return new Promise(function(resolve){

					var idEleccion = $('#idEleccion').val().trim();
					var clave = $('#claveUsuario').val();

					if(clave == ''){
						alerta.fire({
							icon: 'error',
							title: '¡Debes ingresar tu contraseña de usuario para confirmar!'
						}).then(()=>{
							$('.finalizarVotaciones').trigger('click');
						});
						return;
					}

					$.post(SERVERURL + '/elecciones/finalizar', {idEleccion : idEleccion, clave: clave}, function(res){
						var errorMsj = '¡Error al finalizar votaciones!';
						if(res == 'clave_incorrecta'){
							errorMsj = '¡Contraseña de usuario incorrecta!';
						}
						ajaxRespuesta(res, '¡Votaciones Finalizadas!', errorMsj);

					})

				})
			}
		})

	});


	//Cambiar contraseña de votación
	$('.cambiarPassVotacion').click(function(){

		var id = $(this).attr('id').trim();

		alerta.fire({
			title: 'Elegir nueva contraseña de elección',
			html: `
				<div id="alerta-error" class="alert alert-danger w-100 text-center rebote d-none">¡Las contraseñas no son iguales!</div>
				<br>
				<div class="form-group w-75 mx-auto">
					<span class="form-label">Contraseña...</span>
					<input id="pass" class="form-control" type="password" autocomplete="new-password" autofocus/>
				</div>
				<div class="form-group w-75 mx-auto">
					<span class="form-label">Verificar Contraseña...</span>
					<input id="pass2" class="form-control mt-3" type="password" autocomplete="new-password" autofocus/>
				</div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					$('#alerta-error').addClass('d-none');

					var pass = $('#pass').val();
					var pass2 = $('#pass2').val();

					if(pass == ''){
						$('#alerta-error').text('¡La contraseña no puede estar vacía!');
						$('#alerta-error').removeClass('d-none');
						Swal.disableLoading();
						return;
					}

					if(pass != pass2){
						$('#alerta-error').text('¡Las contraseñas no son iguales!');
						$('#alerta-error').removeClass('d-none');
						Swal.disableLoading();
						return;
					}

					$('#alerta-error').addClass('d-none');
					$.post(SERVERURL + '/elecciones/cambiarPassVotacion', {id: id, pass : pass}, function(res){

						ajaxRespuesta(res, '¡Contraseña de elección actualizada!', '¡Error al cambiar contraseña de elección!', false);
								
					});


				});
			}
		});

	});


	//Busqueda de personas por AJAX para filtrar la tabla de personas
	var PERSONASAJAX=[];
	var LISTAPERSONASAJAX=[];
	var buscarPersonaAjaxTimeout;
	$(document).on('keyup', '#buscarPersonaAjax' ,function(){
		//validarBusquedaNombreCedula(this);
		clearTimeout(buscarPersonaAjaxTimeout);
		buscarPersonaAjaxTimeout = setTimeout(()=>{

			PERSONASAJAX.forEach((a, i)=>{
	 			a.abort();
	 			PERSONASAJAX.splice(i, 1);
	 		})

	 		var dato = $(this).val();
	 		dato = dato.trim();
			if(dato == ''){
				$('#tablaDatosAjaxContenedor').html('');
				$('#tablaDatos').removeClass('d-none');
				$('.paginacion').removeClass('d-none');
				$('#paginacionAjax').removeClass('d-flex');
				$('#paginacionAjax').addClass('d-none');
				return;
			}else if(!isNaN(dato) && (dato < 1000000)){
				return;
			}

			var tabla = `
				<table id="tablaDatosAjax" class="table table-hover table-stripped mt-2">
					<th>Cédula</th>
					<th>P. Nombre</th>
					<th>S. Nombre</th>
					<th>P. Apellido</th>
					<th>S. Apellido</th>
					<th>Género</th>
					<th>Edad</th>
					<th>Consejo comunal</th>
					<th id="thEditarAjax">Editar</th>
					<th id="thBorrarAjax">Borrar</th>
			`;

			$('.spinner-input-ajax').remove();
			$('#buscarPersonaAjax').parent().append(`<div class="spinner-border text-primary spinner-input-ajax"></div>`);

			var filtrarConsejo = $('#filtrarConsejo').val();
			var paginaActual = $('#paginacionAjax').attr('paginaActual');
			$('#paginacionAjax').attr('paginaActual', '1');

			PERSONASAJAX.push($.post(SERVERURL + '/personas/getAjaxPaginar', {dato : dato, filtrarConsejo: filtrarConsejo, paginaActual: paginaActual}, function(res){

				$('.spinner-input-ajax').remove();
				$('.swal2-actions').css('z-index','0');

				try{
					res = JSON.parse(res);
				}catch(err){
					$('#listaPersonas').prepend(`<div class="d-block text-danger">¡Ha ocurrido un error!</div>`);
					return;
				}

				$('#tablaDatosAjaxContenedor').html('');
				$('#tablaDatos').addClass('d-none');
				$('.paginacion').addClass('d-none');
				$('#paginacionAjax').removeClass('d-none');
				$('#paginacionAjax').addClass('d-flex');

				if(typeof res.errorInfo == 'object'){
					$('#listaPersonas').prepend(`<div class="d-block text-danger">¡Ha ocurrido un error!</div>`);
					return;
				}

				res.personas.forEach((r)=>{
					if(!LISTAPERSONASAJAX.includes(`${r.cedula}`)){
						if(r.cod_consejocomunal == null){
							r.cod_consejocomunal = '';
							r.nombre = '';
						}
						tabla += `
							<tr>
								<td class="cedulaPersona">${r.cedula}</td>
								<td class="pnombrePersona">${r.pnombre}</td>
								<td class="snombrePersona">${r.snombre}</td>
								<td class="papellidoPersona">${r.papellido}</td>
								<td class="sapellidoPersona">${r.sapellido}</td>
								<td class="sexoPersona">${r.sexo == 'F' ? 'Femenino' : 'Masculino'}</td>
								<td class="edadPersona">
									<span id="link" data-toggle="tooltip" data-placement="bottom" title="${r.fechanacimiento}" class="edad">${r.edad}</span>
									<input id="fecha_nacimiento" type="hidden" value="${r.fecha_nacimiento}" />
								</td>
								<td class="consejocomunalPersona">
									${r.nombre}
									<input class="codConsejocomunalPersona" type="hidden" value="${r.cod_consejocomunal}">
								</td>
								<td>
									<img class="editarImg editarPersona" src="${SERVERURL}/views/img/editar.svg">
								</td>
								<td>
									<img class="borrarImg borrarPersona" src="${SERVERURL}/views/img/borrar.svg">
								</td>
							</tr>
						`
					}
				});

				if(res.personas.length < 1){
					tabla += `
						<tr>
							<td colspan="10">No se encontraron resultados</td>
						</tr>
					`;
				}

				tabla += `<table>`;

				if(res.totalPaginas > 1){
					$('#paginacionAjax').html(paginacion(res.paginaInicio, res.totalPaginas, 'elecciones', res.paginaActual));
					$('.paginacion-item-ajax').click(function(){
						if($(this).attr('disabled') == 'disabled'){
							return;
						}
						$('.paginacion-item-ajax').attr('disabled', 'disabled');
						$('.paginacion-item-ajax').addClass('disabled');
						var pagina = $(this).attr('href');
						$('#paginacionAjax').attr('paginaActual', pagina);
						$('#buscarPersonaAjax').trigger('keyup');
						scrollTo(0, 0);
					});
				}

				$('#tablaDatosAjaxContenedor').html(tabla);
				onEditarPersona();
				onBorrarPersona();
				$('[data-toggle="tooltip"]').tooltip();

				if($('#tipoUsuario').val() == '3'){
					$('.editarImg, .borrarImg').parent().addClass('d-none');
					$('#thEditarAjax, #thBorrarAjax').addClass('d-none');
				}

			}));

			}, 350)

	});

	$(document).on('click', '.importarExcel', function(){
		alerta.fire({
			title: 'Importar Excel',
			html: `
				<input id="archivo" type="file" accept=".xls,.xlsx,.ods" />
				<div class="d-block mt-1">
					<input id="actualizarDuplicados" type="checkbox" value="1" checked />
					<label for="actualizarDuplicados">Sobreescribir existentes</label>
				</div>
				<b class="mt-3 d-block">El archivo debe estar de la siguiente manera:</b>
				<table class="table-excel table-responsive mt-2">
					<tr>
						<th>cedula</th>
						<th>pnombre</th>
						<th>snombre</th>
						<th>papellido</th>
						<th>sapellido</th>
						<th>genero</th>
						<th>fecha nacimiento</th>
						<th>Cód. Comuna</th>
					</tr>
					<tr>
						<td>12345678</td>
						<td>Juan</td>
						<td>Andres</td>
						<td>Perez</td>
						<td>Gomez</td>
						<td>Masculino</td>
						<td>23/04/1997</td>
						<td>12345</td>
					</tr>
					<tr>
						<td>...</td>
						<td>...</td>
						<td>...</td>
						<td>...</td>
						<td>...</td>
						<td>...</td>
						<td>...</td>
						<td>...</td>
					</tr>
				</table>
				<p class="mt-1 text-secondary" style="font-size:12px">Puede tardar varios minutos dependiendo de la cantidad de personas</p>
			`,
			showCancelButton: true,
			confirmButtonText: 'Importar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{

				return new Promise(function(resolve){

					var fd = new FormData();
			    var archivo = $('#archivo')[0].files[0];
			    var actualizarDuplicados = "1";
			    if(!$('#actualizarDuplicados').is(':checked')){
			    	actualizarDuplicados = "0";
			    }
			    if(!archivo){
			    	alerta.fire({
			    		title: '¡No seleccionó el archivo!',
			    		icon: 'error'
			    	}).then(()=>$('.importarExcel').trigger('click'))
			    	return;
			    }
			    fd.append('archivo', archivo);
			    fd.append('actualizarDuplicados', actualizarDuplicados);

					$.ajax({

							url: SERVERURL + '/personas/importarExcel',
							data: fd,
							timeout: 0,
							processData: false,
	  						contentType: false,
	  						type: 'POST',
	  						success: function(res){

								ajaxRespuesta(res, '¡Excel Importado!', '¡No se pudo importar el Excel!', true)

							}

					});

				});

			}
		})
	});


	$(document).on('click', '.eliminarEleccion', function(){
		var id = $(this).attr('idEleccion');
		var nombre = $(this).attr('nombre');

		alerta.fire({
			icon: 'question',
			title: '¿Eliminar Elección?',
			html: `<span>${nombre}</span>`,
			showCancelButton: true,
			confirmButtonText: 'Eliminar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{

				return new Promise(function(resolve){

					$.post(SERVERURL + '/elecciones/eliminar', {id: id}, function(res){
						var errorMsj = '¡No se pudo eliminar la Elección!';
						if(res == 'en_curso'){
							errorMsj = '¡No se puede eliminar una elección en curso!';
						}else if(res == 'no_permitido_finalizado'){
							errroMsj = '¡No puedes eliminar una elección que ha finalizado!';
						}
						ajaxRespuesta(res, '¡Elección Eliminada!', errorMsj, true);

					});

				});

			}
		})
		
	});

	//Borrar todas las personas
	$(document).on('click', '.borrarTodoPersonas', function(){
		var cod = $('#filtrarConsejo').val();
		var nomConsejo = $('#nombreConsejo').val();
		alerta.fire({
			icon: 'question',
			title: '¿Borrar a todas las Personas?',
			text: nomConsejo,
			showCancelButton: true,
			confirmButtonText: 'Borrar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){

					$.post(SERVERURL + '/personas/borrarTodos', {cod: cod}, function(res){
						ajaxRespuesta(res, '¡Todas las personas han sido borradas!', '¡No se pudieron borrar todas las personas!', true);
					})

				})
			}
		})
	});



	$('#filtrarPorConsejo').on('change', function(){
		var cod = $(this).val();
		if(cod != ''){
			window.location.href = SERVERURL + '/personas?consejo=' + cod;
		}else{
			window.location.href = SERVERURL + '/personas/';
		}
	});


	// Editar Usuario
	function onEditarUsuario(){
		$('.editarUsuario').click(function(){
			var id = $(this).parent().attr('idUsuario');
			var nombreUsuario = $(this).parent().attr('nombreUsuario');
			var tipoUsuario = $(this).parent().attr('tipoUsuario');
			var tUsuario = $('#tipoUsuario').val();

			var html = `
					<h5 class="text-secondary mb-3">${nombreUsuario}</h5>
					<div class="form-group">
						<span class="form-label">Nombre de Usuario...</span>
						<input id="nombreusuario" class="form-control" type="text" oninput="validarLongitudMaxima(this, 20); validarAlfanumerico(this)" value="${nombreUsuario}" />
					</div>
				`;

			if(id != '1'){
				html += `
					<div class="form-group ${tUsuario != '1' ? 'd-none' : ''}">
						<span class="form-label">Tipo de Usuario...</span>
						<select id="tipoUsuarioInput" class="form-control">
								<option disabled value="" selected hidden></option>
								<option value="1">Administrador de Comuna</option>
								<option value="2">Administrador de Consejo Comunal</option>
								<option value="3">Administrador de Elección</option>
						</select>
					</div>
				`;
			}

			html += `
				<button id="cambiarClaveUsuario" idUsuario="${id}" nombreUsuario="${nombreUsuario}" class="btn btn-secondary">Cambiar Contraseña</button>
			`;
			
			alerta.fire({
				title: 'Editar Usuario',
				html: html,
				showCancelButton: true,
				confirmButtonText: 'Guardar',
				showLoaderOnConfirm: true,
				preConfirm: () => {
					return new Promise(function(resolve){
						var cedula = $('#tablaPersonasUsuario').find('#ciUsuario').text().trim();
						$('#quitarPersonaUsuarioTh').remove();
						$('#quitarPersonaUsuarioTd').remove();
						$('#cambiarClaveUsuario').remove();
						$('input, select').attr('disabled', 'true');

						nombreUsuario = $('#nombreusuario').val().trim();
						tipoUsuario = $('#tipoUsuarioInput').val();

						$.post(SERVERURL + '/usuarios/modificar', {id: id, usuario: nombreUsuario, tipo: tipoUsuario}, function(res){
							var errMsj = '¡No se pudo modificar el usuario!';
							ajaxRespuesta(res, '¡Usuario Modificado!', errMsj, true);
						});
					});
				}
			});

			$('#tipoUsuarioInput').val(tipoUsuario);
			focusedVerificar();

		});
	}

	onEditarUsuario();

		//Busqueda de personas por AJAX para relacionarlo con usuario
	var PERSONASUSUARIO=[];
	var buscarPersonaUsuarioTimeout;
	$(document).on('keyup', '#buscarPersonaUsuario' ,function(){

		var self = this;
		clearTimeout(buscarPersonaUsuarioTimeout);
		buscarPersonaUsuarioTimeout = setTimeout(function(){

			PERSONASUSUARIO.forEach((a, i)=>{
	 			a.abort();
	 			PERSONASUSUARIO.splice(i, 1);
	 		})

	 		var dato = $(self).val();
			if(dato == ''){
				$('#listaPersonas').html('');
				return;
			}else if(!isNaN(dato) && (dato < 1000000)){
				return;
			}

			$('.spinner-input-ajax').remove();
			$('#buscarPersonaUsuario').parent().append(`<div class="spinner-border text-primary spinner-input-ajax"></div>`);

			PERSONASUSUARIO.push($.post(SERVERURL + '/personas/getSinUsuarioAjax', {dato : dato}, function(res){

				$('.spinner-input-ajax').remove();
				$('.swal2-actions').css('z-index','0');

				res = JSON.parse(res);

				$('#listaPersonas').html('');

				if(typeof res.errorInfo == 'object'){
					$('#listaPersonas').prepend(`<div class="d-block text-danger">¡Ha ocurrido un error!</div>`);
					return;
				}

				res.forEach((r)=>{
					$('#listaPersonas').prepend(`<div class="persona personaListaUsuarioAjax" ci="${r.cedula}" nombres="${r.pnombre} ${r.snombre}" apellidos="${r.papellido} ${r.sapellido}">${r.pnombre} ${r.snombre} ${r.papellido} ${r.sapellido} (${r.cedula})</div>`);
				});

			}));

		},350)

	});

	//Agregar persona a la lista para relacionarlo con usuario
	$(document).on('click', '.personaListaUsuarioAjax', function(){

		$('#listaPersonas').html('');
		$('#buscarPersonaUsuario').val('');

		var cedula = $(this).attr('ci');
		var nombres = $(this).attr('nombres');
		var apellidos = $(this).attr('apellidos');

		$('#tablaPersonasUsuario').removeClass('d-none');
		$('#inputBusquedaPersonaUsuario').addClass('d-none');

		$('#tablaPersonasUsuario').append(`<tr><td id="ciUsuario">${cedula}</td><td>${nombres}</td><td>${apellidos}</td><td id="quitarPersonaUsuarioTd"><img class="borrarImg quitarPersonaListaUsuario" src="${SERVERURL}/views/img/borrar.svg" /></td></tr>`);

	});

	//Quitar persona de la lista de usuario
	$(document).on('click', '.quitarPersonaListaUsuario', function(){
		$(this).parent().parent().remove();
		$('#tablaPersonasUsuario').addClass('d-none');
		$('#inputBusquedaPersonaUsuario').removeClass('d-none');

	});

	// Cambiar contraseña a usuario
	$(document).on('click', '#cambiarClaveUsuario', function(){
		var id = $(this).attr('idUsuario');
		var nombreUsuario = $(this).attr('nombreUsuario');
		var perfil = $(this).attr('perfil');
		var errorMsj = '';

		alerta.fire({
			title: 'Cambiar Contraseña',
			html: `
				<h5 class="mb-3 text-secondary">${nombreUsuario}</h5>
				<div id="alertError" class="alert alert-danger rebote w-full p-2 m-2 mb-3 d-none">${errorMsj}</div>
				<div id="clavePerfil" class="form-group d-none">
					<span class="form-label">Contraseña Actual</span>
					<input id="claveActual" class="form-control" type="password" autocomplete="new-password" />
				</div>
				<div class="form-group">
					<span class="form-label">Nueva Contraseña</span>
					<input id="cambiarClave" class="form-control" type="password" autocomplete="new-password" />
				</div>
				<div class="form-group">
					<span class="form-label">Repetir Contraseña</span>
					<input id="confirmarCambiarClave" class="form-control" type="password" autocomplete="new-password" />
				</div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			preConfirm: ()=>{
				return new Promise(function(resolve){
					var claveActual = $('#claveActual').val();
					var clave = $('#cambiarClave').val();
					var confirmarClave = $('#confirmarCambiarClave').val();

					if(perfil == 'si' && claveActual == ''){
						$('#alertError').text('¡La contraseña actual no puede estar vacía!');
						$('#alertError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}

					if(clave == ''){
						$('#alertError').text('¡La contraseña no puede estar vacía!');
						$('#alertError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}

					if(clave != confirmarClave){
						$('#alertError').text('¡Las contraseñas no son iguales!');
						$('#alertError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}

					$('#alertError').addClass('d-none');

					if(perfil != 'si'){
						claveActual = '';
					}

					$.post(SERVERURL + '/usuarios/cambiarClave', {id: id, clave: clave, claveActual: claveActual}, function(res){

						var errMsj = '¡No se pudo cambiar la contraseña!';
						if(res == 'clave_actual_no'){
							errMsj = '¡Contraseña actual incorrecta!';
						}

						ajaxRespuesta(res, '¡Contraseña Cambiada!', errMsj, false);

					});

				});

			}
		});

		if(perfil == 'si'){
			$('#clavePerfil').removeClass('d-none');
		}
	});


	$('#agregarUsuario').click(function(){
		var tipoUsuario = $('#tipoUsuario').val();
		var errorMsj = '';
		alerta.fire({
			title: 'Agregar Usuario',
			html: `
				<div id="alertError" class="alert alert-danger rebote w-full p-2 m-2 mb-3 d-none">${errorMsj}</div>
				<div class="form-group mt-2">
					<span class="form-label">Nombre de Usuario...</span>
					<input id="nombreusuario" class="form-control" type="text" oninput="validarLongitudMaxima(this, 20); validarAlfanumerico(this)" />
				</div>
				<div class="form-group ${tipoUsuario != '1' ? 'd-none' : ''}">
					<span class="form-label">Tipo de Usuario...</span>
					<select id="tipoUsuarioSelect" class="form-control">
							<option disabled value="" ${tipoUsuario != '1' ? '' : 'selected'} hidden></option>
							<option value="1">Administrador de Comuna</option>
							<option value="2">Administrador de Consejo Comunal</option>
							<option value="3" ${tipoUsuario != '1' ? 'selected' : ''}>Administrador de Elección</option>
					</select>
				</div>
				<div class="form-group">
					<span class="form-label">Contraseña</span>
					<input id="clave" class="form-control" type="password" autocomplete="new-password" />
				</div>
				<div class="form-group">
					<span class="form-label">Verificar Contraseña</span>
					<input id="verificarClave" class="form-control" type="password" autocomplete="new-password" />
				</div>
				<div id="inputBusquedaPersonaUsuario" class="form-group">
					<span class="form-label">Persona (Buscar por cédula o nombre y apellido)</span>
					<input id="buscarPersonaUsuario" class="form-control" type="text" autocomplete="new-password" maxlength="30" oninput="validarBusquedaNombreCedula(this)" />
					<div id="listaPersonas">
					</div>
				</div>
				<table id="tablaPersonasUsuario" class="table table-hover table-stripped d-none">
					<th>Cédula</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th id="quitarPersonaUsuarioTh">Cancelar</th>
				</table>
			`,
			showCancelButton: true,
			showLoaderOnConfirm: true,
			confirmButtonText: 'Guardar',
			preConfirm: () => {
				return new Promise(function(resolve){
					var cedula = $('#tablaPersonasUsuario').find('#ciUsuario').text().trim();
					var nombreUsuario = $('#nombreusuario').val().trim();
					var tipoUsuario = $('#tipoUsuarioSelect').val();
					var clave = $('#clave').val();
					var confirmarClave = $('#verificarClave').val();

					if(nombreUsuario == ''){
						$('#alertError').text('¡El nombre de usuario no puede estar vacío!');
						$('#alertError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}
					if(tipoUsuario == '' || tipoUsuario == null){
						$('#alertError').text('¡Debe seleccionar el tipo de Usuario!');
						$('#alertError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}
					if(clave == ''){
						$('#alertError').text('¡La contraseña no puede estar vacía!');
						$('#alertError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}
					if(clave != confirmarClave){
						$('#alertError').text('¡Las contraseñas no son iguales!');
						$('#alertError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}
					if(cedula == ''){
						$('#alertError').text('¡Debe elegir la persona que tendrá el usuario!');
						$('#alertError').removeClass('d-none');
						Swal.disableLoading();
						return;
					}

					$('#quitarPersonaUsuarioTh').remove();
					$('#quitarPersonaUsuarioTd').remove();
					$('input, select').attr('disabled', 'true');

					$.post(SERVERURL + '/usuarios/agregar', {cedula: cedula, usuario: nombreUsuario, clave: clave, tipo: tipoUsuario}, function(res){
						var errMsj = '¡No se pudo agregar el usuario!';
						if(res == 'ya_existe'){
							errMsj = 'Ese nombre de usuario ya Existe!';
						}
						ajaxRespuesta(res, '¡Usuario Agregado!', errMsj, true);
					});

				});
			}
		});
	});


	$('#botonUsuario').click(function(){
		var nombre = $(this).attr('nombre');
		var id = $(this).attr('idUsuario');
		alerta.fire({
			title: 'Usuario',
			html: `
				<h4 class="mb-3 text-secondary">${nombre}</h4>
				<div class="${id != '1' ? 'd-none' : ''}" idUsuario="${id}" nombreUsuario="${nombre}" tipoUsuario="1">
						<button class="btn btn-secondary editarUsuario" style="width: 175px">Modificar</button>
				</div>
				<button id="cambiarClaveUsuario" perfil="si" idUsuario="${id}" nombreUsuario="${nombre}" class="btn btn-secondary ${id == '1' ? 'd-none' : ''}" style="width: 175px">Cambiar Contraseña</button>
				<a href="${SERVERURL}/logout/" class="btn btn-danger d-block mx-auto mt-2 cursor-pointer text-white" style="cursor: pointer; width: 175px">Cerrar Sesión</a>
			`,
			confirmButtonText: 'Cerrar'
		});
		if(id == '1'){
			onEditarUsuario();
		}
	});

	function onBorrarUsuario(){
		$('.borrarUsuario').click(function(){
			var id = $(this).attr('idUsuario');
			var nombre = $(this).attr('nombreUsuario');
			alerta.fire({
				icon: 'question',
				title: '¿Borrar Usuario?',
				html: `
					<h5 class="mb-3">${nombre}</h5>
				`,
				confirmButtonText: 'Borrar',
				showLoaderOnConfirm: true,
				showCancelButton: true,
				preConfirm: () => {
					return new Promise(function(resolve){
						$.post(SERVERURL + '/usuarios/borrar', {id: id}, function(res){
							ajaxRespuesta(res, '¡Usuario Borrado!', '¡No se pudo borrar el usuario!', true);
						});
					});
				}
			});
		});
	}
	onBorrarUsuario();

	$('#recargarCaptcha').click(function(){
		if($(this).attr('disabled') == 'disabled'){
			return;
		}
		$(this).attr('disabled', 'disabled');
		$(this).addClass('disabled');
		var captchaImage = $('#imgCaptcha').attr('src');
		captchaImage = captchaImage.substring(0,captchaImage.lastIndexOf("?"));
		captchaImage = captchaImage+"?rand="+Math.random()*1000;
		$('#imgCaptcha').attr('src', captchaImage);
	});

	$('#imgCaptcha').on('load', function(){
		$('#recargarCaptcha').removeAttr('disabled');
		$('#recargarCaptcha').removeClass('disabled');
	});



	//Busqueda de usuarios por AJAX para filtrar la tabla de usuarios
	var USUARIOSAJAX=[];
	var LISTAUSUARIOSAJAX=[];
	var buscarUsuarioAjaxTimeout;
	$('#buscarUsuarioAjax').on('keyup', function(){
		clearTimeout(buscarUsuarioAjaxTimeout);
		buscarUsuarioAjaxTimeout = setTimeout(()=>{

			USUARIOSAJAX.forEach((a, i)=>{
	 			a.abort();
	 			USUARIOSAJAX.splice(i, 1);
	 		})

	 		var dato = $(this).val();
	 		dato = dato.trim();
			if(dato == ''){
				$('#tablaDatosAjaxContenedor').html('');
				$('#tablaDatos').removeClass('d-none');
				$('.paginacion').removeClass('d-none');
				$('#paginacionAjax').removeClass('d-flex');
				$('#paginacionAjax').addClass('d-none');
				return;
			}else if(!isNaN(dato) && (dato < 1000000)){
				return;
			}

			var tabla = `
				<table id="tablaDatosAjax" class="table table-hover table-stripped mt-2">
					<th>Cédula</th>
					<th>Nombre y Apellido</th>
					<th>Consejo Comunal</th>
					<th>Nombre de Usuario</th>
					<th>Tipo de Usuario</th>
					<th id="thEditarAjax">Editar</th>
					<th id="thBorrarAjax">Borrar</th>
			`;

			$('.spinner-input-ajax').remove();
			$('#buscarPersonaAjax').parent().append(`<div class="spinner-border text-primary spinner-input-ajax"></div>`);

			var filtrarConsejo = $('#filtrarPorConsejoUsuarios').val();

			USUARIOSAJAX.push($.post(SERVERURL + '/usuarios/getAjax', {dato : dato, filtrarConsejo: filtrarConsejo}, function(res){

				$('.spinner-input-ajax').remove();
				$('.swal2-actions').css('z-index','0');

				res = JSON.parse(res);

				$('#tablaDatosAjaxContenedor').html('');
				$('#tablaDatos').addClass('d-none');
				$('.paginacion').addClass('d-none');
				$('#paginacionAjax').removeClass('d-none');
				$('#paginacionAjax').addClass('d-flex');

				if(typeof res.errorInfo == 'object'){
					$('#listaPersonas').prepend(`<div class="d-block text-danger">¡Ha ocurrido un error!</div>`);
					return;
				}

				res.forEach((r)=>{
					if(!LISTAPERSONASAJAX.includes(`${r.cedula}`)){
						if(r.cod_consejocomunal == null){
							r.cod_consejocomunal = '';
							r.nombre = '';
						}
						tabla += `
							<tr>
								<td class="cedulaPersona">${r.cedula}</td>
								<td>${r.pnombre} ${r.papellido}</td>
								<td>${r.nombre_consejo}</td>
								<td>${r.nombre_usuario}</td>
								<td>${r.tipo}</td>
								<td idUsuario="${r.id}" nombreUsuario="${r.nombre_usuario}" tipoUsuario="${r.tipo_id}">
									<img class="editarImg editarUsuario" src="${SERVERURL}/views/img/editar.svg">
								</td>
								<td>
									<img idUsuario="${r.id}" nombreUsuario="${r.nombre_usuario}" class="borrarImg borrarUsuario" src="${SERVERURL}/views/img/borrar.svg">
								</td>
							</tr>
						`
					}
				});

				if(res.length < 1){
					tabla += `
						<tr>
							<td colspan="10">No se encontraron resultados</td>
						</tr>
					`;
				}

				tabla += `<table>`;

				$('#tablaDatosAjaxContenedor').html(tabla);
				onEditarUsuario();
				onBorrarUsuario();
				$('[data-toggle="tooltip"]').tooltip();

				if($('#tipoUsuario').val() == '3'){
					$('.editarImg, .borrarImg').parent().addClass('d-none');
					$('#thEditarAjax, #thBorrarAjax').addClass('d-none');
				}

			}));

			}, 350)

	});


	$('#filtrarPorConsejoUsuarios').on('change', function(){
		var cod = $(this).val();
		if(cod != ''){
			window.location.href = SERVERURL + '/usuarios?consejo=' + cod;
		}else{
			window.location.href = SERVERURL + '/usuarios/';
		}
	});


	$('.imprimirElectores').on('click', function(){
		alerta.fire({
			title: 'Imprimir Electores',
			html: `
				<h4>Se generará la lista de personas que tienen permitido votar en este consejo comunal</h4>
				<div class="d-block mt-1">
					<input id="mostrarFirma" type="checkbox" value="1" checked />
					<label for="mostrarFirma">Mostrar Firma</label>
				</div>
			`,
			showCancelButton: true,
			confirmButtonText: 'Aceptar',
			showLoaderOnConfirm: true,
			preConfirm: () => {
				return new Promise(function(resolve){
					var cod = document.getElementById('filtrarConsejo').value;
					$.post(SERVERURL + '/personas/imprimirElectores', {cod: cod}, function(res){
						res = JSON.parse(res);
						var consejo = $('#nombreConsejo').val();
						var mostrarFirma = $('#mostrarFirma').get(0).checked;
						var html = `
							<h1>Lista de Electores</h1>
							<h2>${consejo}</h2>
							<h3>Total: ${res.length}</h3>
							<table class="table mt-3 tableElectores">
								<thead style="font-size: 20px">
									<tr>
										<th>Cédula</th>
										<th>Nombres y Apellidos</th>
										<th class="${mostrarFirma ? '' : 'd-none'}">Firma</th>
									</tr>
								</thead>
								<tbody>
						`;
						res.forEach(r=>{
							html += `
								<tr>
									<td>${r.cedula}</td>
									<td>${r.pnombre} ${r.snombre} ${r.papellido} ${r.sapellido}</td>
									<td class="${mostrarFirma ? '' : 'd-none'}"> </td>
								</tr>
							`;
						});
						html += `
								</tbody>
								</table>`;

						$('#electoresContenedor').html(html);
						Swal.close();
						window.print();
					});
				});
			}
		});
	});


	$('.actividad').click(function(){
		var tipo = $(this).attr('tipo');
		var id = $(this).attr('idTipo');
		var paginaActual = $(this).attr('paginaActual');
		alerta.fire({
			title: 'Registro de Actividad',
			html: `
				<h5>${tipo == 'elecciones' ? 'Elección '+id : 'Consejo Comunal '+id}</h5>
				<br>
				<div id="contenido">
					<div class="spinner-border text-primary"></div><span class="cargandoText text-primary">Cargando...</span>
				</div>
				<br>
			`,
			width: '95%',
			showCloseButton: true,
			confirmButtonText: 'Cerrar'
		});

		$.post(SERVERURL + `/${tipo}/actividad`, {id: id, paginaActual: paginaActual}, function(res){
			try{
				res = JSON.parse(res);
			}catch(err){
				alerta.fire({
					icon: 'error',
					title: 'Ocurrió un error al traer el registro de actividad'
				});
				return;
			}
			var html = `
				<div id="paginacionAjax"></div>
				<div class="overflow-y: auto; max-height: 95%">
				<table class="table" style="box-shadow: none!important;">
						<th>Descripción</th>
						<th>Fecha</th>
			`;
			res.actividad.forEach((r)=>{
				html += `
					<tr>
						<td style="text-align:left!important">${r.descripcion}</td>
						<td>${r.fecha}</td>
					</tr>
				`;
			});
			if(res.actividad.length < 1){
				html += '<tr><td colspan="2">No hay registro de actividad</td></tr>'
			}
			html += '</table></div>';
			$('#contenido').html(html);

			$('#paginacionAjax').html(paginacion(res.paginaInicio, res.totalPaginas, 'elecciones', res.paginaActual));
			$('.paginacion-item-ajax').click(function(){
				var pagina = $(this).text();
				$('.actividad').attr('paginaActual', pagina);
				$('.actividad').trigger('click');
				$('.actividad').attr('paginaActual', '1');
			});

		});

	});


	function paginacion(paginaInicio, totalPaginas, controlador, paginaActual, parametros = ''){
		if(totalPaginas <= 1){
			return;
		}
		var html = `
			<div class="text-center w-100 mb-3 paginacion">

				<h5 class="text-center font-weight-bold" style="color:white;text-shadow: 0px 0px 4px black">Página ${paginaActual} de ${totalPaginas}</h5>`;

					if((paginaInicio-1) % 10 == 0 && paginaInicio >= 10){ 
						paginaInicio -= 1;
						if( (totalPaginas - paginaInicio) < 10 && paginaInicio > 10 ){
							html += `
								<div class="paginacion-item-ajax btn btn-primary" href="1">
									<<<
								</div>
								<div class="paginacion-item-ajax btn btn-primary" href="${(paginaInicio-10)}">
									<<
								</div>
							`;
						}else{
							html += `
								<div class="paginacion-item-ajax btn btn-primary" href="1">
									<<<
								</div>
								<div class="paginacion-item-ajax btn btn-primary" href="${(paginaInicio-1)}">
									<<
								</div>
							`;
						} 
					}else{

						if(paginaInicio > 10){
							html += `
								<div class="paginacion-item-ajax btn btn-primary" href="${paginaInicio}">
									<<<
								</div>
								<div class="paginacion-item-ajax btn btn-primary" href="${(paginaInicio-1)}">
									<<
								</div>
							`;
						}

				}

				if( (totalPaginas - paginaInicio) < 10 && paginaInicio > 10 ){
					for(i = (paginaInicio-9); i <= totalPaginas; i++){
						html += `
							<div class="paginacion-item-ajax btn btn-primary ${paginaActual == i ? 'active' : ''}" href="${i}">
								${i}
							</div>
						`;
					}
				}else{

					var cont = 0;
					for(i = paginaInicio; i <= totalPaginas; i++){

						cont++;
						
						html += `
						<div class="paginacion-item-ajax btn btn-primary ${paginaActual == i ? 'active' : ''}" href="${i}">
						`;
							if(cont > 10){ 
								html += '>>';
							}else{
								html += i;
							}
						html += `
						</div>
						`;

						if(cont > 10){
							html += `
								<div class="paginacion-item-ajax btn btn-primary" href="${totalPaginas}">
									>>>
								</div>
							`;
							break;
						} 


					}

				}

		html += '</div>';

		return html;
	}


});


