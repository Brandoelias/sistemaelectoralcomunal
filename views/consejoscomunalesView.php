<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-4">
		<div class="jumbotron p-2 d-block mx-auto w-50 text-center titleBox mb-4">
			<h2 class="m-0">Consejos Comunales</h2>
		</div>

		<?php foreach($banco as $b){ ?>

			<input class="banco d-none" name="<?php echo $b->nombre_banco ?>" value="<?php echo $b->id ?>" />

		<?php } ?>

		<div style="overflow-x: auto;">
			<table class="table table-hover table-stripped">
				
				<th>Cód. SITUR</th>
				<th>Nombre</th>
				<th>Teléfono</th>
				<th>Rif</th>
				<th>Correo</th>
				<th>Banco</th>
				<th>Número de cuenta</th>
				<!--<th>Habitantes</th>-->
				<th>Unidades</th>
				<!--<th>Actividad</th>
				<th>Editar</th>
				<th>Borrar</th>-->
				<th>Editar</th>
				<th>Borrar</th>

				<?php foreach($consejosComunales as $consejoComunal){ ?>

					<tr>
						<td class="codConsejoComunal">
							<?php echo $consejoComunal->cod ?>
						</td>
						<td class="nombreConsejoComunal">
							<?php echo $consejoComunal->nombre ?>		
						</td>
						<td class="telefonoConsejoComunal">
							<?php $consejoComunal->telefono != '' ? print($consejoComunal->telefono) : print(' - ') ?>	
						</td>
						<td class="rifConsejoComunal">
							<?php $consejoComunal->rif != '' ? print($consejoComunal->rif) : print(' - ') ?>	
						</td>
						<td class="correoConsejoComunal">
							<?php $consejoComunal->correo != '' ? print($consejoComunal->correo) : print(' - ') ?>	
						</td>
						<td class="nombrebancoConsejoComunal">
							<span class="idbancoConsejoComunal d-none"><?php echo $consejoComunal->id_banco ?></span>
							<?php $consejoComunal->id_banco != 0 ? print($consejoComunal->nombre_banco) : print(' - ') ?>	
						</td>
						<td class="numerodecuentaConsejoComunal">
							<?php $consejoComunal->id_banco != 0 ? print($consejoComunal->numerodecuenta) : print(' - ') ?>	
						</td>
						<!--<td>
							<a href="<?php echo SERVERURL . '/personas?consejo=' . $consejoComunal->cod ?>">
								<?php echo $consejoComunal->habitantes ?>
							</a>
						</td>-->
						<td class="unidadesConsejoComunal">
							<?php $consejoComunal->unidades > 0 ? print("<span class=verUnidades>Ver ($consejoComunal->unidades)</span>") : print('<span class=verUnidades>No tiene</span>') ?>
						</td>
						<td>
							<img class="editarImg editarConsejoComunal d-inline-block" src="<?php echo SERVERURL ?>/views/img/editar.svg">
						</td>
						<td>
							<img class="borrarImg borrarConsejoComunal d-inline-block" src="<?php echo SERVERURL ?>/views/img/borrar.svg">
						</td>
					</tr>

				<?php } ?>

				<?php if(count($consejosComunales) < 1){ ?>

					<tr>
						<td colspan="10">
							No hay consejos comunales registrados.
						</td>
					</tr>

				<?php } ?>

			</table>
		</div>

		<button class="btn btn-success d-block mx-auto mt-5 agregarConsejoComunal" type="button">Agregar +</button>

		<a class="btn btn-dark d-block mx-auto mt-3" style="width: 100px;" href="<?php echo SERVERURL ?>/menu/consejoscomunales/">
			Regresar
		</a>

	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>