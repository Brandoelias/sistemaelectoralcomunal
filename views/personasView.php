<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>
<div class="no-print">
	<?php include_once('./views/partials/header.php') ?>

	<input id="tipoUsuario" class="d-none" type="hidden" value="<?php echo $tipoUsuario ?>" />

	<main class="p-4">
		<div class="jumbotron p-2 d-block mx-auto w-50 text-center mb-4 titleBox">
			<h2 class="m-0">Lista de Habitantes</h2>
			<?php if($filtrarConsejo){ ?>
				<input id="filtrarConsejo" class="d-none" type="hidden" value="<?php echo $filtrarConsejo ?>" />
			<?php }else{ ?>
				<input id="filtrarConsejo" class="d-none" type="hidden" value="" />
			<?php } ?>
			<h5 class="m-0">Total: <?php echo $totalPersonas ?></h5>
		</div>

		<?php foreach ($consejoscomunales as $consejocomunal) { ?>
				
			<input class="consejoscomunales" type="hidden" value="<?php echo $consejocomunal->id ?>" />

		<?php } ?>

		

		<?php if($tipoUsuario == 1){ ?>
			<select id="filtrarPorConsejo" class="form-control mx-auto d-block text-center w-25 mb-3 mt-4 pl-1" style="<?php !$filtrarConsejo ? print('color: grey') : '' ?>">
				<option disabled <?php !$filtrarConsejo ? print('selected') : '' ?> hidden>Buscar por Consejo Comunal...</option>
				<?php if($filtrarConsejo){ ?>
					<option value="" style="color: red">Cancelar</option>
				<?php } ?>
				<?php foreach ($consejoscomunales as $consejocomunal) { ?>	
					<option class="text-center" value="<?php echo $consejocomunal->cod ?>" <?php $filtrarConsejo == $consejocomunal->cod ? print('selected') : '' ?>><?php echo $consejocomunal->nombre ?></option>
				<?php } ?>
			</select>
		<?php } ?>

		<?php if(count($personas) > 0){ ?>

			<input id="nombreConsejo" class="d-none" type="hidden" value="<?php echo $personas[0]->nombre ?>" />

			<div class="form-group w-25 mx-auto mt-2 mb-0">
				<span class="form-label">Buscar por Cédula, Nombre o Apellido...</span>
				<input class="mx-auto form-control d-block pr-5" id="buscarPersonaAjax" type="text" oninput="validarBusquedaNombreCedula(this)" />
			</div>
		<?php } ?>

		<div id="tablaDatosAjaxContenedor"></div>


		<table id="tablaDatos" class="table table-hover table-stripped mt-2">
			
			<th>Cédula</th>
			<th>P. Nombre</th>
			<th>S. Nombre</th>
			<th>P. Apellido</th>
			<th>S. Apellido</th>
			<th>Género</th>
			<th>Edad</th>
			<th>Consejo comunal</th>
			<?php if($tipoUsuario == 1 || $tipoUsuario == 2){ ?>
				<th>Editar</th>
				<th>Borrar</th>
			<?php } ?>

			<?php foreach($personas as $persona){ ?>

				<tr>
					<td class="cedulaPersona">
						<?php echo $persona->cedula ?>
					</td>
					<td class="pnombrePersona">
						<?php echo $persona->pnombre ?>		
					</td>
					<td class="snombrePersona">
						<?php echo $persona->snombre ?>	
					</td>
					<td class="papellidoPersona">
						<?php echo $persona->papellido ?>	
					</td>
					<td class="sapellidoPersona">
						<?php echo $persona->sapellido ?>	
					</td>
					<td class="sexoPersona">
						<?php strtolower($persona->sexo) == 'm' ? print('Masculino') : ( strtolower($persona->sexo) == 'f' ? print('Femenino') : print('N/A') ) ?>	
					</td>
					<td class="edadPersona">
						<span id="link" data-toggle="tooltip" data-placement="bottom" title="<?php echo $persona->fechanacimiento ?>" class="edad"><?php echo $persona->edad ?></span>
						<input id="fecha_nacimiento" type="hidden" value="<?php echo $persona->fecha_nacimiento ?>" />
					</td>
					<td class="consejocomunalPersona">
						<?php isset($persona->nombre) ? print($persona->nombre) : print('N/A') ?>
						<input class="codConsejocomunalPersona" type="hidden" value="<?php echo $persona->cod_consejocomunal ?>">
					</td>
					<?php if($tipoUsuario == 1 || $tipoUsuario == 2){ ?>
						<td>
							<img class="editarImg editarPersona" src="<?php echo SERVERURL ?>/views/img/editar.svg">
						</td>
						<td>
							<img class="borrarImg borrarPersona" src="<?php echo SERVERURL ?>/views/img/borrar.svg">
						</td>
					<?php } ?>
				</tr>

			<?php } ?>

			<?php if(count($personas) < 1){ ?>

				<tr>
					<td colspan="10">
						No hay personas registradas.
					</td>
				</tr>

			<?php } ?>

		</table>

		<div id="paginacionAjax" class="mx-auto w-100 d-flex justify-content-center" paginaActual="1"></div>

		<?php $modelo->paginar($paginaActual, $totalPersonas, $limite, 'personas', $filtrarConsejo != '' ? '?consejo=' . $filtrarConsejo : '') ?>

		<?php if($tipoUsuario == 1 || $tipoUsuario == 2){ ?>
			<button class="btn btn-success d-block mx-auto mt-5 w-13 agregarPersona" type="button">Agregar +</button>

			<button class="btn btn-primary d-block mx-auto mt-3 w-13 importarExcel" type="button">Importar Excel</button>

			<?php if($filtrarConsejo && count($personas) > 0){ ?>
				<button class="btn btn-info d-block mx-auto mt-3 w-13 imprimirElectores" type="button">Imprimir Electores</button>
				<button class="btn btn-danger d-block mx-auto mt-3 w-13 borrarTodoPersonas" type="button">Borrar Todos</button>
			<?php } ?>
		<?php } ?>

		<a class="d-block mx-auto mt-3 w-13" href="<?php echo SERVERURL ?>/menu/">
			<button class="btn btn-dark d-block mx-auto w-100" type="button">Regresar</button>
		</a>

	</main>

	<?php include_once('./views/partials/footer.php') ?>
</div>
<div id="electoresContenedor" class="text-center d-none print">
</div>
</body>
</html>
<script>
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();
	});
</script>