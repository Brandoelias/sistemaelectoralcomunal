<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body id="votarbackground" style="height: 100%; user-select: none;">

	<main class="pt-0 pb-2 pt-1" style="height: 100%;background: url('')!important">

		<input id="idEleccion" type="hidden" value="<?php echo $eleccion->id ?>">

		<h2 class="text-center bg-white w-25 mx-auto font-weight-bold" style="border-radius: 4px;box-shadow: 0 0 4px black;">VOTANDO</h2>
		<h2 class="text-center bg-white w-75 mx-auto font-weight-bold" style="border-radius: 4px;box-shadow: 0 0 4px black;font-size: 24px;"><?php echo "$votando->pnombre $votando->snombre $votando->papellido $votando->sapellido ($votando->cedula)" ?></h2>


		<div class="boletaElectoral mt-0">
			<?php foreach($unidades as $unidad){ ?>
				<?php if(count($unidad->comites) > 0) { ?>
					<?php foreach($unidad->comites as $comite){ ?>
						<?php if(count($comite->candidatos) > 0) { ?>
							<div class="tarjetaElectoral">
								<div class="nombreUnidadElectoral font-weight-bold" style="border-bottom: none;">
									<?php echo $unidad->nombre_unidad ?>
								</div>
								<div class="nombreComiteElectoral font-weight-bold">
									<?php echo $comite->nombre_comite ?>
								</div>
								<div class="nombreComiteElectoral text-success">
									<?php $comite->votos_maximos > 1 ? print('Máximos '.$comite->votos_maximos.' votos') : print('Máximo '.$comite->votos_maximos.' voto') ?> 
								</div>
								<input class="idUnidad" type="hidden" value="<?php echo $unidad->id ?>">
								<input class="idComite" type="hidden" value="<?php echo $comite->id ?>">
								<input class="nombreUnidad" type="hidden" value="<?php echo $unidad->nombre_unidad ?>">
								<input class="nombreComite" type="hidden" value="<?php echo $comite->nombre_comite ?>">
								<input class="votosMaximos" type="hidden" value="<?php echo $comite->votos_maximos ?>">
								<?php foreach($comite->candidatos as $candidato){ ?>
									<div class="candidatoElectoral">
										<input class="ciPersona" type="hidden" value="<?php echo $candidato->ci_persona ?>">
										<div class="fotoCandidato">
											<img src="<?php echo SERVERURL ?>/views/img/fotos_personas/<?php echo $candidato->ci_persona ?>.jpg" onerror="this.onerror=null; this.src='<?php echo SERVERURL ?>/views/img/sin_foto.png'" />
										</div>
										<div class="nombreCandidato" style="width: 280px!important;">
											<?php echo "$candidato->pnombre $candidato->snombre $candidato->papellido $candidato->sapellido" ?>
										</div>
										<div class="checkCandidato">
											<label class="container">
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>	
					<?php } ?>
				<?php }else{ ?>
					<?php if(count($unidad->candidatos) > 0){ ?>
						<div class="tarjetaElectoral">
							<div class="nombreUnidadElectoral font-weight-bold">
								<?php echo $unidad->nombre_unidad ?>
							</div>
							<div class="nombreComiteElectoral text-success">
								<?php $unidad->votos_maximos > 1 ? print('Máximos '.$unidad->votos_maximos.' votos') : print('Máximo '.$unidad->votos_maximos.' voto') ?> 
							</div>
							<input class="idUnidad" type="hidden" value="<?php echo $unidad->id ?>">
							<input class="nombreUnidad" type="hidden" value="<?php echo $unidad->nombre_unidad ?>">
							<input class="votosMaximos" type="hidden" value="<?php echo $unidad->votos_maximos ?>">
							<?php foreach($unidad->candidatos as $candidato){ ?>
								<div class="candidatoElectoral">
									<input class="ciPersona" type="hidden" value="<?php echo $candidato->ci_persona ?>">
									<div class="fotoCandidato">
										<img src="<?php echo SERVERURL ?>/views/img/fotos_personas/<?php echo $candidato->ci_persona ?>.jpg" onerror="this.onerror=null; this.src='<?php echo SERVERURL ?>/views/img/sin_foto.png'" />
									</div>
									<div class="nombreCandidato" style="width: 280px!important;">
										<?php echo "$candidato->pnombre $candidato->snombre $candidato->papellido $candidato->sapellido" ?>
									</div>
									<div class="checkCandidato">
										<label class="container">
											<input type="checkbox">
											<span class="checkmark"></span>
										</label>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		</div>

		<button class="btn btn-success d-block mx-auto mt-3 font-weight-bold guardarVotacion" style="font-size: 30px;box-shadow: 0 0 8px black;">VOTAR</button>

	</main>
	<div id="comprobante" class="text-center"></div>
</body>
</html>
<script>
	$(document).ready(function(){
	  	$(document).on("contextmenu",function(){
       		return false;
    	}); 
    	$(document).on("keydown", function(){
       		return false;
    	}); 

    	var timeout;

    	function esperar(){
    		var idEleccion = $('#idEleccion').val().trim();
    		$.post(SERVERURL + '/elecciones/esperando', {idEleccion : idEleccion}, function(res){
    			try{
	    			if(res == 'quitar'){
	    				clearTimeout(timeout);
	    				alerta.fire({
	    					icon: 'error',
	    					title: '¡Está máquina de votación fué Desactivada!',
	    					text: 'Por favor, hable con el encargado de la elección para activarle nuevamente una máquina de votación.',
	    					allowOutsideClick: false
	    				}).then(()=>{
	    					window.location.href = SERVERURL + '/elecciones/autenticar/' + idEleccion + '/';
	    				});
	                }else{
	                	clearTimeout(timeout);
	    				timeout = setTimeout(function(){esperar()}, 3000);
	                }
	    		}catch(err){
	    			clearTimeout(timeout);
	    			timeout = setTimeout(function(){esperar()}, 3000);
	    		}

    		});
    	}

    	esperar();

      $(function() {
        $.ajaxSetup({
            error: function(jqXHR, exception) {
            	clearTimeout(timeout);
                timeout = setTimeout(function(){esperar()},3000);
            }
        });
      });

	});
</script>