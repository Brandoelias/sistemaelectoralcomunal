<?php include_once('./models/mainModel.php') ?>

<header>
	<div class="jumbotron jumbotron-fluid text-center p-4">

		<a href="<?php echo SERVERURL . '/' ?>"><span class="titulo-header font-weight-bold">Sistema Electoral de Comunas</span></a>

		<img id="gbvLogo" class="no-print" src="<?php echo SERVERURL ?>/views/img/gbv.png" />
		<img class="d-none print gbvLogo" src="<?php echo SERVERURL ?>/views/img/gbvnegro.png" />

		<img id="comunasLogo" src="<?php echo SERVERURL ?>/views/img/comunasLogo.png" />

	</div>

	<nav class="navbar navbar-default" style="font-size: 20px">
		<?php $fecha = explode('-', mainModel::getFechaActualFormateada()) ?>
		<span class="d-none" id="fechaActualServidor"><?php echo date('d/m/Y') ?></span>
	    <span class="fechaActual"><?php echo $fecha[0] .' - <span id="fechaActual">'. Date('h:i:s A') ?></span></span>
	    <?php if(isset($_SESSION['usuario'])){ $u = $_SESSION['usuario']; $idU = $_SESSION['id']; ?>
	    	<div class="d-inline-block" style="position: absolute; right: 8px;">
	    		<button id="botonUsuario" class="btn btn-secondary" nombre="<?php echo $u ?>" idUsuario="<?php echo $idU ?>">
		    		<img style="height: 25px; width: 25px; margin-bottom: 2px;" src="<?php echo SERVERURL ?>/views/img/usuario.svg" />
		    		<?php $_SESSION['id'] != 1 ? print(mainModel::getPersonaUsuario()) : print('Administrador') ?>		
		    	</button>
	    	</div>
	    	
	    <?php } ?>
	</nav>

</header>
<script>
	actualizarFechaActual('<?php echo date("Y-m-d H:i:s") ?>');
</script>