<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Sistema Electoral de Comunas</title>
	<link rel="icon" href="<?php echo SERVERURL ?>/views/img/favicon.jpg" />
	<link rel="stylesheet" href="<?php echo SERVERURL ?>/views/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php echo SERVERURL ?>/views/css/estilo.css?v=2" />
	<link rel="stylesheet" href="<?php echo SERVERURL ?>/views/css/daterangepicker.css" />
	<script src="<?php echo SERVERURL ?>/views/js/jquery.min.js"></script>
	<script src="<?php echo SERVERURL ?>/views/js/popper.min.js"></script>
	<script src="<?php echo SERVERURL ?>/views/js/bootstrap.min.js"></script>
	<script src="<?php echo SERVERURL ?>/views/js/sweetalert2@9.js"></script>
	<script src="<?php echo SERVERURL ?>/views/js/moment.min.js"></script>
	<script src="<?php echo SERVERURL ?>/views/js/daterangepicker.min.js"></script>
	<script src="<?php echo SERVERURL ?>/views/js/html2canvas.min.js"></script>
	<script>
		const SERVERURL = '<?php echo SERVERURL ?>';
	</script>
	<script src="<?php echo SERVERURL ?>/views/js/funciones.js?v=2"></script>
</head>