<div class="text-center w-100 mb-3 paginacion">

		<h5 class="text-center font-weight-bold" style="color:white;text-shadow: 0px 0px 4px black"><?php echo 'Página ' . $paginaActual . ' de ' . $totalPaginas ?></h5>

		<?php 
			if(($paginaInicio-1) % 10 == 0 && $paginaInicio >= 10){ $paginaInicio -= 1; ?>
				<?php if( ($totalPaginas - $paginaInicio) < 10 && $paginaInicio > 10 ){ ?>
					<a class="paginacion-item btn btn-primary" href="<?php echo SERVERURL . '/' . $controlador . '/' . 1 . $parametros ?>">
						<<<
					</a>
					<a class="paginacion-item btn btn-primary" href="<?php echo SERVERURL . '/' . $controlador . '/' . ($paginaInicio-10) . $parametros ?>">
						<<
					</a>
				<?php }else{ ?>
					<a class="paginacion-item btn btn-primary" href="<?php echo SERVERURL . '/' . $controlador . '/' . 1 . $parametros ?>">
						<<<
					</a>
					<a class="paginacion-item btn btn-primary" href="<?php echo SERVERURL . '/' . $controlador . '/' . ($paginaInicio-1) . $parametros ?>">
						<<
					</a>
				<?php } ?>
		<?php }else{ ?>

			<?php if($paginaInicio > 10){ ?>
				<a class="paginacion-item btn btn-primary" href="<?php echo SERVERURL . '/' . $controlador . '/' . $paginaInicio . $parametros ?>">
					<<<
				</a>
				<a class="paginacion-item btn btn-primary" href="<?php echo SERVERURL . '/' . $controlador . '/' . ($paginaInicio-1) . $parametros ?>">
					<<
				</a>
			<?php } ?>

		<?php } ?>

		<?php if( ($totalPaginas - $paginaInicio) < 10 && $paginaInicio > 10 ){ ?>
			<?php for($i = ($paginaInicio-9); $i <= $totalPaginas; $i++){ ?>
				<a class="paginacion-item btn btn-primary <?php $paginaActual == $i ? print('active') : print('') ?>" href="<?php echo SERVERURL . '/' . $controlador . '/' . $i . $parametros ?>">
					<?php echo $i ?>
				</a>
			<?php } ?>
		<?php }else{ ?>

			<?php $cont = 0; ?>
			<?php for($i = $paginaInicio; $i <= $totalPaginas; $i++){ ?>

				<?php $cont++ ?>

				<a class="paginacion-item btn btn-primary <?php $paginaActual == $i ? print('active') : print('') ?>" href="<?php echo SERVERURL . '/' . $controlador . '/' . $i . $parametros ?>">
				<?php 
					if($cont > 10){ 
						echo '>>';
					}else{
						echo $i;
					}
				?>
				</a>

				<?php 
					if($cont > 10){ ?>
						<a class="paginacion-item btn btn-primary" href="<?php echo SERVERURL . '/' . $controlador . '/' . $totalPaginas . $parametros ?>">
							>>>
						</a>

				<?php		break;
					} 
				?>

			<?php } ?>

		<?php } ?>


</div>