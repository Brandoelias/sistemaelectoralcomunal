<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-3">
		<div class="alert alert-danger w-25 d-block mx-auto text-center">¡Página no encontrada!</div>

		<a class="w-25 d-block mx-auto" href="<?php echo SERVERURL ?>"><button class="btn btn-primary d-block mx-auto">Inicio</button></a>
	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>