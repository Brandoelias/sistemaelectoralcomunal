<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-4">
		<div class="jumbotron p-2 d-block mx-auto w-50 text-center mb-4 titleBox">
			<h2 class="mb-2">Candidatos de la Elección</h2>
			<h3><?php echo $eleccion->nombre ?></h3>
			<h4><?php echo $eleccion->fecha_formato ?></h4>
			<input id="consejocomunal" type="hidden" value="<?php echo $eleccion->cod_consejocomunal ?>" />
			<input id="idEleccion" type="hidden" value="<?php echo $eleccion->id ?>" />
		</div>

		<?php if($finalizado){ ?>
			<div class="w-50 mx-auto p-2 text-center bg-danger text-white font-weight-bold mb-2">LA ELECCIÓN FINALIZÓ<br>NO SE PUEDEN MODIFICAR LOS CANDIDATOS</div>
		<?php }elseif($comenzado){ ?>
			<div class="w-50 mx-auto p-2 text-center bg-danger text-white font-weight-bold mb-2">LA ELECCIÓN YA COMENZÓ<br>NO SE PUEDEN MODIFICAR LOS CANDIDATOS</div>
		<?php } ?>

		<?php if($finalizado || $comenzado){ ?>
			<input id="sePuedeModificar" class="d-none" type="hidden" value="no">
		<?php }else{ ?>
			<input id="sePuedeModificar" class="d-none" type="hidden" value="si">
		<?php } ?>

		<table class="table table-hover table-stripped">
			<th>Unidad</th>
			<th>Votos Máximos</th>
			<th>Candidatos</th>

			<?php foreach($unidades as $unidad){ ?>

				<tr>
					<td class="d-none">
						<span id="link" class="idUnidad"><?php echo $unidad->id_unidad ?></span>
					</td>
					<td class="nombreUnidad">
						<?php echo $unidad->nombre_unidad ?>	
					</td>
					<td>
						<?php $unidad->comites < 1 ? print($unidad->votos_maximos) : print('-') ?>
					</td>
					<td>
						<span id="link" class="verCandidatosUnidad"><?php echo $unidad->candidatos ?></span>
					</td>
				</tr>
				
			<?php } ?>

			<?php if(count($unidades) < 1){ ?>
				<tr>
					<td colspan="2">El consejo comunal no posee unidades</td>
				</tr>
			<?php } ?>

		</table>

		<a class="btn btn-dark d-block mx-auto mt-3" style="width: 100px" href="<?php echo SERVERURL ?>/elecciones/ver/<?php echo $url[2] ?>">
			Regresar
		</a>

	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>
<script>
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();
	});
</script>