<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-4">
		<div class="jumbotron p-2 d-block mx-auto w-25 text-center mb-3 titleBox">
			<h2 class="m-0">Comités</h2>
		</div>


		<table class="table table-hover table-stripped">
			
			<th>Nombre</th>
			<th>Editar</th>
			<th>Borrar</th>

			<?php foreach($comites as $comite){ ?>

				<tr>
					<td class="idComite d-none">
						<?php echo $comite->id ?>
					</td>
					<td class="nombreComite">
						<?php echo $comite->nombre_comite ?>
					</td>
					<td>
						<img class="editarImg editarComite" src="<?php echo SERVERURL ?>/views/img/editar.svg">
					</td>
					<td>
						<img class="borrarImg borrarComite" src="<?php echo SERVERURL ?>/views/img/borrar.svg">
					</td>
				</tr>

			<?php } ?>

			<?php if(count($comites) < 1){ ?>

				<tr>
					<td colspan="8">
						No hay comites registrados.
					</td>
				</tr>

			<?php } ?>

		</table>

		<button class="btn btn-success d-block mx-auto mt-5 agregarComite" type="button">Agregar +</button>

		<a class="btn btn-dark d-block mx-auto mt-3" style="width: 100px;" href="<?php echo SERVERURL ?>/menu/consejoscomunales/">
			Regresar
		</a>

	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>