<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-4">
		<div class="jumbotron p-2 d-block mx-auto w-50 text-center mb-4 titleBox">
			<h2 class="mb-2">Resultados de la Elección</h2>
			<h3><?php echo $eleccion->nombre ?></h3>
			<h4><?php echo $eleccion->fecha_formato ?></h4>
		</div>

		<div class="bg-white mx-auto mb-2 text-center font-weight-bold" style="box-shadow: 0 0 8px black; border-radius: 4px; width: 200px">Participantes: <?php echo $cantParticipantes ?></div>

		<div class="boletaElectoral">
			<?php foreach($unidades as $unidad){ ?>
				<?php if(count($unidad->comites) > 0) { ?>
					<?php foreach($unidad->comites as $comite){ ?>
						<?php if(count($comite->candidatos) > 0) { ?>
							<div class="tarjetaElectoralBoleta">
								<div class="nombreUnidadElectoral" style="border-bottom: none!important;">
									<b><?php echo $unidad->nombre_unidad ?></b>
								</div>
								<div class="nombreComiteElectoral">
									<b><?php echo $comite->nombre_comite ?></b>
								</div>
								<?php foreach($comite->candidatos as $candidato){ ?>
									<div class="candidatoElectoralBoleta">
										<div class="fotoCandidato">
											<img src="<?php echo SERVERURL ?>/views/img/fotos_personas/<?php echo $candidato->ci_persona ?>.jpg" onerror="this.onerror=null; this.src='<?php echo SERVERURL ?>/views/img/sin_foto.png'" />
										</div>
										<div class="nombreCandidato">
											<?php echo "$candidato->pnombre $candidato->snombre $candidato->papellido $candidato->sapellido" ?>
										</div>
										<div class="votosCandidato">
											<b><?php echo $candidato->votos ?></b>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>	
					<?php } ?>
				<?php }else{ ?>
					<?php if(count($unidad->candidatos) > 0){ ?>
						<div class="tarjetaElectoralBoleta">
							<div class="nombreUnidadElectoral">
								<b><?php echo $unidad->nombre_unidad ?></b>
							</div>
							<?php foreach($unidad->candidatos as $candidato){ ?>
								<div class="candidatoElectoralBoleta">
									<div class="fotoCandidato">
										<img src="<?php echo SERVERURL ?>/views/img/fotos_personas/<?php echo $candidato->ci_persona ?>.jpg" onerror="this.onerror=null; this.src='<?php echo SERVERURL ?>/views/img/sin_foto.png'" />
									</div>
									<div class="nombreCandidato">
										<?php echo "$candidato->pnombre $candidato->snombre $candidato->papellido $candidato->sapellido" ?>
									</div>
									<div class="votosCandidato">
										<b><?php echo $candidato->votos ?></b>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		</div>

		
		<button class="btn btn-primary d-block mx-auto mt-5 no-print" style="width: 100px" type="button" onclick="window.print()">Imprimir</button>
		<a class="btn btn-dark d-block mx-auto mt-3 no-print" style="width: 100px;" href="<?php echo SERVERURL ?>/elecciones/finalizadas/">
			Regresar
		</a>

	</main>

	<div class="no-print">
		<?php include_once('./views/partials/footer.php') ?>
	</div>

</body>
</html>
<script>
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();
	});
</script>