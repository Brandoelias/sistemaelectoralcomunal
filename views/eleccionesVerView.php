<!DOCTYPE html>
<html>
	<?php include_once('./views/partials/head.php') ?>
<body>

	<?php include_once('./views/partials/header.php') ?>

	<main class="p-4">
		<div class="jumbotron p-2 d-block mx-auto w-50 text-center titleBox font-weight-bold">
			<h2 class="mb-0">Elección <?php $eleccion->finalizado == 1 ? print('Finalizada') : print('Activa') ?></h2>
			<?php if($eleccion->finalizado != 1){ ?>
				<h2 class="mb-2" style="font-size: 20px"><?php $comenzado ? print('En Curso') : print('Sin Comenzar') ?></h2>
			<?php } ?>
			<h3><?php echo $eleccion->nombre ?></h3>
			<h4><a href="#" class="<?php $eleccion->finalizado || $finalizado || $comenzado || $tipoUsuario == 3 ? print('fechaEleccionFinalizada') : print('fechaEleccion') ?>" fecha="<?php echo $eleccion->fecha ?>"><?php echo $eleccion->fecha_formato ?></a></h4>
			<input id="idEleccion" type="hidden" value="<?php echo $eleccion->id ?>">
		</div>

		<div id="menuContenedor" class="mt-2">

			<?php if($tipoUsuario == 1 || $tipoUsuario == 2){ ?>
				<?php if(!$eleccion->finalizado && !$finalizado && !$comenzado){ ?>

					<a class="d-block mx-auto mt-3 w-50 btn btn-primary d-block mx-auto" href="<?php echo SERVERURL ?>/elecciones/candidatos/<?php echo $id ?>/">
						<img src="<?php echo SERVERURL ?>/views/img/habitantes.svg">Candidatos
					</a>

				<?php }else{ ?>

					<a class="d-block mx-auto mt-3 w-50 btn btn-primary d-block mx-auto disabled" data-toggle="tooltip" title="No se puede modificar los candidatos." data-placement="bottom" href="#">
						<img src="<?php echo SERVERURL ?>/views/img/habitantes.svg">Candidatos
					</a>

				<?php } ?>
			<?php } ?>
			<a class="d-block mx-auto mt-3 w-50 btn btn-primary d-block mx-auto" href="<?php echo SERVERURL ?>/elecciones/boletaelectoral/<?php echo $id ?>/">
				<img src="<?php echo SERVERURL ?>/views/img/boletaelectoral.svg">Boleta Electoral
			</a>

			<?php if($eleccion->finalizado){ ?>

				<a class="d-block mx-auto mt-3 w-50 btn btn-primary d-block mx-auto disabled" data-toggle="tooltip" title="Las votaciones han finalizado." data-placement="bottom" href="#">
					<img src="<?php echo SERVERURL ?>/views/img/administrarvotaciones.svg">Administrar Votaciones
				</a>

			<?php } elseif($iniciarVotacion){ ?>

				<a class="d-block mx-auto mt-3 w-50 btn btn-primary d-block mx-auto" href="<?php echo SERVERURL ?>/elecciones/administrar/<?php echo $id ?>/">
					<img src="<?php echo SERVERURL ?>/views/img/administrarvotaciones.svg">Administrar Votaciones
				</a>

			<?php }else{ ?>

				<a class="d-block mx-auto mt-3 w-50 btn btn-primary d-block mx-auto disabled" data-toggle="tooltip" title="Aún no es el día de las votaciones." data-placement="bottom" href="#">
					<img src="<?php echo SERVERURL ?>/views/img/administrarvotaciones.svg">Administrar Votaciones
				</a>

			<?php } ?>

			<?php if($tipoUsuario == 1 || $tipoUsuario == 2){ ?>
				<?php if($eleccion->finalizado){ ?>

				<a class="d-block mx-auto mt-3 btn btn-primary d-block mx-auto" href="<?php echo SERVERURL ?>/elecciones/resultados/<?php echo $eleccion->id ?>/">
						<img src="<?php echo SERVERURL ?>/views/img/resultados.svg">Resultados
				</a>

				<?php }else{ ?>

					<a class="d-block mx-auto mt-3 btn btn-primary d-block mx-auto disabled" data-toggle="tooltip" title="Aún no finalizan las votaciones." data-placement="bottom" href="#">
						<img src="<?php echo SERVERURL ?>/views/img/resultados.svg">Resultados
					</a>

				<?php } ?>
			<?php } ?>
		</div>

		<a class="btn btn-dark d-block mx-auto mt-3" style="width: 100px;" href="<?php echo SERVERURL ?>/elecciones/<?php $eleccion->finalizado == 1 ? print('finalizadas/') : print('activas/') ?>">
			Regresar
		</a>


	</main>

	<?php include_once('./views/partials/footer.php') ?>

</body>
</html>
<script>
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();
	});
</script>