--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0 (Debian 13.0-4)
-- Dumped by pg_dump version 13.0 (Debian 13.0-4)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: banco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.banco (
    id integer NOT NULL,
    nombre_banco character varying(50)
);


ALTER TABLE public.banco OWNER TO postgres;

--
-- Name: banco_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.banco_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banco_id_seq OWNER TO postgres;

--
-- Name: banco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.banco_id_seq OWNED BY public.banco.id;


--
-- Name: candidato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.candidato (
    ci_persona integer,
    id_eleccion integer,
    id_unidad integer,
    id_comite integer,
    votos integer DEFAULT 0
);


ALTER TABLE public.candidato OWNER TO postgres;

--
-- Name: comite; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comite (
    id integer NOT NULL,
    nombre_comite character varying(70),
    eliminado boolean DEFAULT false
);


ALTER TABLE public.comite OWNER TO postgres;

--
-- Name: comite_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.comite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comite_id_seq OWNER TO postgres;

--
-- Name: comite_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.comite_id_seq OWNED BY public.comite.id;


--
-- Name: comite_unidad_consejo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comite_unidad_consejo (
    id_comite integer NOT NULL,
    id_unidad integer NOT NULL,
    cod_consejo character varying(50) NOT NULL,
    votos_maximos integer DEFAULT 5
);


ALTER TABLE public.comite_unidad_consejo OWNER TO postgres;

--
-- Name: consejocomunal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.consejocomunal (
    cod character varying(50) NOT NULL,
    nombre character varying(50),
    telefono character varying(15),
    rif character varying(50),
    correo character varying(50),
    numerodecuenta character varying(50),
    id_banco integer DEFAULT 0
);


ALTER TABLE public.consejocomunal OWNER TO postgres;

--
-- Name: consejocomunal_unidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.consejocomunal_unidad (
    cod_consejocomunal character varying(50) NOT NULL,
    id_unidad integer NOT NULL,
    votos_maximos integer DEFAULT 1
);


ALTER TABLE public.consejocomunal_unidad OWNER TO postgres;

--
-- Name: eleccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.eleccion (
    id integer NOT NULL,
    fecha date,
    cod_consejocomunal character varying(50),
    finalizado boolean DEFAULT false,
    "contraseña" character varying DEFAULT '$2y$12$XTOAo17sXWT7oiFQcX3UzOyv3KPRCh0dswSqO2bmY7cKEI4o/opxq'::character varying
);


ALTER TABLE public.eleccion OWNER TO postgres;

--
-- Name: eleccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.eleccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eleccion_id_seq OWNER TO postgres;

--
-- Name: eleccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.eleccion_id_seq OWNED BY public.eleccion.id;


--
-- Name: maquina_votante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.maquina_votante (
    id integer NOT NULL,
    ip character varying(50),
    actualizado timestamp without time zone DEFAULT now(),
    activado boolean,
    id_eleccion integer,
    votando integer
);


ALTER TABLE public.maquina_votante OWNER TO postgres;

--
-- Name: maquina_votante_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.maquina_votante_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.maquina_votante_id_seq OWNER TO postgres;

--
-- Name: maquina_votante_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.maquina_votante_id_seq OWNED BY public.maquina_votante.id;


--
-- Name: maquina_votante_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.maquina_votante_usuario (
    id_maquina bigint,
    id_usuario bigint
);


ALTER TABLE public.maquina_votante_usuario OWNER TO postgres;

--
-- Name: persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona (
    cedula integer NOT NULL,
    pnombre character varying(30),
    snombre character varying(30),
    papellido character varying(30),
    sapellido character varying(30),
    sexo character(1),
    fecha_nacimiento date,
    cod_consejocomunal character varying(50),
    eliminado boolean DEFAULT false
);


ALTER TABLE public.persona OWNER TO postgres;

--
-- Name: unidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unidad (
    id integer NOT NULL,
    nombre_unidad character varying(50),
    eliminado boolean DEFAULT false
);


ALTER TABLE public.unidad OWNER TO postgres;

--
-- Name: unidad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unidad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidad_id_seq OWNER TO postgres;

--
-- Name: unidad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unidad_id_seq OWNED BY public.unidad.id;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id integer NOT NULL,
    nombre character varying(30),
    "contraseña" character varying(150),
    cedula integer,
    tipo integer
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_seq OWNER TO postgres;

--
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;


--
-- Name: usuario_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario_tipo (
    id integer NOT NULL,
    nombre character varying
);


ALTER TABLE public.usuario_tipo OWNER TO postgres;

--
-- Name: usuario_tipo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_tipo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_tipo_id_seq OWNER TO postgres;

--
-- Name: usuario_tipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_tipo_id_seq OWNED BY public.usuario_tipo.id;


--
-- Name: votante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.votante (
    ci_persona integer NOT NULL,
    id_eleccion integer NOT NULL
);


ALTER TABLE public.votante OWNER TO postgres;

--
-- Name: banco id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banco ALTER COLUMN id SET DEFAULT nextval('public.banco_id_seq'::regclass);


--
-- Name: comite id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comite ALTER COLUMN id SET DEFAULT nextval('public.comite_id_seq'::regclass);


--
-- Name: eleccion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.eleccion ALTER COLUMN id SET DEFAULT nextval('public.eleccion_id_seq'::regclass);


--
-- Name: maquina_votante id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.maquina_votante ALTER COLUMN id SET DEFAULT nextval('public.maquina_votante_id_seq'::regclass);


--
-- Name: unidad id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad ALTER COLUMN id SET DEFAULT nextval('public.unidad_id_seq'::regclass);


--
-- Name: usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);


--
-- Name: usuario_tipo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_tipo ALTER COLUMN id SET DEFAULT nextval('public.usuario_tipo_id_seq'::regclass);


--
-- Data for Name: banco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.banco (id, nombre_banco) FROM stdin;
1	Banco de Venezuela
2	Banco del Tesoro
3	Mercantil
4	Banesco
5	Banco bicentenario del pueblo
0	No tiene
6	Banco Fondo Comun
7	Venezolano de Credito
8	BANFANB
9	Bancamiga
10	Bancaribe
11	Banco Caroni
\.


--
-- Data for Name: candidato; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.candidato (ci_persona, id_eleccion, id_unidad, id_comite, votos) FROM stdin;
9356859	53	17	7	0
10941111	53	16	\N	0
7705277	53	20	\N	0
14492292	53	16	\N	0
7038124	53	16	\N	0
6313740	53	17	7	0
6386292	53	17	4	0
12711975	53	17	15	0
6512398	53	17	15	0
5077953	53	17	15	0
8207147	53	17	19	0
5006952	53	17	7	0
11583636	53	17	4	0
13911305	53	17	15	0
12610718	53	17	15	0
14076294	53	16	\N	0
13476350	53	20	\N	0
14973664	53	17	19	0
5672366	53	17	19	0
13510259	53	17	19	0
9763106	53	17	7	0
9704287	53	17	4	0
9212476	53	17	15	0
14243668	54	16	\N	0
9781336	55	20	\N	1
7690024	55	16	\N	3
9951718	54	17	\N	1
7528319	55	20	\N	2
13803554	55	17	9	3
13325584	55	17	4	4
11238233	55	17	15	6
11980383	55	17	18	11
14969677	54	17	\N	1
6626971	55	16	\N	3
8982810	54	16	\N	2
8898690	55	20	\N	2
8543976	54	17	\N	2
6948215	55	20	\N	3
11530198	55	17	9	4
14314094	54	20	\N	4
11472916	55	17	9	4
9878064	55	17	4	5
11026806	55	17	15	5
6313740	55	17	18	6
9057280	54	17	\N	4
6679854	54	16	\N	3
6640567	54	20	\N	1
11709205	54	17	\N	3
5105733	54	16	\N	2
11275041	54	20	\N	2
13299442	54	17	\N	2
9264396	55	16	\N	0
12150794	55	17	9	9
12150794	53	20	\N	0
9543836	56	17	9	4
7910666	55	17	4	5
12581279	55	17	15	3
8695993	58	17	19	4
14306293	56	17	4	6
12394413	56	16	\N	2
9621453	56	16	\N	5
11348358	58	16	\N	4
6285882	56	17	9	7
11393771	56	16	\N	6
8093326	56	16	\N	3
8265675	56	16	\N	3
6367770	56	17	9	6
5599518	56	17	9	3
5550731	56	17	4	7
10862612	56	17	4	7
6855207	56	17	4	5
11124065	57	16	\N	0
11058675	55	20	\N	5
12515617	57	16	\N	0
13824866	58	17	9	5
12120337	57	16	\N	1
10199132	57	16	\N	1
5403471	58	17	19	5
10868611	57	16	\N	5
10739181	58	20	\N	1
9723946	57	16	\N	2
8928937	57	16	\N	1
5778151	55	16	\N	4
10415527	55	20	\N	2
9921369	55	17	9	7
7067934	57	16	\N	2
14044986	55	17	9	4
14384755	56	17	9	1
12661303	58	16	\N	0
11690426	55	17	4	6
10701064	55	17	18	6
6747054	55	16	\N	3
13756968	55	17	4	4
12271393	55	17	15	6
7291294	58	20	\N	0
10725405	58	20	\N	4
10152565	58	17	9	6
8347507	58	17	9	6
6903513	58	17	19	6
9862600	58	17	13	9
12639394	58	17	13	4
13868769	56	16	\N	4
6555728	58	16	\N	4
9103629	58	20	\N	8
13031970	58	17	9	5
9094103	58	17	19	6
7987782	58	17	13	10
11791075	58	16	\N	2
8065167	58	17	19	4
10335815	58	16	\N	2
\.


--
-- Data for Name: comite; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comite (id, nombre_comite, eliminado) FROM stdin;
4	Comité de Planificación Comunal	f
9	Comité de Cultura, Comunicación y Formación	f
15	Comité De Protección Social	f
18	Comité De Seguridad y Defensa Territorial	f
19	Comité De Economía Popular	f
13	Comité De Servicios	f
7	Comité De Habitad Vivienda y Tierra	f
\.


--
-- Data for Name: comite_unidad_consejo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comite_unidad_consejo (id_comite, id_unidad, cod_consejo, votos_maximos) FROM stdin;
9	17	01-01-22-001-0064	5
19	17	01-01-22-001-0064	5
7	17	01-01-22-001-0064	5
4	17	01-01-22-001-0064	5
15	17	01-01-22-001-0064	5
18	17	01-01-22-001-0064	5
9	17	01-01-22-001-0016	1
7	17	01-01-22-001-0016	2
13	17	01-01-22-001-0016	3
13	17	CC-02-URB-0024	5
4	17	CC-02-URB-0024	5
18	17	CC-02-URB-0024	5
4	17	01-01-22-001-0070	5
19	17	01-01-22-001-0070	5
15	17	01-01-22-001-0070	5
13	17	01-01-22-001-0070	5
9	17	01-01-22-001-0070	5
9	17	01-01-22-001-0043	5
13	17	2017-10-0013	5
4	17	2017-10-0013	5
18	17	2017-10-0013	5
9	17	01-01-22-001-0008	2
7	17	01-01-22-001-0008	89
4	17	01-01-22-001-0008	3
15	17	01-01-22-001-0008	5
4	17	01-01-22-0001-0005	5
9	17	01-01-22-0001-0005	5
18	17	01-01-22-0001-0005	5
19	17	01-01-22-0001-0005	5
13	17	01-01-22-0001-0005	5
15	17	01-01-22-0001-0005	5
9	17	01-01-22-0001-0028	5
19	17	01-01-22-0001-0028	5
7	17	01-01-22-0001-0028	5
4	17	01-01-22-0001-0028	5
15	17	01-01-22-0001-0028	5
18	17	01-01-22-0001-0028	5
13	17	01-01-22-0001-0028	5
\.


--
-- Data for Name: consejocomunal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.consejocomunal (cod, nombre, telefono, rif, correo, numerodecuenta, id_banco) FROM stdin;
01-01-22-001-0008	Venciendo Barreras		C-294967477			0
01-01-22-0001-0005	Planicie a Descanso		C-306924906			0
01-01-22-0001-0027	La Piedrita		C-299687294			0
01-01-22-001-0043	Felicia Gomez		C-299922420			0
01-01-22-001-0070	Las Palmas		C-317429770			0
2017-10-0013	Las 4 B		C-317113098	las4bcssb@gmail.com		0
01-01-22-001-0016	Juan Manuel Cagigal	02126827601	C-312600942		0565468795646467	1
01-01-22-0001-0028	Andres Bello		C-317392868		0163456745678456	2
01-01-22-0001-0014	Bloque 12		C-299679590	bloque12cssb2@hotmail.com		0
01-01-22-001-0079	Aveledo Real Arbolitos II	04145787888	C-405938241			0
01-01-22-001-0049	El Bicentenario	02124656451	C-232323423	asdadadxd@gmai.com		0
01-01-22-001-0064	Monte Piedad Socialista	02125458754	C-299677671	cssbmontepiedad@gmail.com	0163456784646456	2
CC-02-URB-0024	Renacer Soberano					0
\.


--
-- Data for Name: consejocomunal_unidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.consejocomunal_unidad (cod_consejocomunal, id_unidad, votos_maximos) FROM stdin;
01-01-22-001-0064	17	1
CC-02-URB-0024	16	1
CC-02-URB-0024	17	1
01-01-22-0001-0027	16	3
01-01-22-001-0070	17	1
01-01-22-0001-0028	16	1
01-01-22-001-0070	20	1
01-01-22-001-0049	16	1
01-01-22-001-0070	16	1
01-01-22-0001-0028	17	1
01-01-22-001-0043	17	1
01-01-22-001-0079	20	1
01-01-22-0001-0014	16	1
01-01-22-0001-0014	20	1
01-01-22-001-0043	20	1
01-01-22-001-0016	16	1
01-01-22-001-0016	20	1
2017-10-0013	17	1
01-01-22-001-0008	17	1
01-01-22-0001-0027	17	1
01-01-22-001-0079	16	1
01-01-22-0001-0027	20	1
01-01-22-0001-0028	20	1
01-01-22-001-0064	16	1
01-01-22-0001-0014	17	4
01-01-22-001-0064	20	1
01-01-22-0001-0005	17	1
01-01-22-001-0016	17	5
01-01-22-001-0008	16	3
\.


--
-- Data for Name: eleccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.eleccion (id, fecha, cod_consejocomunal, finalizado, "contraseña") FROM stdin;
53	2022-01-30	01-01-22-001-0064	f	$2y$12$yKYe4vGQOurankqxjO8FZeRIFEHNBf58u3jIJxHGOuWBNQ/aHLml.
54	2022-01-18	01-01-22-0001-0014	t	$2y$12$XTOAo17sXWT7oiFQcX3UzOyv3KPRCh0dswSqO2bmY7cKEI4o/opxq
55	2022-01-18	01-01-22-001-0064	t	$2y$12$XTOAo17sXWT7oiFQcX3UzOyv3KPRCh0dswSqO2bmY7cKEI4o/opxq
56	2022-01-18	01-01-22-001-0008	t	$2y$12$XTOAo17sXWT7oiFQcX3UzOyv3KPRCh0dswSqO2bmY7cKEI4o/opxq
57	2022-01-18	01-01-22-001-0049	t	$2y$12$XTOAo17sXWT7oiFQcX3UzOyv3KPRCh0dswSqO2bmY7cKEI4o/opxq
58	2022-01-18	01-01-22-001-0070	t	$2y$12$XTOAo17sXWT7oiFQcX3UzOyv3KPRCh0dswSqO2bmY7cKEI4o/opxq
\.


--
-- Data for Name: maquina_votante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.maquina_votante (id, ip, actualizado, activado, id_eleccion, votando) FROM stdin;
\.


--
-- Data for Name: maquina_votante_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.maquina_votante_usuario (id_maquina, id_usuario) FROM stdin;
\.


--
-- Data for Name: persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.persona (cedula, pnombre, snombre, papellido, sapellido, sexo, fecha_nacimiento, cod_consejocomunal, eliminado) FROM stdin;
10543413	Lidia	Naia	Ochoa	Galindo	f	1955-03-25	01-01-22-0001-0028	f
13343303	Celia	Marina	Muñóz	Ozuna	f	1961-02-16	01-01-22-0001-0028	f
10256376	Antonio	Marcos	Aragón	Posada	m	1967-09-07	01-01-22-0001-0028	f
10076635	Nil	Jana	Ochoa	Alemán	f	1952-04-04	01-01-22-0001-0028	f
6366326	Ian	Domingo	Terrazas	Nájera	m	1956-05-24	01-01-22-0001-0028	f
13097927	Alejandra	Alexia	Palomino	Angulo	f	1962-02-15	01-01-22-0001-0028	f
14552141	Abril	Sofia	Sáez	Espinal	f	1961-02-22	01-01-22-0001-0028	f
5592017	Guillermo	Gabriel	Peres	Aguado	m	1969-08-02	01-01-22-0001-0028	f
8008574	Candela	Vera	Zúñiga	Espinal	f	1968-09-03	01-01-22-0001-0028	f
9936288	Ander	Mario	Cabán	Arguello	m	1962-09-07	01-01-22-0001-0028	f
8809278	Marco	Raul	Duran	Laboy	m	1964-07-19	01-01-22-0001-0028	f
8716149	Elena	Vega	Sierra	Avilés	f	1963-03-24	01-01-22-0001-0028	f
10596688	Aleix	Domingo	Rosa	Quintero	m	1955-04-07	01-01-22-0001-0028	f
6563153	Jordi	Aaron	Quiroz	Ledesma	m	1962-12-17	01-01-22-0001-0028	f
13934207	Ivan	Ian	Gómez	Montoya	m	1969-01-30	01-01-22-0001-0028	f
10697862	Abril	Gabriela	Saiz	Cotto	f	1955-05-09	01-01-22-0001-0028	f
6597008	Alonso	Alvaro	Ayala	Meza	m	1960-10-19	01-01-22-0001-0028	f
5274499	Ines	Laia	Hidalgo	Heredia	f	1966-07-08	01-01-22-0001-0028	f
10376479	Marta	Alicia	Lucio	Pizarro	f	1961-07-24	01-01-22-0001-0028	f
6628150	Sandra	Naia	Godoy	Velázquez	f	1967-12-12	01-01-22-0001-0028	f
13067305	Adrian	Jordi	Reyna	Melgar	m	1965-08-18	01-01-22-0001-0028	f
14112016	Paola	Berta	Cardenas	Valladares	f	1965-07-25	01-01-22-0001-0028	f
6179051	Blanca	Ainara	Esparza	Bahena	f	1955-12-21	01-01-22-0001-0028	f
5573486	Blanca	Noelia	Alba	Galván	f	1969-02-27	01-01-22-0001-0028	f
9033737	Ona	Pau	Roldan	Lozano	f	1952-05-04	01-01-22-0001-0028	f
8585643	Vera	Patricia	Vera	Páez	f	1962-05-14	01-01-22-0001-0028	f
14543886	Lara	Claudia	Anguiano	Maldonado	f	1954-12-06	01-01-22-0001-0028	f
12661733	Gonzalo	Enrique	Valles	Mejía	m	1969-01-04	01-01-22-0001-0028	f
10991883	Enrique	Jaime	Grijalva	Esquibel	m	1956-10-18	01-01-22-0001-0028	f
7738371	Luisa	Luisa	Cotto	Bernal	f	1954-09-27	01-01-22-0001-0028	f
7605788	Gonzalo	Guillem	Salcido	Aguayo	m	1958-09-09	01-01-22-0001-0028	f
14464057	Leo	Luis	De Jesús	Fierro	m	1957-01-28	01-01-22-0001-0028	f
12396804	Beatriz	Patricia	Galarza	Romero	f	1963-10-30	01-01-22-0001-0028	f
14805075	Rayan	Saul	Mayorga	Salgado	m	1954-02-23	01-01-22-0001-0028	f
8942439	Isaac	Aaron	Solorio	Castellanos	m	1969-12-11	01-01-22-0001-0028	f
6703534	Nicolas	Santiago	Benavides	Benítez	m	1955-08-05	01-01-22-0001-0028	f
6265704	Cesar	Oswaldo	Delapaz	Vergara	m	1956-02-12	01-01-22-0001-0028	f
6167849	David	Hugo	Caraballo	Escudero	m	1952-12-31	01-01-22-0001-0028	f
12850135	Ane	Marina	Martínez	Aguilera	f	1969-02-20	01-01-22-0001-0028	f
11747442	Roberto	Pedro	Gimeno	Suárez	m	1960-11-19	01-01-22-0001-0028	f
7600223	Paula	Sandra	Burgos	Alcaraz	f	1954-10-30	01-01-22-0001-0028	f
14954395	Oscar	Joel	Ramírez	Sanabria	m	1959-12-23	01-01-22-0001-0028	f
8895669	Antonio	Leandro	Velasco	Córdoba	m	1955-10-18	01-01-22-0001-0028	f
5828692	Celia	Rocio	Acosta	Jaimes	f	1964-07-23	01-01-22-0001-0028	f
6542466	Manuel	Cristian	Lira	Cisneros	m	1952-09-04	01-01-22-0001-0028	f
13304769	Manuela	Olivia	Griego	Frías	f	1954-07-25	01-01-22-0001-0028	f
10194642	Carmen	Paula	Sáenz	Silva	f	1962-10-16	01-01-22-0001-0028	f
12155719	Leyre	Alexandra	Arias	Rendón	f	1959-09-06	01-01-22-0001-0028	f
8990936	Ainara	Nayara	Sánchez	Rocha	f	1956-06-08	01-01-22-0001-0028	f
12977011	Hector	Miguel	Benavídez	Alonso	m	1955-10-07	01-01-22-0001-0028	f
10151840	Guillem	Martin	Adorno	Yáñez	m	1964-02-01	01-01-22-0001-0028	f
8066084	Lara	Silvia	Campos	Carrasquillo	f	1958-11-18	01-01-22-0001-0028	f
10590471	Rodrigo	Ivan	Sisneros	Quezada	m	1954-01-14	01-01-22-0001-0028	f
12409361	Izan	Omar	Castro	Gaitán	m	1959-11-28	01-01-22-0001-0028	f
9620431	Diana	Nayara	Guajardo	Camarillo	f	1952-10-01	01-01-22-0001-0028	f
9306567	Vera	Cristina	Villanueva	Tórrez	f	1957-10-31	01-01-22-0001-0028	f
9022431	Bella	Nahia	Santiago	Corral	f	1956-05-25	01-01-22-0001-0028	f
11601366	Olivia	Isabel	Rodríguez	Samaniego	f	1969-12-26	01-01-22-0001-0028	f
7509940	Olivia	Ane	Matos	Salinas	f	1958-10-26	01-01-22-0001-0028	f
7536650	Ander	Erik	Anguiano	Bahena	m	1961-06-15	01-01-22-0001-0028	f
11018151	Ian	Oscar	Riera	Madrigal	m	1966-02-06	01-01-22-0001-0028	f
5939204	Javier	Enrique	Cruz	Tovar	m	1958-05-06	01-01-22-0001-0028	f
7355533	Clara	Noa	Guillen	Uribe	f	1967-02-24	01-01-22-0001-0028	f
12009488	Elena	Paola	Moran	Águilar	f	1962-02-12	01-01-22-0001-0028	f
5932398	Daniela	Alexandra	Casado	Valadez	f	1964-10-20	01-01-22-0001-0028	f
13574248	Cristian	Nicolas	Feliciano	Nevárez	m	1955-08-05	01-01-22-0001-0028	f
11009237	Francisco Javier	Hector	Rodarte	Alfonso	m	1959-03-04	01-01-22-0001-0028	f
13182808	Naia	Olivia	Tafoya	Olmos	f	1961-12-21	01-01-22-0001-0028	f
11632965	Mario	Gonzalo	Conde	Vera	m	1966-04-22	01-01-22-0001-0028	f
5821296	Candela	Alba	Baeza	Bautista	f	1955-01-26	01-01-22-0001-0028	f
9598190	Sofia	Diana	Chacón	Carrasquillo	f	1957-10-25	01-01-22-0001-0028	f
9819575	Leandro	Adria	Ochoa	Villareal	m	1952-05-22	01-01-22-0001-0028	f
14003178	Pablo	Miguel	Rolón	Pichardo	m	1954-07-21	01-01-22-0001-0028	f
9414976	Ivan	Dario	Armenta	Ramírez	m	1954-09-16	01-01-22-0001-0028	f
12968189	Clara	Naiara	Rodrigo	Cerda	f	1956-08-08	01-01-22-0001-0028	f
11778078	Ana	Alicia	Román	Ochoa	f	1960-07-04	01-01-22-0001-0028	f
14905655	Enrique	Erik	Sarabia	Fuentes	m	1962-12-30	01-01-22-0001-0028	f
11558251	Elena	Pau	Rodríguez	Laboy	f	1965-02-09	01-01-22-0001-0028	f
12929647	Yaiza	Paola	Arriaga	Cabán	f	1957-01-03	01-01-22-0001-0028	f
7922168	Carlota	Claudia	Polo	Ros	f	1960-08-29	01-01-22-0001-0028	f
6591682	Marcos	Marc	Aparicio	Estrada	m	1965-07-03	01-01-22-0001-0028	f
10869534	Iria	Andrea	Montoya	Villagómez	f	1955-10-28	01-01-22-0001-0028	f
6618278	Leyre	Paula	Tapia	Robledo	f	1957-07-22	01-01-22-0001-0028	f
7608558	Alejandro	David	Marroquín	Gil	m	1959-01-11	01-01-22-0001-0028	f
10773249	Leandro	Alvaro	Domingo	Carretero	m	1962-06-05	01-01-22-001-0016	f
14620929	Carla	Miriam	Terán	Arteaga	f	1965-08-09	01-01-22-0001-0028	f
10011882	Roberto	Dario	Robles	Gimeno	m	1963-05-24	01-01-22-0001-0028	f
9921804	Noelia	Berta	Mota	Morales	f	1967-06-24	01-01-22-0001-0028	f
14219832	Iria	Ines	Carmona	Barreto	f	1953-07-24	01-01-22-0001-0028	f
14113236	Gabriel	Francisco	Otero	Delatorre	m	1967-04-06	01-01-22-0001-0028	f
6612275	Cesar	Hugo	Ruelas	Caro	m	1963-03-22	01-01-22-0001-0028	f
11996027	Jon	Pedro	Villagómez	Solorio	m	1962-10-01	01-01-22-0001-0028	f
12657440	Carolina	Martina	Candelaria	Requena	f	1955-04-09	01-01-22-0001-0028	f
9561355	Aroa	Adriana	Ortiz	Delvalle	f	1964-04-13	01-01-22-0001-0028	f
5819437	Ismael	Santiago	Jáquez	Escamilla	m	1962-03-20	01-01-22-0001-0028	f
7442604	Mohamed	Adria	Pizarro	Colón	m	1953-12-07	01-01-22-0001-0028	f
13699083	Elena	Nahia	Luján	Alonzo	f	1966-02-13	01-01-22-0001-0028	f
10252270	Angel	Hugo	Olivas	De la torre	m	1967-09-10	01-01-22-0001-0028	f
11780856	Naia	Ane	Oquendo	Peres	f	1963-08-28	01-01-22-0001-0028	f
9988941	Mateo	Antonio	Sierra	Vicente	m	1956-02-17	01-01-22-0001-0028	f
12891583	Adam	Mateo	Urrutia	Medrano	m	1968-01-08	01-01-22-0001-0028	f
6487372	Maria	Valeria	Dávila	Vila	f	1955-07-21	01-01-22-0001-0028	f
11800575	Oliver	Daniel	Rendón	Bétancourt	m	1955-10-01	01-01-22-0001-0028	f
8014271	Ignacio	Andres	Caldera	Castañeda	m	1961-08-20	01-01-22-0001-0028	f
12992573	Javier	Ivan	Barrera	Serna	m	1964-09-18	01-01-22-0001-0028	f
5099765	Marcos	Antonio	Puente	Sánchez	m	1963-03-01	01-01-22-0001-0028	f
7373770	Beatriz	Cristina	Pedroza	Benítez	f	1968-07-23	01-01-22-0001-0028	f
9964556	Alma	Paola	Arteaga	Arias	f	1968-04-09	01-01-22-0001-0028	f
8936407	Adria	Ian	Parra	Baca	m	1959-06-13	01-01-22-0001-0028	f
8752935	Valeria	Alejandra	Valencia	Adame	f	1969-01-27	01-01-22-0001-0028	f
13224366	Luis	Bruno	Miguel	Manzano	m	1967-07-19	01-01-22-0001-0028	f
5580955	Oscar	Sergio	Maestas	Juan	m	1955-03-30	01-01-22-0001-0028	f
11246112	Mario	Ruben	Crespo	Zambrano	m	1965-08-10	01-01-22-0001-0028	f
5722290	Berta	Alejandra	Laboy	Valladares	f	1968-09-14	01-01-22-0001-0028	f
9591851	Carla	Naia	Moreno	Mesa	f	1961-05-27	01-01-22-0001-0028	f
12810590	Jon	Antonio	Magaña	Centeno	m	1968-04-29	01-01-22-0001-0028	f
7678139	Alex	Marcos	Castaño	Rodarte	m	1968-12-17	01-01-22-0001-0028	f
6429010	Olivia	Africa	Adame	Loya	f	1960-09-20	01-01-22-0001-0028	f
9794666	Javier	Manuel	Pedraza	Armijo	m	1969-06-16	01-01-22-0001-0028	f
11868356	Alejandro	Aleix	Barrios	Elizondo	m	1968-10-07	01-01-22-0001-0028	f
13134359	Jesus	Jordi	Vega	Lomeli	m	1953-10-28	01-01-22-0001-0028	f
11249054	Noa	Emma	Riojas	Pabón	f	1963-06-07	01-01-22-0001-0028	f
9656135	Isaac	Diego	Noriega	Villaseñor	m	1952-02-11	01-01-22-0001-0028	f
6732181	Bella	Angela	Alva	Palomo	f	1969-07-01	01-01-22-0001-0028	f
5417670	Jan	Ignacio	Tamez	Vega	m	1959-06-22	01-01-22-0001-0028	f
8267321	Alexia	Vera	Toledo	Vigil	f	1966-07-02	01-01-22-0001-0028	f
8629104	Aleix	Andres	Solano	Lozano	m	1958-04-11	01-01-22-0001-0028	f
10979720	Pau	Nerea	Campos	Riera	f	1964-02-01	01-01-22-0001-0028	f
5285227	Jorge	Diego	Galarza	Cadena	m	1964-01-18	01-01-22-0001-0028	f
6177320	Fernando	Eduardo	Blanco	Rosa	m	1965-10-20	01-01-22-0001-0028	f
13744935	Bella	Claudia	Araña	Escalante	f	1953-10-06	01-01-22-0001-0028	f
9561401	Oscar	Ismael	Briseño	Pozo	m	1954-08-06	01-01-22-0001-0028	f
5143302	Rafael	Oswaldo	Otero	Soriano	m	1961-06-04	01-01-22-0001-0028	f
11190749	Isabel	Ines	Barraza	Alemán	f	1957-03-27	01-01-22-0001-0028	f
10049986	Oscar	Marc	Santillán	Quezada	m	1964-12-12	01-01-22-0001-0028	f
14692873	Omar	Alex	Ávalos	Ulibarri	m	1965-10-02	01-01-22-0001-0028	f
5893289	Marti	Teresa	Fonseca	Anguiano	f	1953-03-09	01-01-22-0001-0028	f
7578674	Ander	Hector	Peralta	Girón	m	1961-12-17	01-01-22-0001-0028	f
5680840	Candela	Leire	Sáez	Escudero	f	1968-10-08	01-01-22-0001-0028	f
7428020	Guillermo	Jaime	Andrés	Marrero	m	1956-11-23	01-01-22-0001-0028	f
11825079	Clara	Carlota	Alonzo	Serna	f	1957-06-26	01-01-22-0001-0028	f
5018504	Julia	Africa	Marcos	Espinosa	f	1954-04-01	01-01-22-0001-0028	f
14855457	Alexandra	Marta	Corona	Valenzuela	f	1955-09-12	01-01-22-0001-0028	f
11820357	Zulay	Elena	Reyna	Cardona	f	1966-07-14	01-01-22-0001-0028	f
11065666	David	Jordi	Caraballo	Córdoba	m	1953-08-08	01-01-22-0001-0028	f
8531345	Ian	Jaime	Santillán	Aguilar	m	1964-01-19	01-01-22-0001-0028	f
11660869	Zulay	Silvia	Vera	Riojas	f	1964-10-10	01-01-22-0001-0028	f
5665504	Jimena	Ane	Carretero	Bernal	f	1954-06-08	01-01-22-0001-0028	f
5572626	Victoria	Julia	Trujillo	Gastélum	f	1960-09-26	01-01-22-0001-0028	f
9031706	Cristina	Yaiza	Yáñez	Terrazas	f	1952-04-28	01-01-22-0001-0028	f
11562640	Oliver	Ian	Serra	Carbajal	m	1964-11-15	01-01-22-0001-0028	f
12686413	Miguel	Jan	Calvillo	Bueno	m	1969-11-11	01-01-22-0001-0028	f
14305631	Cesar	Juan	Palomino	Estrada	m	1964-03-02	01-01-22-0001-0028	f
10300061	Adam	Jordi	Iglesias	Meléndez	m	1966-02-21	01-01-22-0001-0028	f
9255152	Miriam	Blanca	Báez	Alanis	f	1961-07-23	01-01-22-0001-0028	f
8408818	Oswaldo	Hugo	Plaza	Mares	m	1955-05-20	01-01-22-0001-0028	f
7821765	Lola	Mireia	Bermúdez	Chapa	f	1953-09-22	01-01-22-0001-0028	f
13127782	Zulay	Alejandra	Quezada	Gamboa	f	1954-09-11	01-01-22-0001-0028	f
12562036	Diana	Nuria	Bermejo	Ruiz	f	1959-05-05	01-01-22-0001-0028	f
13083102	Andres	Santiago	Sedillo	Nájera	m	1962-07-30	01-01-22-0001-0028	f
14743737	Elsa	Jimena	Rey	Del río	f	1962-08-11	01-01-22-0001-0028	f
7427282	Anna	Nil	Gracia	Anaya	f	1959-09-22	01-01-22-0001-0028	f
12425941	Carlota	Manuela	Villegas	Tafoya	f	1966-10-28	01-01-22-0001-0028	f
14839675	Claudia	Alicia	Mendoza	Álvarez	f	1969-10-24	01-01-22-0001-0028	f
5616750	Lola	Nayara	Cedillo	Castaño	f	1966-03-22	01-01-22-0001-0028	f
12569217	Marc	Francisco	Vigil	Calvo	m	1955-04-26	01-01-22-0001-0028	f
5963841	Gabriel	Manuel	Figueroa	Naranjo	m	1963-01-24	01-01-22-0001-0028	f
10197142	Jose	Ivan	Barrios	Matías	m	1957-11-13	01-01-22-0001-0028	f
6897047	Fernando	Yorman	Rodarte	Barreto	m	1967-12-31	01-01-22-0001-0028	f
6624195	Alicia	Nora	Calero	Garibay	f	1967-06-13	01-01-22-0001-0028	f
10200056	Daniela	Nadia	Casillas	Huerta	f	1968-12-15	01-01-22-0001-0028	f
8585092	Dario	Martin	Aragón	Corona	m	1968-12-14	01-01-22-0001-0028	f
7812368	Salma	Blanca	Covarrubias	Gaona	f	1961-08-26	01-01-22-0001-0028	f
8558431	Joan	Pau	Lucas	Jaimes	f	1961-05-19	01-01-22-0001-0028	f
12217868	Alma	Carmen	Bahena	Solís	f	1968-05-13	01-01-22-0001-0028	f
9083332	Dario	Aaron	Olivares	Peralta	m	1961-11-17	01-01-22-0001-0028	f
9546315	Marti	Lidia	Dueñas	Toledo	f	1956-03-15	01-01-22-0001-0028	f
9073491	Carolina	Noa	Jiménez	Guerra	f	1963-11-16	01-01-22-0001-0028	f
13329625	Valeria	Manuela	Terrazas	Soler	f	1961-06-10	01-01-22-0001-0028	f
9523561	Yorman	Jesus	Almanza	Alcántar	m	1963-07-17	01-01-22-0001-0028	f
14062833	Bruno	Leonardo	Barraza	Rentería	m	1966-11-23	01-01-22-0001-0028	f
6590799	Ruben	Nicolas	Toro	Pineda	m	1964-12-29	01-01-22-0001-0028	f
5936450	Nadia	Alicia	Carballo	Lugo	f	1966-04-18	01-01-22-0001-0028	f
12275094	Africa	Yaiza	Preciado	Gallego	f	1961-01-13	01-01-22-0001-0028	f
14520170	Carlota	Anna	Ortiz	Flórez	f	1953-08-05	01-01-22-0001-0028	f
8504721	Alejandro	Jon	Girón	Quezada	m	1969-06-05	01-01-22-0001-0028	f
7980476	Teresa	Bella	Duarte	Mota	f	1961-08-26	01-01-22-0001-0028	f
14888880	Omar	Enrique	Villanueva	Martínez	m	1954-08-01	01-01-22-0001-0028	f
6792402	Eduardo	Jose	Gamez	Mora	m	1969-11-30	01-01-22-0001-0028	f
6761646	Lucas	Ismael	Acosta	Gonzales	m	1960-02-27	01-01-22-0001-0028	f
11399404	Izan	Alejandro	Soler	Valle	m	1959-02-13	01-01-22-0001-0028	f
8591187	Nora	Erika	Elizondo	Armas	f	1956-08-28	01-01-22-0001-0028	f
8027717	Pablo	Rafael	Camarillo	Puga	m	1962-03-31	01-01-22-0001-0028	f
10143774	Marina	Clara	Galarza	Gamboa	f	1961-10-13	01-01-22-0001-0028	f
7476905	Mar	Marina	Valero	Amador	f	1967-04-12	01-01-22-0001-0028	f
5762151	Miguel	Adam	Mondragón	García	m	1966-06-16	01-01-22-0001-0028	f
10175825	Vera	Nerea	Figueroa	Ibáñez	f	1954-02-09	01-01-22-0001-0028	f
8173821	Gabriela	Jana	Segura	Pantoja	f	1954-08-06	01-01-22-0001-0028	f
12264972	Andrea	Abril	Saucedo	Tafoya	f	1956-02-22	01-01-22-0001-0028	f
5825045	Fatima	Alexandra	Moya	Carrión	f	1963-02-17	01-01-22-0001-0028	f
7288403	Alexia	Mara	Ozuna	Urías	f	1957-12-21	01-01-22-0001-0028	f
8261529	Ismael	Victor	Alva	Avilés	m	1965-07-20	01-01-22-0001-0028	f
10624331	Lidia	Laura	Salazar	Escobar	f	1967-08-29	01-01-22-0001-0028	f
10661730	Eduardo	Rafael	Nájera	Leyva	m	1968-06-30	01-01-22-0001-0028	f
14618308	Marc	Jordi	Mojica	Hernádez	m	1968-11-05	01-01-22-0001-0028	f
5439895	Adam	Martin	Padrón	Almaraz	m	1961-04-10	01-01-22-0001-0028	f
12669527	Nuria	Victoria	Tirado	Córdova	f	1955-05-24	01-01-22-0001-0028	f
11271160	Ivan	Javier	De Anda	Farías	m	1962-07-18	01-01-22-0001-0028	f
7266185	Adria	Eric	Peres	Cadena	m	1954-08-08	01-01-22-0001-0028	f
11278314	Sofia	Valentina	Ceja	Miguel	f	1958-06-07	01-01-22-0001-0028	f
9450688	Raquel	Luisa	Barraza	Mas	f	1957-02-22	01-01-22-0001-0028	f
6456934	Ainara	Alicia	Roldán	Gallego	f	1960-05-20	01-01-22-0001-0028	f
6586379	Manuel	Angel	Leyva	Rolón	m	1953-09-14	01-01-22-0001-0028	f
6118911	Manuela	Candela	Velasco	Valero	f	1956-09-13	01-01-22-0001-0028	f
9739238	Martin	Hector	Arroyo	Yáñez	m	1967-03-17	01-01-22-0001-0028	f
5843406	Joan	Cristina	Gastélum	Maldonado	f	1963-10-21	01-01-22-0001-0028	f
8247715	Marco	Marcos	Paredes	Orosco	m	1967-12-03	01-01-22-0001-0028	f
10654075	Eva	Erika	Crespo	Montenegro	f	1957-11-13	01-01-22-0001-0028	f
12502692	Daniel	Ivan	Ruelas	Flórez	m	1954-04-18	01-01-22-0001-0028	f
12444714	Naiara	Miriam	Adorno	Enríquez	f	1960-10-22	01-01-22-0001-0028	f
14906760	Pau	Rocio	Lorenzo	Barraza	f	1968-04-22	01-01-22-0001-0028	f
12570326	Silvia	Rocio	Garza	Araña	f	1968-06-02	01-01-22-0001-0028	f
8985500	Noa	Miriam	Ávila	Palomo	f	1969-05-22	01-01-22-0001-0028	f
14907981	Alberto	Alvaro	Luque	Benito	m	1956-12-16	01-01-22-0001-0028	f
5838388	Andres	Omar	Negrón	Toro	m	1957-01-07	01-01-22-0001-0028	f
12448036	Adrian	Ander	Meléndez	Ríos	m	1964-06-04	01-01-22-0001-0028	f
9830073	Valeria	Rocio	Ojeda	Villalpando	f	1964-01-17	01-01-22-0001-0028	f
8016421	Pedro	Alonso	Blázquez	Márquez	m	1955-10-07	01-01-22-0001-0028	f
9443669	Carmen	Fatima	Lozano	Soliz	f	1956-09-20	01-01-22-0001-0028	f
7776819	Helena	Victoria	Salcido	Garica	f	1958-03-07	01-01-22-0001-0028	f
9728836	Lucas	Eduardo	Simón	Espino	m	1957-05-16	01-01-22-0001-0028	f
6302112	Angel	Guillem	Juan	Cornejo	m	1956-06-22	01-01-22-0001-0028	f
10055237	Roberto	Miguel	Lemus	Rincón	m	1956-08-04	01-01-22-0001-0028	f
5803126	Martin	Fernando	Hidalgo	Moral	m	1957-12-06	01-01-22-0001-0028	f
11446525	Oswaldo	Lucas	Rosario	Román	m	1969-02-19	01-01-22-0001-0028	f
13897933	Mireia	Alicia	Carvajal	Henríquez	f	1955-05-02	01-01-22-0001-0028	f
14847120	Marcos	Miguel	Romero	Crespo	m	1963-06-02	01-01-22-0001-0028	f
7505594	Mireia	Candela	Salazar	Navas	f	1963-01-01	01-01-22-0001-0028	f
10866190	Bruno	Adam	Arguello	Rocha	m	1954-05-27	01-01-22-0001-0028	f
14864265	Luna	Nadia	Lara	Zamudio	f	1955-02-02	01-01-22-0001-0028	f
14040001	Rocio	Lara	Quesada	Santacruz	f	1962-08-14	01-01-22-0001-0028	f
12747423	Marc	Omar	Balderas	Cornejo	m	1960-03-24	01-01-22-0001-0028	f
9682788	Ariadna	Nora	Esteban	Samaniego	f	1964-03-29	01-01-22-0001-0028	f
6809816	Miguel	Hector	Chavarría	Gurule	m	1955-06-30	01-01-22-0001-0028	f
8920347	Guillermo	Ian	Burgos	Carrion	m	1960-07-05	01-01-22-0001-0028	f
13753303	Cesar	Jaime	Nieto	Mayorga	m	1954-11-21	01-01-22-0001-0028	f
10741505	Roberto	Guillermo	Lucero	Armas	m	1963-11-26	01-01-22-0001-0028	f
9502719	Miriam	Africa	Rocha	Armenta	f	1955-02-18	01-01-22-0001-0028	f
5976667	Diana	Valentina	Reyes	Perea	f	1953-10-05	01-01-22-0001-0028	f
9456327	Pol	Eric	Olivo	Cardona	m	1958-07-01	01-01-22-0001-0028	f
6722646	Marti	Salma	Granados	Cabán	f	1952-09-21	01-01-22-0001-0028	f
5891426	Rafael	Leandro	Ramos	Pereira	m	1959-11-12	01-01-22-0001-0028	f
8949811	Elena	Alba	Rolón	Gurule	f	1961-11-27	01-01-22-0001-0028	f
6088541	Ane	Blanca	Fonseca	Delgadillo	f	1965-11-16	01-01-22-0001-0028	f
7678858	Ines	Paula	Salazar	Serra	f	1965-04-24	01-01-22-0001-0028	f
13919495	Marina	Mireia	Linares	Ibáñez	f	1957-05-08	01-01-22-0001-0028	f
10401989	Pedro	Izan	Perales	Hernandes	m	1958-09-09	01-01-22-0001-0028	f
6323019	Ismael	Cristian	Lozada	Arredondo	m	1969-02-22	01-01-22-0001-0028	f
11276813	Miriam	Blanca	Polo	Hernando	f	1969-03-26	01-01-22-0001-0028	f
5282917	Noelia	Marta	Santacruz	Abreu	f	1965-12-15	01-01-22-0001-0028	f
10551661	Jana	Naiara	Pagan	Verdugo	f	1967-10-02	01-01-22-0001-0028	f
7877530	Mateo	Sergio	Ornelas	Naranjo	m	1960-10-16	01-01-22-0001-0028	f
9864356	David	Lucas	Pereira	Olmos	m	1953-05-03	01-01-22-0001-0028	f
7932051	Guillem	Bruno	Santillán	Sarabia	m	1965-06-12	01-01-22-0001-0028	f
10169111	Noa	Nadia	Barraza	Muñiz	f	1953-08-09	01-01-22-0001-0028	f
10677871	Miguel	Ivan	Hidalgo	Olivas	m	1967-02-11	01-01-22-0001-0028	f
11879286	Alberto	Miguel	Paredes	Aguilar	m	1969-08-26	01-01-22-0001-0028	f
5795165	Saul	Adrian	Fierro	Verduzco	m	1963-08-16	01-01-22-0001-0028	f
9553868	Alvaro	Ismael	Velázquez	Armijo	m	1954-11-01	01-01-22-0001-0028	f
12932854	Isabel	Miriam	Muñiz	Pagan	f	1963-02-05	01-01-22-0001-0028	f
6061292	Mario	Leonardo	Acevedo	Lucas	m	1959-10-10	01-01-22-0001-0028	f
10700496	Nerea	Sara	Valladares	Olivera	f	1952-02-29	01-01-22-0001-0028	f
14732919	Marina	Vega	Ros	Lara	f	1963-10-13	01-01-22-0001-0028	f
13506334	Nerea	Alejandra	Porras	Muro	f	1967-09-15	01-01-22-0001-0028	f
8091768	Adria	Francisco	Arce	Ríos	m	1956-12-08	01-01-22-0001-0028	f
14560659	Samuel	Juan	Alicea	Arenas	m	1953-12-08	01-01-22-0001-0028	f
5874180	Alma	Noelia	Soto	Páez	f	1952-01-04	01-01-22-0001-0028	f
5348312	Rodrigo	Antonio	Garibay	Robledo	m	1964-05-04	01-01-22-0001-0028	f
11495355	Yorman	Alex	Godoy	Chacón	m	1957-10-24	01-01-22-0001-0028	f
6951231	Enrique	Ismael	Aragón	Salcido	m	1962-06-26	01-01-22-0001-0028	f
9489145	Oliver	Lucas	Barroso	Santillán	m	1954-04-01	01-01-22-0001-0028	f
12088025	Alex	Cesar	Escamilla	Sauceda	m	1962-02-01	01-01-22-0001-0028	f
14094098	Leyre	Nahia	Robles	Leiva	f	1956-05-27	01-01-22-0001-0028	f
12750836	Alex	Pol	Nieves	Toro	m	1955-08-03	01-01-22-0001-0028	f
8754147	Salma	Iria	Sosa	Gómez	f	1965-02-25	01-01-22-0001-0028	f
5208320	Antonio	Hector	Zúñiga	Quintero	m	1964-02-29	01-01-22-0001-0028	f
10875174	Erik	Mario	Camarillo	Guevara	m	1959-12-11	01-01-22-0001-0028	f
13448202	Valentina	Naiara	Delapaz	Camarillo	f	1967-07-16	01-01-22-0001-0028	f
14178589	Eva	Alexia	Chacón	Montalvo	f	1960-10-29	01-01-22-0001-0028	f
12229068	Emma	Elsa	Águilar	Mesa	f	1956-08-29	01-01-22-0001-0028	f
10615102	Fernando	Oliver	Carrión	Aguayo	m	1962-02-03	01-01-22-0001-0028	f
7438263	Victor	Jesus	Cisneros	Rodríquez	m	1954-01-24	01-01-22-0001-0028	f
13505814	Omar	Aaron	Madera	Serra	m	1966-01-19	01-01-22-0001-0028	f
9263336	Alex	Mario	Torres	Vicente	m	1958-06-23	01-01-22-0001-0028	f
14465396	Sara	Olivia	Flórez	Escobedo	f	1965-01-27	01-01-22-0001-0028	f
9442074	Adam	Santiago	Salas	Valenzuela	m	1957-05-25	01-01-22-0001-0028	f
9402525	Gabriela	Miriam	Sola	Cordero	f	1959-02-26	01-01-22-0001-0028	f
14877951	Oswaldo	Izan	Aguirre	Quintanilla	m	1969-06-06	01-01-22-0001-0028	f
12117720	Antonia	Ines	Herrera	Caraballo	f	1962-02-02	01-01-22-0001-0028	f
9625919	Antonia	Nadia	Urrutia	Sáez	f	1964-10-27	01-01-22-0001-0028	f
6642083	Martin	Cristian	Reynoso	Simón	m	1962-07-28	01-01-22-0001-0028	f
8989412	Yorman	Francisco Javier	Martín	Cardenas	m	1968-11-20	01-01-22-0001-0028	f
5840210	Vega	Ane	Mateos	Serrano	f	1968-12-06	01-01-22-0001-0028	f
6598877	Ruben	Luis	Abrego	Alicea	m	1964-04-13	01-01-22-0001-0028	f
10505538	Alonso	Erik	Roque	Zamora	m	1964-03-02	01-01-22-0001-0028	f
8700156	Aleix	Ismael	Lomeli	Cordero	m	1956-03-23	01-01-22-0001-0028	f
5836609	Ona	Silvia	González	Bustos	f	1956-04-02	01-01-22-001-0079	f
10922037	Naiara	Ona	Izquierdo	Mas	f	1953-03-28	01-01-22-001-0079	f
5530881	Carlos	Santiago	Tello	Cuevas	m	1958-04-24	01-01-22-001-0079	f
12326152	Naiara	Claudia	Verdugo	Blázquez	f	1964-05-09	01-01-22-001-0079	f
6977984	Ariadna	Pau	Pereira	Rodríquez	f	1956-05-05	01-01-22-001-0079	f
12540553	Lola	Iria	Sancho	Mondragón	f	1961-12-30	01-01-22-001-0079	f
8484940	Adriana	Alba	Nieto	Anguiano	f	1954-03-08	01-01-22-001-0079	f
14796705	Joel	Ivan	Partida	Sevilla	m	1968-10-06	01-01-22-001-0079	f
11428199	Nicolas	Adrian	Vásquez	Vargas	m	1965-09-12	01-01-22-001-0079	f
6464519	Pedro	Jorge	Alemán	Esteve	m	1969-12-31	01-01-22-001-0079	f
7556891	Angela	Celia	Casárez	Puente	f	1967-11-04	01-01-22-001-0079	f
12598802	Lola	Andrea	Varela	Macías	f	1958-07-20	01-01-22-001-0079	f
14628941	David	Ian	Gracia	Alfaro	m	1954-10-29	01-01-22-001-0079	f
14886225	Adria	Jon	Cardenas	Medina	m	1958-12-25	01-01-22-001-0079	f
10932383	Ian	Rayan	Uribe	Olivas	m	1963-04-09	01-01-22-001-0079	f
5038708	Sara	Yaiza	Carrillo	Andreu	f	1955-08-14	01-01-22-001-0079	f
10092852	Nicolas	Francisco Javier	Perea	Fierro	m	1961-08-30	01-01-22-001-0079	f
10091761	Jimena	Leyre	Quiñones	Cadena	f	1966-02-09	01-01-22-001-0079	f
8619689	Ariadna	Nahia	Jiménez	Reina	f	1962-02-28	01-01-22-001-0079	f
5237125	Marta	Laura	Silva	Tovar	f	1960-09-26	01-01-22-001-0079	f
14236437	Jan	Marcos	Román	Cano	m	1968-02-18	01-01-22-001-0079	f
6945195	Vera	Lidia	Medrano	Soliz	f	1965-06-08	01-01-22-001-0079	f
12812443	Candela	Carmen	Miramontes	Rolón	f	1955-07-22	01-01-22-001-0079	f
10686165	Francisco	Izan	Rivera	Ortíz	m	1967-04-02	01-01-22-001-0079	f
9727849	Oscar	Enrique	Bueno	Bueno	m	1969-01-21	01-01-22-001-0079	f
9614220	Cristina	Leyre	Navas	Vigil	f	1961-02-15	01-01-22-001-0079	f
6256368	Jon	Ismael	De la torre	Villaseñor	m	1959-04-07	01-01-22-001-0079	f
5980322	Gabriela	Miriam	Abeyta	Sosa	f	1965-02-06	01-01-22-001-0079	f
13135679	Jimena	Alicia	Lerma	Urías	f	1952-11-01	01-01-22-001-0079	f
11262552	Nahia	Jana	Ortega	Tovar	f	1966-02-23	01-01-22-001-0079	f
10699354	Iria	Clara	Tafoya	Sosa	f	1954-06-01	01-01-22-001-0079	f
14074800	Nicolas	Martin	Castaño	Zapata	m	1955-01-04	01-01-22-001-0079	f
13236284	Abril	Teresa	Limón	Almanza	f	1959-01-20	01-01-22-001-0079	f
11178502	Vera	Anna	Vargas	Garrido	f	1960-06-28	01-01-22-001-0079	f
13798676	Bella	Anna	Acosta	Hernández	f	1953-09-09	01-01-22-001-0079	f
14944201	Lucas	Daniel	Espinosa	Armendáriz	m	1955-12-29	01-01-22-001-0079	f
8502329	Roberto	Isaac	Tirado	Perea	m	1967-01-15	01-01-22-001-0079	f
10919513	Alvaro	Hugo	Carrasquillo	Pelayo	m	1964-07-22	01-01-22-001-0079	f
9893396	Lara	Zulay	Diez	Posada	f	1969-10-29	01-01-22-001-0079	f
9605675	Eva	Iria	Bravo	Cruz	f	1965-11-20	01-01-22-001-0079	f
11458954	Manuela	Emma	Pantoja	Correa	f	1956-04-17	01-01-22-001-0079	f
13630603	Manuela	Natalia	Rosas	Ozuna	f	1956-05-12	01-01-22-001-0079	f
12633844	Jaime	Aleix	Limón	Cervántez	m	1955-04-20	01-01-22-001-0079	f
9959036	Nil	Nerea	Expósito	Vergara	f	1956-02-04	01-01-22-001-0079	f
6859005	Alma	Jimena	Olivera	Díaz	f	1963-03-30	01-01-22-001-0079	f
8973091	Manuela	Gabriela	Peláez	Echevarría	f	1951-11-11	01-01-22-001-0079	f
10763279	Ian	Mateo	Rivas	Palomo	m	1968-12-14	01-01-22-001-0079	f
13461821	Leyre	Nora	Aragón	Nieto	f	1964-12-07	01-01-22-001-0079	f
12849315	Mara	Nora	Galarza	Gaytán	f	1952-04-01	01-01-22-001-0079	f
8481570	Eric	Ander	Benítez	Lucio	m	1969-07-08	01-01-22-001-0079	f
5220670	Ignacio	Pol	Serrano	Andreu	m	1964-03-29	01-01-22-001-0079	f
5052400	Javier	Mohamed	Mata	Pedroza	m	1969-04-18	01-01-22-001-0079	f
14314499	Adam	Omar	Perea	Carrillo	m	1962-11-10	01-01-22-001-0079	f
5250851	Ane	Teresa	Meraz	Montemayor	f	1959-07-04	01-01-22-001-0079	f
9559779	Cristian	Manuel	Cuenca	Cervantes	m	1962-10-16	01-01-22-001-0079	f
12668715	Lucas	Rayan	Redondo	Llorente	m	1955-02-13	01-01-22-001-0079	f
11115895	Aaron	Manuel	Caraballo	Sancho	m	1969-08-29	01-01-22-001-0079	f
5896540	Alejandro	Carlos	Orosco	Guevara	m	1959-12-06	01-01-22-001-0079	f
6179582	Carlos	Angel	Sanches	Salinas	m	1960-05-08	01-01-22-001-0079	f
8254732	Elsa	Claudia	Leal	Solís	f	1959-09-14	01-01-22-001-0079	f
10893408	Enrique	Victor	Soria	Tórrez	m	1967-09-14	01-01-22-001-0079	f
8398106	Alberto	Oswaldo	Montemayor	Elizondo	m	1955-04-21	01-01-22-001-0079	f
10869272	Naiara	Noelia	Perea	Martí	f	1962-05-20	01-01-22-001-0079	f
13017959	Ana	Nerea	Méndez	Salazar	f	1954-03-10	01-01-22-001-0079	f
8645800	Samuel	Ander	Salas	Barraza	m	1957-01-08	01-01-22-001-0079	f
5515096	Sergio	Martin	Gurule	Delrío	m	1958-07-26	01-01-22-001-0079	f
6292249	Manuela	Helena	Puig	Ortega	f	1966-04-12	01-01-22-001-0079	f
5620243	Noelia	Daniela	Ontiveros	Domínquez	f	1969-04-21	01-01-22-001-0079	f
10979779	Lola	Eva	Tomas	Ceja	f	1952-10-15	01-01-22-001-0079	f
14075299	Lola	Adriana	Barajas	Camacho	f	1967-07-08	01-01-22-001-0079	f
9956824	Erika	Zulay	Puig	Covarrubias	f	1953-02-04	01-01-22-001-0079	f
11107964	Alexandra	Alba	Salazar	Ruiz	f	1955-12-01	01-01-22-001-0079	f
9712754	Noelia	Lola	Llorente	Costa	f	1961-05-02	01-01-22-001-0079	f
5005960	Naiara	Lidia	Juan	Martos	f	1958-07-28	01-01-22-001-0079	f
9891267	Aroa	Carlota	Mendoza	Terán	f	1958-05-08	01-01-22-001-0079	f
5233003	Ainara	Antonia	Vélez	Quezada	f	1955-04-11	01-01-22-001-0079	f
8425440	Hugo	Raul	Navarro	Pons	m	1957-02-02	01-01-22-001-0079	f
5408440	Gabriela	Victoria	Olvera	Herrera	f	1961-05-01	01-01-22-001-0079	f
9140274	Cesar	Jordi	Solorio	Espinal	m	1960-12-11	01-01-22-001-0079	f
6893209	Luis	Angel	Miramontes	Yáñez	m	1960-04-26	01-01-22-001-0079	f
12217411	Alma	Paula	Santos	Serna	f	1957-03-22	01-01-22-001-0079	f
7758640	Sebastian	Oscar	Rey	Cortez	m	1967-10-28	01-01-22-001-0079	f
11511836	Luisa	Nerea	Gimeno	Barraza	f	1955-10-21	01-01-22-001-0079	f
12509331	Dario	Domingo	Castro	Toro	m	1952-08-05	01-01-22-001-0079	f
14110477	Ander	Adria	Muñóz	Jurado	m	1960-09-23	01-01-22-001-0079	f
14902980	Nuria	Claudia	Urías	Parra	f	1965-04-12	01-01-22-001-0079	f
8571273	Alma	Beatriz	Roca	Villanueva	f	1964-08-22	01-01-22-001-0079	f
10284035	Enrique	Marco	Collado	López	m	1956-04-20	01-01-22-001-0079	f
7393846	Lola	Fatima	Montes	Mateos	f	1964-01-07	01-01-22-001-0079	f
5586037	Rafael	Cristian	Aparicio	Fierro	m	1961-07-17	01-01-22-001-0079	f
12422453	Alonso	Pedro	Flores	Villalba	m	1958-12-29	01-01-22-001-0079	f
9379078	Ana	Zulay	Alonzo	Zavala	f	1967-09-19	01-01-22-001-0079	f
13113040	Samuel	Rodrigo	Hinojosa	Carrera	m	1967-10-22	01-01-22-001-0079	f
13784629	Adam	Dario	Burgos	Pérez	m	1958-01-01	01-01-22-001-0079	f
14610089	Raquel	Vera	González	Vásquez	f	1952-02-22	01-01-22-001-0079	f
7374853	Rafael	Andres	Escribano	Méndez	m	1964-03-24	01-01-22-001-0079	f
11198903	Alicia	Salma	Redondo	Barela	f	1966-02-26	01-01-22-001-0079	f
13206077	Erik	Fernando	Rentería	Aparicio	m	1965-12-26	01-01-22-001-0079	f
14974988	Eduardo	Jaime	Córdoba	Gallegos	m	1960-10-19	01-01-22-001-0079	f
13739098	Joel	Jan	Baca	Nazario	m	1959-08-30	01-01-22-001-0079	f
12560459	Marcos	Alvaro	Sanabria	Velasco	m	1964-05-20	01-01-22-001-0079	f
8072679	Rodrigo	Jordi	Urbina	Zavala	m	1969-05-28	01-01-22-001-0079	f
6316884	Antonia	Julia	Prieto	Ocasio	f	1969-10-19	01-01-22-001-0079	f
11914703	Santiago	Roberto	Duarte	Requena	m	1957-05-20	01-01-22-001-0079	f
10776720	Ian	Isaac	Urías	Tijerina	m	1954-01-08	01-01-22-001-0079	f
8004888	Rodrigo	Mohamed	Rael	Deleón	m	1959-08-07	01-01-22-001-0079	f
11391070	Mario	Marco	Domínquez	Cisneros	m	1964-11-08	01-01-22-001-0079	f
9406716	Martin	Joel	Mascareñas	Soliz	m	1964-09-25	01-01-22-001-0079	f
9223244	Erik	Ander	Valero	Sosa	m	1954-05-25	01-01-22-001-0079	f
8336797	Luna	Valentina	Solorio	Cornejo	f	1967-11-13	01-01-22-001-0079	f
5226211	Zulay	Fatima	Casanova	Salcedo	f	1952-04-20	01-01-22-001-0079	f
14200403	Jon	Jaime	Montaño	Caraballo	m	1966-07-28	01-01-22-001-0079	f
14816521	Leire	Olivia	León	Avilés	f	1962-03-02	01-01-22-001-0079	f
13380864	Nicolas	Jose	Lira	Briones	m	1958-04-28	01-01-22-001-0079	f
14404403	Javier	Dario	Melgar	Centeno	m	1959-02-19	01-01-22-001-0079	f
13505549	Candela	Alicia	Rincón	Reséndez	f	1952-08-07	01-01-22-001-0079	f
6660966	Hector	Alejandro	Espinoza	Orta	m	1963-03-21	01-01-22-001-0079	f
11869315	Lola	Aroa	Arreola	Serrano	f	1963-04-11	01-01-22-001-0079	f
5663643	Francisco Javier	Hector	Uribe	Chávez	m	1966-05-02	01-01-22-001-0079	f
9765078	Ander	Enrique	Santacruz	Sierra	m	1968-03-28	01-01-22-001-0079	f
10268570	Rayan	Rayan	Pichardo	Pulido	m	1960-05-21	01-01-22-001-0079	f
7815100	Ian	Aleix	Cantú	Andrés	m	1953-09-28	01-01-22-001-0079	f
11684545	Oswaldo	Leo	Arroyo	Montero	m	1953-01-19	01-01-22-001-0079	f
14431820	Silvia	Raquel	Olivera	Mondragón	f	1961-04-03	01-01-22-001-0079	f
10162333	Sofia	Candela	Leal	Raya	f	1964-11-11	01-01-22-001-0079	f
13614714	Saul	Sergio	Saavedra	García	m	1951-11-23	01-01-22-001-0079	f
9229748	Gonzalo	Pablo	Guardado	Esteban	m	1957-05-15	01-01-22-001-0079	f
6832117	Alexia	Ariadna	Carvajal	Treviño	f	1958-11-30	01-01-22-001-0079	f
14523469	Ruben	Ignacio	Ortega	Díaz	m	1959-02-02	01-01-22-001-0079	f
14024137	Eduardo	Angel	Arteaga	Pagan	m	1967-08-07	01-01-22-001-0079	f
10300633	Alicia	Silvia	Ornelas	Moreno	f	1958-12-02	01-01-22-001-0079	f
7817541	Eva	Sara	Preciado	Esteban	f	1960-11-23	01-01-22-001-0079	f
12323626	Alba	Ines	Cazares	Olivares	f	1969-04-20	01-01-22-001-0079	f
13119176	Leo	Mateo	Olivárez	Reyes	m	1958-05-16	01-01-22-001-0079	f
11042581	Guillermo	Adam	Godoy	Mondragón	m	1966-03-29	01-01-22-001-0079	f
13895479	Daniela	Clara	Ozuna	Tomas	f	1965-09-15	01-01-22-001-0079	f
9306349	Luisa	Maria	Villagómez	Mota	f	1966-01-10	01-01-22-001-0079	f
13111862	Domingo	Pedro	Colón	Lucero	m	1964-02-10	01-01-22-001-0079	f
9298169	Helena	Joan	Vidal	Ruelas	f	1954-12-05	01-01-22-001-0079	f
5728264	Cesar	Alberto	Perea	Olvera	m	1953-12-08	01-01-22-001-0079	f
13515635	Luisa	Alba	Olivárez	Alicea	f	1966-06-17	01-01-22-001-0079	f
6982348	Sergio	Ismael	Leiva	Salgado	m	1964-12-06	01-01-22-001-0079	f
11778206	Adam	Bruno	Rangel	Urrutia	m	1956-01-19	01-01-22-001-0079	f
14581653	Cristian	Izan	Alicea	Botello	m	1963-10-10	01-01-22-001-0079	f
9714678	Nicolas	Eduardo	Urrutia	Zambrano	m	1965-05-16	01-01-22-001-0079	f
9852293	Andrea	Alicia	Maya	Esquivel	f	1968-07-02	01-01-22-001-0079	f
7194278	Eva	Laura	Silva	Pagan	f	1959-10-17	01-01-22-001-0079	f
14890004	Rafael	Pablo	Tovar	Puig	m	1963-08-13	01-01-22-001-0079	f
10350978	Adam	Yorman	Barragán	Montaño	m	1964-04-01	01-01-22-001-0079	f
9940172	Elsa	Sandra	Pascual	Corona	f	1953-06-27	01-01-22-001-0079	f
6865978	Jaime	Marc	Quintero	Rodrigo	m	1953-02-08	01-01-22-001-0079	f
12697589	Anna	Rocio	Riera	Coronado	f	1964-07-12	01-01-22-001-0079	f
12256944	Beatriz	Pau	Cortez	Rodríquez	f	1969-08-15	01-01-22-001-0079	f
6917735	Izan	Hugo	Montemayor	Casillas	m	1959-06-12	01-01-22-001-0079	f
7330518	Adria	Oliver	Casanova	Olmos	m	1965-06-09	01-01-22-001-0079	f
6397591	Jimena	Naia	Mercado	Romero	f	1954-09-23	01-01-22-001-0079	f
12359381	Patricia	Diana	Rodrigo	Carrillo	f	1968-08-25	01-01-22-001-0079	f
8253828	Jordi	Francisco	Valladares	Roybal	m	1957-12-07	01-01-22-001-0079	f
9969433	Pol	Isaac	Huerta	Salinas	m	1963-11-01	01-01-22-001-0079	f
5436877	Victor	Andres	Frías	Ruvalcaba	m	1963-04-30	01-01-22-001-0079	f
12325814	Jana	Vera	Rico	Valdés	f	1966-07-30	01-01-22-001-0079	f
13850652	Ignacio	Sebastian	Guillen	Rendón	m	1967-01-30	01-01-22-001-0079	f
8995761	Alberto	Lucas	Contreras	Santillán	m	1954-03-20	01-01-22-001-0079	f
6711450	Berta	Sandra	Garica	Aparicio	f	1968-02-27	01-01-22-001-0079	f
10128556	Enrique	Leandro	Ferrer	Abrego	m	1952-10-19	01-01-22-001-0079	f
14495448	Alba	Nil	Vigil	Alonso	f	1969-07-07	01-01-22-001-0079	f
5633106	Hector	Yorman	Mesa	Expósito	m	1956-03-08	01-01-22-001-0079	f
5443293	Hector	Cesar	Medina	Becerra	m	1955-06-23	01-01-22-001-0079	f
13988595	Marta	Nuria	Aparicio	Toro	f	1952-08-19	01-01-22-001-0079	f
8107911	Bruno	Nicolas	Vila	Calvo	m	1960-05-29	01-01-22-001-0079	f
7017613	Berta	Joan	Roldan	Porras	f	1962-11-15	01-01-22-001-0079	f
13830447	Ana	Eva	Tamez	Alcaraz	f	1953-09-01	01-01-22-001-0079	f
9349078	Yaiza	Jimena	Bautista	Sauceda	f	1955-08-12	01-01-22-001-0079	f
11427547	Angel	Marcos	Valles	Blasco	m	1968-09-12	01-01-22-001-0079	f
5132797	Jordi	Saul	Gracia	Puente	m	1952-06-01	01-01-22-001-0079	f
11511090	Ismael	Ignacio	Ramón	Arteaga	m	1960-12-01	01-01-22-001-0079	f
14980676	Enrique	Rayan	Cantú	Arreola	m	1956-12-02	01-01-22-001-0079	f
12572138	Alonso	Oscar	Rendón	Paredes	m	1951-11-06	01-01-22-001-0079	f
6875444	Noelia	Clara	Moreno	Rosario	f	1965-06-28	01-01-22-001-0079	f
14824326	Celia	Eva	Deleón	Guzmán	f	1953-11-15	01-01-22-001-0079	f
10840040	Gabriela	Nil	Jáquez	Ruelas	f	1957-03-12	01-01-22-001-0079	f
9341843	Santiago	Jesus	Godínez	Fernández	m	1957-11-20	01-01-22-001-0079	f
7200677	Laura	Silvia	Montes	Cabello	f	1952-12-29	01-01-22-001-0079	f
13001162	Angel	Miguel	Puig	Olivares	m	1968-12-31	01-01-22-001-0079	f
14932392	Samuel	Marco	Cruz	Sáenz	m	1963-01-01	01-01-22-001-0079	f
10820283	Cristian	Lucas	Páez	Almaraz	m	1967-11-17	01-01-22-001-0079	f
6470134	Nuria	Paula	Alcaraz	Aragón	f	1963-01-24	01-01-22-001-0079	f
14134223	Vega	Ane	Muro	Pabón	f	1954-06-02	01-01-22-001-0079	f
11248384	Mario	Jesus	Saldaña	Sisneros	m	1960-04-18	01-01-22-001-0079	f
12405456	Marc	Isaac	Mateos	Munguía	m	1963-08-05	01-01-22-001-0079	f
9142018	Nadia	Natalia	Villarreal	Prieto	f	1965-09-01	01-01-22-001-0079	f
5223887	Naia	Paula	Saucedo	Rael	f	1969-05-28	01-01-22-001-0079	f
10796351	Paula	Naia	Beltrán	Sarabia	f	1968-03-15	01-01-22-001-0079	f
12885499	Andres	Miguel	Suárez	De Jesús	m	1964-01-27	01-01-22-001-0079	f
6473771	Marina	Natalia	Araña	Delatorre	f	1954-01-31	01-01-22-001-0079	f
12402308	Naiara	Vera	Cortés	Castellanos	f	1965-12-05	01-01-22-001-0079	f
12394512	Noa	Nayara	Flores	Vila	f	1961-04-11	01-01-22-001-0079	f
14177214	Oswaldo	Jose	Sevilla	Rey	m	1961-11-23	01-01-22-001-0079	f
8831467	Angela	Sara	Bernal	Camarillo	f	1967-12-26	01-01-22-001-0079	f
14031917	Martin	Aaron	Alanis	Delgado	m	1961-02-07	01-01-22-001-0079	f
9852546	Saul	Pedro	Velásquez	Montes	m	1957-05-10	01-01-22-001-0079	f
7066239	Mireia	Carlota	Lucas	Delgadillo	f	1961-08-21	01-01-22-001-0079	f
14770327	Omar	Dario	Arredondo	Arce	m	1956-07-06	01-01-22-001-0079	f
6246712	Mohamed	Luis	Anguiano	Adorno	m	1954-03-20	01-01-22-001-0079	f
12481749	Izan	Victor	Alva	Aguilera	m	1962-04-29	01-01-22-001-0079	f
14324811	Patricia	Manuela	Rincón	Garza	f	1961-04-21	01-01-22-001-0079	f
6219162	Antonia	Ona	Pedroza	Diez	f	1952-04-03	01-01-22-001-0079	f
10253144	Rodrigo	Sergio	Chacón	Olivas	m	1967-05-19	01-01-22-001-0079	f
12767220	Alejandro	Alonso	Alonzo	Camarillo	m	1957-03-09	01-01-22-001-0079	f
14113688	Cesar	Enrique	Orosco	Moran	m	1952-07-09	01-01-22-001-0079	f
14504451	Helena	Paula	Giménez	Maestas	f	1968-08-30	01-01-22-001-0079	f
7070414	Celia	Raquel	Domínguez	Sáenz	f	1952-07-31	01-01-22-001-0079	f
7534474	Claudia	Salma	Terrazas	Aguayo	f	1959-02-01	01-01-22-001-0079	f
7156546	Ian	Eric	Prado	Salazar	m	1959-09-29	01-01-22-001-0079	f
7958325	Joan	Claudia	Portillo	Núñez	f	1958-01-14	01-01-22-001-0079	f
13570446	Oliver	Jesus	Bermúdez	Villareal	m	1961-02-10	01-01-22-001-0079	f
13607033	Ruben	Jose	Soto	Sedillo	m	1967-04-14	01-01-22-001-0079	f
14123078	Eric	Lucas	Magaña	Lucio	m	1954-04-20	01-01-22-001-0079	f
11792910	Jordi	Dario	Baeza	Ulloa	m	1951-12-31	01-01-22-001-0079	f
8428980	Victoria	Silvia	Villalobos	Oliver	f	1964-12-17	01-01-22-001-0079	f
9623367	Leo	Jose	Cabello	Rivera	m	1955-08-18	01-01-22-001-0079	f
11428918	Carlota	Anna	Elizondo	Laureano	f	1958-01-20	01-01-22-001-0079	f
8339318	Maria	Lara	Jurado	Meza	f	1954-09-18	01-01-22-001-0079	f
5671273	Ariadna	Joan	Lemus	Lebrón	f	1960-12-23	01-01-22-001-0079	f
7587255	Mario	Dario	Covarrubias	Arroyo	m	1958-02-07	01-01-22-001-0079	f
9685821	Marti	Pau	Pedraza	Villareal	f	1963-11-02	01-01-22-001-0079	f
7661131	Candela	Vega	Nájera	Pichardo	f	1961-08-09	01-01-22-001-0079	f
13605061	Paula	Paula	Adame	Loya	f	1957-11-28	01-01-22-001-0079	f
7779552	Saul	Javier	Madrid	Rodarte	m	1966-06-03	01-01-22-001-0079	f
11867412	Juan	Martin	Diez	Zaragoza	m	1969-10-28	01-01-22-001-0079	f
14705254	Yaiza	Ines	Loera	Mena	f	1966-04-11	01-01-22-001-0079	f
14182241	Africa	Aroa	Pérez	Quiñónez	f	1963-11-05	01-01-22-001-0079	f
14359875	Rodrigo	Jon	Del río	Serra	m	1959-01-10	01-01-22-001-0079	f
9354935	Sergio	Lucas	Armas	Carbonell	m	1965-05-24	01-01-22-001-0079	f
9790844	Domingo	Rayan	Patiño	Domingo	m	1957-06-29	01-01-22-001-0079	f
14890648	Alonso	Cesar	Rueda	Garrido	m	1958-03-14	01-01-22-001-0079	f
7425932	Angel	Enrique	Delrío	Matos	m	1955-05-19	01-01-22-001-0079	f
6152399	Martina	Valeria	Santamaría	Moya	f	1954-11-30	01-01-22-001-0079	f
11953586	Sofia	Nuria	Montemayor	Lucio	f	1962-09-01	01-01-22-001-0079	f
13742529	Daniela	Rocio	Roldán	Araña	f	1965-07-22	01-01-22-001-0079	f
5706266	Francisco Javier	Pedro	Pacheco	Molina	m	1964-01-17	01-01-22-001-0079	f
6282112	Gonzalo	Cesar	Caldera	Cisneros	m	1965-12-27	01-01-22-001-0079	f
9843553	Jose	Alberto	Redondo	Navarro	m	1954-09-02	01-01-22-001-0079	f
7885407	Alexia	Cristina	Reyna	Arteaga	f	1962-08-09	01-01-22-001-0079	f
11492485	Rayan	Rodrigo	Ochoa	Peres	m	1958-04-06	01-01-22-001-0079	f
13114651	Eva	Natalia	Delvalle	Tijerina	f	1969-08-29	01-01-22-001-0079	f
6874159	Domingo	Leandro	Urbina	Cintrón	m	1968-01-30	01-01-22-001-0079	f
7975240	Oscar	Adam	Paz	Lira	m	1959-06-24	01-01-22-001-0079	f
9588203	Vera	Ana	Palomo	Esparza	f	1963-07-28	01-01-22-001-0079	f
6167909	Manuela	Helena	González	Tello	f	1955-07-14	01-01-22-001-0079	f
6385292	Jose	Eduardo	Muñóz	Rico	m	1956-09-04	01-01-22-001-0079	f
6992746	Andrea	Berta	Zamudio	Santamaría	f	1959-09-30	01-01-22-001-0079	f
6209343	Izan	Pablo	Barraza	Carmona	m	1964-06-17	01-01-22-001-0079	f
8313208	Miriam	Miriam	Olivárez	Negrón	f	1959-02-13	01-01-22-001-0079	f
6995055	Marcos	Ruben	Apodaca	Garrido	m	1959-09-05	01-01-22-001-0079	f
9119733	Isaac	Rodrigo	Magaña	Bañuelos	m	1952-09-21	01-01-22-001-0079	f
11897499	Francisco Javier	Ivan	Barraza	Vanegas	m	1958-08-18	01-01-22-001-0079	f
8785094	Isaac	Javier	Ibarra	Castaño	m	1954-09-30	01-01-22-001-0079	f
7883590	Santiago	Leo	Lira	Olivárez	m	1969-03-21	01-01-22-001-0079	f
10075061	Jaime	Rodrigo	Alaniz	Espinoza	m	1965-12-17	01-01-22-001-0079	f
14927351	Andrea	Alexandra	Abeyta	Quezada	f	1966-03-28	01-01-22-001-0079	f
14422461	Andres	Sergio	Zamora	Cuellar	m	1955-08-18	01-01-22-001-0079	f
8493949	Victor	Adrian	Luque	Delvalle	m	1968-10-10	01-01-22-001-0079	f
6984032	Alexia	Alma	León	Heredia	f	1968-06-10	01-01-22-001-0079	f
7865944	Hector	Mohamed	Cortes	Domínquez	m	1968-02-21	01-01-22-001-0079	f
6840089	Andrea	Carlota	Sandoval	Tirado	f	1969-08-23	01-01-22-001-0079	f
10208648	Enrique	Isaac	Galindo	López	m	1953-02-05	01-01-22-001-0079	f
8893604	Aleix	Isaac	Peres	Orellana	m	1955-07-09	01-01-22-001-0079	f
11153645	Alexandra	Bella	Carbonell	Meza	f	1953-03-28	01-01-22-001-0079	f
12548915	Abril	Noelia	Cadena	Alva	f	1957-06-05	01-01-22-001-0079	f
8883680	Nil	Elena	Yáñez	Negrete	f	1958-01-13	01-01-22-001-0079	f
9956188	Sandra	Gabriela	Carrero	Girón	f	1964-09-22	01-01-22-001-0079	f
14202289	Carla	Eva	Aranda	Saldaña	f	1965-07-31	01-01-22-001-0079	f
14117823	Lola	Alicia	Mata	Valles	f	1952-03-06	01-01-22-001-0079	f
5872251	Javier	Daniel	Marrero	Carrasco	m	1958-06-30	01-01-22-001-0079	f
13850760	Mateo	Oswaldo	Galindo	Abreu	m	1953-09-26	01-01-22-001-0079	f
5925175	Vera	Claudia	Serra	Cardona	f	1964-12-28	01-01-22-001-0079	f
13478798	Vega	Angela	Andreu	Mena	f	1961-10-18	01-01-22-001-0079	f
13976299	Roberto	Hector	Salcedo	Anguiano	m	1954-02-27	01-01-22-001-0079	f
8439008	Alejandra	Jimena	Arce	Saavedra	f	1963-10-04	01-01-22-001-0079	f
9054652	Sara	Mara	Vila	Madrid	f	1965-09-17	01-01-22-001-0079	f
8026614	David	Alberto	Canales	Collazo	m	1968-09-29	01-01-22-001-0079	f
9730020	Andres	Mario	Domenech	Gallardo	m	1951-12-12	01-01-22-001-0079	f
11557902	Fatima	Jana	Olivas	Ochoa	f	1954-09-28	01-01-22-001-0079	f
7733044	Diana	Marina	Urías	Perea	f	1958-05-10	01-01-22-001-0079	f
6976022	Angela	Carlota	Diez	Font	f	1961-06-04	01-01-22-001-0079	f
13179609	Ariadna	Eva	Villagómez	Guevara	f	1962-05-24	01-01-22-001-0079	f
7458734	Oscar	Izan	Izquierdo	Ibarra	m	1956-07-02	01-01-22-001-0079	f
7548558	Mario	Omar	Estrada	Guillen	m	1957-06-17	01-01-22-001-0079	f
5917340	Aaron	Omar	Gaytán	Domínquez	m	1965-11-17	01-01-22-001-0079	f
11381077	Ignacio	Hugo	Valadez	Puig	m	1960-07-02	CC-02-URB-0024	f
7119046	Leandro	Diego	Archuleta	Padrón	m	1965-01-26	01-01-22-001-0079	f
10650365	Blanca	Carla	Delvalle	Vicente	f	1966-10-22	01-01-22-0001-0014	f
12518557	Pedro	Marco	Carvajal	Pedroza	m	1962-08-02	01-01-22-0001-0014	f
5118492	Diana	Sara	Villar	Maldonado	f	1962-10-11	01-01-22-0001-0014	f
10670338	Joel	Pedro	Montez	Lucero	m	1961-07-04	01-01-22-0001-0014	f
5870492	Victor	Leandro	Sedillo	Guardado	m	1955-08-29	01-01-22-0001-0014	f
13426004	Miriam	Naiara	Olmos	Avilés	f	1954-05-22	01-01-22-0001-0014	f
13245654	Adria	Jorge	Bernal	Laboy	m	1967-10-22	01-01-22-0001-0014	f
5482616	Teresa	Leire	Jiménez	Loera	f	1969-12-02	01-01-22-0001-0014	f
5101791	Aaron	Pablo	Berríos	Robles	m	1964-04-05	01-01-22-0001-0014	f
13917455	Naia	Alma	Tejada	Luna	f	1965-06-24	01-01-22-0001-0014	f
13491890	Guillermo	Alonso	Olvera	Monroy	m	1963-05-28	01-01-22-0001-0014	f
9326171	Leo	Ruben	Lozada	Alvarado	m	1969-08-10	01-01-22-0001-0014	f
13883221	Cristian	Francisco Javier	Barajas	Barrera	m	1964-08-04	01-01-22-0001-0014	f
11224122	Antonio	Guillem	Sancho	Alaniz	m	1959-03-13	01-01-22-0001-0014	f
6539394	Isaac	Marcos	Matos	Arias	m	1956-02-13	01-01-22-0001-0014	f
8382038	Izan	Bruno	Cervántez	Alemán	m	1965-01-31	01-01-22-0001-0014	f
14931767	Angel	Oliver	Carranza	Negrón	m	1956-12-22	01-01-22-0001-0014	f
9623795	Blanca	Carolina	Baca	Tamayo	f	1955-08-09	01-01-22-0001-0014	f
5345797	Sergio	Jorge	Rodrigo	Giménez	m	1956-09-16	01-01-22-0001-0014	f
14788608	Nuria	Paola	Castañeda	Herrera	f	1964-06-29	01-01-22-0001-0014	f
9645858	Andrea	Fatima	Caldera	Barroso	f	1956-06-27	01-01-22-0001-0014	f
13509590	Alejandra	Irene	Acevedo	Vila	f	1963-06-05	01-01-22-0001-0014	f
8677945	Adria	Jon	Sedillo	Gómez	m	1966-09-27	01-01-22-0001-0014	f
12609392	Adam	Martin	Oliva	Yáñez	m	1955-07-16	01-01-22-0001-0014	f
6836576	Jon	Jaime	Dueñas	Mas	m	1953-01-31	01-01-22-0001-0014	f
12407164	Sebastian	Eric	Cortes	Partida	m	1964-10-22	01-01-22-0001-0014	f
5369567	Ian	Manuel	Montaño	Ocampo	m	1956-07-10	01-01-22-0001-0014	f
9189280	Ona	Laura	Martos	Naranjo	f	1953-08-24	01-01-22-0001-0014	f
10823147	Eduardo	Jon	Mojica	Arroyo	m	1956-12-16	01-01-22-0001-0014	f
14186268	Daniela	Naia	Valverde	Malave	f	1965-11-16	01-01-22-0001-0014	f
9289451	Sara	Abril	Ponce	Peláez	f	1955-08-22	01-01-22-0001-0014	f
12043061	Iria	Pau	Gómez	Alcala	f	1959-08-04	01-01-22-0001-0014	f
13161788	Laia	Leyre	Muro	Curiel	f	1952-03-24	01-01-22-0001-0014	f
9098203	Marc	Hugo	Nieto	Pérez	m	1951-11-23	01-01-22-0001-0014	f
6774440	Nerea	Alejandra	Menchaca	Vaca	f	1963-09-07	01-01-22-0001-0014	f
12557927	Alex	Miguel	Estrada	Leyva	m	1962-10-27	01-01-22-0001-0014	f
8974183	Yorman	Marc	Grijalva	Santana	m	1958-05-13	01-01-22-0001-0014	f
11095548	Anna	Carlota	Llamas	Bustos	f	1967-12-27	01-01-22-0001-0014	f
9057280	Teresa	Valeria	Reséndez	Sosa	f	1953-05-17	01-01-22-0001-0014	f
10880125	Adrian	Leo	Rojas	Salvador	m	1965-11-05	01-01-22-0001-0014	f
11822430	Laia	Elsa	Alarcón	Vaca	f	1951-12-15	01-01-22-0001-0014	f
14748795	Gabriela	Valentina	Romo	Franco	f	1967-11-28	01-01-22-0001-0014	f
14375111	Mario	Fernando	Riera	Barroso	m	1959-09-02	01-01-22-0001-0014	f
12931732	Helena	Irene	Saucedo	Quesada	f	1958-12-04	01-01-22-0001-0014	f
14089559	Marti	Claudia	Llamas	Pineda	f	1968-07-21	01-01-22-0001-0014	f
9492314	Ainara	Abril	Mas	Madrid	f	1966-07-11	01-01-22-0001-0014	f
12769571	Mar	Mireia	Matos	Muñoz	f	1966-10-10	01-01-22-0001-0014	f
7991162	Helena	Alexandra	Bétancourt	Enríquez	f	1955-05-24	01-01-22-0001-0014	f
6869255	Lara	Jimena	Meza	Mayorga	f	1967-01-14	01-01-22-0001-0014	f
11797204	Nora	Luisa	Esquivel	Canales	f	1961-11-28	01-01-22-0001-0014	f
7990442	Gabriel	Jaime	Vélez	Macias	m	1961-04-13	01-01-22-0001-0014	f
6606160	Angela	Mara	Riera	Carrera	f	1961-09-18	01-01-22-0001-0014	f
12549082	Celia	Martina	Castañeda	Briones	f	1966-12-17	01-01-22-0001-0014	f
10629534	Nadia	Ines	Borrego	Quesada	f	1957-04-12	01-01-22-0001-0014	f
8136787	Isabel	Jimena	Galindo	Luque	f	1969-07-22	01-01-22-0001-0014	f
12650508	Oswaldo	Oswaldo	Reina	Quintanilla	m	1959-01-19	01-01-22-0001-0014	f
6724832	Rocio	Jimena	Gamez	Lozada	f	1958-11-07	01-01-22-0001-0014	f
10414404	Julia	Daniela	Delafuente	Archuleta	f	1969-11-03	01-01-22-0001-0014	f
8799195	Laura	Nerea	Loya	Manzanares	f	1965-07-13	01-01-22-0001-0014	f
8993318	Hugo	Leandro	Cortés	Garica	m	1965-09-09	01-01-22-0001-0014	f
14169110	Patricia	Aroa	Solorzano	Yáñez	f	1961-07-16	01-01-22-0001-0014	f
8167391	Izan	Bruno	Ochoa	Álvarez	m	1957-11-04	01-01-22-0001-0014	f
12874944	Bella	Luisa	Mora	Perea	f	1956-11-10	01-01-22-0001-0014	f
9150564	Salma	Sandra	Cerda	Prado	f	1953-11-29	01-01-22-0001-0014	f
12695637	Cristian	Juan	Jaramillo	Asensio	m	1964-12-12	01-01-22-0001-0014	f
5215388	Raquel	Ines	Mojica	Silva	f	1958-11-06	01-01-22-0001-0014	f
8212939	Claudia	Pau	Matías	Aparicio	f	1964-12-20	01-01-22-0001-0014	f
10779143	Erika	Vera	Zayas	Cabán	f	1953-08-30	01-01-22-0001-0014	f
11455921	Erika	Andrea	Monroy	Gamez	f	1968-02-22	01-01-22-0001-0014	f
14969677	Rafael	Jorge	Roque	Crespo	m	1959-01-30	01-01-22-0001-0014	f
11168223	Angel	Joel	Paz	Blasco	m	1959-02-16	01-01-22-0001-0014	f
12252436	Yorman	Roberto	Sedillo	Saldivar	m	1959-10-16	01-01-22-0001-0014	f
9314076	Oscar	Samuel	Mota	Gómez	m	1962-05-22	01-01-22-0001-0014	f
9919310	Patricia	Naiara	Bañuelos	Rincón	f	1959-02-12	01-01-22-0001-0014	f
6509889	Paola	Clara	Crespo	Juárez	f	1964-05-09	01-01-22-0001-0014	f
6203920	Oswaldo	Adrian	Cabrera	Domínguez	m	1958-10-02	01-01-22-0001-0014	f
6037432	Vera	Carlota	Romero	Ornelas	f	1960-03-15	01-01-22-0001-0014	f
8361331	Abril	Mireia	Lebrón	Rivero	f	1963-03-24	01-01-22-0001-0014	f
5428407	Carlota	Berta	Crespo	Arguello	f	1967-08-25	01-01-22-0001-0014	f
10161094	Candela	Nerea	Álvarez	Candelaria	f	1956-10-01	01-01-22-0001-0014	f
8316318	Valentina	Nahia	Villarreal	Quiroz	f	1954-02-01	01-01-22-0001-0014	f
13969851	Marina	Ana	Riojas	Alejandro	f	1957-10-22	01-01-22-0001-0014	f
11709205	Guillermo	Rayan	Mares	Ibáñez	m	1966-06-23	01-01-22-0001-0014	f
13698269	Angela	Alexia	Alaniz	Arce	f	1960-11-02	01-01-22-0001-0014	f
9951718	Roberto	Yorman	Rangel	Noriega	m	1953-05-27	01-01-22-0001-0014	f
13837549	Vera	Sara	Juan	López	f	1959-11-21	01-01-22-0001-0014	f
14751087	Diana	Erika	Acevedo	Delgado	f	1969-09-23	01-01-22-0001-0014	f
10218543	Daniel	Rayan	Parra	Lara	m	1957-04-29	01-01-22-0001-0014	f
9366955	Jon	Rayan	Irizarry	Aragón	m	1960-05-31	01-01-22-0001-0014	f
7032294	Laura	Marti	Rosas	Colunga	f	1966-09-14	01-01-22-0001-0014	f
8577854	Ines	Cristina	Casárez	Sauceda	f	1964-08-08	01-01-22-0001-0014	f
7590366	Ismael	Marc	Vigil	García	m	1959-05-11	01-01-22-0001-0014	f
12477949	Noelia	Miriam	Mendoza	Ros	f	1958-05-22	01-01-22-0001-0014	f
7216768	Paola	Isabel	Pozo	Soler	f	1957-03-22	01-01-22-0001-0014	f
10358798	Izan	Bruno	Alfonso	Cruz	m	1957-12-20	01-01-22-0001-0014	f
8681969	Noa	Raquel	Asensio	Montero	f	1956-01-24	01-01-22-0001-0014	f
9691925	Eduardo	Erik	Rolón	Amaya	m	1961-02-07	01-01-22-0001-0014	f
6926242	Sara	Carolina	Cornejo	Rosario	f	1960-09-13	01-01-22-0001-0014	f
8727518	Luna	Marta	Ruiz	Antón	f	1957-11-08	01-01-22-0001-0014	f
11541544	Cristina	Nadia	Rico	Herrero	f	1966-04-14	01-01-22-0001-0014	f
6679854	Francisco	Jordi	Ortíz	Botello	m	1957-05-14	01-01-22-0001-0014	f
10959460	Jordi	Cristian	Robledo	Orosco	m	1962-04-07	01-01-22-0001-0014	f
8344404	Carlos	Javier	Castillo	Cortés	m	1967-01-06	01-01-22-0001-0014	f
11275041	Saul	Sergio	Brito	Barroso	m	1960-09-05	01-01-22-0001-0014	f
7110162	Hector	Ignacio	Morales	Gálvez	m	1959-03-12	01-01-22-0001-0014	f
7375409	Laia	Valeria	Calvillo	Moya	f	1955-12-31	01-01-22-0001-0014	f
8407955	Ona	Luisa	Carbonell	Leiva	f	1966-08-10	01-01-22-0001-0014	f
6874609	Jimena	Noa	Rueda	Vega	f	1957-03-20	01-01-22-0001-0014	f
6502235	Adrian	David	Salas	Clemente	m	1953-11-01	01-01-22-0001-0014	f
8495243	Pol	Domingo	Águilar	Carrillo	m	1966-03-15	01-01-22-0001-0014	f
10326642	Oscar	Fernando	Sancho	Ulibarri	m	1958-03-05	01-01-22-0001-0014	f
9230413	Miguel	Oswaldo	Sáenz	Navas	m	1956-08-19	01-01-22-0001-0014	f
8025628	Carla	Mara	Padilla	Garica	f	1957-06-24	01-01-22-0001-0014	f
8466662	Fernando	Jesus	Viera	Marín	m	1961-12-15	01-01-22-0001-0014	f
14890026	Vera	Ona	Collado	Briseño	f	1969-11-24	01-01-22-0001-0014	f
7277793	Aroa	Sandra	Abeyta	Carrion	f	1968-12-10	01-01-22-0001-0014	f
14122799	Lidia	Nahia	Bahena	Zavala	f	1954-10-15	01-01-22-0001-0014	f
11657631	Rayan	Daniel	Tórrez	Hernádez	m	1955-12-20	01-01-22-0001-0014	f
12554261	Nora	Ane	De la torre	Moran	f	1954-03-06	01-01-22-0001-0014	f
9661667	Blanca	Ines	Berríos	Muñoz	f	1957-01-15	01-01-22-0001-0014	f
7697109	Patricia	Bella	Cerda	Rocha	f	1969-08-14	01-01-22-0001-0014	f
5628687	Nadia	Ana	Amador	Piñeiro	f	1956-02-15	01-01-22-0001-0014	f
13586127	Diana	Erika	Rico	Cotto	f	1966-08-09	01-01-22-0001-0014	f
10866058	Marta	Natalia	Concepción	Ávila	f	1964-03-14	01-01-22-0001-0014	f
11860595	Saul	Guillem	Benito	Alfaro	m	1966-03-15	01-01-22-0001-0014	f
14578405	Lucas	Ivan	Polanco	Tirado	m	1952-12-10	01-01-22-0001-0014	f
7907300	Ismael	Ian	Vila	Puente	m	1953-07-01	01-01-22-0001-0014	f
7944920	Ana	Erika	Verduzco	Briseño	f	1966-12-04	01-01-22-0001-0014	f
7258839	Alicia	Carmen	Soler	Delarosa	f	1961-09-01	01-01-22-0001-0014	f
14573192	Paula	Ana	Gamez	Tello	f	1952-07-18	01-01-22-0001-0014	f
13126349	Guillem	Antonio	Terán	Espino	m	1953-04-19	01-01-22-0001-0014	f
7307673	Jaime	Cristian	Álvarez	Guzmán	m	1961-04-13	01-01-22-0001-0014	f
6777891	Noa	Lara	Valdivia	Olivera	f	1955-01-01	01-01-22-0001-0014	f
10233111	Mireia	Carlota	De la cruz	Silva	f	1958-01-09	01-01-22-0001-0014	f
14749239	Ismael	Mario	Castaño	Bautista	m	1958-10-12	01-01-22-0001-0014	f
13071578	Dario	Hector	Saucedo	Sancho	m	1953-11-09	01-01-22-0001-0014	f
11266478	Jana	Zulay	Rentería	Ocampo	f	1957-06-13	01-01-22-0001-0014	f
11943181	Carlota	Fatima	Corrales	Bétancourt	f	1962-01-16	01-01-22-0001-0014	f
14247548	Yorman	Martin	Balderas	Cedillo	m	1968-07-05	01-01-22-0001-0014	f
10341212	Nahia	Ainara	Aguilera	Mota	f	1963-07-24	01-01-22-0001-0014	f
14243668	Rodrigo	Ivan	Amaya	Urbina	m	1959-05-07	01-01-22-0001-0014	f
5513442	Carla	Noa	Palomino	Roldan	f	1969-03-26	01-01-22-0001-0014	f
5077779	Laura	Helena	Pantoja	Montoya	f	1955-04-14	01-01-22-0001-0014	f
7073121	Noa	Luna	Alfonso	Vicente	f	1963-05-23	01-01-22-0001-0014	f
7600615	Naiara	Celia	Cobo	Delacrúz	f	1957-12-30	01-01-22-0001-0014	f
13578028	Ariadna	Lola	Ozuna	Saldivar	f	1968-08-25	01-01-22-0001-0014	f
6648214	Martina	Alexia	Delgado	Llamas	f	1969-12-24	01-01-22-0001-0014	f
6491865	Candela	Helena	Ledesma	Espinoza	f	1961-09-14	01-01-22-0001-0014	f
10524882	Oliver	Jorge	Moral	Molina	m	1951-12-29	01-01-22-0001-0014	f
5169091	Carlos	Lucas	Almanza	Reséndez	m	1952-09-30	01-01-22-0001-0014	f
14713308	Marcos	Carlos	Brito	Carrero	m	1969-09-07	01-01-22-0001-0014	f
10639962	Leyre	Victoria	Valencia	Sáenz	f	1969-09-07	01-01-22-0001-0014	f
6758789	Zulay	Paula	Villanueva	Cordero	f	1952-01-21	01-01-22-0001-0014	f
8543976	Hector	Hector	Domínguez	Cardenas	m	1963-01-25	01-01-22-0001-0014	f
14314094	Raquel	Jana	Salazar	Naranjo	f	1957-08-23	01-01-22-0001-0014	f
12004764	Mateo	Alvaro	Segura	Quezada	m	1954-01-16	01-01-22-0001-0014	f
5138191	Juan	Miguel	Ojeda	Jáquez	m	1956-08-09	01-01-22-0001-0014	f
5750293	Hector	Rodrigo	Peralta	Zelaya	m	1967-04-16	01-01-22-0001-0014	f
14699332	Adrian	Andres	Cabello	Hinojosa	m	1963-01-01	01-01-22-0001-0014	f
8268417	Bruno	Cesar	Otero	Rodrigo	m	1955-12-08	01-01-22-0001-0014	f
8339309	Manuel	Lucas	Diez	Haro	m	1968-09-27	01-01-22-0001-0014	f
7355567	Mireia	Erika	Abad	Nazario	f	1956-08-21	01-01-22-0001-0014	f
11892927	Pablo	Eric	Farías	Esteve	m	1953-06-30	01-01-22-0001-0014	f
6866542	Fernando	Sebastian	Puente	Sanabria	m	1964-01-31	01-01-22-0001-0014	f
10474208	Andres	Rafael	Contreras	Nieto	m	1952-07-05	01-01-22-0001-0014	f
9541267	Nayara	Marta	Angulo	Tijerina	f	1956-06-02	01-01-22-0001-0014	f
11493260	Lara	Nahia	Villalba	Águilar	f	1964-08-30	01-01-22-0001-0014	f
10849594	Erik	Aleix	Otero	Riojas	m	1960-12-08	01-01-22-0001-0014	f
10468833	Mar	Carla	Gallegos	Rentería	f	1957-02-25	01-01-22-0001-0014	f
10322443	Miguel	Jorge	Trejo	Villa	m	1968-08-27	01-01-22-0001-0014	f
9250820	Jimena	Marta	Torres	Domínguez	f	1951-12-15	01-01-22-0001-0014	f
9917106	Aleix	Cristian	Botello	Trejo	m	1953-03-22	01-01-22-0001-0014	f
10752438	Alicia	Carmen	Villanueva	Huerta	f	1958-01-20	01-01-22-0001-0014	f
6422513	Sergio	Daniel	Sola	Granado	m	1969-02-11	01-01-22-0001-0014	f
7131383	Jordi	Adria	Ocampo	Ortiz	m	1966-07-05	01-01-22-0001-0014	f
11930547	Sandra	Ana	De la cruz	Rivas	f	1960-12-30	01-01-22-0001-0014	f
8613787	Berta	Erika	Leyva	Linares	f	1956-08-03	01-01-22-0001-0014	f
13719600	Mar	Teresa	Fuentes	Redondo	f	1967-05-31	01-01-22-0001-0014	f
13924583	Angel	Diego	Villarreal	Granado	m	1959-08-07	01-01-22-0001-0014	f
6003932	Alejandro	Erik	Girón	Montaño	m	1966-12-30	01-01-22-0001-0014	f
9444581	Paola	Nayara	Robledo	Crespo	f	1952-01-13	01-01-22-0001-0014	f
8687519	Nerea	Laura	Gonzales	Pichardo	f	1968-02-14	01-01-22-0001-0014	f
10068165	Alejandro	Rafael	Mejía	Herrera	m	1960-11-24	01-01-22-0001-0014	f
12025657	Oscar	Mohamed	Caraballo	Cortés	m	1967-11-30	01-01-22-0001-0014	f
11082261	Emma	Nil	Diez	Enríquez	f	1967-11-12	01-01-22-0001-0014	f
8838980	Manuela	Paola	Fonseca	Gurule	f	1968-08-24	01-01-22-0001-0014	f
13365254	Marti	Nerea	Trejo	Tejada	f	1961-05-05	01-01-22-0001-0014	f
5469998	Sara	Vega	Sanz	Delapaz	f	1960-05-02	01-01-22-0001-0014	f
10946865	Nil	Laura	Hernando	Piña	f	1965-10-26	01-01-22-0001-0014	f
8141338	Mar	Irene	Jurado	Martín	f	1962-02-04	01-01-22-0001-0014	f
9996960	Samuel	Pol	Velázquez	Adame	m	1965-12-06	01-01-22-0001-0014	f
13779830	Eva	Nahia	Juárez	Benito	f	1959-05-14	01-01-22-0001-0014	f
11988329	Guillem	Adam	Delvalle	Lorenzo	m	1959-05-02	01-01-22-0001-0014	f
6686237	Jorge	Andres	Fuentes	Marrero	m	1955-10-23	01-01-22-0001-0014	f
10861648	Berta	Manuela	Alaniz	Apodaca	f	1965-08-11	01-01-22-0001-0014	f
5939792	Javier	Leandro	Alemán	Rodrigo	m	1953-09-12	01-01-22-0001-0014	f
13905068	Juan	Nicolas	Madrid	Carrasquillo	m	1952-10-13	01-01-22-0001-0014	f
7783951	Elsa	Silvia	Jaime	Ordoñez	f	1967-10-26	01-01-22-0001-0014	f
6954713	Lola	Leire	Zavala	Gómez	f	1964-01-02	01-01-22-0001-0014	f
13102271	Celia	Rocio	Saiz	Tapia	f	1962-11-11	01-01-22-0001-0014	f
8406760	Erik	Roberto	Gastélum	Pulido	m	1965-02-13	01-01-22-0001-0014	f
6640567	Erik	Dario	Benavídez	Toro	m	1956-09-25	01-01-22-0001-0014	f
12413437	Hugo	Lucas	Armas	Montes	m	1965-06-28	01-01-22-0001-0014	f
14699886	Emma	Martina	Chávez	Lemus	f	1963-01-11	01-01-22-0001-0014	f
7917266	Rafael	Raul	Altamirano	Muñóz	m	1969-09-03	01-01-22-0001-0014	f
13877752	Nuria	Alexandra	Navas	Botello	f	1952-10-30	01-01-22-0001-0014	f
7035607	Aaron	Guillermo	Delacrúz	Casanova	m	1967-03-27	01-01-22-0001-0014	f
9634872	Nadia	Elsa	Calvillo	Arriaga	f	1957-02-19	01-01-22-0001-0014	f
10576677	Nadia	Cristina	Berríos	Guevara	f	1961-05-18	01-01-22-0001-0014	f
13671657	Juan	Bruno	Adorno	Adame	m	1966-06-02	01-01-22-0001-0014	f
6037840	Ane	Paola	Alba	Crespo	f	1956-02-06	01-01-22-0001-0014	f
12138146	Leo	Juan	Hidalgo	Acosta	m	1954-11-03	01-01-22-0001-0014	f
5425656	Adrian	Martin	Valle	Paredes	m	1967-10-27	01-01-22-0001-0014	f
6690870	David	Aleix	Manzano	Ortíz	m	1961-12-11	01-01-22-0001-0014	f
6582463	Omar	Gonzalo	Treviño	Caro	m	1954-05-05	01-01-22-0001-0014	f
5380288	Laura	Sara	Menéndez	Leiva	f	1955-02-11	01-01-22-0001-0014	f
8782949	Jaime	David	Chacón	Cavazos	m	1953-05-01	01-01-22-0001-0014	f
13299442	Enrique	Victor	Calvo	Candelaria	m	1954-09-12	01-01-22-0001-0014	f
6732914	Ona	Nahia	Pineda	Prieto	f	1964-07-07	01-01-22-0001-0014	f
6152609	Candela	Elsa	Pereira	Polo	f	1953-06-04	01-01-22-0001-0014	f
9210174	Antonia	Laia	Monroy	Santamaría	f	1954-09-09	01-01-22-0001-0014	f
6764145	Daniela	Blanca	Roca	Barela	f	1963-09-15	01-01-22-0001-0014	f
6376489	Adrian	Alejandro	Sotelo	Navas	m	1963-03-27	01-01-22-0001-0014	f
8471072	Fatima	Gabriela	Andrés	Calderón	f	1958-10-24	01-01-22-0001-0014	f
8494440	Abril	Beatriz	Ontiveros	Aponte	f	1965-07-04	01-01-22-0001-0014	f
10706519	Santiago	Alex	Blázquez	Garay	m	1969-04-19	01-01-22-0001-0014	f
5734699	David	Andres	Sosa	Soler	m	1959-01-07	01-01-22-0001-0014	f
6205164	Alonso	Leonardo	Lozano	Ros	m	1952-01-31	01-01-22-0001-0014	f
14333023	Adrian	Pablo	Moral	Meza	m	1968-09-23	01-01-22-0001-0014	f
5770384	Angel	Rodrigo	Moya	Dueñas	m	1962-11-08	01-01-22-0001-0014	f
11621849	Roberto	Victor	Páez	Griego	m	1959-06-12	01-01-22-0001-0014	f
5829409	Claudia	Alexia	Núñez	Torres	f	1954-10-03	01-01-22-0001-0014	f
8982810	Eva	Marina	Madera	Sosa	f	1953-10-26	01-01-22-0001-0014	f
10091223	Martin	Adrian	Bermúdez	Ocasio	m	1968-07-13	01-01-22-0001-0014	f
8213540	Alvaro	Fernando	Calvo	Valdés	m	1969-01-25	01-01-22-0001-0014	f
7501081	Jose	Guillermo	Zapata	Romo	m	1958-08-20	01-01-22-0001-0014	f
6389050	Marcos	Raul	Diez	Avilés	m	1967-10-10	01-01-22-0001-0014	f
9781603	Adria	Nicolas	Sáez	Silva	m	1967-03-03	01-01-22-0001-0014	f
11354404	Joan	Nora	Piñeiro	Estrada	f	1965-07-28	01-01-22-0001-0014	f
12554352	Leyre	Isabel	Ponce	Santos	f	1957-04-09	01-01-22-0001-0014	f
10679957	Marta	Carla	Benavídez	Alejandro	f	1966-04-04	01-01-22-0001-0014	f
8073158	Guillermo	Antonio	Manzanares	Delarosa	m	1958-11-12	01-01-22-0001-0014	f
14269667	Yorman	Martin	Archuleta	Uribe	m	1962-03-24	01-01-22-0001-0014	f
12205595	Ines	Lara	Mojica	Laboy	f	1954-03-01	01-01-22-0001-0014	f
7574938	Aleix	Roberto	Esquivel	Negrete	m	1955-12-28	01-01-22-0001-0014	f
10400903	Victor	Eduardo	Bermúdez	Córdoba	m	1966-11-21	01-01-22-0001-0014	f
10517928	Jordi	Diego	Rosales	Terán	m	1961-07-24	01-01-22-0001-0014	f
11801155	Beatriz	Mireia	Tejada	Tapia	f	1956-07-09	01-01-22-0001-0014	f
12598974	Paola	Valentina	Raya	De Anda	f	1953-10-26	01-01-22-0001-0014	f
12671620	Pau	Isabel	Abad	García	f	1959-10-06	01-01-22-0001-0014	f
12601213	Blanca	Naia	León	Luján	f	1956-09-01	01-01-22-0001-0014	f
13268278	Maria	Lucia	Rincón	Ledesma	f	1952-07-25	01-01-22-0001-0014	f
12031737	Zulay	Elena	Murillo	Lorente	f	1959-02-11	01-01-22-0001-0014	f
12344211	Claudia	Raquel	Cedillo	Gallegos	f	1960-10-28	01-01-22-0001-0014	f
6955569	Iria	Ana	Pascual	Caballero	f	1959-09-02	01-01-22-0001-0014	f
5483035	Alicia	Nadia	Sotelo	Frías	f	1959-08-22	01-01-22-0001-0014	f
8719076	Africa	Jimena	Guzmán	Cortes	f	1956-04-21	01-01-22-0001-0014	f
10561587	Cristian	Rodrigo	Caballero	Luevano	m	1962-03-29	01-01-22-0001-0014	f
8536416	Jaime	Bruno	Rangel	Lemus	m	1967-07-11	01-01-22-0001-0014	f
12642994	Andrea	Miriam	Juan	Borrego	f	1967-12-11	01-01-22-0001-0014	f
11723594	Angel	Rodrigo	Ornelas	Uribe	m	1965-10-09	01-01-22-0001-0014	f
6974793	Andres	Rodrigo	Arteaga	Cortes	m	1962-07-21	01-01-22-0001-0014	f
6029003	Beatriz	Pau	Ocasio	Portillo	f	1959-02-06	01-01-22-0001-0014	f
12320938	Oswaldo	Alvaro	Puga	Olvera	m	1959-02-12	01-01-22-0001-0014	f
13489507	Claudia	Bella	Lucas	Pérez	f	1969-12-14	01-01-22-0001-0014	f
9099917	Ines	Alexia	Olivo	Guajardo	f	1952-03-23	01-01-22-0001-0014	f
12133967	Celia	Lara	Téllez	Alanis	f	1969-01-10	01-01-22-0001-0014	f
5105733	Daniel	Ruben	Montaño	Barrientos	m	1966-09-05	01-01-22-0001-0014	f
8330000	Beatriz	Sandra	Mora	Araña	f	1963-01-03	01-01-22-0001-0014	f
11479261	Andres	Marc	Escudero	Olmos	m	1951-12-24	01-01-22-0001-0014	f
14515042	Blanca	Daniela	Manzanares	López	f	1957-04-23	01-01-22-0001-0014	f
7692859	Antonio	Carlos	Cardona	Gutiérrez	m	1956-02-04	01-01-22-0001-0014	f
12506535	Juan	Miguel	Calderón	Medrano	m	1953-02-01	01-01-22-0001-0014	f
13727568	Angela	Alma	Andreu	Ontiveros	f	1962-04-07	01-01-22-0001-0014	f
9819229	Leandro	Erik	Aguilera	Olivárez	m	1967-03-03	01-01-22-0001-0014	f
5363610	Isaac	Alejandro	Pons	Vergara	m	1965-09-23	01-01-22-0001-0014	f
8035718	Aaron	Diego	Diez	Cabán	m	1954-06-27	01-01-22-0001-0014	f
8418429	Naia	Leire	Téllez	Luna	f	1963-04-08	01-01-22-0001-0014	f
12032804	Eduardo	Javier	Blasco	Piñeiro	m	1959-04-23	01-01-22-0001-0014	f
13873039	Cristian	Jorge	Salazar	Nájera	m	1969-05-04	01-01-22-0001-0014	f
13826393	Carmen	Ona	Gil	Roybal	f	1957-02-08	01-01-22-001-0049	f
11177959	Carla	Sofia	Briones	Enríquez	f	1968-09-05	01-01-22-001-0049	f
10215890	Nadia	Valeria	Garica	Blázquez	f	1968-01-25	01-01-22-001-0049	f
7204882	Berta	Beatriz	Rosa	Esparza	f	1965-05-01	01-01-22-001-0049	f
11025586	Saul	Antonio	Carrion	Alvarado	m	1969-09-26	01-01-22-001-0049	f
9192772	Adam	Joel	Delatorre	Nieves	m	1959-10-31	01-01-22-001-0049	f
6439654	Raquel	Valeria	Benito	Valentín	f	1956-05-05	01-01-22-001-0049	f
12708043	Jana	Zulay	Martí	Puente	f	1962-06-20	01-01-22-001-0049	f
13439763	Antonia	Mara	Roybal	Carbajal	f	1957-11-12	01-01-22-001-0049	f
7446282	Pedro	Victor	Santamaría	Reséndez	m	1967-03-16	01-01-22-001-0049	f
6038770	Adriana	Luna	Zelaya	Clemente	f	1963-04-28	01-01-22-001-0049	f
12863771	Laura	Eva	Garica	Sandoval	f	1967-05-01	01-01-22-001-0049	f
7065257	Natalia	Sandra	Montoya	Pastor	f	1951-12-20	01-01-22-001-0049	f
8890188	Hugo	Jaime	Zaragoza	Santos	m	1958-09-06	01-01-22-001-0049	f
13432829	Alejandra	Alicia	Varela	Quintero	f	1968-04-13	01-01-22-001-0049	f
6258642	Rafael	Dario	Niño	Becerra	m	1967-11-11	01-01-22-001-0049	f
9405630	Nicolas	Erik	Gálvez	Acosta	m	1956-08-25	01-01-22-001-0049	f
13505571	Aleix	Ivan	Ulibarri	Aguayo	m	1967-07-07	01-01-22-001-0049	f
12567211	Cristina	Carmen	Quezada	Lucero	f	1958-08-22	01-01-22-001-0049	f
5126709	Marcos	Izan	Collado	Castellano	m	1959-12-16	01-01-22-001-0049	f
10957421	Lucia	Ariadna	Olvera	Delgado	f	1968-06-18	01-01-22-001-0049	f
14138585	Joel	Angel	Alva	Zavala	m	1963-11-27	01-01-22-001-0049	f
6688365	Fatima	Daniela	Malave	Tejeda	f	1962-03-04	01-01-22-001-0049	f
14960398	Joel	Ivan	Lovato	Montemayor	m	1955-03-23	01-01-22-001-0049	f
9059533	Sebastian	Sebastian	Fajardo	Aparicio	m	1958-03-10	01-01-22-001-0049	f
10044927	Alberto	Martin	Urrutia	Reynoso	m	1958-10-28	01-01-22-001-0049	f
6268197	Ivan	Jaime	Delagarza	Barroso	m	1958-06-09	01-01-22-001-0049	f
11826421	Miguel	Oliver	Zamora	Mojica	m	1966-11-14	01-01-22-001-0049	f
8157963	Rafael	Andres	Miguel	Vega	m	1954-04-11	01-01-22-001-0049	f
9471797	Nicolas	Izan	Garay	Santana	m	1956-09-26	01-01-22-001-0049	f
12814348	Martin	Andres	Godínez	Tejada	m	1968-06-20	01-01-22-001-0049	f
9743039	Angel	Oliver	Alcántar	Haro	m	1962-09-27	01-01-22-001-0049	f
11253096	Lidia	Vera	Rodríguez	Aragón	f	1952-05-20	01-01-22-001-0049	f
12570783	Alberto	Yorman	Oliva	Paz	m	1968-12-30	01-01-22-001-0049	f
14138120	Cristian	Angel	Salazar	Delacrúz	m	1952-03-15	01-01-22-001-0049	f
10534814	Fatima	Bella	Figueroa	Velasco	f	1969-07-09	01-01-22-001-0049	f
13843328	Martina	Eva	Díaz	Palomino	f	1953-12-20	01-01-22-001-0049	f
6691938	Leandro	Hugo	Calvo	Caro	m	1964-06-08	01-01-22-001-0049	f
11633365	Adam	Jose	Delacrúz	Verduzco	m	1960-07-28	01-01-22-001-0049	f
9972430	Ruben	Miguel	Benavides	Marín	m	1952-06-15	01-01-22-001-0049	f
5181282	Aaron	Dario	Rodríguez	Soler	m	1968-01-28	01-01-22-001-0049	f
5602122	Celia	Carolina	Alba	Macias	f	1967-03-17	01-01-22-001-0049	f
9790962	Anna	Lucia	Maldonado	Loya	f	1962-01-24	01-01-22-001-0049	f
13175956	Oscar	Alberto	Zamora	Antón	m	1954-02-18	01-01-22-001-0049	f
9967497	Ismael	Mohamed	Urbina	Benítez	m	1967-10-25	01-01-22-001-0049	f
9500865	Eduardo	Sebastian	Zaragoza	Pereira	m	1954-06-25	01-01-22-001-0049	f
7318445	Naia	Alejandra	Abrego	Téllez	f	1955-07-30	01-01-22-001-0049	f
5751534	Miguel	Roberto	Pulido	Ruvalcaba	m	1961-08-14	01-01-22-001-0049	f
13200845	Ismael	Cesar	Hernando	Quesada	m	1965-10-15	01-01-22-001-0049	f
7555319	Santiago	Rodrigo	Arellano	Mendoza	m	1952-04-24	01-01-22-001-0049	f
10409607	Aaron	Javier	Rangel	Murillo	m	1963-01-22	01-01-22-001-0049	f
11332766	Laura	Lola	Tomas	Villagómez	f	1965-01-16	01-01-22-001-0049	f
14460015	Alexia	Carmen	Guerrero	Burgos	f	1952-01-10	01-01-22-001-0049	f
12013284	Martin	Daniel	Vergara	Arribas	m	1963-06-11	01-01-22-001-0049	f
13928916	Yaiza	Iria	Rivas	Casares	f	1959-08-11	01-01-22-0001-0028	f
12828350	Lucas	Francisco Javier	Saiz	Gurule	m	1966-12-07	01-01-22-001-0049	f
12129084	Anna	Pau	Tijerina	Lozada	f	1958-04-20	01-01-22-001-0049	f
6812882	Carmen	Laura	Llorente	Guillen	f	1964-04-20	01-01-22-001-0049	f
12866623	Marta	Andrea	Muñiz	Galindo	f	1963-03-05	01-01-22-001-0049	f
8802204	Luisa	Silvia	Vergara	Hernández	f	1954-09-22	01-01-22-001-0049	f
11849708	Joan	Sofia	Tejeda	Simón	f	1968-07-21	01-01-22-001-0049	f
14264612	Hector	Gabriel	Cazares	Zapata	m	1956-03-02	01-01-22-001-0049	f
7264357	Pedro	Gabriel	Guillen	Domínquez	m	1955-07-18	01-01-22-001-0049	f
13374168	Iria	Ana	Trujillo	Roca	f	1954-11-05	01-01-22-001-0049	f
11750202	Raul	Oscar	Calvillo	Blasco	m	1966-06-19	01-01-22-001-0049	f
11104794	Adria	Eric	Bernal	Lorente	m	1953-02-08	01-01-22-001-0049	f
10863476	Saul	Sergio	Ayala	Sáez	m	1956-12-08	01-01-22-001-0049	f
10847216	Maria	Carlota	Olivera	Piñeiro	f	1967-04-20	01-01-22-001-0049	f
8783037	Jose	Ivan	Armas	Herrera	m	1967-11-17	01-01-22-001-0049	f
11458929	Pedro	Miguel	León	Madrid	m	1952-05-09	01-01-22-001-0049	f
13689918	Carla	Rocio	Nevárez	Piña	f	1963-10-17	01-01-22-001-0049	f
10256751	Izan	Luis	Madrigal	Carbajal	m	1965-12-16	01-01-22-001-0049	f
9547392	Cesar	Francisco Javier	Osorio	Rodríquez	m	1957-08-16	01-01-22-001-0049	f
9342210	Naiara	Andrea	Lira	Estévez	f	1956-05-23	01-01-22-001-0049	f
11166979	Marti	Lara	Tomas	Chavarría	f	1968-07-05	01-01-22-001-0049	f
12222329	Alba	Marti	Sandoval	Rangel	f	1969-02-11	01-01-22-001-0049	f
11018557	Cesar	Javier	Bustos	Sancho	m	1966-08-19	01-01-22-001-0049	f
11020088	Joel	Jesus	Maldonado	Granado	m	1969-11-27	01-01-22-001-0049	f
8747628	Carmen	Isabel	Asensio	Leyva	f	1969-10-11	01-01-22-001-0049	f
6408239	Nadia	Carlota	García	Aguilera	f	1953-11-02	01-01-22-001-0049	f
7368471	Isaac	Marcos	Rubio	Ledesma	m	1967-07-17	01-01-22-001-0049	f
11407608	Roberto	Ignacio	Laureano	Olvera	m	1968-03-14	01-01-22-001-0049	f
6165965	Mara	Mar	Ureña	Sáenz	f	1957-07-30	01-01-22-001-0049	f
12093648	Dario	Yorman	Marrero	Moya	m	1952-07-28	01-01-22-001-0049	f
7680058	Javier	Alvaro	Mendoza	Muñoz	m	1954-03-02	01-01-22-001-0049	f
5237272	Ainara	Diana	Domínquez	Zúñiga	f	1960-07-03	01-01-22-001-0049	f
9727455	Guillem	Angel	Sotelo	Salcedo	m	1969-05-20	01-01-22-001-0049	f
14850062	Domingo	Aleix	Abreu	Balderas	m	1952-12-13	01-01-22-001-0049	f
8126372	Blanca	Yaiza	Venegas	Reséndez	f	1965-05-25	01-01-22-001-0049	f
14272411	Juan	Manuel	Valdez	Flórez	m	1966-08-15	01-01-22-001-0049	f
6717617	Mohamed	Isaac	Cuesta	Mateo	m	1953-01-05	01-01-22-001-0049	f
6653201	Gabriel	Cesar	Soliz	Gaitán	m	1956-05-04	01-01-22-001-0049	f
11481852	Domingo	Jordi	Urbina	Tomas	m	1961-10-23	01-01-22-001-0049	f
10521168	Lara	Alexia	Ayala	Armendáriz	f	1954-01-20	01-01-22-001-0049	f
12388736	Adriana	Nayara	Heredia	Irizarry	f	1963-10-03	01-01-22-001-0049	f
7869153	Iria	Nayara	Escribano	Patiño	f	1960-05-17	01-01-22-001-0049	f
14888287	Nayara	Jimena	Quiñónez	Llorente	f	1962-03-12	01-01-22-001-0049	f
10005432	Jorge	Oswaldo	Ochoa	Vega	m	1965-08-21	01-01-22-001-0049	f
14486087	Lola	Nerea	Alvarado	Prado	f	1964-11-03	01-01-22-001-0049	f
8775704	Adam	Jon	Becerra	Llorente	m	1966-09-18	01-01-22-001-0049	f
12045026	Leo	Mateo	Rey	Pardo	m	1959-08-12	01-01-22-001-0049	f
12474844	Celia	Marina	Almanza	Olivera	f	1953-01-18	01-01-22-001-0049	f
13726540	Eric	Jan	Vela	Mena	m	1954-04-06	01-01-22-001-0049	f
12485683	Joan	Valentina	Briseño	Delgado	f	1962-09-04	01-01-22-001-0049	f
6165305	Jordi	Cristian	Hinojosa	Santos	m	1954-08-31	01-01-22-001-0049	f
8353016	Hugo	Alvaro	Blasco	Ceja	m	1962-02-14	01-01-22-001-0049	f
12367033	Lara	Ainara	Cano	Zapata	f	1956-04-15	01-01-22-001-0049	f
12515617	Ruben	Omar	Rey	Cordero	m	1960-03-17	01-01-22-001-0049	f
13831968	Ruben	Diego	Peña	Fuentes	m	1959-01-17	01-01-22-001-0049	f
13425199	Nora	Eva	Mayorga	Perea	f	1957-01-14	01-01-22-001-0049	f
6958294	Leo	Aaron	Sanches	Bernal	m	1954-01-20	01-01-22-001-0049	f
14853245	Oscar	Mario	Manzano	Quintero	m	1963-06-14	01-01-22-001-0049	f
14189713	Aroa	Blanca	Verdugo	Roca	f	1969-06-26	01-01-22-001-0049	f
10259712	Jan	Bruno	Quesada	Rivas	m	1966-10-04	01-01-22-001-0049	f
13762777	Domingo	Eric	Rael	Peres	m	1960-06-29	01-01-22-001-0049	f
6419645	Maria	Africa	Acevedo	Olmos	f	1962-01-08	01-01-22-001-0049	f
10862380	Cristina	Mireia	Navas	Oliver	f	1955-07-17	01-01-22-001-0049	f
8444734	Clara	Carlota	Niño	Henríquez	f	1952-05-10	01-01-22-001-0049	f
5086429	Eric	Manuel	Aguilar	Valladares	m	1966-06-22	01-01-22-001-0049	f
7714847	Celia	Antonia	Garica	Ochoa	f	1969-11-12	01-01-22-001-0049	f
12256567	Javier	Rayan	Alva	Delagarza	m	1963-04-29	01-01-22-001-0049	f
11276669	Alonso	Samuel	Lira	Navarro	m	1965-02-09	01-01-22-001-0049	f
7103247	Ian	Mohamed	Naranjo	Apodaca	m	1966-08-03	01-01-22-001-0049	f
14975964	Salma	Nora	Laureano	Cazares	f	1968-11-16	01-01-22-001-0049	f
12582229	Ivan	Jorge	Anguiano	Sisneros	m	1964-04-13	01-01-22-001-0049	f
9562214	Cristina	Paula	Delgadillo	Guzmán	f	1953-03-23	01-01-22-001-0049	f
6188801	Saul	Carlos	Vila	Zarate	m	1956-07-03	01-01-22-001-0049	f
11719063	Sara	Sara	Arenas	Valero	f	1961-08-30	01-01-22-001-0049	f
12539885	Nil	Ona	Lugo	Caro	f	1965-08-25	01-01-22-001-0049	f
14221057	Mario	Aleix	Espinal	Alonzo	m	1957-01-28	01-01-22-001-0049	f
6259902	Jan	Ruben	Mota	Zayas	m	1964-12-16	01-01-22-001-0049	f
7064933	Omar	Jose	Quintero	Miguel	m	1961-11-15	01-01-22-001-0049	f
14179379	Emma	Leyre	Limón	Colón	f	1969-08-08	01-01-22-001-0049	f
6283746	Alicia	Andrea	Toledo	Calvo	f	1968-08-01	01-01-22-001-0049	f
5678302	Dario	Rodrigo	Esquibel	Cruz	m	1960-06-13	01-01-22-001-0049	f
13140973	Rafael	Pedro	Blázquez	Medina	m	1953-11-12	01-01-22-001-0049	f
11105405	Lucas	Marco	Ledesma	Sandoval	m	1961-12-25	01-01-22-001-0049	f
12513825	Jimena	Alejandra	Loera	Valadez	f	1959-04-05	01-01-22-001-0049	f
12435447	Santiago	Cristian	Cepeda	Castellanos	m	1963-08-07	01-01-22-001-0049	f
12501060	Leo	Oswaldo	Bermejo	Requena	m	1961-10-01	01-01-22-001-0049	f
9205304	Cesar	Leandro	Sedillo	Vargas	m	1969-09-02	01-01-22-0001-0028	f
8660059	Alvaro	Ignacio	Montoya	Urbina	m	1965-01-19	01-01-22-001-0049	f
8569668	Nora	Jana	Centeno	Fuentes	f	1956-01-05	01-01-22-001-0049	f
5946544	David	Cesar	Muñoz	Córdova	m	1962-12-08	01-01-22-001-0049	f
5077875	Pol	Roberto	Valverde	Nevárez	m	1960-05-14	01-01-22-001-0049	f
11080248	Ona	Lidia	Banda	Trujillo	f	1963-03-30	01-01-22-001-0049	f
6971375	Gonzalo	Ismael	Urrutia	Balderas	m	1954-07-21	01-01-22-001-0049	f
11090788	Ines	Natalia	Romero	Gallardo	f	1957-12-25	01-01-22-001-0049	f
11835520	Oliver	Yorman	Sáenz	Sarabia	m	1953-03-07	01-01-22-001-0049	f
10511541	Gabriel	Jorge	Núñez	Niño	m	1959-07-30	01-01-22-001-0049	f
10539685	Rodrigo	Ander	Aragón	Preciado	m	1954-11-02	01-01-22-001-0049	f
14609179	Luisa	Sofia	Casares	Mata	f	1955-06-20	01-01-22-001-0049	f
8560763	Isaac	Marcos	Nazario	Medrano	m	1964-01-25	01-01-22-001-0049	f
11888963	Blanca	Ana	Aguirre	Munguía	f	1952-06-28	01-01-22-001-0049	f
6939377	Laia	Vega	Vela	Colón	f	1956-09-01	01-01-22-001-0049	f
5378528	Ariadna	Eva	Castellano	Alicea	f	1954-03-16	01-01-22-001-0049	f
7633370	Fatima	Luna	Rincón	Zambrano	f	1959-06-14	01-01-22-001-0049	f
6999873	Alexia	Elsa	Piña	Duarte	f	1968-01-29	01-01-22-001-0049	f
10308899	Yaiza	Leire	Ortíz	Nava	f	1959-01-05	01-01-22-001-0049	f
13736625	Alba	Ana	Ochoa	Paz	f	1960-07-15	01-01-22-001-0049	f
5388228	Carolina	Ariadna	Tejeda	Hidalgo	f	1961-10-07	01-01-22-001-0049	f
5069944	Lara	Lidia	Nava	Rivas	f	1952-02-23	01-01-22-001-0049	f
6026156	Leire	Alexandra	Tórrez	Segovia	f	1966-10-07	01-01-22-001-0049	f
5149373	Joan	Laia	Quintana	Alfaro	f	1961-07-21	01-01-22-001-0049	f
9308538	Ignacio	Ian	Segovia	Zapata	m	1958-02-20	01-01-22-001-0049	f
14535848	Leo	Mohamed	Ocampo	Díaz	m	1964-09-08	01-01-22-001-0049	f
5083899	Martina	Berta	Blázquez	Castro	f	1957-09-14	01-01-22-001-0049	f
13721945	Carlos	Pablo	Frías	Berríos	m	1956-03-05	01-01-22-001-0049	f
12875440	Daniela	Patricia	Alcaraz	Sancho	f	1965-05-15	01-01-22-001-0049	f
11661448	Miguel	Alonso	Lorente	Fonseca	m	1963-08-26	01-01-22-001-0049	f
10446891	Julia	Luisa	Botello	Lemus	f	1963-09-03	01-01-22-001-0049	f
5519371	Gonzalo	Fernando	Gamez	Aparicio	m	1954-11-27	01-01-22-001-0049	f
11124065	Gonzalo	Rodrigo	Serra	Escobedo	m	1957-05-02	01-01-22-001-0049	f
14306019	Sandra	Mar	Cantú	Meraz	f	1955-01-11	01-01-22-001-0049	f
13633777	Carmen	Nerea	Quintanilla	Carrillo	f	1962-12-30	01-01-22-001-0049	f
10183380	Ian	Daniel	Manzano	Cantú	m	1953-01-05	01-01-22-001-0049	f
12490711	Alexandra	Irene	Sandoval	Quiroz	f	1968-10-01	01-01-22-001-0049	f
14460987	Ane	Valentina	Abreu	Deleón	f	1969-01-07	01-01-22-001-0049	f
6954142	Pedro	Saul	Robles	Zamudio	m	1960-02-24	01-01-22-001-0049	f
7816728	Adrian	Gabriel	Oquendo	Moral	m	1955-09-26	01-01-22-001-0049	f
5843447	Jesus	Ander	Mesa	Bautista	m	1959-01-03	01-01-22-001-0049	f
10705966	Rocio	Alexandra	Banda	Delgadillo	f	1960-12-31	01-01-22-001-0049	f
8621106	Miguel	Martin	Delafuente	Escribano	m	1964-09-06	01-01-22-001-0049	f
8258738	Miguel	Oliver	Castillo	Galindo	m	1963-01-28	01-01-22-001-0049	f
6684942	Emma	Diana	Alemán	Tovar	f	1962-01-04	01-01-22-001-0049	f
14477776	Marta	Mireia	Rosario	Rincón	f	1960-11-29	01-01-22-001-0049	f
12217944	Daniel	Ivan	Alanis	Herrera	m	1961-08-26	01-01-22-001-0049	f
13954377	Irene	Marti	Ozuna	Alfaro	f	1956-05-29	01-01-22-001-0049	f
8671299	Dario	Cesar	Carbonell	Barajas	m	1967-08-24	01-01-22-001-0049	f
10653734	Leonardo	Luis	Saucedo	Mojica	m	1963-10-27	01-01-22-001-0049	f
10199132	Ruben	Miguel	Espino	Solorzano	m	1966-02-14	01-01-22-001-0049	f
7067934	David	Alberto	Jiménez	Curiel	m	1955-08-13	01-01-22-001-0049	f
13579514	Javier	Santiago	Aponte	Archuleta	m	1967-02-22	01-01-22-001-0049	f
11931228	Isaac	Cristian	Sánchez	Véliz	m	1966-12-30	01-01-22-001-0049	f
10199632	Joan	Marina	Toledo	Camacho	f	1956-03-31	01-01-22-001-0049	f
14881146	Pau	Paola	Hernández	Zamora	f	1963-01-27	01-01-22-001-0049	f
10975730	Lucia	Candela	Benítez	Tejeda	f	1954-05-12	01-01-22-001-0049	f
13240692	Alvaro	Joel	Carrero	Arriaga	m	1964-10-11	01-01-22-001-0049	f
13107069	Marti	Blanca	Ojeda	López	f	1965-02-14	01-01-22-001-0049	f
9159907	Pedro	Daniel	Calero	Aparicio	m	1967-09-14	01-01-22-001-0049	f
5988342	Daniel	Alberto	Castañeda	Guevara	m	1956-12-20	01-01-22-001-0049	f
8727534	Eric	Alvaro	Rosado	Quintanilla	m	1964-06-25	01-01-22-001-0049	f
13872968	Sofia	Ainara	Esparza	Ruiz	f	1966-07-16	01-01-22-001-0049	f
8435511	Antonia	Elsa	Vila	Verdugo	f	1952-09-18	01-01-22-001-0049	f
5577517	Guillermo	Izan	Martínez	Bravo	m	1966-09-27	01-01-22-001-0049	f
8928937	Helena	Luisa	Apodaca	Navarrete	f	1961-12-15	01-01-22-001-0049	f
14628612	Joan	Elena	Gaona	Armas	f	1953-12-24	01-01-22-001-0049	f
11296502	Miguel	Francisco Javier	Armendáriz	Serra	m	1962-08-13	01-01-22-001-0049	f
13201328	Alexia	Alma	Romo	Giménez	f	1964-02-09	01-01-22-001-0049	f
8939818	Samuel	Daniel	Tejada	Laboy	m	1960-10-14	01-01-22-001-0049	f
10438920	Aaron	Ivan	Ceballos	Vila	m	1969-10-12	01-01-22-001-0049	f
5525086	Roberto	Hugo	Villaseñor	Pizarro	m	1968-09-23	01-01-22-001-0049	f
14342137	Sandra	Victoria	Montenegro	Olivares	f	1958-08-22	01-01-22-001-0049	f
11412348	Silvia	Naiara	Oliva	Alaniz	f	1958-12-02	01-01-22-001-0049	f
10223233	Hector	Luis	Font	Quintero	m	1954-12-08	01-01-22-001-0049	f
13712918	Naia	Claudia	Mora	Munguía	f	1959-11-04	01-01-22-001-0049	f
7143280	Anna	Valentina	Cruz	Giménez	f	1963-03-08	01-01-22-001-0049	f
10651223	Jaime	Victor	Suárez	Caro	m	1962-03-02	01-01-22-001-0049	f
12431962	Ana	Elsa	Lucero	Herrera	f	1959-02-20	01-01-22-001-0049	f
8966243	Eric	Manuel	Pacheco	Huerta	m	1957-10-07	01-01-22-001-0049	f
5969968	Cristina	Noelia	Banda	Serna	f	1953-12-08	01-01-22-001-0049	f
10169526	Jordi	Aleix	Barrientos	Rojo	m	1967-06-03	01-01-22-001-0049	f
9933984	Lara	Carmen	Hernández	Briseño	f	1956-12-30	01-01-22-001-0049	f
10753863	Gabriel	Jesus	Pascual	Ocasio	m	1962-09-01	01-01-22-001-0049	f
12489139	Daniel	Francisco	Deleón	Echevarría	m	1967-09-21	01-01-22-001-0049	f
8307495	Adria	Francisco	Santamaría	Sáez	m	1968-09-09	01-01-22-0001-0028	f
11155251	Hector	Sebastian	Padilla	Partida	m	1956-07-19	01-01-22-001-0049	f
5061725	Iria	Carolina	Luis	Holguín	f	1959-09-30	01-01-22-001-0049	f
7421884	Mireia	Isabel	Razo	Guillen	f	1959-10-28	01-01-22-001-0049	f
10201616	Berta	Nil	Meléndez	Velásquez	f	1958-06-14	01-01-22-001-0049	f
9993649	Candela	Isabel	Mondragón	Frías	f	1967-02-02	01-01-22-001-0049	f
10097215	Marco	Eric	Luna	Valero	m	1967-06-25	01-01-22-001-0049	f
7759658	Ander	Mohamed	Perea	Alba	m	1958-06-30	01-01-22-001-0049	f
7982576	Francisco Javier	Fernando	Alva	Castellano	m	1967-12-02	01-01-22-001-0049	f
9167674	Saul	Dario	Cerda	Bustamante	m	1968-09-02	01-01-22-001-0049	f
5236852	Miguel	Raul	Olivera	Conde	m	1968-05-02	01-01-22-001-0049	f
11327457	Nadia	Maria	Barreto	Armas	f	1955-12-16	01-01-22-001-0049	f
9895647	Aaron	Miguel	Villaseñor	Llorente	m	1969-08-09	01-01-22-001-0049	f
10124495	Martin	Jorge	Morales	Acevedo	m	1955-11-07	01-01-22-001-0049	f
5822732	Leire	Maria	Benavides	Jaime	f	1960-02-06	01-01-22-001-0049	f
10020317	Marc	Adrian	Adame	Badillo	m	1965-11-27	01-01-22-001-0049	f
8412061	Luisa	Alma	Sáenz	Girón	f	1960-11-03	01-01-22-001-0049	f
13361313	Berta	Leire	Regalado	Casillas	f	1960-06-25	01-01-22-001-0049	f
10615761	Ivan	Oswaldo	Ramos	Carrión	m	1964-10-17	01-01-22-001-0049	f
5641666	Ainara	Julia	Caraballo	Peres	f	1966-12-01	01-01-22-001-0049	f
9757857	Aroa	Bella	Nazario	Sandoval	f	1966-08-16	01-01-22-001-0049	f
7997879	Oswaldo	Jesus	Figueroa	Luna	m	1967-02-09	01-01-22-001-0049	f
6458018	Martina	Andrea	Quesada	Becerra	f	1969-10-27	01-01-22-001-0049	f
13201787	Mar	Cristina	Izquierdo	Alvarado	f	1969-05-10	01-01-22-001-0049	f
8991294	Elena	Marti	León	Abeyta	f	1961-10-24	01-01-22-001-0049	f
6203146	Jana	Elsa	Valverde	Chavarría	f	1968-01-13	01-01-22-001-0049	f
12230094	Mateo	Santiago	Sanabria	Rangel	m	1962-07-07	01-01-22-001-0049	f
5520640	Dario	Manuel	Guzmán	Carvajal	m	1961-06-19	01-01-22-001-0049	f
5369170	Domingo	Pablo	Moral	Márquez	m	1952-03-23	01-01-22-001-0049	f
12622630	Oscar	Gonzalo	De Anda	Abrego	m	1967-08-14	01-01-22-001-0049	f
11361576	Miguel	Sergio	Guzmán	Angulo	m	1956-12-04	01-01-22-001-0049	f
7969434	Francisco Javier	Aaron	Delgadillo	Armijo	m	1964-11-18	01-01-22-001-0049	f
9396420	Ana	Silvia	Ruiz	Soler	f	1953-06-05	01-01-22-001-0049	f
5214456	Gabriel	Hugo	Roybal	Font	m	1969-02-16	01-01-22-001-0049	f
14946404	Yorman	Adria	De la torre	Montes	m	1969-03-13	01-01-22-001-0049	f
11077341	Joan	Jana	Santana	Delao	f	1954-01-23	01-01-22-001-0049	f
10832539	Helena	Sofia	Cruz	Alonso	f	1951-12-28	01-01-22-001-0049	f
7826843	Carlos	Luis	Arroyo	Peralta	m	1967-08-13	01-01-22-001-0049	f
6753285	Aleix	Angel	Cantú	Ortiz	m	1958-03-25	01-01-22-001-0049	f
5943551	Eric	Diego	Blanco	Casanova	m	1960-02-15	01-01-22-001-0049	f
11028371	Ian	Enrique	Mota	Cuesta	m	1962-09-23	01-01-22-001-0049	f
13987640	Nicolas	Antonio	Portillo	De la cruz	m	1966-12-20	01-01-22-001-0049	f
12120337	Victoria	Africa	Guerrero	Córdova	f	1961-04-19	01-01-22-001-0049	f
8945650	Jaime	Aleix	Abrego	Verdugo	m	1962-05-12	01-01-22-001-0049	f
14484214	Nadia	Celia	Collazo	Olivera	f	1954-02-16	01-01-22-001-0049	f
9723946	Enrique	Jan	Becerra	Esteve	m	1958-08-13	01-01-22-001-0049	f
11689196	Valeria	Natalia	Ocasio	Varela	f	1963-05-18	01-01-22-001-0049	f
8813094	Helena	Miriam	Velásquez	Miramontes	f	1960-01-03	01-01-22-001-0049	f
14062637	Nadia	Candela	Samaniego	Oquendo	f	1959-09-19	01-01-22-001-0049	f
9518159	Bruno	Lucas	Lázaro	Bañuelos	m	1969-06-18	01-01-22-001-0049	f
13620768	Jan	David	Delafuente	Alba	m	1961-08-11	01-01-22-001-0049	f
13051405	Aaron	Gabriel	Feliciano	Ontiveros	m	1959-07-05	01-01-22-001-0049	f
7872063	Ainara	Helena	Sola	Hernandes	f	1957-08-24	01-01-22-001-0049	f
13549457	Emma	Luisa	Luque	Sánchez	f	1955-02-13	01-01-22-001-0049	f
11828871	Aaron	Raul	Delvalle	Sauceda	m	1954-04-28	01-01-22-001-0049	f
7472825	Abril	Martina	Viera	Saiz	f	1954-07-04	01-01-22-001-0043	f
7307824	Alba	Abril	Mota	Gaytán	f	1952-04-28	01-01-22-001-0043	f
6230122	Omar	Daniel	Soto	Abad	m	1952-07-22	01-01-22-001-0043	f
8319519	Oliver	Adria	Villalba	Figueroa	m	1967-06-21	01-01-22-001-0043	f
6205367	Carlos	Raul	Jimínez	Aguado	m	1963-05-08	01-01-22-001-0043	f
6608717	Francisco	Marcos	Lorente	Bravo	m	1958-11-21	01-01-22-001-0043	f
8748169	Mateo	Oliver	Mireles	Espinosa	m	1957-08-10	01-01-22-001-0043	f
5915797	Mateo	Martin	Terán	Zepeda	m	1960-02-18	01-01-22-001-0043	f
7821422	Nora	Mar	Tórrez	Caballero	f	1968-10-11	01-01-22-001-0043	f
5835121	Pol	Rayan	Alfaro	Carballo	m	1952-05-26	01-01-22-001-0043	f
6166596	Oscar	Samuel	Gurule	Hurtado	m	1961-08-18	01-01-22-001-0043	f
12181260	Domingo	Joel	Quesada	Lira	m	1969-01-24	01-01-22-001-0043	f
11949759	Eduardo	Ivan	Fierro	Gaona	m	1958-04-21	01-01-22-001-0043	f
13112748	Patricia	Lola	Pichardo	Espinoza	f	1954-11-06	01-01-22-001-0043	f
12491859	Teresa	Vega	Garibay	Tamez	f	1960-08-01	01-01-22-001-0043	f
10767732	Sara	Beatriz	Lorente	Cervantes	f	1962-09-09	01-01-22-001-0043	f
10535150	Helena	Adriana	Quintero	Garibay	f	1965-05-19	01-01-22-001-0043	f
14655828	Nil	Natalia	Navarrete	De la cruz	f	1965-02-17	01-01-22-001-0043	f
6516941	Pedro	Miguel	Hernádez	Jaime	m	1969-08-21	01-01-22-001-0043	f
6685655	Carlos	Francisco	Romero	Ríos	m	1966-06-06	01-01-22-001-0043	f
12924073	Hugo	Manuel	Pichardo	Soto	m	1964-08-06	01-01-22-001-0043	f
8367148	Adriana	Luna	Ruvalcaba	Meléndez	f	1962-03-01	01-01-22-001-0043	f
13822224	Nadia	Africa	Amaya	Domingo	f	1959-08-29	01-01-22-001-0043	f
14996653	Noa	Lara	De Anda	Alvarado	f	1954-03-10	01-01-22-001-0043	f
6853199	Claudia	Jimena	Preciado	Montenegro	f	1962-01-09	01-01-22-001-0043	f
12638877	Pau	Carolina	Nevárez	Ortíz	f	1968-03-30	01-01-22-001-0043	f
7804554	Jordi	Lucas	Carvajal	Espino	m	1965-01-14	01-01-22-001-0043	f
12398136	Gabriel	Pedro	Deleón	Franco	m	1954-10-13	01-01-22-001-0043	f
9895281	Mireia	Yaiza	Vanegas	Hernandes	f	1961-10-10	01-01-22-001-0043	f
14507805	Gonzalo	Manuel	Mateos	Matías	m	1952-07-25	01-01-22-001-0043	f
12685362	Bruno	Mario	Carrasquillo	Regalado	m	1963-08-15	01-01-22-001-0043	f
14079688	Adriana	Clara	Benavides	Sierra	f	1969-02-20	01-01-22-001-0043	f
10959870	Alonso	Oscar	Adorno	Rodrígez	m	1957-03-18	01-01-22-001-0043	f
9159456	Berta	Daniela	Armenta	Calderón	f	1968-01-20	01-01-22-001-0043	f
6856179	Pol	Erik	Mateos	Monroy	m	1968-07-01	01-01-22-001-0043	f
6661720	Nerea	Joan	León	Velásquez	f	1959-02-21	01-01-22-001-0043	f
11590094	Rodrigo	Adrian	Salcedo	Farías	m	1964-02-18	01-01-22-001-0043	f
7001414	Guillem	Andres	Venegas	Ávila	m	1969-05-28	01-01-22-001-0043	f
5796588	Jon	Izan	Carrion	Escribano	m	1962-02-20	01-01-22-001-0043	f
6941898	Juan	Ian	Botello	Véliz	m	1963-04-29	01-01-22-001-0043	f
12059183	Miguel	Oswaldo	Cano	Diez	m	1954-06-21	01-01-22-001-0043	f
13887561	Alonso	Angel	Martín	Marcos	m	1962-09-27	01-01-22-001-0043	f
7049217	Ariadna	Leire	Tamez	Ramos	f	1956-02-18	01-01-22-001-0043	f
6098675	Vera	Blanca	Barrios	Nájera	f	1968-06-28	01-01-22-001-0043	f
6345519	Victoria	Erika	Viera	Lozada	f	1969-09-17	01-01-22-001-0043	f
5405669	Alonso	Antonio	Casas	Tello	m	1967-08-23	01-01-22-001-0043	f
14290454	Sandra	Marta	Fernández	Armenta	f	1957-12-16	01-01-22-001-0043	f
5209966	Pablo	Adria	Montoya	Alvarado	m	1962-10-05	01-01-22-001-0043	f
7048742	Andrea	Nayara	Solorio	Concepción	f	1965-10-31	01-01-22-001-0043	f
6849626	Alejandro	Sebastian	Gómez	Osorio	m	1954-08-07	01-01-22-001-0043	f
5390620	Jaime	Aleix	Heredia	Llamas	m	1958-06-11	01-01-22-001-0043	f
6877538	Fernando	Rodrigo	Navarrete	Paz	m	1961-11-18	01-01-22-001-0043	f
9516481	Carlota	Maria	Mateo	Segovia	f	1952-08-20	01-01-22-001-0043	f
14053732	Jose	Juan	Espinosa	Velasco	m	1954-09-11	01-01-22-001-0043	f
9559217	Emma	Salma	Rendón	Treviño	f	1960-06-01	01-01-22-001-0043	f
8057186	Hugo	Jon	Alcala	Girón	m	1956-09-22	01-01-22-001-0043	f
10027514	Ona	Candela	Santiago	Chapa	f	1965-09-16	01-01-22-001-0043	f
14013112	Carolina	Anna	Vera	Lucero	f	1957-12-26	01-01-22-001-0043	f
11807991	Adrian	Erik	Leiva	Ruíz	m	1952-07-15	01-01-22-001-0043	f
8171704	Yorman	Andres	Granados	Sotelo	m	1965-04-06	01-01-22-001-0043	f
8107139	Miguel	Cesar	Velázquez	Montemayor	m	1953-06-04	01-01-22-001-0043	f
11567505	Raul	Jorge	Mateo	Velásquez	m	1962-03-03	01-01-22-001-0043	f
14207701	Cristian	Alex	Porras	Fernández	m	1968-01-12	01-01-22-001-0043	f
12386212	Eduardo	Santiago	Quesada	Marco	m	1965-11-02	01-01-22-001-0043	f
7295084	Luisa	Lola	Peralta	Rosado	f	1960-07-31	01-01-22-001-0043	f
5966386	Sofia	Patricia	Córdova	Negrete	f	1963-11-15	01-01-22-001-0043	f
5567653	Ruben	Lucas	Sevilla	Viera	m	1968-11-23	01-01-22-001-0043	f
9064065	Alvaro	Aleix	Guerrero	Pardo	m	1957-02-21	01-01-22-001-0043	f
12048043	Izan	Ander	Pelayo	Bonilla	m	1963-06-15	01-01-22-001-0043	f
13415387	Luis	Enrique	Riojas	Pozo	m	1955-10-06	01-01-22-001-0043	f
5383184	Adam	Rodrigo	Olivares	Leal	m	1969-03-20	01-01-22-001-0043	f
14214215	Lucas	Alvaro	Costa	Santos	m	1968-08-06	01-01-22-001-0043	f
13678025	Mario	Gonzalo	Valverde	Redondo	m	1968-06-28	01-01-22-001-0043	f
11497984	Jesus	Hector	Navarro	Muñiz	m	1955-03-21	01-01-22-001-0043	f
7178557	Oswaldo	Jan	Verdugo	Sánchez	m	1956-02-21	01-01-22-001-0043	f
12846524	Berta	Maria	Juan	Palacios	f	1968-01-17	01-01-22-001-0043	f
14046820	Alberto	David	Juan	Benito	m	1959-03-29	01-01-22-001-0043	f
12434002	Vera	Claudia	Abeyta	Salcedo	f	1956-07-22	01-01-22-001-0043	f
11624458	Isabel	Martina	Franco	Adame	f	1959-12-03	01-01-22-001-0043	f
14920495	Leandro	Antonio	Roybal	Villa	m	1958-01-05	01-01-22-001-0043	f
6113469	Ariadna	Mar	Cuellar	Blanco	f	1965-03-20	01-01-22-001-0043	f
11432433	Valentina	Irene	Dueñas	Verdugo	f	1952-03-22	01-01-22-001-0043	f
6531879	Lucia	Julia	Caro	Fuentes	f	1953-10-27	01-01-22-001-0043	f
10953515	Marina	Laura	Anguiano	Patiño	f	1967-08-15	01-01-22-001-0043	f
9566372	Elsa	Valentina	Gallegos	Villareal	f	1964-09-15	01-01-22-001-0043	f
12925638	Mario	Ander	Altamirano	Rico	m	1957-06-10	01-01-22-001-0043	f
13587853	Naiara	Silvia	Noriega	Valencia	f	1964-07-17	01-01-22-001-0043	f
12198660	Paula	Silvia	Correa	Juárez	f	1952-11-11	01-01-22-001-0043	f
10855962	Jon	Aaron	Montalvo	Palomino	m	1959-02-12	01-01-22-001-0043	f
7655047	Ane	Raquel	Castaño	Hernádez	f	1958-03-05	01-01-22-001-0043	f
10171779	Joel	Bruno	Redondo	Mendoza	m	1952-10-07	01-01-22-001-0043	f
12448663	David	Joel	Botello	Acosta	m	1964-07-04	01-01-22-001-0043	f
11083569	Nerea	Nora	De Anda	Ureña	f	1961-07-27	01-01-22-001-0043	f
11181159	Rodrigo	Leo	Angulo	Jimínez	m	1959-11-23	01-01-22-001-0043	f
10952074	Cristina	Mara	Marroquín	Muro	f	1963-04-20	01-01-22-001-0043	f
5662417	Carlos	Alex	Hernández	Benito	m	1965-08-31	01-01-22-001-0043	f
9273140	Lucas	Ignacio	Rivas	Balderas	m	1957-10-30	01-01-22-001-0043	f
8235681	Jon	Leo	Delarosa	Riojas	m	1958-01-22	01-01-22-001-0043	f
9256065	Cristian	Lucas	Mendoza	Roldán	m	1968-11-01	01-01-22-001-0043	f
14319145	Jesus	David	Jurado	Matías	m	1954-06-12	01-01-22-001-0043	f
11729596	Isaac	Rafael	Hernádez	Rolón	m	1962-08-10	01-01-22-001-0043	f
9819659	Miguel	Aaron	Armenta	Roldán	m	1955-12-20	01-01-22-001-0043	f
5236533	Jose	Jan	Quintana	Olvera	m	1953-07-18	01-01-22-001-0043	f
9263957	Angela	Noa	Vera	Lemus	f	1965-02-12	01-01-22-001-0043	f
10713560	Domingo	Mario	Frías	Cabrera	m	1958-07-21	01-01-22-001-0043	f
14722797	Alejandra	Nerea	Ramos	Villegas	f	1969-01-08	01-01-22-001-0043	f
9212295	Marta	Rocio	Holguín	Montero	f	1968-06-19	01-01-22-001-0043	f
7265992	Cesar	Rafael	Véliz	Benavídez	m	1968-02-29	01-01-22-001-0043	f
11389111	Joan	Manuela	Batista	Luis	f	1963-05-21	01-01-22-001-0043	f
11431883	Africa	Lidia	Nájera	Cardona	f	1959-07-16	01-01-22-001-0043	f
11670635	Marc	Bruno	Gaitán	Gallego	m	1959-08-12	01-01-22-001-0043	f
5111620	Yorman	Jose	Villareal	Ochoa	m	1960-03-21	01-01-22-001-0043	f
7951551	Jordi	Jesus	Leyva	Sotelo	m	1957-02-15	01-01-22-001-0043	f
5537202	Javier	Jordi	Escobar	Villaseñor	m	1967-02-22	01-01-22-001-0043	f
13353713	Sebastian	Eduardo	Loya	Zúñiga	m	1958-05-13	01-01-22-001-0043	f
8172328	Juan	Alex	Medrano	Rico	m	1955-07-27	01-01-22-001-0043	f
14353929	Jimena	Nuria	Orellana	Viera	f	1959-01-03	01-01-22-001-0043	f
8510274	Marcos	Bruno	Zepeda	Ibáñez	m	1952-10-02	01-01-22-001-0043	f
9115221	Luna	Mireia	Guajardo	Hernádez	f	1962-02-27	01-01-22-001-0043	f
14713571	Luis	Adria	Gracia	Arenas	m	1960-04-19	01-01-22-001-0043	f
7413992	Ignacio	Guillermo	Quiñones	Verduzco	m	1968-07-15	01-01-22-001-0043	f
13651232	Anna	Aroa	Villalobos	Cano	f	1954-09-19	01-01-22-001-0043	f
12400554	Luisa	Elsa	Morales	Valero	f	1957-01-05	01-01-22-001-0043	f
5594352	Olivia	Blanca	Ibarra	Ledesma	f	1956-04-03	01-01-22-001-0043	f
8719028	Mario	Angel	Arce	Tovar	m	1969-04-03	01-01-22-001-0043	f
6556001	Rodrigo	Luis	Tórrez	Escobar	m	1965-05-01	01-01-22-001-0043	f
5699473	Aroa	Ane	Aguado	Saucedo	f	1967-12-31	01-01-22-001-0043	f
5426283	Lucia	Laura	Verdugo	Expósito	f	1964-11-17	01-01-22-001-0043	f
12334990	Luis	Manuel	Valero	Gastélum	m	1954-09-24	01-01-22-001-0043	f
13803807	Iria	Alba	Olivera	Garza	f	1953-03-24	01-01-22-001-0043	f
8001742	Laia	Alma	Zúñiga	Leiva	f	1959-01-29	01-01-22-001-0043	f
11722276	Elena	Carmen	Bueno	Terrazas	f	1960-02-02	01-01-22-001-0043	f
10507745	Adam	Leandro	Guevara	Nájera	m	1961-10-22	01-01-22-001-0043	f
12962864	Ignacio	Angel	Lucas	Roldan	m	1964-09-10	01-01-22-001-0043	f
11413713	Oswaldo	Domingo	Costa	Cavazos	m	1952-08-12	01-01-22-001-0043	f
9284980	Marti	Nuria	Vigil	Rosa	f	1962-07-25	01-01-22-001-0043	f
8506432	Leire	Valeria	Cisneros	Preciado	f	1961-01-24	01-01-22-001-0043	f
9354616	Marcos	Oscar	Ruvalcaba	Reséndez	m	1966-12-14	01-01-22-001-0043	f
9606671	Ivan	Manuel	Negrete	Zambrano	m	1966-06-26	01-01-22-001-0043	f
7376372	Samuel	Eduardo	Ibarra	Cintrón	m	1965-11-29	01-01-22-001-0043	f
13688051	Roberto	Alberto	Sanches	Vallejo	m	1962-07-29	01-01-22-001-0043	f
9406264	Nora	Nuria	Casares	Valdés	f	1962-11-15	01-01-22-001-0043	f
6871533	Emma	Antonia	Dávila	Ureña	f	1967-04-28	01-01-22-001-0043	f
7493085	Mara	Mireia	Trujillo	Ulibarri	f	1963-07-18	01-01-22-001-0043	f
5445104	Angel	Cristian	Gálvez	Jimínez	m	1966-02-07	01-01-22-001-0043	f
9850741	Nuria	Naia	Mayorga	Peres	f	1961-08-01	01-01-22-001-0043	f
13207347	Nahia	Ane	Navarro	Estévez	f	1955-05-19	01-01-22-001-0043	f
5203686	Helena	Ariadna	Paredes	Escudero	f	1959-04-10	01-01-22-001-0043	f
9793389	Cesar	Leo	Cortez	Apodaca	m	1962-10-17	01-01-22-001-0043	f
8506104	Jimena	Yaiza	Zelaya	Ordoñez	f	1966-11-25	01-01-22-001-0043	f
14797867	Vega	Vega	Domenech	Gallego	f	1964-11-21	01-01-22-001-0043	f
9979104	Eric	Leo	Razo	Carretero	m	1954-07-11	01-01-22-001-0043	f
5426222	Carlos	Adrian	Contreras	Gallardo	m	1958-10-09	01-01-22-001-0043	f
9604236	Cesar	Manuel	Lira	Rael	m	1962-06-03	01-01-22-001-0043	f
7879718	Oswaldo	Ivan	Espino	Corrales	m	1966-07-12	01-01-22-001-0043	f
7736630	Aroa	Alma	Saiz	Escamilla	f	1960-01-03	01-01-22-001-0043	f
14512439	Iria	Pau	Bañuelos	Jiménez	f	1959-08-18	01-01-22-001-0043	f
12592605	Marc	Juan	Piñeiro	Bahena	m	1956-09-29	01-01-22-001-0043	f
6028559	Domingo	Samuel	Costa	Garay	m	1960-03-02	01-01-22-001-0043	f
8420988	Nahia	Victoria	Pacheco	Baca	f	1956-07-11	01-01-22-001-0043	f
6568218	Lucas	Leandro	Montero	Delagarza	m	1955-10-09	01-01-22-001-0043	f
6415469	Jana	Claudia	Polanco	Serra	f	1966-05-07	01-01-22-001-0043	f
8020460	Eduardo	Rafael	Jasso	Alcaraz	m	1969-05-14	01-01-22-001-0043	f
10325907	Nadia	Diana	Arreola	Sancho	f	1958-10-02	01-01-22-001-0043	f
9349519	Adria	Rodrigo	Peralta	Trujillo	m	1969-03-18	01-01-22-001-0043	f
10551264	Ivan	Javier	Domenech	Girón	m	1966-03-13	01-01-22-001-0043	f
14908367	Fatima	Rocio	Peña	Enríquez	f	1952-08-02	01-01-22-001-0043	f
10991980	Laia	Ana	Maya	Montemayor	f	1962-11-06	01-01-22-001-0043	f
9852373	Ines	Elena	Merino	Salazar	f	1968-01-26	01-01-22-001-0043	f
6622717	Valentina	Gabriela	Aguado	Magaña	f	1963-11-15	01-01-22-001-0043	f
5907131	Jose	Saul	Ruíz	Ballesteros	m	1955-10-24	01-01-22-001-0043	f
8611663	Ismael	Pedro	Pelayo	Piña	m	1953-02-11	01-01-22-001-0043	f
9827018	Aleix	Roberto	Zayas	Galán	m	1952-06-13	01-01-22-001-0043	f
6342741	Rayan	Isaac	Villa	Jasso	m	1958-11-04	01-01-22-001-0043	f
6605179	Marti	Elsa	Arteaga	Treviño	f	1960-01-16	01-01-22-001-0043	f
14653566	Jan	Saul	Ortiz	Irizarry	m	1968-08-05	01-01-22-001-0043	f
9350006	Irene	Beatriz	Munguía	Pantoja	f	1954-09-29	01-01-22-001-0043	f
8918442	Mar	Ines	Cordero	Casado	f	1953-01-21	01-01-22-001-0043	f
10486148	Jaime	Victor	Herrera	Jáquez	m	1958-12-03	01-01-22-001-0043	f
9713477	Adrian	Jan	Nieto	Benavides	m	1965-12-15	01-01-22-001-0043	f
5295555	Rayan	Aaron	Merino	Velásquez	m	1952-11-04	01-01-22-001-0043	f
13711380	Hugo	Mateo	Riera	Gastélum	m	1951-11-19	01-01-22-001-0043	f
13976166	Diana	Andrea	Requena	Aparicio	f	1962-04-27	01-01-22-001-0043	f
5252260	Ariadna	Luisa	González	Ledesma	f	1957-03-28	01-01-22-001-0043	f
13885960	Rocio	Paola	Haro	Villalba	f	1960-01-16	01-01-22-001-0043	f
7149220	Mohamed	Mohamed	Zambrano	Moreno	m	1957-04-15	01-01-22-001-0043	f
13081991	Antonia	Laia	Delarosa	Zúñiga	f	1966-07-11	01-01-22-001-0043	f
7102624	Alicia	Natalia	Escribano	Escobar	f	1952-07-14	01-01-22-001-0043	f
11305090	Oscar	Dario	Briones	Oliver	m	1967-08-22	01-01-22-001-0043	f
6044200	Sofia	Abril	Carbonell	Zapata	f	1965-08-28	01-01-22-001-0043	f
14661263	Miriam	Diana	Arroyo	Curiel	f	1963-09-23	01-01-22-001-0043	f
5919970	Joel	Adam	Aguayo	Ulibarri	m	1969-12-11	01-01-22-001-0043	f
5459819	Diana	Alexandra	Almaraz	Henríquez	f	1966-10-12	01-01-22-001-0043	f
13324666	Silvia	Alma	Zelaya	Montenegro	f	1954-04-01	01-01-22-001-0043	f
6379176	Ona	Carla	Asensio	Segura	f	1956-04-04	01-01-22-001-0043	f
14532012	Mireia	Alicia	Fajardo	Palacios	f	1952-11-23	01-01-22-001-0043	f
8523273	Berta	Sara	Soriano	Aragón	f	1968-04-26	01-01-22-001-0043	f
10666893	Mara	Silvia	Salcedo	Calvillo	f	1963-08-11	01-01-22-001-0043	f
9965834	Nahia	Aroa	Almonte	Pulido	f	1967-04-11	01-01-22-001-0043	f
12544798	Sebastian	Luis	Escribano	Delgado	m	1959-01-09	01-01-22-001-0043	f
8505041	Lucia	Ana	Espinosa	Solano	f	1955-04-27	01-01-22-001-0043	f
13272105	Nil	Paola	Carmona	Cordero	f	1958-12-29	01-01-22-001-0043	f
5946131	Jon	Oscar	Rico	Cervántez	m	1967-01-26	01-01-22-001-0043	f
13263302	Hugo	Sergio	Delgado	Quiñones	m	1961-10-12	01-01-22-001-0043	f
5594806	Pol	Samuel	Marín	Aponte	m	1957-10-13	01-01-22-001-0043	f
8549300	Jose	Rayan	Serrato	Ureña	m	1957-06-17	01-01-22-001-0043	f
14979870	Leo	Jordi	Puig	Lucas	m	1954-07-18	01-01-22-001-0043	f
11545756	Sebastian	Yorman	Segovia	Román	m	1958-05-30	01-01-22-001-0043	f
11422826	Zulay	Diana	Rosales	Montañez	f	1967-02-09	01-01-22-001-0043	f
13622547	Mara	Alicia	Razo	Camarillo	f	1957-08-15	01-01-22-001-0043	f
10792536	Alma	Olivia	Cavazos	Alba	f	1964-03-27	01-01-22-001-0043	f
10278259	Aroa	Candela	Melgar	Mena	f	1969-05-02	01-01-22-001-0043	f
5440606	Mateo	Francisco	Rubio	Jaime	m	1955-09-20	01-01-22-001-0043	f
6817818	Lola	Martina	Barajas	Montoya	f	1958-06-09	01-01-22-001-0043	f
11261432	Diego	Isaac	Laureano	Navarro	m	1954-07-26	01-01-22-001-0043	f
9883471	Manuel	Marc	Mascareñas	Menchaca	m	1962-03-16	01-01-22-001-0043	f
11972518	Omar	Enrique	Montero	Aponte	m	1954-10-22	01-01-22-001-0043	f
8485841	Valentina	Carolina	Valadez	Rivas	f	1965-10-18	01-01-22-001-0043	f
11981648	Ariadna	Valentina	Ferrer	Altamirano	f	1952-07-02	01-01-22-001-0043	f
13477582	Luna	Celia	Moreno	Alonso	f	1962-04-06	01-01-22-001-0043	f
14202318	Isaac	Cesar	Martos	Romero	m	1957-09-04	01-01-22-001-0043	f
7819416	Diego	Sergio	Conde	Lucas	m	1962-12-11	01-01-22-001-0043	f
9759442	Nerea	Maria	Alonso	Meza	f	1967-03-30	01-01-22-001-0043	f
12475883	Alexia	Sandra	Camacho	Vela	f	1963-03-09	01-01-22-001-0043	f
10866596	Patricia	Marina	Armas	Marcos	f	1966-09-24	01-01-22-001-0043	f
12925313	Oswaldo	Alex	Reina	Saucedo	m	1954-04-08	01-01-22-001-0043	f
11480896	Blanca	Nayara	Porras	Camarillo	f	1960-11-21	01-01-22-001-0043	f
10608462	Enrique	Izan	Olivera	Collado	m	1962-12-12	01-01-22-001-0043	f
14599952	Eduardo	Pol	Linares	Piña	m	1952-05-12	01-01-22-001-0043	f
10226046	Nahia	Anna	Rico	Aparicio	f	1961-05-19	01-01-22-001-0043	f
7920167	Leire	Claudia	Farías	Sedillo	f	1962-11-21	01-01-22-001-0043	f
12101839	Helena	Pau	Oliva	Pulido	f	1957-11-09	01-01-22-001-0043	f
13509652	Oscar	Leandro	Valero	Garica	m	1957-02-25	01-01-22-001-0043	f
6659612	Leandro	Francisco Javier	Vergara	Concepción	m	1956-05-01	01-01-22-001-0043	f
6079139	Ines	Leyre	Segovia	Arriaga	f	1952-02-19	01-01-22-001-0043	f
7019877	Raul	Hugo	Moreno	Ortíz	m	1953-10-19	01-01-22-001-0043	f
5698177	Ariadna	Sara	Pagan	Samaniego	f	1967-05-01	01-01-22-001-0043	f
9980199	Leire	Valeria	Gonzales	Toledo	f	1963-06-25	01-01-22-001-0043	f
14380355	Leo	Alvaro	Jáquez	Negrete	m	1958-09-11	01-01-22-001-0043	f
14925944	Samuel	Jose	Mateos	Téllez	m	1966-01-10	01-01-22-001-0043	f
14325372	Leyre	Abril	Bravo	Martos	f	1966-10-25	01-01-22-001-0043	f
9548714	Sofia	Marti	Izquierdo	Valentín	f	1960-05-05	01-01-22-001-0043	f
5611587	Miriam	Lara	Mondragón	Cabán	f	1964-10-07	01-01-22-001-0043	f
10610836	Mateo	Mateo	Galán	Gómez	m	1964-06-02	01-01-22-001-0043	f
9279289	Adam	Leandro	Hidalgo	Ortíz	m	1953-02-01	01-01-22-001-0043	f
9526575	Martina	Alba	Montez	Carrión	f	1957-07-16	01-01-22-001-0043	f
7865984	Alicia	Africa	Luna	Valles	f	1958-04-02	01-01-22-001-0043	f
5833761	Teresa	Raquel	Grijalva	Reina	f	1955-03-04	01-01-22-001-0043	f
9378217	Jan	Oscar	Adame	Peláez	m	1965-02-21	01-01-22-001-0043	f
14602195	David	Izan	Escalante	Jaime	m	1953-06-06	01-01-22-001-0043	f
7422109	Santiago	Jan	Olmos	Ocasio	m	1954-01-24	01-01-22-001-0043	f
10159015	Andres	Ian	Moreno	Santos	m	1955-03-28	01-01-22-001-0043	f
7067707	Paula	Celia	Soto	Gimeno	f	1956-08-21	01-01-22-001-0043	f
7163603	Joan	Natalia	Abeyta	Saldaña	f	1963-03-01	01-01-22-001-0043	f
13418565	Omar	Adria	Vallejo	Adorno	m	1960-01-01	01-01-22-001-0043	f
9342343	Erika	Fatima	Delarosa	Olivárez	f	1953-05-07	01-01-22-001-0043	f
14182261	Rafael	Oswaldo	Diez	Maldonado	m	1953-06-25	01-01-22-001-0043	f
8534147	Luis	Oswaldo	Téllez	Pelayo	m	1956-06-22	01-01-22-001-0043	f
5953587	Clara	Nayara	Dueñas	Lozano	f	1962-08-08	01-01-22-001-0043	f
8550278	Hugo	Juan	Crespo	Atencio	m	1955-02-17	01-01-22-001-0043	f
12955470	Oswaldo	Izan	Montoya	Méndez	m	1968-05-26	01-01-22-001-0043	f
12984645	Nayara	Lola	Guardado	Cotto	f	1959-08-17	01-01-22-001-0043	f
11674327	Salma	Adriana	Palomo	Laboy	f	1958-01-23	01-01-22-001-0043	f
14992458	Nuria	Candela	Crespo	Escobar	f	1959-11-25	01-01-22-001-0043	f
12033049	Omar	Miguel	Avilés	Salvador	m	1968-04-10	01-01-22-001-0043	f
14716842	Guillem	Ismael	Navarro	Amador	m	1957-02-16	01-01-22-001-0043	f
6419260	Andres	Pol	Barrios	Leyva	m	1957-09-28	01-01-22-001-0043	f
7784779	Eva	Ainara	Tórrez	Aguado	f	1961-08-01	01-01-22-001-0043	f
12600142	Ivan	Rodrigo	Sola	Zayas	m	1968-03-16	01-01-22-001-0043	f
13603770	Patricia	Eva	Villaseñor	Pichardo	f	1964-12-01	01-01-22-001-0043	f
12607568	Leire	Andrea	Carvajal	Bravo	f	1957-09-04	01-01-22-001-0043	f
10242345	Mateo	Leo	De Jesús	Rosa	m	1952-12-18	01-01-22-001-0043	f
5241207	Adam	Victor	Millán	Crespo	m	1963-02-11	01-01-22-001-0043	f
7563645	Teresa	Naiara	Ruiz	Montañez	f	1956-02-07	01-01-22-001-0043	f
10460209	Abril	Lucia	Sevilla	Domingo	f	1952-08-14	01-01-22-001-0043	f
12051370	Marta	Lidia	Sauceda	Rosado	f	1958-05-07	01-01-22-001-0043	f
8481618	Pedro	Jaime	Tovar	Arredondo	m	1957-04-18	01-01-22-001-0043	f
7864170	Oliver	Erik	Farías	Rivera	m	1960-12-06	01-01-22-001-0043	f
8434694	Ona	Alicia	Aguilar	Ozuna	f	1965-06-05	01-01-22-001-0043	f
10497442	Ignacio	Marco	Pereira	Cortés	m	1960-08-11	01-01-22-001-0043	f
11383292	Marco	Oliver	Chavarría	Adame	m	1952-03-30	01-01-22-001-0043	f
10697472	Maria	Julia	Urbina	Feliciano	f	1953-11-21	01-01-22-001-0043	f
12676653	Jana	Ana	Caldera	Solorzano	f	1958-05-16	01-01-22-001-0043	f
14242672	Helena	Lola	Nazario	Montes	f	1954-03-30	01-01-22-001-0043	f
12455004	Jana	Diana	Santillán	Luis	f	1963-01-25	01-01-22-001-0043	f
11420292	Naia	Cristina	Mayorga	Márquez	f	1952-05-10	01-01-22-001-0043	f
14615252	Miguel	Marco	Velásquez	Meza	m	1960-12-18	01-01-22-001-0043	f
13306112	Paula	Gabriela	Gallego	Martínez	f	1966-03-08	01-01-22-001-0043	f
9479911	Domingo	Yorman	Campos	Cortés	m	1965-09-20	01-01-22-001-0043	f
10856230	Victor	Gabriel	Esquibel	Naranjo	m	1956-04-09	01-01-22-001-0016	f
6296658	Daniela	Leire	Sarabia	Raya	f	1956-03-16	01-01-22-001-0016	f
14019840	Anna	Teresa	Casares	Collado	f	1962-08-07	01-01-22-001-0016	f
9512480	Silvia	Raquel	Soliz	Vicente	f	1967-02-08	01-01-22-001-0016	f
12164938	Mar	Cristina	Santamaría	Pons	f	1969-07-26	01-01-22-001-0016	f
9596024	Pedro	Bruno	Arguello	Barraza	m	1968-12-18	01-01-22-001-0016	f
10826477	Elena	Emma	Sarabia	Valles	f	1961-04-28	01-01-22-001-0016	f
6064618	Fatima	Alejandra	Canales	Lorenzo	f	1967-02-17	01-01-22-001-0016	f
7441146	Isabel	Sofia	Miguel	Pelayo	f	1952-03-18	01-01-22-001-0016	f
9957782	Miriam	Fatima	Marcos	Crespo	f	1962-10-27	01-01-22-001-0016	f
5355292	Noelia	Noa	Cuevas	Almanza	f	1954-12-03	01-01-22-001-0016	f
7812753	Andrea	Valeria	Fonseca	Burgos	f	1961-11-23	01-01-22-001-0016	f
9345898	Pedro	Juan	Delvalle	Tovar	m	1956-04-08	01-01-22-001-0016	f
14394157	Aaron	Nicolas	Cuenca	Casado	m	1960-03-21	01-01-22-001-0016	f
10007431	Fernando	Guillermo	Gurule	Bermejo	m	1964-11-17	01-01-22-001-0016	f
7848099	Carmen	Nil	Delacrúz	Alfonso	f	1958-03-26	01-01-22-001-0016	f
8900371	Victoria	Alba	Rocha	Costa	f	1964-03-03	01-01-22-001-0016	f
13635934	Natalia	Martina	Sola	Nazario	f	1953-01-29	01-01-22-001-0016	f
10687952	Rafael	Marco	Gracia	Tamayo	m	1968-10-28	01-01-22-001-0016	f
7215781	Pau	Laura	Terán	Ochoa	f	1965-02-06	01-01-22-001-0016	f
10875692	Alejandro	Miguel	Zamudio	Bahena	m	1960-12-04	01-01-22-001-0016	f
5401610	Erika	Natalia	Saavedra	Aguado	f	1961-04-09	01-01-22-001-0016	f
8044971	Jana	Beatriz	Montalvo	Del río	f	1967-02-14	01-01-22-001-0016	f
5529063	Oliver	Jesus	Guzmán	Olivares	m	1961-02-25	01-01-22-001-0016	f
6622389	Carmen	Paula	Piñeiro	Domínguez	f	1956-11-13	01-01-22-001-0016	f
9612338	Jaime	Leonardo	Quintero	Abeyta	m	1964-06-21	01-01-22-001-0016	f
12239136	Oliver	Gabriel	Pardo	Ramón	m	1964-07-14	01-01-22-001-0016	f
14463886	Eduardo	Oscar	Heredia	Lugo	m	1953-05-21	01-01-22-001-0016	f
9375687	Rodrigo	Jose	Ruelas	Olvera	m	1965-02-28	01-01-22-001-0016	f
8770766	Jimena	Natalia	Burgos	Espino	f	1957-11-10	01-01-22-001-0016	f
14410371	Guillem	Domingo	Urías	Cuellar	m	1961-07-09	01-01-22-001-0016	f
7703774	Adriana	Yaiza	Herrera	Rosas	f	1957-08-30	01-01-22-001-0016	f
5291288	Jaime	Erik	Benavides	Rosa	m	1960-04-13	01-01-22-001-0016	f
12787321	Laia	Alma	Benavídez	Romo	f	1952-11-24	01-01-22-001-0016	f
9881355	Santiago	Oliver	Rojas	Armenta	m	1952-11-06	01-01-22-001-0016	f
6203831	Cesar	Carlos	Costa	Ledesma	m	1967-01-07	01-01-22-001-0016	f
7977287	Manuel	Martin	Collado	Otero	m	1962-09-18	01-01-22-001-0016	f
9100631	Ana	Laura	Cordero	Longoria	f	1958-10-24	01-01-22-001-0016	f
6111087	Fernando	Mateo	Noriega	Peres	m	1968-10-12	01-01-22-001-0016	f
5701132	Adria	Ander	Loya	Sisneros	m	1961-10-13	01-01-22-001-0016	f
9982827	Yorman	Alonso	Tapia	Costa	m	1959-09-12	01-01-22-001-0016	f
8528883	Samuel	Alex	Álvarez	Ríos	m	1957-03-22	01-01-22-001-0016	f
12879397	Alma	Alejandra	Cordero	Zambrano	f	1964-07-30	01-01-22-001-0016	f
5805708	Mateo	Hector	Moreno	Montes	m	1964-05-19	01-01-22-001-0016	f
13328932	Mara	Vera	Zambrano	Ruelas	f	1968-08-16	01-01-22-001-0016	f
8218434	Marta	Laura	Lucero	Alcala	f	1965-12-11	01-01-22-001-0016	f
9324387	Eduardo	Jesus	Candelaria	Meléndez	m	1964-04-01	01-01-22-001-0016	f
5545211	Rayan	Martin	Robledo	Naranjo	m	1956-02-22	01-01-22-001-0016	f
11179008	Marco	Mohamed	Orosco	Dávila	m	1955-11-14	01-01-22-001-0016	f
9610213	Mario	Cesar	Meléndez	Pedraza	m	1961-07-29	01-01-22-001-0016	f
8864857	Alexia	Erika	Sarabia	De Jesús	f	1959-11-11	01-01-22-001-0016	f
8277063	Zulay	Iria	Luevano	González	f	1967-06-19	01-01-22-001-0016	f
5749425	Lucia	Iria	Espino	Burgos	f	1954-02-27	01-01-22-001-0016	f
12511321	Angela	Nahia	Bétancourt	Esteve	f	1962-07-21	01-01-22-001-0016	f
9490399	Olivia	Elsa	Pérez	Barrientos	f	1956-07-07	01-01-22-001-0016	f
5562524	Rodrigo	Ismael	Salazar	Paz	m	1968-05-29	01-01-22-001-0016	f
5013273	Elsa	Carlota	Venegas	Lozada	f	1961-04-01	01-01-22-001-0016	f
8665994	Luis	Carlos	Carranza	Luna	m	1967-05-21	01-01-22-001-0016	f
10032332	Jaime	Izan	Escobedo	Colón	m	1963-04-10	01-01-22-001-0016	f
9311214	Aroa	Beatriz	Bravo	Fajardo	f	1957-10-12	01-01-22-001-0016	f
14596109	Andres	Hugo	Bueno	Cabrera	m	1957-08-18	01-01-22-001-0016	f
6205658	Nuria	Gabriela	Vanegas	Zúñiga	f	1968-04-10	01-01-22-001-0016	f
12611342	Marc	Guillem	Mateo	Galindo	m	1955-06-13	01-01-22-001-0016	f
8265981	Luisa	Leire	Báez	Roca	f	1963-06-23	01-01-22-001-0016	f
7369608	Andres	Cesar	Quesada	Rosado	m	1957-08-16	01-01-22-001-0016	f
12836585	Nora	Isabel	Casillas	Orozco	f	1959-04-01	01-01-22-001-0016	f
6684945	Anna	Candela	Santana	Nieves	f	1960-11-11	01-01-22-001-0016	f
13768083	Omar	Jan	Mares	Delarosa	m	1954-08-14	01-01-22-001-0016	f
8025021	Mateo	Domingo	Almanza	Carmona	m	1960-03-02	01-01-22-001-0016	f
8296118	Lucas	Mohamed	Olvera	Casas	m	1959-12-10	01-01-22-001-0016	f
11210707	Helena	Lidia	Leal	Marcos	f	1962-04-29	01-01-22-001-0016	f
7017737	Mario	Pablo	Montenegro	Tomas	m	1962-12-17	01-01-22-001-0016	f
7841102	Ana	Clara	Olivárez	Varela	f	1954-05-30	01-01-22-001-0016	f
14634329	Alex	Pablo	Arredondo	Muñoz	m	1954-08-02	01-01-22-001-0016	f
9816443	Mario	Pedro	Ortiz	Raya	m	1951-11-15	01-01-22-001-0016	f
14802793	Carlos	Eric	Carrera	Briones	m	1960-06-07	01-01-22-001-0016	f
5040558	Manuel	Jon	Cortez	Huerta	m	1955-06-17	01-01-22-001-0016	f
8822563	Ivan	David	Soliz	Juan	m	1957-01-05	01-01-22-001-0016	f
7716283	Ona	Miriam	Valentín	Ontiveros	f	1958-07-21	01-01-22-001-0016	f
8437696	Jan	Javier	Berríos	Almaraz	m	1965-12-09	01-01-22-001-0016	f
8659177	Samuel	Diego	Corral	Tello	m	1967-08-05	01-01-22-001-0016	f
10057437	Pablo	Marc	Escobedo	Pastor	m	1958-10-28	01-01-22-001-0016	f
10402216	Zulay	Victoria	Asensio	Juan	f	1969-10-15	01-01-22-001-0016	f
11960016	Ines	Naiara	Alicea	Sevilla	f	1969-05-16	01-01-22-001-0016	f
8458405	Leire	Nahia	Alicea	Yáñez	f	1961-09-15	01-01-22-001-0016	f
12461080	Jon	Juan	Benito	Matos	m	1965-09-13	01-01-22-001-0016	f
12828270	Alexandra	Lidia	Girón	Barroso	f	1968-08-07	01-01-22-001-0016	f
14426116	Adam	Marc	Polo	Bautista	m	1964-01-04	01-01-22-001-0016	f
10142966	Miriam	Erika	Loera	Aguayo	f	1957-09-17	01-01-22-001-0016	f
11128986	Alonso	Isaac	Gaitán	Rueda	m	1961-11-26	01-01-22-001-0016	f
12529370	Valeria	Antonia	Vallejo	Trujillo	f	1952-09-21	01-01-22-001-0016	f
6363743	Laura	Rocio	Arenas	Rodarte	f	1966-04-24	01-01-22-001-0016	f
6040175	Emma	Eva	Mateo	De Anda	f	1958-10-25	01-01-22-001-0016	f
8087878	Marc	Nicolas	Oliva	Brito	m	1956-02-11	01-01-22-001-0016	f
10464266	Yaiza	Naiara	Pozo	Casares	f	1965-06-30	01-01-22-001-0016	f
14426486	Eva	Berta	Conde	Acevedo	f	1955-05-22	01-01-22-001-0016	f
12050355	Alvaro	Eduardo	Lozada	Villalpando	m	1961-06-06	01-01-22-001-0016	f
9557715	Isaac	Jordi	Bahena	Madrigal	m	1965-10-04	01-01-22-001-0016	f
6047710	Nil	Noelia	Rubio	Cardenas	f	1966-01-30	01-01-22-001-0016	f
11998097	Miguel	Alonso	Carmona	Bautista	m	1955-12-02	01-01-22-001-0016	f
14295493	Adria	Sebastian	Corona	Rivas	m	1961-04-24	01-01-22-001-0016	f
12085059	Juan	Marc	Dávila	Preciado	m	1956-05-04	01-01-22-001-0016	f
5854730	Dario	Jose	Deleón	Blasco	m	1965-06-23	01-01-22-001-0016	f
14422119	Raquel	Diana	Rosales	Urrutia	f	1960-06-26	01-01-22-001-0016	f
14321653	Eduardo	Enrique	Morales	Bétancourt	m	1954-01-11	01-01-22-001-0016	f
10302866	Aaron	Daniel	Uribe	Díaz	m	1957-06-02	01-01-22-001-0016	f
13097548	Jana	Africa	Covarrubias	Garibay	f	1958-06-01	01-01-22-001-0016	f
12940155	Yaiza	Leyre	Zambrano	Zavala	f	1969-07-24	01-01-22-001-0016	f
8554249	Martin	Ivan	Quiñones	Salgado	m	1961-09-18	01-01-22-001-0016	f
13364093	Alvaro	Pol	Palomino	Estrada	m	1963-02-22	01-01-22-001-0016	f
14202953	Miguel	Angel	Román	Mercado	m	1960-02-09	01-01-22-001-0016	f
12553239	Alejandro	Erik	Bustos	Menchaca	m	1953-12-21	01-01-22-001-0016	f
5138494	Marco	Rayan	Ozuna	Lugo	m	1964-02-08	01-01-22-001-0016	f
6248696	Mohamed	Santiago	Zaragoza	Ríos	m	1962-11-02	01-01-22-001-0016	f
9847029	Santiago	Izan	Alcala	Lorenzo	m	1959-09-24	01-01-22-001-0016	f
12374830	Lucas	Adrian	Pardo	Rangel	m	1955-10-30	01-01-22-001-0016	f
10090595	Cesar	Guillem	Zapata	Cuellar	m	1959-07-01	01-01-22-001-0016	f
9335218	Carlota	Julia	Ruvalcaba	Coronado	f	1966-06-16	01-01-22-001-0016	f
10737305	Alvaro	Manuel	Zapata	Estrada	m	1958-06-21	01-01-22-001-0016	f
11467312	Jon	Martin	Salazar	Crespo	m	1962-03-04	01-01-22-001-0016	f
8358018	Carlos	Marco	Preciado	Palacios	m	1962-02-06	01-01-22-001-0016	f
9122894	Lola	Lola	Barrios	Centeno	f	1956-03-20	01-01-22-001-0016	f
12286878	Miguel	Carlos	Valles	Tejeda	m	1953-05-12	01-01-22-001-0016	f
7550893	Saul	Jordi	Lucero	Solorio	m	1957-08-23	01-01-22-001-0016	f
12254034	Nadia	Gabriela	Rentería	Meléndez	f	1954-09-27	01-01-22-001-0016	f
10430850	Maria	Andrea	Flores	Gálvez	f	1957-12-30	01-01-22-001-0016	f
13947832	Bella	Iria	Quintana	Lovato	f	1963-09-10	01-01-22-001-0016	f
9644734	Luna	Nerea	Hinojosa	Blasco	f	1962-05-04	01-01-22-001-0016	f
7054557	Mario	Cristian	Escobar	Burgos	m	1955-12-04	01-01-22-001-0016	f
11908364	Jan	Omar	Sola	Partida	m	1966-08-10	01-01-22-001-0016	f
10436980	Valentina	Patricia	Escamilla	Rosario	f	1963-07-09	01-01-22-001-0016	f
12173570	Sebastian	Alejandro	Adame	Roybal	m	1966-06-27	01-01-22-001-0016	f
12549323	Mara	Manuela	Corrales	Ruelas	f	1963-06-24	01-01-22-001-0016	f
9852962	Vera	Isabel	Cervantes	Márquez	f	1963-03-08	01-01-22-001-0016	f
5343735	Saul	Guillermo	Calvillo	Haro	m	1966-03-02	01-01-22-001-0016	f
5166322	Irene	Noelia	Alemán	Reyna	f	1963-03-18	01-01-22-001-0016	f
14332035	Miguel	Luis	Maldonado	Salcido	m	1969-03-09	01-01-22-001-0016	f
9106429	Oscar	Cristian	Covarrubias	Ybarra	m	1954-08-22	01-01-22-001-0016	f
10378838	Nuria	Ane	Mateo	Quezada	f	1957-06-25	01-01-22-001-0016	f
12578975	Adriana	Lola	Vicente	Valero	f	1969-05-24	01-01-22-001-0016	f
12489224	Victor	Marcos	Lebrón	Holguín	m	1963-06-11	01-01-22-001-0016	f
8623959	Ana	Ona	Tapia	Caraballo	f	1959-08-12	01-01-22-001-0016	f
7947862	Ona	Carlota	Delagarza	Castro	f	1969-03-15	01-01-22-001-0016	f
5213513	Jon	Joel	Guzmán	Morales	m	1964-08-31	01-01-22-001-0016	f
14469883	Ian	Lucas	Blanco	Agosto	m	1952-07-20	01-01-22-001-0016	f
5389159	Laia	Patricia	Ontiveros	Acosta	f	1963-02-24	01-01-22-001-0016	f
5626085	Leo	Bruno	Bustamante	Avilés	m	1962-01-19	01-01-22-001-0016	f
5504561	Eric	Leonardo	Aguirre	Carrasco	m	1964-03-07	01-01-22-001-0016	f
14468391	Olivia	Fatima	Vergara	Luevano	f	1959-02-01	01-01-22-001-0016	f
5390199	Blanca	Isabel	Roque	Mendoza	f	1952-08-24	01-01-22-001-0016	f
7809962	Naia	Alexia	Terrazas	Pastor	f	1959-05-24	01-01-22-001-0016	f
11046230	Vega	Eva	Nazario	Véliz	f	1964-05-02	01-01-22-001-0016	f
10659198	Ian	Dario	Esparza	Roldán	m	1955-04-30	01-01-22-001-0016	f
11194740	Isabel	Emma	Valdivia	Uribe	f	1960-10-13	01-01-22-001-0016	f
8075399	Angel	Erik	Saldivar	Noriega	m	1960-01-19	01-01-22-001-0016	f
11081139	Leire	Paola	De Anda	Villalba	f	1959-05-02	01-01-22-001-0016	f
14702142	Luna	Yaiza	Domínguez	Sanz	f	1957-04-01	01-01-22-001-0016	f
11288519	Hector	Lucas	Cintrón	Castillo	m	1959-10-20	01-01-22-001-0016	f
7209411	Carla	Noa	Abrego	Roig	f	1961-06-02	01-01-22-001-0016	f
10881024	Nadia	Nadia	Vargas	Bernal	f	1967-08-08	01-01-22-001-0016	f
7961826	Olivia	Lidia	Valadez	Lovato	f	1968-04-11	01-01-22-001-0016	f
13762405	Adriana	Iria	Bustos	Polo	f	1964-07-07	01-01-22-001-0016	f
12770896	Jana	Noa	Ureña	Gamboa	f	1960-08-22	01-01-22-001-0016	f
13947587	Jorge	Isaac	Riojas	Noriega	m	1960-11-23	01-01-22-001-0016	f
13676736	Victoria	Sandra	Baca	Sisneros	f	1953-08-15	01-01-22-001-0016	f
6912024	Fernando	Mario	Alcala	Castañeda	m	1963-08-08	01-01-22-001-0016	f
5776366	Erik	Sergio	Briones	Brito	m	1967-10-21	01-01-22-001-0016	f
6388496	Oscar	Mateo	Martos	Cervantes	m	1960-04-13	01-01-22-001-0016	f
6600072	Lola	Lucia	Rosado	De la cruz	f	1956-10-08	01-01-22-001-0016	f
7438181	Francisco Javier	Guillermo	Márquez	Garza	m	1967-04-15	01-01-22-001-0016	f
7107117	Gabriela	Mireia	Ríos	Román	f	1953-12-06	01-01-22-001-0016	f
5643573	Zulay	Nil	Blázquez	Márquez	f	1965-03-22	01-01-22-001-0016	f
13464365	Pau	Ainara	Lara	Roybal	f	1958-04-13	01-01-22-001-0016	f
12206064	Adrian	Adria	Nazario	Valdez	m	1956-09-19	01-01-22-001-0016	f
12507128	Carolina	Raquel	Lucero	Méndez	f	1969-11-19	01-01-22-001-0016	f
6007891	Jesus	Carlos	Sola	Zavala	m	1967-09-19	01-01-22-001-0016	f
10059197	Fernando	Aleix	Melgar	Quintero	m	1958-09-05	01-01-22-001-0016	f
13298159	Mateo	Jan	Ureña	Ponce	m	1953-04-09	01-01-22-001-0016	f
14301717	Carla	Noelia	Olivares	Fonseca	f	1961-02-15	01-01-22-001-0016	f
10219914	Martin	Martin	Peralta	Pacheco	m	1959-12-15	01-01-22-001-0016	f
14560055	Isabel	Lara	Sisneros	Cobo	f	1968-03-30	01-01-22-001-0016	f
6908428	Laia	Claudia	Negrete	Águilar	f	1952-08-03	01-01-22-001-0016	f
12046137	Luis	Angel	Pérez	Franco	m	1955-06-04	01-01-22-001-0016	f
5863767	Emma	Maria	Girón	Linares	f	1956-05-13	01-01-22-001-0016	f
7132305	Alvaro	Angel	Madera	Rascón	m	1969-02-04	01-01-22-001-0016	f
5603315	Marti	Alma	Armijo	Guardado	f	1967-04-04	01-01-22-001-0016	f
12805222	Erika	Ane	Farías	Paz	f	1952-11-18	01-01-22-001-0016	f
14468704	Guillem	Marco	Sandoval	Grijalva	m	1955-08-12	01-01-22-001-0016	f
6655225	Gonzalo	Adam	Rosario	Salgado	m	1955-09-21	01-01-22-001-0016	f
5031070	Jorge	Yorman	Santos	Vera	m	1965-03-10	01-01-22-001-0016	f
13369087	Omar	Samuel	Rentería	Castellano	m	1959-11-15	01-01-22-001-0016	f
13990601	Salma	Nadia	Rincón	Rivera	f	1962-02-05	01-01-22-001-0016	f
10800014	Berta	Helena	Segura	Valdivia	f	1965-08-29	01-01-22-001-0016	f
10129974	Anna	Gabriela	Bermúdez	Arreola	f	1955-02-23	01-01-22-001-0016	f
8109958	Roberto	Enrique	Sandoval	Llamas	m	1965-04-05	01-01-22-001-0016	f
13081054	Rafael	Eduardo	Piña	Saldaña	m	1961-12-13	01-01-22-001-0016	f
10473978	Diana	Erika	Gurule	Villalobos	f	1957-10-08	01-01-22-001-0016	f
9860104	Aleix	Roberto	Romo	Arreola	m	1964-12-01	01-01-22-001-0016	f
7692178	Leyre	Zulay	Santos	Ruelas	f	1962-03-16	01-01-22-001-0016	f
12257434	Natalia	Antonia	Garza	Acuña	f	1953-11-22	01-01-22-001-0016	f
7365493	Anna	Noa	Quintanilla	Porras	f	1953-02-17	01-01-22-001-0016	f
12020094	Adria	Nicolas	Menchaca	Beltrán	m	1959-07-23	01-01-22-001-0016	f
12510821	Nahia	Eva	Parra	Quesada	f	1966-05-08	01-01-22-001-0016	f
5212669	Jose	Martin	Arteaga	Menchaca	m	1960-12-04	01-01-22-001-0016	f
5740788	Isaac	Marc	Treviño	Vélez	m	1963-09-16	01-01-22-001-0016	f
12783632	Nil	Blanca	Macías	Serra	f	1953-12-06	01-01-22-001-0016	f
9463518	Rafael	Bruno	Hernádez	Samaniego	m	1966-12-10	01-01-22-001-0016	f
10820871	Marti	Bella	Fajardo	Mora	f	1969-09-25	01-01-22-001-0016	f
7995670	Ana	Raquel	Núñez	Soler	f	1959-01-28	01-01-22-001-0016	f
9640552	Ander	Sergio	Gutiérrez	Rincón	m	1955-05-15	01-01-22-001-0016	f
5447362	Eric	Jon	Cano	Estévez	m	1969-05-05	01-01-22-001-0016	f
7428443	Martina	Leyre	Chávez	Sanz	f	1969-03-25	01-01-22-001-0016	f
14808236	Marta	Miriam	Jaramillo	Armijo	f	1956-10-31	01-01-22-001-0016	f
10373794	Ignacio	Alvaro	Berríos	Cardona	m	1956-12-19	01-01-22-001-0016	f
11235696	Mateo	Domingo	Moral	Arguello	m	1967-02-17	01-01-22-001-0016	f
14806286	Oscar	Ignacio	Roldán	Barroso	m	1961-05-05	01-01-22-001-0016	f
11182283	Guillem	Aleix	Orellana	Puig	m	1952-11-20	01-01-22-001-0016	f
6856010	Domingo	Ismael	Santacruz	Meraz	m	1964-11-01	01-01-22-001-0016	f
5540186	Victoria	Eva	Aranda	Sarabia	f	1968-05-20	01-01-22-001-0016	f
14814901	Yorman	Oswaldo	Hernández	Bustamante	m	1960-12-07	01-01-22-001-0016	f
11760603	Maria	Fatima	Sanches	Madrigal	f	1962-05-16	01-01-22-001-0016	f
9409270	Lara	Sandra	Villegas	Ordoñez	f	1969-11-16	01-01-22-001-0016	f
9311354	Lidia	Nil	Pelayo	Villalba	f	1961-11-02	01-01-22-001-0016	f
7792679	Noa	Iria	Briones	Martos	f	1953-12-12	01-01-22-001-0016	f
11198208	Domingo	Oliver	Curiel	Arguello	m	1969-04-02	01-01-22-001-0016	f
11241802	Yaiza	Martina	Sauceda	Cervantes	f	1958-04-26	01-01-22-001-0016	f
10745376	Iria	Emma	Gallardo	Serna	f	1954-02-22	01-01-22-001-0016	f
14569841	Leire	Valeria	Delatorre	Arredondo	f	1960-05-27	01-01-22-001-0016	f
8470572	Jimena	Bella	Trujillo	Delatorre	f	1966-03-02	01-01-22-001-0016	f
10680998	Lucas	Jorge	Muñiz	Leyva	m	1966-01-31	01-01-22-001-0016	f
11897426	Jon	Sergio	Murillo	Córdova	m	1962-09-06	01-01-22-001-0016	f
13341574	Joel	Joel	Andrés	Valero	m	1961-08-04	01-01-22-001-0016	f
8531378	Naiara	Angela	Méndez	Gaona	f	1966-08-21	01-01-22-001-0016	f
6618909	Valentina	Alma	Oropeza	Silva	f	1967-12-12	01-01-22-001-0016	f
10781388	Leo	Rafael	Arguello	Camacho	m	1952-08-13	01-01-22-001-0016	f
13127261	Iria	Diana	Villar	Velasco	f	1960-10-18	01-01-22-001-0016	f
12325989	Emma	Nora	Ocasio	Anguiano	f	1960-11-01	01-01-22-001-0016	f
6214389	Nadia	Elsa	Quiñónez	Moya	f	1965-03-29	01-01-22-001-0016	f
8918439	Erika	Mara	Cabrera	Quintanilla	f	1968-12-06	01-01-22-001-0016	f
8821737	Domingo	Alex	Nava	Acosta	m	1963-01-08	01-01-22-001-0016	f
14725444	Abril	Nora	Montoya	Ureña	f	1962-04-01	01-01-22-001-0016	f
14147698	Emma	Carmen	Bernal	Briones	f	1953-07-11	01-01-22-001-0016	f
5122996	Sergio	Mohamed	Delagarza	Barraza	m	1962-04-19	01-01-22-001-0016	f
5772741	Emma	Noa	Casillas	Zelaya	f	1960-05-26	01-01-22-001-0016	f
10849587	Gabriela	Ines	Bustos	Portillo	f	1954-11-27	01-01-22-001-0016	f
14450459	Mara	Andrea	Montalvo	Loya	f	1961-03-13	01-01-22-001-0016	f
14502497	Valeria	Sandra	Serna	Barrientos	f	1966-11-28	01-01-22-001-0016	f
8261044	Mohamed	Adria	Arenas	Calero	m	1966-11-01	01-01-22-0001-0028	f
7116600	Aleix	Alonso	Guajardo	Vázquez	m	1962-01-26	01-01-22-0001-0028	f
11717424	Nahia	Eva	Prado	Armendáriz	f	1953-08-04	01-01-22-0001-0028	f
11478421	Daniela	Vega	Santamaría	Gallardo	f	1955-08-15	01-01-22-0001-0028	f
13667525	Daniela	Nil	Muñóz	Delapaz	f	1956-09-09	01-01-22-0001-0028	f
14020139	Leonardo	Francisco	Escamilla	Altamirano	m	1959-02-25	01-01-22-0001-0028	f
12828600	Naiara	Berta	Oropeza	Zayas	f	1957-06-29	01-01-22-0001-0028	f
9518505	Olivia	Manuela	Batista	Vaca	f	1964-07-27	01-01-22-0001-0028	f
5725267	Jana	Naiara	Meza	Valdivia	f	1957-05-31	01-01-22-0001-0028	f
12497308	Adria	Oliver	Villalobos	Valdez	m	1957-10-15	01-01-22-0001-0028	f
10273978	Eric	Angel	Tamez	Corrales	m	1954-08-16	01-01-22-0001-0028	f
5577753	Rafael	Gabriel	Dávila	Cedillo	m	1965-12-25	01-01-22-0001-0028	f
7858126	Angela	Alma	Arriaga	Juárez	f	1965-03-29	01-01-22-0001-0028	f
10067723	Luisa	Africa	Bermejo	Expósito	f	1962-08-25	01-01-22-0001-0028	f
9190344	Martina	Marina	Cuellar	Rico	f	1960-02-18	01-01-22-0001-0028	f
9131774	Mario	Leandro	Arevalo	Cano	m	1969-04-02	01-01-22-001-0079	f
12950910	Berta	Erika	Zayas	Del río	f	1962-07-14	01-01-22-001-0079	f
13582580	Gonzalo	Izan	Sandoval	Ayala	m	1961-03-08	01-01-22-001-0079	f
5490657	Ismael	Eric	Apodaca	Ocampo	m	1968-12-18	01-01-22-001-0079	f
7141709	Nahia	Daniela	Verduzco	Barreto	f	1969-04-07	01-01-22-001-0079	f
12945394	Fatima	Valeria	Pantoja	Carrillo	f	1960-08-07	01-01-22-001-0079	f
10973782	Santiago	Mateo	Soliz	Cardenas	m	1953-05-26	01-01-22-001-0079	f
8495302	Antonia	Marina	Baca	Huerta	f	1963-06-02	01-01-22-001-0079	f
6371039	Martina	Alba	Giménez	Vázquez	f	1967-09-03	01-01-22-001-0079	f
5729360	Jaime	Luis	Calvillo	Ortíz	m	1961-02-21	01-01-22-001-0079	f
12996467	Elena	Lara	Tamayo	Girón	f	1961-11-10	01-01-22-001-0079	f
13810593	Emma	Abril	Canales	Enríquez	f	1952-06-08	01-01-22-001-0079	f
6514933	Fernando	Leandro	Merino	Téllez	m	1969-07-21	01-01-22-001-0079	f
13895760	Helena	Raquel	Tovar	Villar	f	1961-12-25	01-01-22-001-0079	f
11021172	Oscar	Eduardo	Barragán	Bétancourt	m	1957-08-29	01-01-22-001-0079	f
7701718	Valentina	Sandra	Navarro	Millán	f	1964-06-28	01-01-22-001-0079	f
9646018	Omar	Nicolas	Zambrano	Bermejo	m	1955-02-07	01-01-22-0001-0014	f
13849859	Elsa	Julia	Cuellar	Cepeda	f	1968-11-10	01-01-22-0001-0014	f
12329202	Pau	Ariadna	Oropeza	Samaniego	f	1957-04-08	01-01-22-0001-0014	f
11532174	Jose	Eric	Henríquez	Zambrano	m	1954-03-05	01-01-22-0001-0014	f
8114550	Adriana	Gabriela	Moral	Méndez	f	1959-01-30	01-01-22-0001-0014	f
11853977	Nayara	Berta	Maldonado	Candelaria	f	1956-06-06	01-01-22-0001-0014	f
6422700	Juan	Adrian	Marrero	Carmona	m	1965-06-02	01-01-22-0001-0014	f
14687143	Oscar	Pol	Arroyo	Olivárez	m	1953-11-19	01-01-22-0001-0014	f
8474808	Izan	Lucas	Pizarro	Escobar	m	1952-07-02	01-01-22-0001-0014	f
10377494	Pedro	Santiago	Casanova	Giménez	m	1967-04-30	01-01-22-0001-0014	f
13957869	Mateo	Marcos	Berríos	Espinal	m	1966-02-27	01-01-22-0001-0014	f
9585267	Helena	Ainara	Paredes	Cervántez	f	1967-08-08	01-01-22-0001-0014	f
8427282	Hugo	Oliver	Quesada	Calderón	m	1952-03-01	01-01-22-0001-0014	f
6451836	Diana	Luisa	Ordóñez	Rivas	f	1952-11-04	01-01-22-0001-0014	f
7031542	Naiara	Beatriz	Aragón	Villar	f	1965-01-19	01-01-22-0001-0014	f
6363599	Miguel	David	Aguilar	Varela	m	1959-01-19	01-01-22-0001-0014	f
14707005	Zulay	Clara	Asensio	Pacheco	f	1958-08-11	01-01-22-0001-0014	f
5284778	Leyre	Lidia	Pineda	Valenzuela	f	1959-03-12	01-01-22-001-0049	f
12800364	Daniel	Oswaldo	Jasso	Rangel	m	1968-09-28	01-01-22-001-0016	f
5066149	Miguel	Eduardo	Alcaraz	Delacrúz	m	1957-12-05	01-01-22-001-0016	f
11834749	Jana	Sofia	Cano	Guevara	f	1963-04-07	01-01-22-001-0016	f
12412058	Yorman	Alberto	Valencia	Blasco	m	1959-07-09	01-01-22-001-0016	f
6994201	Luisa	Cristina	Casárez	Cortez	f	1968-12-08	01-01-22-001-0016	f
6937260	Omar	Hector	Canales	García	m	1955-06-14	01-01-22-001-0016	f
9904641	Jose	Fernando	Vera	Ávila	m	1967-06-17	01-01-22-001-0016	f
14349477	Hugo	Daniel	Casas	Garza	m	1967-06-08	01-01-22-001-0016	f
14061718	Teresa	Daniela	Torres	Orta	f	1963-04-20	01-01-22-001-0016	f
5031813	Iria	Carlota	Ruvalcaba	Alfaro	f	1958-06-26	01-01-22-001-0016	f
12571605	Ariadna	Alicia	Solorio	Pabón	f	1953-05-14	01-01-22-001-0016	f
9078605	Cristina	Mar	Alejandro	Uribe	f	1962-06-04	01-01-22-001-0016	f
5591502	Lucas	Ignacio	Tapia	Olivares	m	1967-10-08	01-01-22-001-0016	f
13354221	Eduardo	Sergio	Cepeda	Marrero	m	1968-09-09	01-01-22-001-0016	f
14784794	Joel	Ruben	Perales	Narváez	m	1953-08-19	01-01-22-001-0016	f
13329725	Martina	Paula	Otero	Olivera	f	1965-08-16	01-01-22-001-0016	f
11880103	Mateo	Adrian	Linares	Cardona	m	1955-08-27	01-01-22-001-0016	f
8994532	Diana	Irene	Zayas	Chavarría	f	1967-10-23	01-01-22-001-0016	f
9248471	Fernando	Leandro	Olivo	Peralta	m	1966-10-12	01-01-22-001-0016	f
7063903	Hector	Pedro	Banda	Magaña	m	1968-10-02	01-01-22-001-0016	f
13646530	Alma	Nora	De Anda	Pacheco	f	1960-03-28	01-01-22-001-0016	f
13558310	Rayan	Aaron	Armas	Herrero	m	1965-03-10	01-01-22-001-0016	f
7300946	Alicia	Fatima	Alicea	Alcántar	f	1955-09-04	01-01-22-001-0016	f
14775132	Manuel	Jordi	Muñóz	Esquibel	m	1969-07-13	01-01-22-001-0016	f
8551844	Adam	Alonso	Beltrán	Vega	m	1959-08-18	01-01-22-001-0016	f
13216198	Hector	Aleix	Zavala	Gallegos	m	1967-02-01	01-01-22-001-0016	f
9061484	Rayan	Cesar	Vidal	Covarrubias	m	1955-07-27	01-01-22-001-0016	f
13159695	Nahia	Raquel	Verduzco	Miranda	f	1954-06-21	01-01-22-001-0016	f
14504758	Alvaro	Miguel	Barraza	Montez	m	1969-10-22	01-01-22-001-0016	f
8743159	Adria	Santiago	Peralta	Palomino	m	1959-09-12	01-01-22-001-0016	f
8675178	Martina	Carolina	Oliver	Soliz	f	1951-12-25	01-01-22-001-0016	f
11466425	Silvia	Aroa	Rodríquez	Amaya	f	1954-05-24	01-01-22-001-0016	f
5407798	Ainara	Mara	Gallardo	Barraza	f	1958-01-22	01-01-22-001-0016	f
10239624	Celia	Claudia	Moran	Suárez	f	1953-12-16	01-01-22-001-0016	f
8385600	Alberto	Miguel	Pastor	Gaytán	m	1961-06-10	01-01-22-001-0016	f
11279519	Dario	Pol	Raya	Cornejo	m	1968-02-05	01-01-22-001-0016	f
6911264	Oliver	Sergio	Gastélum	Escobedo	m	1958-03-24	01-01-22-001-0070	f
12399809	Bella	Carolina	Echevarría	Manzanares	f	1951-11-08	01-01-22-001-0049	f
14955184	Guillermo	Alex	Beltrán	Portillo	m	1964-05-06	01-01-22-001-0049	f
7621005	Clara	Marina	Meléndez	Cotto	f	1956-01-12	01-01-22-001-0049	f
11555516	Sandra	Yaiza	De la cruz	Puente	f	1965-03-02	01-01-22-001-0049	f
14490123	Rodrigo	Adrian	Serra	Casillas	m	1963-06-15	01-01-22-001-0049	f
9833462	Santiago	Alonso	Caldera	Urías	m	1967-03-31	01-01-22-001-0049	f
13975616	Raul	Diego	Córdova	Rodríquez	m	1969-11-24	01-01-22-001-0049	f
10088978	Erik	Mohamed	Blanco	Trujillo	m	1952-06-14	01-01-22-001-0049	f
14575250	Angel	Mohamed	Vergara	Chávez	m	1961-06-01	01-01-22-001-0049	f
10868611	Bella	Beatriz	Nava	Alarcón	f	1966-08-04	01-01-22-001-0049	f
5218444	Luna	Sara	Miramontes	Llamas	f	1960-01-25	01-01-22-001-0049	f
14620084	Sofia	Fatima	Alcántar	Millán	f	1956-02-29	01-01-22-001-0049	f
10813604	Ignacio	Jaime	Águilar	Laboy	m	1957-09-17	01-01-22-001-0049	f
5565482	Lara	Sara	Herrero	Aparicio	f	1958-02-09	01-01-22-001-0049	f
8302118	Alexia	Lola	Lira	Iglesias	f	1957-03-19	01-01-22-001-0049	f
13460735	Hugo	Juan	Anguiano	Delarosa	m	1963-02-25	01-01-22-001-0049	f
10442988	Beatriz	Alexia	Ibáñez	Montalvo	f	1958-03-09	01-01-22-001-0049	f
10192019	Ander	Alvaro	Luque	Barragán	m	1953-12-29	01-01-22-001-0049	f
9321379	Roberto	Bruno	Ceballos	Alarcón	m	1960-03-04	01-01-22-001-0049	f
10577459	Bruno	Pol	Agosto	Zamudio	m	1953-02-26	01-01-22-001-0049	f
8199026	Alejandro	Hugo	Saavedra	Ortiz	m	1965-12-01	01-01-22-001-0049	f
11381050	Samuel	Miguel	Hurtado	Sandoval	m	1962-10-28	01-01-22-001-0049	f
14119033	Daniela	Claudia	Méndez	Plaza	f	1967-06-08	01-01-22-001-0043	f
9362322	Alonso	Ivan	Mascareñas	Toro	m	1952-06-14	01-01-22-001-0043	f
10860436	Diego	Adam	Matías	Raya	m	1960-08-05	01-01-22-001-0043	f
6272666	Manuel	Domingo	Muñiz	Miguel	m	1955-05-05	01-01-22-001-0043	f
8408633	Aaron	Alejandro	Almonte	Palacios	m	1966-03-30	01-01-22-001-0043	f
6288918	Leire	Alexandra	Sepúlveda	Alcaraz	f	1957-06-07	01-01-22-001-0043	f
11315116	Alejandra	Claudia	Marrero	Rivero	f	1964-09-25	01-01-22-001-0043	f
9372852	Guillem	Roberto	Zaragoza	Solís	m	1963-04-24	01-01-22-001-0043	f
11013353	Marc	Rodrigo	León	Rivero	m	1954-05-26	01-01-22-001-0043	f
7173056	Alba	Sandra	Polanco	Arellano	f	1965-07-29	01-01-22-001-0043	f
9278868	Maria	Daniela	Castellanos	Ulloa	f	1968-11-22	01-01-22-001-0043	f
10602998	Pedro	Jesus	Escamilla	Heredia	m	1951-11-05	01-01-22-001-0043	f
12000559	Bruno	Alvaro	Cortés	De Anda	m	1953-05-16	01-01-22-001-0043	f
7661118	Pau	Nerea	Alemán	Carbajal	f	1968-05-16	01-01-22-001-0043	f
5492158	Cesar	Diego	Millán	Barroso	m	1968-11-24	01-01-22-001-0043	f
11549765	Patricia	Miriam	Galván	Esteban	f	1955-01-16	01-01-22-001-0043	f
14712676	Fatima	Natalia	Orta	Acosta	f	1967-05-19	01-01-22-001-0043	f
5478478	Oscar	Alvaro	Salinas	Armas	m	1963-12-04	01-01-22-001-0043	f
8873944	Martina	Candela	Sisneros	Riera	f	1969-11-30	01-01-22-001-0043	f
13962823	Jesus	Ander	Conde	Carvajal	m	1961-01-02	01-01-22-001-0043	f
9695798	Ignacio	Guillermo	Lorenzo	Velázquez	m	1955-01-17	01-01-22-001-0043	f
5377021	Jan	Sergio	Meléndez	Zepeda	m	1967-06-15	01-01-22-001-0043	f
12426359	Ana	Iria	Corrales	Carretero	f	1967-06-27	01-01-22-001-0043	f
5398001	Miguel	Miguel	Pastor	Pabón	m	1958-07-07	01-01-22-001-0043	f
10610430	Bella	Teresa	Leal	Jasso	f	1963-05-02	01-01-22-001-0043	f
6678123	Roberto	Hugo	Montero	Apodaca	m	1968-07-06	01-01-22-001-0043	f
10112634	Alexandra	Emma	Urías	Arroyo	f	1954-05-06	01-01-22-001-0016	f
7925953	Cristina	Victoria	Abeyta	Salvador	f	1957-10-24	01-01-22-001-0016	f
14849880	Marc	Marc	Ontiveros	Ulloa	m	1969-04-02	01-01-22-001-0016	f
6001523	Alexia	Zulay	Conde	Gallardo	f	1962-11-24	01-01-22-001-0016	f
8189020	Angel	Joel	Briseño	Galarza	m	1961-02-05	01-01-22-001-0016	f
6541830	Maria	Carlota	Quintanilla	Guajardo	f	1968-06-19	01-01-22-001-0016	f
13358823	Diego	Raul	Valverde	Valdés	m	1963-02-22	01-01-22-001-0016	f
7737946	Yorman	Rodrigo	Villa	Leiva	m	1955-09-02	01-01-22-001-0016	f
13590123	Alma	Leyre	Ayala	Porras	f	1958-12-31	01-01-22-001-0016	f
11377899	Raquel	Ainara	Pascual	Riera	f	1955-11-29	01-01-22-001-0016	f
9468420	Ainara	Raquel	Balderas	Serrato	f	1955-08-27	01-01-22-001-0016	f
7016696	Leandro	Jorge	Pedraza	Padilla	m	1967-06-24	01-01-22-001-0016	f
9665524	Valentina	Beatriz	Soriano	Jiménez	f	1965-08-04	01-01-22-001-0016	f
10013346	Ines	Leyre	Gallardo	Melgar	f	1956-09-24	01-01-22-001-0016	f
13361996	Jimena	Mireia	Suárez	Escobedo	f	1959-05-06	01-01-22-001-0016	f
12009418	Carolina	Valentina	Marroquín	Antón	f	1958-06-17	01-01-22-001-0016	f
14016310	Diego	Adam	Borrego	Corral	m	1956-04-10	01-01-22-0001-0027	f
13156410	Mateo	Santiago	Domínguez	Olmos	m	1954-08-14	01-01-22-0001-0027	f
7345580	Nerea	Blanca	Centeno	Quintanilla	f	1958-02-26	01-01-22-0001-0027	f
9026232	Jose	Cesar	Mercado	Santillán	m	1969-08-03	01-01-22-0001-0027	f
10444191	Candela	Elena	Quezada	Trejo	f	1953-09-20	01-01-22-0001-0027	f
12656630	Irene	Alma	Ramón	Quezada	f	1952-02-07	01-01-22-0001-0027	f
11971165	Maria	Elsa	Acosta	Toro	f	1963-04-05	01-01-22-0001-0027	f
10965343	Jaime	Pablo	Carrión	Cazares	m	1966-03-27	01-01-22-0001-0027	f
10891271	Vega	Anna	Urrutia	Rivero	f	1959-03-31	01-01-22-0001-0027	f
11893403	Lucia	Elsa	Casillas	Requena	f	1968-10-05	01-01-22-0001-0027	f
10852880	Samuel	Antonio	Cadena	Mas	m	1964-10-07	01-01-22-0001-0027	f
11884512	Fatima	Olivia	Sáenz	Borrego	f	1952-07-09	01-01-22-0001-0027	f
6213604	Yaiza	Valeria	Ortíz	Pabón	f	1969-02-28	01-01-22-0001-0027	f
12043732	Fernando	Guillem	Ureña	Henríquez	m	1966-08-25	01-01-22-0001-0027	f
11343395	Rodrigo	Jaime	Muñoz	Benavídez	m	1955-12-27	01-01-22-0001-0027	f
5761228	Alexia	Ines	Rangel	Córdova	f	1953-05-24	01-01-22-0001-0027	f
7032441	Rocio	Jimena	Aponte	Fierro	f	1964-09-19	01-01-22-0001-0027	f
9417204	Roberto	Martin	Valencia	Salazar	m	1967-08-06	01-01-22-0001-0027	f
7019303	Aroa	Rocio	Olivas	Alcaraz	f	1956-07-13	01-01-22-0001-0027	f
10831049	Nuria	Africa	Peña	Rico	f	1961-08-16	01-01-22-0001-0027	f
11676878	Patricia	Mireia	Soriano	Barraza	f	1961-10-19	01-01-22-0001-0027	f
10694073	Luna	Antonia	Oliva	Reyna	f	1952-01-29	01-01-22-0001-0027	f
10219717	Laia	Irene	Quezada	Santacruz	f	1967-07-04	01-01-22-0001-0027	f
11305244	Ariadna	Laura	Anaya	Madrigal	f	1954-12-23	01-01-22-0001-0027	f
12164372	Pau	Miriam	Valentín	Botello	f	1963-03-08	01-01-22-0001-0027	f
12547717	Isabel	Valeria	Izquierdo	Merino	f	1966-04-17	01-01-22-0001-0027	f
14694748	Ainara	Paola	Gaytán	Caldera	f	1965-05-26	01-01-22-0001-0027	f
5080306	Enrique	Aaron	Delao	Rascón	m	1965-05-23	01-01-22-0001-0027	f
11323100	Ian	Juan	Cuesta	Bueno	m	1967-12-04	01-01-22-0001-0027	f
14455933	Maria	Vera	Delgadillo	Carrion	f	1962-01-22	01-01-22-0001-0027	f
6731445	Samuel	Gonzalo	Valdés	Olivera	m	1955-11-27	01-01-22-0001-0027	f
5696368	Miguel	Oliver	Almonte	Carranza	m	1963-08-02	01-01-22-0001-0027	f
12726426	Izan	Dario	Ramos	Cabán	m	1956-04-01	01-01-22-0001-0027	f
14756690	Vera	Erika	Puga	Salcido	f	1962-11-18	01-01-22-0001-0027	f
10377525	Iria	Alexandra	Gil	Jaramillo	f	1956-03-28	01-01-22-0001-0027	f
9073962	Nuria	Yaiza	Padilla	Archuleta	f	1962-01-24	01-01-22-0001-0027	f
8814689	Mateo	Rafael	Villegas	Lara	m	1958-01-24	01-01-22-0001-0027	f
5022257	Sebastian	Javier	Cardenas	Piña	m	1967-01-17	01-01-22-0001-0027	f
5099661	Nahia	Berta	Almaraz	Puig	f	1967-01-30	01-01-22-0001-0027	f
10183628	Angela	Valentina	Pabón	Cornejo	f	1963-06-13	01-01-22-0001-0027	f
13147255	Leonardo	Jaime	Casanova	Guajardo	m	1959-06-10	01-01-22-0001-0027	f
5303601	Ian	Pedro	Jaramillo	Cepeda	m	1955-08-10	01-01-22-0001-0027	f
14042307	Eva	Joan	Venegas	Benítez	f	1955-05-09	01-01-22-0001-0027	f
10981491	Nahia	Celia	Márquez	Roque	f	1953-06-28	01-01-22-0001-0027	f
12604211	Paula	Celia	Pagan	Gil	f	1968-08-07	01-01-22-0001-0027	f
8260031	Alejandro	Gabriel	Lucero	Niño	m	1961-10-15	01-01-22-0001-0027	f
14771612	Valeria	Leire	Álvarez	Reséndez	f	1960-04-30	01-01-22-0001-0027	f
13702518	Roberto	Marco	Ruiz	Giménez	m	1959-09-08	01-01-22-0001-0027	f
12884462	Carlos	Fernando	Berríos	Riera	m	1954-04-17	01-01-22-0001-0027	f
12944665	Jesus	Gabriel	Gallegos	Mendoza	m	1952-05-19	01-01-22-0001-0027	f
12444389	Emma	Helena	Pascual	Esparza	f	1953-06-25	01-01-22-0001-0027	f
6641279	Noelia	Alba	Cordero	Páez	f	1953-03-13	01-01-22-0001-0027	f
13034754	Adria	Saul	Santiago	Corral	m	1955-03-06	01-01-22-0001-0027	f
5782338	Rocio	Naiara	Guardado	Orta	f	1965-04-28	01-01-22-0001-0027	f
8902721	Salma	Claudia	Aguilera	Arroyo	f	1952-02-05	01-01-22-0001-0027	f
12198472	Lidia	Gabriela	Zamora	Alanis	f	1956-07-14	01-01-22-0001-0027	f
6791810	Marc	Oliver	Lemus	Gonzales	m	1960-01-25	01-01-22-0001-0027	f
10921416	Lara	Nadia	Lira	Mendoza	f	1963-02-08	01-01-22-0001-0027	f
6503510	Eva	Gabriela	Caballero	Mojica	f	1964-05-01	01-01-22-0001-0027	f
11281947	Alma	Berta	Meza	Giménez	f	1957-10-04	01-01-22-0001-0027	f
12306692	Ismael	Pedro	Segovia	Méndez	m	1960-07-26	01-01-22-0001-0027	f
6252829	Samuel	Jaime	Vaca	Valladares	m	1952-11-05	01-01-22-0001-0027	f
10287599	Maria	Anna	Alcántar	Alejandro	f	1957-07-15	01-01-22-0001-0027	f
8488126	Alvaro	Ian	Vergara	Llorente	m	1962-01-22	01-01-22-0001-0027	f
14725648	Rocio	Andrea	Villar	Sáenz	f	1962-11-07	01-01-22-0001-0027	f
6185257	Lidia	Joan	Aguirre	Avilés	f	1957-03-13	01-01-22-0001-0027	f
14248316	Carolina	Martina	Ayala	Ordoñez	f	1962-05-28	01-01-22-0001-0027	f
10924090	Alejandra	Julia	Domínquez	Zayas	f	1952-06-26	01-01-22-0001-0027	f
9727271	Alexia	Iria	Rubio	Guardado	f	1968-05-16	01-01-22-0001-0027	f
13472255	Nerea	Julia	Arce	Gaona	f	1968-08-08	01-01-22-0001-0027	f
8367231	Sara	Alexandra	Román	Monroy	f	1961-07-15	01-01-22-0001-0027	f
9915567	Adria	Diego	Patiño	Maldonado	m	1960-07-25	01-01-22-0001-0027	f
13349682	Marcos	Leo	Gonzales	Fierro	m	1952-07-21	01-01-22-0001-0027	f
12266434	Naia	Angela	Solís	Cepeda	f	1965-09-14	01-01-22-0001-0027	f
14353540	Diego	Marc	Barrientos	Peres	m	1954-01-02	01-01-22-0001-0027	f
7081409	Nadia	Carolina	Redondo	Santamaría	f	1959-08-14	01-01-22-0001-0027	f
12072766	Ona	Nayara	Galván	Ybarra	f	1965-06-12	01-01-22-0001-0027	f
7520699	Bella	Mar	Nieto	Peres	f	1952-08-06	01-01-22-0001-0027	f
14505545	Aaron	Jose	Caballero	Apodaca	m	1958-01-05	01-01-22-0001-0027	f
8608328	Carla	Mar	Valverde	Pulido	f	1957-06-08	01-01-22-0001-0027	f
6924091	Lucas	Pedro	Cornejo	Puente	m	1951-11-07	01-01-22-0001-0027	f
5727754	Abril	Leyre	Heredia	Rueda	f	1954-09-10	01-01-22-0001-0027	f
6369484	Izan	Santiago	Hernando	Llamas	m	1957-06-20	01-01-22-0001-0027	f
5384529	Daniela	Isabel	Carvajal	Casanova	f	1964-09-27	01-01-22-0001-0027	f
6127879	Erik	Joel	Escobar	Henríquez	m	1957-09-24	01-01-22-0001-0027	f
14087595	Martin	Gonzalo	Bueno	Jaime	m	1967-02-13	01-01-22-0001-0027	f
8425591	Mohamed	Nicolas	Parra	Frías	m	1954-11-05	01-01-22-0001-0027	f
5580560	Lucas	Marco	Alicea	Marco	m	1956-09-21	01-01-22-0001-0027	f
12158501	Aaron	Leonardo	Abad	Andreu	m	1957-09-16	01-01-22-0001-0027	f
8331097	Fatima	Mar	Benito	Pereira	f	1957-05-31	01-01-22-0001-0027	f
7019733	Ruben	Jaime	Ballesteros	Solorio	m	1957-12-22	01-01-22-0001-0027	f
6816043	Sofia	Joan	Ibáñez	Salazar	f	1968-09-25	01-01-22-0001-0027	f
8377969	Claudia	Yaiza	Tamayo	Delacrúz	f	1965-12-08	01-01-22-0001-0027	f
13620602	Oswaldo	Miguel	Perales	Pérez	m	1962-10-25	01-01-22-0001-0027	f
14834143	Cristian	Jon	Escobedo	Guerrero	m	1957-12-02	01-01-22-0001-0027	f
10135292	Dario	David	Salas	Bernal	m	1959-10-24	01-01-22-0001-0027	f
6167762	Daniel	Samuel	Jaramillo	Saavedra	m	1965-01-15	01-01-22-0001-0027	f
5312684	Adriana	Africa	Rendón	Cantú	f	1957-05-31	01-01-22-0001-0027	f
13400595	Laia	Clara	Guzmán	Cortez	f	1959-01-09	01-01-22-0001-0027	f
10560402	Erika	Marta	Zelaya	Sanabria	f	1958-01-15	01-01-22-0001-0027	f
11930255	Iria	Cristina	Vela	Olivares	f	1969-10-09	01-01-22-0001-0027	f
8819744	Julia	Elsa	Bravo	Puente	f	1959-09-15	01-01-22-0001-0027	f
12834667	Diana	Nadia	Sandoval	Aguayo	f	1964-06-09	01-01-22-0001-0027	f
6408927	Jordi	Santiago	Pabón	Expósito	m	1962-08-21	01-01-22-0001-0027	f
7036759	Alexandra	Daniela	Santiago	Negrete	f	1957-06-18	01-01-22-0001-0027	f
5436562	Pol	Rayan	Estévez	Lugo	m	1964-08-15	01-01-22-0001-0027	f
13099389	Ian	Ignacio	Ruíz	Ponce	m	1956-11-13	01-01-22-0001-0027	f
10206763	Salma	Raquel	Girón	López	f	1962-04-12	01-01-22-0001-0027	f
7562431	Nuria	Leire	Zapata	Castro	f	1963-07-22	01-01-22-0001-0027	f
12080905	Marti	Marti	Méndez	Vásquez	f	1968-04-03	01-01-22-0001-0027	f
8904497	Manuel	Alvaro	Guevara	Alaniz	m	1959-10-13	01-01-22-0001-0027	f
7605532	Marc	Aaron	Duarte	Cortes	m	1955-07-03	01-01-22-0001-0027	f
14399512	Ainara	Ariadna	Arreola	Vigil	f	1961-10-31	01-01-22-0001-0027	f
8286460	Irene	Iria	Delacrúz	Parra	f	1967-01-27	01-01-22-0001-0027	f
14742737	Alvaro	Leandro	Peláez	Esteban	m	1969-06-01	01-01-22-0001-0027	f
11433181	Sergio	Oliver	Solorzano	Almanza	m	1965-01-27	01-01-22-0001-0027	f
13675922	Silvia	Lucia	Ayala	Aparicio	f	1956-06-02	01-01-22-0001-0027	f
12271552	Noelia	Rocio	Batista	Castro	f	1962-01-07	01-01-22-0001-0027	f
5714155	Paola	Elena	Ybarra	Zaragoza	f	1968-01-12	01-01-22-0001-0027	f
7584334	Erik	Marco	Pardo	Rivera	m	1968-01-21	01-01-22-0001-0027	f
6614027	Jesus	Manuel	Pichardo	Rojas	m	1954-08-23	01-01-22-0001-0027	f
11712277	Ignacio	Gonzalo	Loera	Muro	m	1954-12-16	01-01-22-0001-0027	f
5857976	Pau	Luna	Negrón	Andreu	f	1957-08-18	01-01-22-0001-0027	f
12271481	Lucas	Fernando	Cardona	Santillán	m	1969-10-05	01-01-22-0001-0027	f
7944818	Omar	Bruno	Arredondo	Ocasio	m	1963-07-30	01-01-22-0001-0027	f
10581458	Sara	Victoria	Vila	Fajardo	f	1963-03-13	01-01-22-0001-0027	f
11271346	Nuria	Leyre	Avilés	Laureano	f	1963-01-07	01-01-22-0001-0027	f
9827292	Africa	Martina	Madrigal	Carretero	f	1961-05-05	01-01-22-0001-0027	f
7037692	Marti	Ana	Estrada	Ochoa	f	1959-11-11	01-01-22-0001-0027	f
6082117	Mohamed	Francisco Javier	Sierra	Carvajal	m	1968-01-05	01-01-22-0001-0027	f
6412212	Miguel	Nicolas	Treviño	Sanz	m	1965-10-04	01-01-22-0001-0027	f
10483673	Aroa	Julia	Acuña	Cabán	f	1958-11-07	01-01-22-0001-0027	f
11863619	Jimena	Andrea	Pineda	Menchaca	f	1952-04-30	01-01-22-0001-0027	f
12898507	Alex	Cristian	Mayorga	Soriano	m	1961-07-10	01-01-22-0001-0027	f
11406218	Leandro	Marc	Calero	Delafuente	m	1953-09-11	01-01-22-0001-0027	f
7031331	Natalia	Lucia	Ríos	Sotelo	f	1967-12-02	01-01-22-0001-0027	f
9668657	Ane	Raquel	Castro	Álvarez	f	1954-05-19	01-01-22-0001-0027	f
11283167	Pablo	Eric	Armenta	Oquendo	m	1959-08-30	01-01-22-0001-0027	f
10182609	Francisco Javier	Aaron	Nazario	Corona	m	1961-12-02	01-01-22-0001-0027	f
12549230	Omar	Jan	Lerma	Carreón	m	1966-01-27	01-01-22-0001-0027	f
10478175	Joan	Ainara	Ruelas	Robledo	f	1955-12-03	01-01-22-0001-0027	f
12029684	Ane	Luna	Aponte	Carreón	f	1958-10-04	01-01-22-0001-0027	f
14520526	Jimena	Sofia	Diez	Pelayo	f	1967-05-06	01-01-22-0001-0027	f
6649118	Bruno	Rayan	Cordero	Serna	m	1961-09-01	01-01-22-0001-0027	f
6336263	Alvaro	Adria	Ortíz	Mora	m	1963-03-09	01-01-22-0001-0027	f
5339519	Jesus	Juan	Valadez	Gallego	m	1967-05-14	01-01-22-0001-0027	f
10796488	Nuria	Berta	Romero	Alicea	f	1965-03-25	01-01-22-0001-0027	f
11852720	Mara	Luna	Ballesteros	Campos	f	1954-03-07	01-01-22-0001-0027	f
10085408	Alvaro	Jon	Flores	Cortez	m	1955-03-01	01-01-22-0001-0027	f
5363253	Jesus	David	Apodaca	Zamora	m	1952-02-18	01-01-22-0001-0027	f
13642814	Ruben	Hugo	Tamayo	Rosado	m	1967-09-11	01-01-22-0001-0027	f
9239296	Daniel	Aleix	Blázquez	Maestas	m	1959-04-04	01-01-22-0001-0027	f
12220989	Noa	Bella	Vega	Negrón	f	1967-03-04	01-01-22-0001-0027	f
6524603	Julia	Sara	Carballo	Mejía	f	1964-01-18	01-01-22-0001-0027	f
6843668	Miguel	Izan	Banda	Abrego	m	1954-12-25	01-01-22-0001-0027	f
5266820	Jana	Yaiza	Pozo	Zavala	f	1962-08-30	01-01-22-0001-0027	f
6938527	Francisco Javier	Eduardo	Rodríquez	Nava	m	1966-07-28	01-01-22-0001-0027	f
10096102	Juan	Leo	López	Cortes	m	1962-02-16	01-01-22-0001-0027	f
12844656	Andres	Alejandro	Lemus	Castellano	m	1959-05-18	01-01-22-0001-0027	f
14596760	Vera	Celia	Estévez	Domenech	f	1966-06-03	01-01-22-0001-0027	f
12239438	Gabriela	Patricia	Lozada	Estévez	f	1966-02-08	01-01-22-0001-0027	f
14639080	Hugo	Francisco	Menéndez	Guerra	m	1959-09-04	01-01-22-0001-0027	f
14989738	Lidia	Elsa	Ortíz	Vargas	f	1963-08-07	01-01-22-0001-0027	f
7261155	Zulay	Alejandra	López	Yáñez	f	1964-12-19	01-01-22-0001-0027	f
5180479	Mireia	Alexia	Rodrígez	Jurado	f	1958-08-14	01-01-22-0001-0027	f
14998579	Celia	Nadia	Alba	Samaniego	f	1956-12-18	01-01-22-0001-0027	f
9152251	Gabriel	Sergio	Monroy	Badillo	m	1963-01-31	01-01-22-0001-0027	f
7703149	Jimena	Sandra	Vicente	Pons	f	1954-10-23	01-01-22-0001-0027	f
5998957	Jesus	Adam	Ramón	Delatorre	m	1959-08-25	01-01-22-0001-0027	f
11058478	Maria	Leyre	Benavídez	Vidal	f	1960-08-14	01-01-22-0001-0027	f
11279354	Laia	Patricia	Zamudio	Cornejo	f	1964-01-28	01-01-22-0001-0027	f
7852377	Adriana	Nil	Pérez	Preciado	f	1966-12-10	2017-10-0013	f
12952706	Alexia	Mar	Reyna	Delrío	f	1964-10-04	2017-10-0013	f
6471190	Jesus	Joel	Ortíz	Mireles	m	1960-04-19	2017-10-0013	f
8468166	Omar	Mohamed	Marco	Solorio	m	1961-04-27	2017-10-0013	f
8605527	Rocio	Vera	Cervántez	Castaño	f	1953-07-21	2017-10-0013	f
6335452	Clara	Iria	Ibarra	Rivas	f	1957-07-09	2017-10-0013	f
8865741	Victor	Raul	Mayorga	Pizarro	m	1964-10-13	2017-10-0013	f
9302550	Laia	Raquel	Griego	Escamilla	f	1957-11-17	2017-10-0013	f
10516151	Erika	Lola	Alicea	Olivárez	f	1952-05-30	2017-10-0013	f
13209797	Eva	Antonia	Gimeno	Naranjo	f	1953-11-11	2017-10-0013	f
9491388	Carolina	Eva	Zelaya	Antón	f	1959-07-11	2017-10-0013	f
8625304	Luna	Nora	Yáñez	Arenas	f	1967-02-18	2017-10-0013	f
9272441	Rafael	Ian	Ruíz	Adame	m	1952-04-18	2017-10-0013	f
11299515	Miguel	Oscar	Ruiz	Ortega	m	1955-12-29	2017-10-0013	f
7101247	Joel	Leonardo	Yáñez	Aguayo	m	1952-05-18	2017-10-0013	f
13690924	Natalia	Carolina	Cervantes	Mejía	f	1964-03-16	2017-10-0013	f
11911111	Carla	Isabel	Marco	Ponce	f	1957-01-08	2017-10-0013	f
13130465	Ivan	Alberto	Valverde	Rocha	m	1963-03-06	2017-10-0013	f
10197546	Mara	Aroa	Monroy	Bermúdez	f	1955-08-21	2017-10-0013	f
14883307	Alvaro	Manuel	Almonte	Mendoza	m	1958-02-16	2017-10-0013	f
13717131	Victor	Oscar	Colón	Escamilla	m	1952-08-02	2017-10-0013	f
10911719	Alejandra	Natalia	Botello	Carreón	f	1953-11-17	2017-10-0013	f
5049644	Alexia	Naia	Adame	Heredia	f	1956-07-24	2017-10-0013	f
10713622	Pol	Ander	Blasco	Zelaya	m	1963-11-16	2017-10-0013	f
11943177	Francisco	Santiago	Medina	Maya	m	1962-06-20	2017-10-0013	f
8779243	Roberto	Leandro	Solano	Quezada	m	1965-01-30	2017-10-0013	f
11769877	Jaime	Oscar	Tello	Castellanos	m	1958-12-10	2017-10-0013	f
5461678	Ignacio	Leo	Madera	Reynoso	m	1967-11-04	2017-10-0013	f
14275605	Andres	Samuel	Páez	Viera	m	1962-08-25	2017-10-0013	f
6633832	Alba	Alma	Quesada	Gaitán	f	1966-01-04	2017-10-0013	f
7338225	Santiago	Domingo	Lozada	Cerda	m	1958-07-06	2017-10-0013	f
12990207	Elena	Carlota	Calvillo	Barrios	f	1960-01-09	2017-10-0013	f
5942314	Martina	Sara	Osorio	Carrasco	f	1967-04-23	2017-10-0013	f
10590936	Alonso	Adria	Pereira	Cornejo	m	1969-11-26	2017-10-0013	f
11246294	Carolina	Natalia	Segovia	Escobedo	f	1957-11-23	2017-10-0013	f
12195926	Emma	Miriam	Madrigal	Cedillo	f	1963-05-16	2017-10-0013	f
8805270	Ariadna	Candela	Mejía	Colunga	f	1966-11-30	2017-10-0013	f
8683107	Pol	Oliver	Cortez	Herrero	m	1965-01-11	2017-10-0013	f
8191152	Nadia	Mireia	Alarcón	Valencia	f	1967-09-19	2017-10-0013	f
12914003	Francisco	Miguel	Arenas	Silva	m	1955-08-04	2017-10-0013	f
10723011	Patricia	Ona	Quintana	Rodrígez	f	1953-10-01	2017-10-0013	f
11622062	Paola	Marti	Figueroa	Camarillo	f	1960-03-18	2017-10-0013	f
9747818	Nora	Carmen	Nazario	Solano	f	1953-01-27	2017-10-0013	f
8068672	Nuria	Ana	Aragón	Barrientos	f	1967-02-16	2017-10-0013	f
12167221	Pablo	Pol	Bonilla	Mejía	m	1962-12-27	2017-10-0013	f
5241318	Lucia	Sofia	Orta	Pedraza	f	1962-08-18	2017-10-0013	f
6830574	Ivan	Erik	Reina	Aguilera	m	1968-06-30	2017-10-0013	f
13889359	Mara	Julia	Hernádez	Bustamante	f	1962-03-11	2017-10-0013	f
6159046	Domingo	Antonio	Gil	Salvador	m	1965-11-17	2017-10-0013	f
11949555	Ana	Natalia	Caro	Carretero	f	1967-03-13	2017-10-0013	f
7257033	Valeria	Gabriela	Arellano	Almanza	f	1956-09-27	2017-10-0013	f
10960342	Isabel	Naia	Carrera	Vidal	f	1959-09-22	2017-10-0013	f
11937803	Mara	Marta	Guillen	Abreu	f	1954-03-24	2017-10-0013	f
5683328	Oscar	Mateo	Manzanares	Canales	m	1954-05-23	2017-10-0013	f
6129020	Alberto	Alex	Carrion	Villagómez	m	1954-09-19	2017-10-0013	f
7810588	Olivia	Berta	Solorio	Lomeli	f	1964-11-25	2017-10-0013	f
13568908	Oscar	Izan	Simón	De la cruz	m	1962-07-22	2017-10-0013	f
8814113	Luna	Noa	Navarrete	Vargas	f	1966-11-26	2017-10-0013	f
6621345	Bruno	Miguel	Gallegos	Preciado	m	1962-04-07	2017-10-0013	f
11529563	Noelia	Anna	Díaz	Alfaro	f	1962-06-09	2017-10-0013	f
13953527	Noa	Claudia	Quiñones	Naranjo	f	1961-01-29	2017-10-0013	f
6321877	Francisco	Izan	Vigil	Rosas	m	1966-05-05	2017-10-0013	f
11780496	Francisco Javier	Dario	Pulido	Blanco	m	1958-01-12	2017-10-0013	f
11768637	Manuela	Paula	Briones	Gamboa	f	1961-01-09	2017-10-0013	f
5543437	Roberto	Mohamed	Rentería	Zamora	m	1960-10-06	2017-10-0013	f
10372072	Leandro	Jan	Carrillo	Valencia	m	1954-05-09	2017-10-0013	f
5943172	Jaime	Lucas	Badillo	Montalvo	m	1955-07-25	2017-10-0013	f
13063762	Erik	Adrian	Cerda	Agosto	m	1959-04-04	2017-10-0013	f
11060286	Javier	Samuel	Solano	Serna	m	1957-01-16	2017-10-0013	f
6207847	Erik	Adria	Quiroz	Casas	m	1957-01-24	2017-10-0013	f
14068580	Adam	Saul	Malave	López	m	1967-12-18	2017-10-0013	f
8570590	Sebastian	Enrique	Arredondo	Jimínez	m	1954-12-11	2017-10-0013	f
14257279	Alexia	Luna	Juárez	Córdova	f	1966-05-18	2017-10-0013	f
9546780	Alexia	Antonia	Giménez	Rivas	f	1953-02-01	2017-10-0013	f
7210849	Marina	Sofia	Ruiz	Miramontes	f	1957-01-28	2017-10-0013	f
14454190	Miguel	Jan	Puga	Polo	m	1958-01-27	2017-10-0013	f
6662123	Rocio	Jimena	Cadena	Villa	f	1965-11-12	2017-10-0013	f
5430010	Raul	Lucas	Quesada	Cabrera	m	1962-02-10	2017-10-0013	f
12357449	Clara	Alexia	Fonseca	Varela	f	1969-06-08	2017-10-0013	f
5648170	Alma	Marina	Lorente	Córdoba	f	1962-04-25	2017-10-0013	f
14739694	Nicolas	Manuel	Mejía	Madera	m	1953-07-10	2017-10-0013	f
12530600	Manuela	Elsa	Moran	Blázquez	f	1956-12-21	2017-10-0013	f
5724975	Marco	Jon	Casado	Roybal	m	1960-01-14	2017-10-0013	f
13632163	Carlos	Izan	Hernando	Blanco	m	1968-11-24	2017-10-0013	f
12941438	Izan	Leandro	Marrero	Cuellar	m	1957-11-14	2017-10-0013	f
11803965	Hugo	Joel	Roque	Diez	m	1965-01-16	2017-10-0013	f
14194435	Leonardo	Carlos	Flores	Puig	m	1967-10-07	2017-10-0013	f
13801504	Carolina	Paola	Villalpando	Esteve	f	1960-06-11	2017-10-0013	f
7224059	Izan	Fernando	Almanza	De la fuente	m	1963-09-25	2017-10-0013	f
7872904	Izan	Guillermo	Botello	Salcido	m	1960-05-11	2017-10-0013	f
14439670	Saul	Gabriel	Ocampo	Mota	m	1959-05-04	2017-10-0013	f
9997988	Candela	Lidia	Zamora	Alemán	f	1961-12-25	2017-10-0013	f
14784584	Gabriela	Valentina	Girón	Alanis	f	1955-01-02	2017-10-0013	f
11068353	Daniela	Nadia	García	Valero	f	1969-02-21	2017-10-0013	f
5925519	Valentina	Manuela	Soler	Gallegos	f	1955-04-03	2017-10-0013	f
14817462	Carlos	Cesar	Covarrubias	Serrano	m	1956-03-15	2017-10-0013	f
5236913	Elena	Carla	Saiz	Galán	f	1957-09-22	2017-10-0013	f
9537057	Iria	Manuela	Sanz	Tijerina	f	1968-02-23	2017-10-0013	f
11624484	Joel	Cesar	Alvarado	Anguiano	m	1959-05-20	2017-10-0013	f
13042117	Guillermo	Marc	Blázquez	Soriano	m	1964-10-18	2017-10-0013	f
14635114	Jan	Yorman	Orellana	Bustos	m	1960-10-30	2017-10-0013	f
11485337	Adria	Pablo	Vigil	Pozo	m	1964-06-18	2017-10-0013	f
10160273	Hugo	Hugo	Garibay	Delao	m	1964-01-28	2017-10-0013	f
9847055	Nicolas	Carlos	Reina	Delao	m	1963-12-02	2017-10-0013	f
11131572	Raquel	Laia	Vallejo	Paz	f	1956-08-13	2017-10-0013	f
13247473	Sara	Alejandra	Esquivel	Páez	f	1967-07-03	2017-10-0013	f
7135730	Gabriela	Carolina	Galindo	Ybarra	f	1965-01-28	2017-10-0013	f
6231650	Aroa	Nadia	Meza	Manzano	f	1955-04-10	2017-10-0013	f
11295594	Eric	Miguel	Sotelo	Bautista	m	1960-04-26	2017-10-0013	f
8689111	Sandra	Lara	Garay	Pagan	f	1953-02-22	2017-10-0013	f
5375547	Jan	Rodrigo	Soto	Anguiano	m	1965-06-11	2017-10-0013	f
13503688	Cesar	Eduardo	Mora	Rosa	m	1956-08-30	2017-10-0013	f
9228825	Eduardo	Aaron	Ballesteros	Casillas	m	1967-09-20	2017-10-0013	f
14932414	Maria	Alejandra	Villaseñor	Polo	f	1960-06-19	2017-10-0013	f
5802553	Valeria	Yaiza	Cortes	Alemán	f	1967-04-07	2017-10-0013	f
14071403	Ainara	Victoria	Agosto	Castellano	f	1954-02-03	2017-10-0013	f
13325416	Oscar	Victor	Valenzuela	Pabón	m	1962-12-10	2017-10-0013	f
9256501	Izan	Roberto	Briseño	Perea	m	1960-01-14	2017-10-0013	f
5560470	Julia	Daniela	Robledo	Palacios	f	1964-01-10	2017-10-0013	f
11198617	Rodrigo	Dario	Cepeda	Serrato	m	1954-02-06	2017-10-0013	f
10095965	Mara	Nerea	Salvador	Valdés	f	1958-07-10	2017-10-0013	f
11633289	Carla	Miriam	Carrasco	Galán	f	1954-07-12	2017-10-0013	f
8451581	Luis	Ignacio	Rico	Lucas	m	1960-01-22	2017-10-0013	f
12643840	Francisco	Rodrigo	Arreola	González	m	1964-09-04	2017-10-0013	f
10640005	Clara	Candela	Ledesma	Gonzales	f	1969-06-28	2017-10-0013	f
13991538	Mireia	Julia	Tórrez	Salgado	f	1969-08-10	2017-10-0013	f
8501611	Izan	Aleix	Báez	Sáez	m	1968-05-21	2017-10-0013	f
14024893	Adam	Guillem	Batista	Costa	m	1965-07-28	2017-10-0013	f
6987724	Julia	Paula	Urías	Páez	f	1955-08-07	2017-10-0013	f
8484953	Carlota	Claudia	Gallego	Calvo	f	1964-03-15	2017-10-0013	f
13000535	Manuel	Rodrigo	Piña	Valles	m	1963-11-26	2017-10-0013	f
11743792	Adrian	Erik	Sarabia	Marroquín	m	1963-04-13	2017-10-0013	f
13414907	Emma	Rocio	Vergara	Ulibarri	f	1967-04-03	2017-10-0013	f
7743132	Iria	Julia	Oropeza	Romero	f	1964-06-29	2017-10-0013	f
14060352	Adria	Daniel	Quintero	Alvarado	m	1966-05-02	2017-10-0013	f
5792935	Adria	Leo	Alcaraz	Berríos	m	1962-01-13	2017-10-0013	f
14006301	Andrea	Diana	Castellano	Costa	f	1959-02-20	2017-10-0013	f
9132962	Ander	Diego	Valenzuela	Figueroa	m	1952-08-22	2017-10-0013	f
8635391	Nuria	Paola	Maya	Acuña	f	1953-04-06	2017-10-0013	f
12180158	Samuel	Erik	Peláez	Acosta	m	1968-05-16	2017-10-0013	f
6531948	Celia	Gabriela	Orellana	Llamas	f	1954-12-05	2017-10-0013	f
9807771	Alexia	Elena	Expósito	Solorio	f	1959-05-23	2017-10-0013	f
8814897	Alonso	Pablo	Vergara	Fernández	m	1963-10-25	2017-10-0013	f
6703295	Juan	Rodrigo	Saucedo	Estévez	m	1953-11-25	2017-10-0013	f
8445978	Manuela	Eva	Munguía	Costa	f	1956-01-26	2017-10-0013	f
10347414	Ignacio	Miguel	Llorente	Otero	m	1961-02-02	2017-10-0013	f
13205188	Blanca	Nayara	Rael	Calero	f	1958-10-01	2017-10-0013	f
14062011	Cristian	Mario	Urrutia	Blázquez	m	1964-06-12	2017-10-0013	f
10991632	Julia	Maria	Figueroa	Pardo	f	1957-11-04	2017-10-0013	f
13695682	Noa	Eva	Mas	Jaimes	f	1954-09-11	2017-10-0013	f
7385354	Cesar	Adria	Atencio	Ulibarri	m	1969-10-17	2017-10-0013	f
10042293	Claudia	Elena	Carreón	Rey	f	1954-06-10	2017-10-0013	f
13817074	Joel	Hugo	Calvillo	Álvarez	m	1953-08-09	2017-10-0013	f
8954727	Mar	Mar	Andreu	Bautista	f	1956-06-06	2017-10-0013	f
14304900	Alberto	Leonardo	Luevano	Pizarro	m	1954-07-27	2017-10-0013	f
6909387	Yaiza	Andrea	Armas	Escalante	f	1969-07-17	2017-10-0013	f
6853684	Beatriz	Carmen	Montañez	Aguilar	f	1957-02-28	2017-10-0013	f
14155326	Nora	Ariadna	Aragón	Caballero	f	1957-10-28	2017-10-0013	f
14194921	Enrique	Angel	Lemus	Rangel	m	1969-05-13	2017-10-0013	f
7186953	Cristian	Gabriel	Maestas	Quintana	m	1959-04-30	2017-10-0013	f
5209085	Hugo	Pol	Garibay	Quiñones	m	1968-01-14	2017-10-0013	f
13616643	Enrique	Jorge	Ortíz	Lozano	m	1964-11-15	2017-10-0013	f
9878036	Hector	Izan	Ontiveros	Pedroza	m	1960-10-07	2017-10-0013	f
13161449	Salma	Claudia	Ibarra	Corona	f	1967-07-12	2017-10-0013	f
8247595	Isaac	Alejandro	Verdugo	Montes	m	1963-06-08	2017-10-0013	f
8766797	Helena	Noa	Jáquez	Sáenz	f	1968-11-16	2017-10-0013	f
11088472	Victoria	Laia	Santos	Apodaca	f	1964-04-22	2017-10-0013	f
13637672	Luis	Ander	Gonzales	Collazo	m	1964-09-27	2017-10-0013	f
11279414	Oswaldo	Sebastian	Barela	Peralta	m	1962-02-24	2017-10-0013	f
5921490	Nayara	Joan	Antón	Pantoja	f	1965-09-17	2017-10-0013	f
12751735	Daniel	Rayan	Mas	Beltrán	m	1963-02-11	2017-10-0013	f
7533538	Nerea	Clara	Valdez	Carvajal	f	1965-07-03	2017-10-0013	f
14517433	Leandro	Pedro	Toledo	Bañuelos	m	1958-01-11	2017-10-0013	f
5637562	Ismael	Izan	Villagómez	Arteaga	m	1958-08-02	2017-10-0013	f
10357188	Jaime	Jordi	Concepción	Luque	m	1954-09-02	2017-10-0013	f
11770911	Pol	Jesus	Armenta	Casárez	m	1961-06-30	2017-10-0013	f
11793844	Ignacio	Victor	Martínez	Valladares	m	1958-04-17	2017-10-0013	f
12274518	Berta	Leire	Olivares	Cotto	f	1959-09-30	2017-10-0013	f
12683309	Martin	Nicolas	Gamboa	Farías	m	1967-05-25	2017-10-0013	f
14531451	Jaime	Ruben	Tovar	Jurado	m	1964-02-27	2017-10-0013	f
12274562	Alexia	Olivia	Sanz	Aragón	f	1955-02-09	2017-10-0013	f
7297469	Jon	Ruben	Téllez	Cazares	m	1958-03-03	2017-10-0013	f
12867103	Jan	Rayan	Domínquez	Pons	m	1952-09-09	2017-10-0013	f
14629500	Laia	Jimena	Guerra	Dueñas	f	1960-08-23	2017-10-0013	f
8415283	Alonso	Manuel	Botello	Regalado	m	1960-08-14	2017-10-0013	f
11131437	Marc	Marcos	Jimínez	Guerra	m	1968-02-29	2017-10-0013	f
9419361	Pablo	Daniel	Anguiano	Quiñones	m	1961-12-26	2017-10-0013	f
8494444	Leo	David	Gallegos	Díaz	m	1954-11-06	2017-10-0013	f
13686958	Leire	Bella	Jaime	Arce	f	1954-10-06	2017-10-0013	f
7932061	Rayan	Rayan	Tomas	Trujillo	m	1961-01-31	2017-10-0013	f
5458908	Pablo	Jaime	Peres	Treviño	m	1964-10-27	2017-10-0013	f
7558094	Eric	Luis	Valenzuela	Clemente	m	1957-01-04	2017-10-0013	f
7639737	Cesar	Jordi	Cervántez	Ceballos	m	1959-04-23	2017-10-0013	f
9235530	Alexandra	Iria	Carrasco	Almanza	f	1964-03-18	2017-10-0013	f
10353230	Mohamed	Jon	Roldán	Hurtado	m	1952-02-10	2017-10-0013	f
7227472	Alexandra	Natalia	Balderas	Elizondo	f	1962-07-28	2017-10-0013	f
8345233	Julia	Valentina	Reynoso	Naranjo	f	1954-12-05	2017-10-0013	f
5880824	Irene	Carolina	Peña	Bonilla	f	1959-08-17	2017-10-0013	f
10951425	Jose	Francisco	Antón	Laboy	m	1968-01-30	2017-10-0013	f
10107896	Erika	Iria	Salvador	Jiménez	f	1962-07-20	2017-10-0013	f
6570003	Alexandra	Alexia	Jáquez	Andreu	f	1962-10-31	2017-10-0013	f
10256078	Saul	Guillermo	Acuña	Cuellar	m	1960-02-21	2017-10-0013	f
7154102	Sofia	Yaiza	Acuña	Ayala	f	1967-06-18	2017-10-0013	f
8307929	Nahia	Vera	Silva	Carranza	f	1962-02-20	2017-10-0013	f
11721479	Leire	Beatriz	Villaseñor	Benítez	f	1964-09-25	2017-10-0013	f
12493405	Marina	Silvia	Delgado	Ibáñez	f	1960-03-03	2017-10-0013	f
7668303	Antonio	Francisco	Estrada	Samaniego	m	1958-03-12	2017-10-0013	f
12812695	Patricia	Patricia	Marroquín	Vázquez	f	1952-03-23	2017-10-0013	f
11688721	Paola	Nahia	Madrid	Galán	f	1955-10-29	2017-10-0013	f
10902659	Irene	Olivia	Bustamante	Castro	f	1958-05-03	2017-10-0013	f
12734196	Helena	Irene	Ortíz	Sánchez	f	1966-05-10	2017-10-0013	f
6080582	Mara	Luna	Álvarez	Velasco	f	1967-04-12	2017-10-0013	f
6925373	Nayara	Ainara	Benítez	Leyva	f	1955-12-13	2017-10-0013	f
5230424	Marc	Daniel	Rey	Pedroza	m	1963-02-25	2017-10-0013	f
6240226	Jose	Roberto	Piña	Sedillo	m	1967-04-25	2017-10-0013	f
9854445	Noelia	Africa	Ros	Rodrigo	f	1963-03-13	2017-10-0013	f
10678976	Luisa	Salma	Alva	Sáenz	f	1955-02-20	2017-10-0013	f
11312557	Jose	Sergio	Flores	Rivas	m	1959-12-25	2017-10-0013	f
10883911	Iria	Mireia	Holguín	Verduzco	f	1969-05-23	2017-10-0013	f
13961962	Alvaro	Mohamed	Sedillo	Pastor	m	1964-08-05	2017-10-0013	f
8158735	Gabriel	Mateo	Llorente	Fonseca	m	1962-01-08	2017-10-0013	f
14731235	Nil	Carolina	Cintrón	Viera	f	1961-06-24	2017-10-0013	f
11821546	Oliver	Domingo	Duarte	Urías	m	1954-09-15	2017-10-0013	f
10796516	Mario	Francisco	Villa	Montes	m	1960-03-18	2017-10-0013	f
14283845	Julia	Eva	Cabello	Requena	f	1956-05-06	2017-10-0013	f
8863268	Adriana	Diana	Santiago	Trujillo	f	1962-03-31	2017-10-0013	f
10424648	Pau	Claudia	Ríos	Santos	f	1960-09-22	2017-10-0013	f
5709881	Mateo	Martin	Patiño	Limón	m	1958-05-17	2017-10-0013	f
8267389	Martina	Alba	Luevano	Velasco	f	1954-11-06	2017-10-0013	f
12760005	Mireia	Mar	Linares	Hinojosa	f	1953-11-15	2017-10-0013	f
8845775	Nil	Irene	Baca	Tello	f	1964-07-03	2017-10-0013	f
9538212	Juan	Oscar	Merino	Rodarte	m	1955-10-09	2017-10-0013	f
14963194	Ander	Enrique	Sevilla	Razo	m	1955-03-12	2017-10-0013	f
14688849	Jimena	Jana	Nazario	Llamas	f	1956-05-31	2017-10-0013	f
14092083	Nicolas	Guillem	Carmona	Rosas	m	1955-10-03	2017-10-0013	f
7918366	Dario	Pablo	Quintanilla	Sancho	m	1955-05-07	2017-10-0013	f
5104170	Adrian	Marcos	Olivárez	Reyes	m	1962-08-15	2017-10-0013	f
12804068	Alejandro	Alberto	Ozuna	Orozco	m	1963-12-24	2017-10-0013	f
5291509	Claudia	Carla	Antón	Zepeda	f	1954-10-13	2017-10-0013	f
12836713	Leyre	Nerea	Villar	Franco	f	1958-02-03	2017-10-0013	f
9145622	Candela	Nerea	Pulido	Tirado	f	1967-07-29	2017-10-0013	f
8971644	Lucas	Jordi	Rivera	Sarabia	m	1954-06-23	2017-10-0013	f
6194295	Mohamed	Adria	Collado	Segura	m	1959-06-14	2017-10-0013	f
9892553	Abril	Natalia	Andrés	Polanco	f	1956-05-10	2017-10-0013	f
13298094	Leo	Lucas	Garay	Bermejo	m	1952-01-11	2017-10-0013	f
6351426	Eric	Andres	Matías	Soler	m	1967-08-08	2017-10-0013	f
13360013	Cristian	Oliver	Giménez	Hidalgo	m	1964-12-31	2017-10-0013	f
10673293	Pedro	Fernando	Bautista	Esteban	m	1952-11-12	2017-10-0013	f
10062224	Marta	Noa	Lucero	Estrada	f	1952-11-18	2017-10-0013	f
8176710	Marco	Adam	Matos	Deleón	m	1964-11-25	2017-10-0013	f
11581030	Laura	Candela	Figueroa	Ordoñez	f	1964-06-26	2017-10-0013	f
14951101	Aleix	Pablo	Matos	Zamora	m	1967-03-15	2017-10-0013	f
7686987	Ivan	Victor	Luque	Pozo	m	1968-06-02	2017-10-0013	f
6965990	Eric	Jan	Sáenz	Gamez	m	1957-01-03	2017-10-0013	f
5133452	Martina	Noa	Escribano	Valle	f	1966-07-29	2017-10-0013	f
7242301	Leo	Dario	Domínquez	Macias	m	1957-03-28	2017-10-0013	f
10365717	Alex	Eric	Luna	Rael	m	1954-08-05	2017-10-0013	f
8736689	Mario	Manuel	Fernández	Andreu	m	1953-11-28	2017-10-0013	f
6882429	Victor	Hector	Godoy	Pineda	m	1957-01-24	2017-10-0013	f
8166270	Mireia	Daniela	Curiel	Alcala	f	1964-06-25	2017-10-0013	f
8172870	Nayara	Martina	Chavarría	Esquivel	f	1966-11-18	2017-10-0013	f
9971429	Lola	Cristina	Alva	Vaca	f	1959-08-14	2017-10-0013	f
10059552	Adriana	Natalia	Ornelas	Requena	f	1960-11-26	2017-10-0013	f
10818207	Mateo	Rafael	Bahena	Sisneros	m	1969-06-24	2017-10-0013	f
14405810	Alberto	Jesus	Valdés	Villalba	m	1955-07-04	2017-10-0013	f
11886773	Teresa	Abril	Manzano	Suárez	f	1957-07-30	2017-10-0013	f
8750056	Alejandra	Emma	Manzano	Domínguez	f	1957-07-06	2017-10-0013	f
8531836	Marc	Victor	Duran	Ozuna	m	1955-01-24	2017-10-0013	f
10803797	Enrique	Mateo	Menchaca	Tórrez	m	1953-10-20	2017-10-0013	f
6146475	Luis	Lucas	Curiel	Murillo	m	1969-04-02	2017-10-0013	f
7553657	Samuel	Rayan	Carrasco	Fernández	m	1964-02-13	2017-10-0013	f
10870692	Saul	Javier	Domínquez	Caro	m	1958-07-15	2017-10-0013	f
10360849	Ainara	Ana	Cuellar	Leyva	f	1963-07-16	01-01-22-001-0070	f
13948380	Bella	Nerea	Juan	Mojica	f	1969-05-16	01-01-22-001-0070	f
14525149	Mar	Iria	Monroy	Reyna	f	1952-03-26	01-01-22-001-0070	f
14393137	Erika	Silvia	De la cruz	Mascareñas	f	1963-01-20	01-01-22-001-0070	f
7971226	Diego	Izan	Hurtado	Barrera	m	1958-07-22	01-01-22-001-0070	f
14396163	Juan	Dario	Murillo	Martín	m	1963-12-09	01-01-22-001-0070	f
9178163	Guillem	Carlos	Arce	Expósito	m	1961-01-10	01-01-22-001-0070	f
12508075	Alex	Nicolas	Olvera	Serra	m	1958-08-12	01-01-22-001-0070	f
10451188	Nora	Naia	Zavala	Sanz	f	1960-05-04	01-01-22-001-0070	f
10167580	Carla	Manuela	López	Valentín	f	1958-05-03	01-01-22-001-0070	f
6727777	Elsa	Sara	Martín	Andreu	f	1966-06-05	01-01-22-001-0070	f
9568918	Ivan	Ander	Bueno	Barrientos	m	1967-12-08	01-01-22-001-0070	f
13741495	Nuria	Pau	Murillo	Zamudio	f	1953-05-03	01-01-22-001-0070	f
5454507	Sara	Adriana	Villalpando	Ramón	f	1966-08-25	01-01-22-001-0070	f
5123093	Ian	Daniel	Arribas	Chávez	m	1965-11-04	01-01-22-001-0070	f
10727399	Rodrigo	Angel	Colón	Mota	m	1959-12-24	01-01-22-001-0070	f
10152565	Francisco	Jaime	Delrío	Espinosa	m	1968-07-31	01-01-22-001-0070	f
13531855	Angela	Paola	Ulloa	Delarosa	f	1959-12-20	01-01-22-001-0070	f
7420485	Ian	Roberto	Asensio	De la fuente	m	1954-08-24	01-01-22-001-0070	f
11949954	Pedro	Miguel	De la cruz	Campos	m	1965-05-27	01-01-22-001-0070	f
6462692	Nicolas	Martin	Armenta	Beltrán	m	1969-07-25	01-01-22-001-0070	f
6879305	Izan	Marc	Riojas	Henríquez	m	1965-09-06	01-01-22-001-0070	f
14991717	Mario	Jesus	Noriega	Caballero	m	1961-10-31	01-01-22-001-0070	f
10912677	Carla	Daniela	Crespo	Expósito	f	1960-02-14	01-01-22-001-0070	f
12476817	Ander	Rafael	Jaimes	Suárez	m	1961-10-08	01-01-22-001-0070	f
12738688	Saul	Miguel	Rael	Roldan	m	1968-12-25	01-01-22-001-0070	f
9353767	Eduardo	Alejandro	Téllez	Vega	m	1961-10-19	01-01-22-001-0070	f
12163705	Yaiza	Anna	Simón	Jimínez	f	1957-11-23	01-01-22-001-0070	f
7803513	Claudia	Naia	Covarrubias	Cardona	f	1963-07-18	01-01-22-001-0070	f
11293478	Noelia	Manuela	Cuenca	Páez	f	1963-06-26	01-01-22-001-0070	f
11843084	Manuela	Cristina	Atencio	Franco	f	1964-09-15	01-01-22-001-0070	f
11403184	Luisa	Zulay	Ozuna	Chavarría	f	1953-09-18	01-01-22-001-0070	f
14912678	Dario	Cristian	Godínez	Terrazas	m	1952-01-21	01-01-22-001-0070	f
14595405	Anna	Paula	Burgos	Zambrano	f	1956-05-25	01-01-22-001-0070	f
13065093	Leire	Anna	Posada	Cornejo	f	1963-01-16	01-01-22-001-0070	f
9163353	Adrian	Santiago	Curiel	Cerda	m	1954-02-13	01-01-22-001-0070	f
8968514	Anna	Clara	Arellano	Hernández	f	1967-06-21	01-01-22-001-0070	f
6176970	Marcos	Jon	Barrientos	Gálvez	m	1965-10-18	01-01-22-001-0070	f
14711213	Ismael	Cristian	Cornejo	Vaca	m	1952-04-06	01-01-22-001-0070	f
5559678	Gabriel	Bruno	Pardo	Delgado	m	1960-01-20	01-01-22-001-0070	f
12006525	Ismael	Jose	Guajardo	Ruvalcaba	m	1957-11-05	01-01-22-001-0070	f
13091530	Aleix	Marco	Esquivel	Millán	m	1957-12-11	01-01-22-001-0070	f
14435363	Nuria	Rocio	Roca	Cervantes	f	1956-01-22	01-01-22-001-0070	f
12259710	Cesar	Javier	Valle	Lugo	m	1953-02-03	01-01-22-001-0070	f
11752759	Alejandro	Gonzalo	Villalba	Mares	m	1952-03-25	01-01-22-001-0070	f
14144681	Raul	Miguel	Urías	Zamudio	m	1952-07-19	01-01-22-001-0070	f
8698327	Jon	Mateo	Castro	Casares	m	1967-08-16	01-01-22-001-0070	f
8816462	Victoria	Ariadna	Carballo	Sandoval	f	1963-01-21	01-01-22-001-0070	f
14178932	Maria	Natalia	Villalobos	Montes	f	1964-04-21	01-01-22-001-0070	f
12160757	Fernando	Rayan	Velasco	Angulo	m	1956-03-25	01-01-22-001-0070	f
5016482	Adam	Izan	Otero	Mena	m	1952-09-20	01-01-22-001-0070	f
8143472	Miguel	Jose	Tamayo	Tomas	m	1959-02-07	01-01-22-001-0070	f
6872119	Alexia	Mar	Alcala	Colón	f	1967-03-04	01-01-22-001-0070	f
5403471	Francisco	Francisco Javier	Montenegro	Guerra	m	1957-10-03	01-01-22-001-0070	f
11635894	Andrea	Carmen	Sauceda	Uribe	f	1969-11-02	01-01-22-001-0070	f
14931088	Gonzalo	Rodrigo	Olivo	Díaz	m	1957-01-16	01-01-22-001-0070	f
7936294	Alexia	Carolina	Duarte	Iglesias	f	1957-06-05	01-01-22-001-0070	f
8619546	Alba	Ines	Lebrón	Garibay	f	1964-07-17	01-01-22-001-0070	f
10270287	Leo	Isaac	Pacheco	Galarza	m	1955-04-04	01-01-22-001-0070	f
8347507	Rafael	Fernando	Sanches	Collado	m	1952-09-27	01-01-22-001-0070	f
12087741	Ignacio	Leo	Piñeiro	Quesada	m	1968-09-11	01-01-22-001-0070	f
11654551	Celia	Mireia	De la torre	Quiroz	f	1968-04-19	01-01-22-001-0070	f
13161091	Pedro	Erik	Lozada	Véliz	m	1957-07-19	01-01-22-001-0070	f
11400532	Isaac	Jon	Niño	Riojas	m	1963-03-26	01-01-22-001-0070	f
8498971	Sara	Clara	Delapaz	Santillán	f	1968-11-18	01-01-22-001-0070	f
11480351	Andrea	Anna	Tirado	Laboy	f	1954-01-27	01-01-22-001-0070	f
6344176	Marc	Luis	Orosco	Rico	m	1962-02-11	01-01-22-001-0070	f
9100442	Alberto	Santiago	Llorente	Salinas	m	1969-11-07	01-01-22-001-0070	f
5389496	Eduardo	Leo	Oropeza	Escobedo	m	1959-11-16	01-01-22-001-0070	f
13304162	Gabriela	Nayara	Acuña	Giménez	f	1965-02-27	01-01-22-001-0070	f
7677880	Paola	Olivia	Ramos	Santiago	f	1968-06-14	01-01-22-001-0070	f
11791075	Francisco	Fernando	Gracia	Segura	m	1962-02-15	01-01-22-001-0070	f
5058910	Alvaro	Rodrigo	Jaime	Ocasio	m	1966-07-11	01-01-22-001-0070	f
10932400	Joel	Ivan	Samaniego	Valdés	m	1958-02-17	01-01-22-001-0070	f
6291413	Pablo	Leonardo	Altamirano	Cardona	m	1965-05-15	01-01-22-001-0070	f
9807509	Alexandra	Blanca	Quezada	Ontiveros	f	1965-11-30	01-01-22-001-0070	f
12907432	Eduardo	Ander	Pelayo	Esteve	m	1967-10-02	01-01-22-001-0070	f
12955739	Antonio	Marc	Maya	Tijerina	m	1966-10-13	01-01-22-001-0070	f
9862600	Roberto	Lucas	Orozco	Riojas	m	1956-03-18	01-01-22-001-0070	f
12128911	Mohamed	Manuel	Alvarado	Sánchez	m	1958-03-29	01-01-22-001-0070	f
7119760	Oliver	Marco	Pulido	Olivas	m	1968-03-17	01-01-22-001-0070	f
13087274	Sofia	Silvia	Polo	Ozuna	f	1961-04-05	01-01-22-001-0070	f
7392983	Mohamed	Manuel	Narváez	Niño	m	1965-04-21	01-01-22-001-0070	f
13698124	Nadia	Patricia	Escobar	Casanova	f	1959-08-20	01-01-22-001-0070	f
9714026	Jordi	Adrian	Hernádez	Longoria	m	1969-05-11	01-01-22-001-0070	f
10860859	Emma	Alejandra	Rojas	Andreu	f	1967-05-31	01-01-22-001-0070	f
6191247	Diego	Ruben	Zelaya	Chávez	m	1968-10-31	01-01-22-001-0070	f
8518278	Erika	Mar	Partida	Clemente	f	1963-09-18	01-01-22-001-0070	f
7033983	Saul	Victor	Orellana	Marroquín	m	1956-01-29	01-01-22-001-0070	f
10739181	Rafael	Manuel	Lira	Toledo	m	1956-10-10	01-01-22-001-0070	f
11264468	Oswaldo	Santiago	Ordoñez	Puig	m	1967-08-07	01-01-22-001-0070	f
11593482	Nuria	Zulay	Báez	Miramontes	f	1963-01-13	01-01-22-001-0070	f
9414387	Erika	Gabriela	Salazar	Domínguez	f	1969-08-03	01-01-22-001-0070	f
9845667	Ona	Elsa	Ibarra	Garay	f	1957-12-25	01-01-22-001-0070	f
11711200	Olivia	Mar	Ponce	Quiñones	f	1963-06-07	01-01-22-001-0070	f
8135621	Irene	Alicia	Valle	Mayorga	f	1954-10-16	01-01-22-001-0070	f
9103629	Fernando	Rodrigo	Casanova	Luna	m	1957-02-25	01-01-22-001-0070	f
6061918	Luis	Santiago	Vela	Carranza	m	1959-06-23	01-01-22-001-0070	f
14011689	Jesus	Yorman	Casillas	Ybarra	m	1958-09-23	01-01-22-001-0070	f
7987782	Fernando	Diego	Sanz	Domínquez	m	1953-09-03	01-01-22-001-0070	f
6211497	Beatriz	Helena	Vallejo	Mora	f	1955-09-14	01-01-22-001-0070	f
9354646	Eric	Rodrigo	Mesa	Cabrera	m	1963-08-21	01-01-22-001-0070	f
13392315	Martin	Yorman	Regalado	Valdés	m	1967-05-15	01-01-22-001-0070	f
13008370	Gabriela	Raquel	Jaramillo	Orellana	f	1967-12-03	01-01-22-001-0070	f
6578158	Alberto	Alonso	Collado	Valladares	m	1953-04-13	01-01-22-001-0070	f
14707259	Enrique	Jose	Oliver	Contreras	m	1953-03-06	01-01-22-001-0070	f
6975194	Vera	Teresa	Ibarra	Apodaca	f	1952-05-14	01-01-22-001-0070	f
10658201	Lucia	Marina	Montemayor	Ramírez	f	1961-03-29	01-01-22-001-0070	f
9958763	Daniel	Sebastian	Cobo	Montañez	m	1958-03-30	01-01-22-001-0070	f
14008368	Santiago	David	Macías	Delrío	m	1952-06-16	01-01-22-001-0070	f
7478695	Cesar	Miguel	Yáñez	Adorno	m	1969-06-28	01-01-22-001-0070	f
13308478	Noa	Patricia	Altamirano	Luque	f	1958-04-24	01-01-22-001-0070	f
10335815	Raquel	Angela	Carbajal	Calvo	f	1967-11-02	01-01-22-001-0070	f
13394963	Manuel	Nicolas	Zepeda	Simón	m	1958-03-10	01-01-22-001-0070	f
9882124	Mario	Martin	Agosto	Simón	m	1961-08-06	01-01-22-001-0070	f
9540107	Angel	Oliver	Puga	Esparza	m	1954-04-03	01-01-22-001-0070	f
8893699	Mireia	Carlota	Rojo	Nevárez	f	1960-08-20	01-01-22-001-0070	f
5682095	Enrique	Rafael	Castellano	Narváez	m	1953-10-22	01-01-22-001-0070	f
12584032	Marta	Lara	Santos	Serra	f	1951-11-15	01-01-22-001-0070	f
12037451	Angela	Carla	Aragón	Cazares	f	1966-11-10	01-01-22-001-0070	f
12519777	Carmen	Luisa	Delacrúz	Otero	f	1960-08-17	01-01-22-001-0070	f
9600685	Naiara	Victoria	Navarrete	Segura	f	1966-08-11	01-01-22-001-0070	f
11031430	Leire	Jana	Collado	Barreto	f	1958-01-04	01-01-22-001-0070	f
9746882	Nuria	Sofia	Luque	Valero	f	1956-05-21	01-01-22-001-0070	f
12875145	Ariadna	Ines	Niño	Bueno	f	1960-08-08	01-01-22-001-0070	f
12765305	Oswaldo	Joel	Portillo	Lucio	m	1969-10-05	01-01-22-001-0070	f
6832484	Antonia	Manuela	Padrón	Acosta	f	1967-08-09	01-01-22-001-0070	f
7954158	Nil	Helena	Adorno	Quiñones	f	1969-10-28	01-01-22-001-0070	f
14587075	Nahia	Rocio	Santacruz	Nájera	f	1961-03-06	01-01-22-001-0070	f
14605080	Yorman	Mario	Santillán	Matos	m	1954-05-17	01-01-22-001-0070	f
6816832	Miguel	Ruben	Acosta	Rodríquez	m	1962-09-08	01-01-22-001-0070	f
9927485	Leyre	Fatima	Rolón	Urbina	f	1962-05-08	01-01-22-001-0070	f
6117550	Manuel	Miguel	Piña	Murillo	m	1956-09-17	01-01-22-001-0070	f
14301011	Enrique	Jordi	Nieto	Manzanares	m	1961-11-23	01-01-22-001-0070	f
11627855	Julia	Martina	Anguiano	Urrutia	f	1964-07-14	01-01-22-001-0070	f
12316243	Oliver	Omar	Pantoja	Cortez	m	1969-10-13	01-01-22-001-0070	f
13858776	Jan	Lucas	Trujillo	Perea	m	1953-08-04	01-01-22-001-0070	f
6974787	Patricia	Helena	Galindo	Ruiz	f	1954-04-25	01-01-22-001-0070	f
14761763	Africa	Carolina	Moral	Toledo	f	1953-08-08	01-01-22-001-0070	f
9728241	Jaime	Ignacio	Cordero	Piña	m	1956-01-12	01-01-22-001-0070	f
14807018	Mario	Jose	Sanches	Serrato	m	1963-03-07	01-01-22-001-0070	f
12455119	Miriam	Lidia	Mascareñas	Sola	f	1963-09-16	01-01-22-001-0070	f
10119923	Martin	Samuel	Delrío	Verduzco	m	1968-11-26	01-01-22-001-0070	f
5220381	Sandra	Marta	Bañuelos	Arribas	f	1957-01-10	01-01-22-001-0070	f
9518208	Jon	Marc	Rentería	Moran	m	1957-07-25	01-01-22-001-0070	f
8882629	Ona	Miriam	Gallego	Barraza	f	1958-01-28	01-01-22-001-0070	f
7543305	Manuela	Carlota	Tejeda	Mateo	f	1969-10-17	01-01-22-001-0070	f
7645787	Olivia	Sara	Zelaya	Limón	f	1966-09-04	01-01-22-001-0070	f
8769801	Eduardo	Aaron	Saavedra	Noriega	m	1952-03-20	01-01-22-001-0070	f
10809441	Ian	Hector	Alonso	Gaitán	m	1957-01-23	01-01-22-001-0070	f
10872132	Abril	Paula	Ontiveros	Escobar	f	1964-01-02	01-01-22-001-0070	f
6859302	Nadia	Noa	Marrero	Montalvo	f	1962-08-26	01-01-22-001-0070	f
8337710	Luna	Marina	Córdova	Heredia	f	1953-03-05	01-01-22-001-0070	f
10595733	Lidia	Lara	Vicente	Aguilar	f	1961-07-27	01-01-22-001-0070	f
11860494	Adam	Jesus	Holguín	Sánchez	m	1953-07-23	01-01-22-001-0070	f
7852001	Enrique	Jorge	Toro	Fierro	m	1969-07-03	01-01-22-001-0070	f
14156892	Guillermo	Marc	Nájera	Pereira	m	1957-04-27	01-01-22-001-0070	f
5749475	Erik	Pablo	Crespo	Mayorga	m	1960-08-23	01-01-22-001-0070	f
9931295	Jose	Mateo	Bautista	Plaza	m	1965-07-09	01-01-22-001-0070	f
8782423	Mohamed	Sergio	Rodrigo	Caldera	m	1958-08-14	01-01-22-001-0070	f
13783510	Maria	Elsa	Barela	Cuenca	f	1964-11-03	01-01-22-001-0070	f
11409955	Clara	Lidia	Hernandes	Villar	f	1951-12-25	01-01-22-001-0070	f
7444661	Martin	Oliver	Mata	Rueda	m	1952-04-25	01-01-22-001-0070	f
14366188	Sergio	Izan	Puente	Razo	m	1957-12-16	01-01-22-001-0070	f
6033186	Adam	Alvaro	Olivas	Montoya	m	1969-03-01	01-01-22-001-0070	f
10717641	Carlos	Jon	Espinosa	Olivo	m	1957-10-05	01-01-22-001-0070	f
7462475	Lidia	Vega	Cazares	Benavídez	f	1955-06-01	01-01-22-001-0070	f
14787525	Africa	Victoria	Santacruz	Caldera	f	1960-12-07	01-01-22-001-0070	f
5813526	Andrea	Raquel	Castellano	Carrasco	f	1964-11-05	01-01-22-001-0070	f
6903513	Fernando	Oswaldo	Cadena	Báez	m	1965-08-25	01-01-22-001-0070	f
8695993	Diego	Leo	Arguello	Tafoya	m	1958-04-05	01-01-22-001-0070	f
6900471	Lola	Blanca	Gálvez	Reyes	f	1962-10-25	01-01-22-001-0070	f
12115147	Leyre	Noelia	Verdugo	Molina	f	1959-11-19	01-01-22-001-0070	f
5910150	Alex	Miguel	Orellana	Bétancourt	m	1956-03-13	01-01-22-001-0070	f
6938895	Joel	Carlos	Ureña	Meléndez	m	1966-11-23	01-01-22-001-0070	f
14088591	Carlos	Mario	Juan	Alcántar	m	1953-11-25	01-01-22-001-0070	f
11114569	Alejandro	Eric	Caballero	Aragón	m	1966-03-30	01-01-22-001-0070	f
10725405	Gonzalo	Aleix	Yáñez	Font	m	1967-09-17	01-01-22-001-0070	f
5974148	Abril	Joan	Soler	Galán	f	1953-11-25	01-01-22-001-0070	f
8776843	Marti	Marti	Redondo	Paredes	f	1966-03-11	01-01-22-001-0070	f
6961276	Ander	Ian	Vela	Gastélum	m	1966-06-07	01-01-22-001-0070	f
14231911	Nerea	Salma	Ponce	Matos	f	1962-10-31	01-01-22-001-0070	f
10364733	Adam	Javier	Cardenas	Batista	m	1964-06-25	01-01-22-001-0070	f
14430140	Claudia	Angela	Soria	Matías	f	1969-02-23	01-01-22-001-0070	f
8709150	Carla	Natalia	Velasco	Naranjo	f	1966-08-21	01-01-22-001-0070	f
6982045	Bella	Yaiza	Zarate	Domínguez	f	1955-03-16	01-01-22-001-0070	f
14594365	Luna	Paula	Lucio	Nazario	f	1954-07-18	01-01-22-001-0070	f
9618370	Leandro	Cesar	Cuevas	Velasco	m	1952-03-23	01-01-22-001-0070	f
5876706	Pablo	Leo	Luna	Gallegos	m	1965-03-20	01-01-22-001-0070	f
6768058	Carolina	Patricia	Carbajal	Requena	f	1963-06-12	01-01-22-001-0070	f
14165932	Elsa	Patricia	Pineda	Salazar	f	1953-01-11	01-01-22-001-0070	f
7983595	Julia	Naia	Rincón	Alaniz	f	1953-10-20	01-01-22-001-0070	f
6279143	Naiara	Zulay	Lomeli	Villaseñor	f	1953-02-04	01-01-22-001-0070	f
9599223	Clara	Alma	Cazares	Lozano	f	1959-02-09	01-01-22-001-0070	f
11141397	Luna	Isabel	Pulido	Alonzo	f	1957-05-14	01-01-22-001-0070	f
14922249	Aaron	Pedro	Heredia	Maya	m	1953-03-31	01-01-22-001-0070	f
13718766	Valentina	Antonia	Aranda	Abeyta	f	1966-12-24	01-01-22-001-0070	f
11348358	Eva	Diana	Lázaro	Olivo	f	1957-09-14	01-01-22-001-0070	f
9791585	Clara	Mara	Aponte	Domingo	f	1968-02-20	01-01-22-001-0070	f
13669511	Miriam	Mar	Olvera	Arreola	f	1962-05-23	01-01-22-001-0070	f
8315517	Omar	Diego	Banda	Delagarza	m	1955-08-06	01-01-22-001-0070	f
10627930	Marina	Nora	Romero	Linares	f	1956-06-21	01-01-22-001-0070	f
11999987	Sofia	Ines	Borrego	Alfaro	f	1963-11-28	01-01-22-001-0070	f
10034058	Claudia	Manuela	Abad	Rosario	f	1961-01-14	01-01-22-001-0070	f
7291294	Diego	Santiago	Loera	Deleón	m	1962-10-25	01-01-22-001-0070	f
12088576	Ariadna	Ariadna	Escobedo	Segura	f	1963-06-24	01-01-22-001-0070	f
14486872	Naiara	Nora	Pacheco	Roca	f	1956-09-26	01-01-22-001-0070	f
12107647	Pol	Rafael	Méndez	Casillas	m	1953-04-01	01-01-22-001-0070	f
14984294	Aroa	Africa	Frías	Alejandro	f	1968-03-09	01-01-22-001-0070	f
6555728	Diego	Francisco Javier	Casillas	Alfonso	m	1956-06-11	01-01-22-001-0070	f
10246043	Raquel	Mara	Lovato	Escobedo	f	1957-12-20	01-01-22-001-0070	f
11944489	Sara	Ane	Galindo	Galarza	f	1956-03-20	01-01-22-001-0070	f
13074316	Pol	Cesar	Zaragoza	Padilla	m	1963-01-02	01-01-22-001-0070	f
5241893	Elsa	Patricia	Santana	Valentín	f	1956-05-20	01-01-22-001-0070	f
13031970	Guillem	Manuel	Madrigal	Santamaría	m	1968-12-27	01-01-22-001-0070	f
10707434	Jesus	Aleix	Reséndez	Ortega	m	1964-10-13	01-01-22-001-0070	f
14454098	Elsa	Antonia	Rosas	Salvador	f	1954-12-04	01-01-22-001-0070	f
9890182	Alejandra	Erika	Aparicio	Hernández	f	1955-02-04	01-01-22-001-0070	f
8267032	Victor	Manuel	Aguilera	Figueroa	m	1969-10-17	01-01-22-001-0070	f
14753168	Miguel	Daniel	Luis	Quesada	m	1958-08-25	01-01-22-001-0070	f
7405694	Maria	Marta	Andreu	Fierro	f	1969-05-28	01-01-22-001-0070	f
13824866	Dario	Adrian	Robledo	Hernádez	m	1958-11-13	01-01-22-001-0070	f
13866008	Jaime	Santiago	Carballo	Martos	m	1966-06-29	01-01-22-001-0070	f
13895770	Jan	Ignacio	Maldonado	Roca	m	1963-07-22	01-01-22-001-0070	f
7516462	Adriana	Natalia	Cisneros	Casanova	f	1958-07-08	01-01-22-001-0070	f
12934197	Cesar	Diego	Marín	Agosto	m	1954-03-24	01-01-22-001-0070	f
12991085	Julia	Anna	Ruiz	Duran	f	1958-11-25	01-01-22-001-0070	f
6761834	Jon	Bruno	Delarosa	Franco	m	1964-10-15	01-01-22-001-0070	f
6604825	Jose	Fernando	Esteve	Serrato	m	1954-03-24	01-01-22-001-0070	f
12298125	Mar	Noelia	Ibarra	Alvarado	f	1952-08-16	01-01-22-001-0070	f
14843213	Ruben	Rayan	Parra	Santiago	m	1955-01-29	01-01-22-001-0070	f
9256691	Beatriz	Mara	Tijerina	De la torre	f	1969-11-18	01-01-22-001-0070	f
13727530	Alexia	Nuria	Méndez	Madrid	f	1956-07-19	01-01-22-001-0070	f
7357836	Adriana	Lucia	Carreón	Salcedo	f	1961-12-14	01-01-22-001-0070	f
14958037	Aroa	Ane	Mares	Pérez	f	1958-06-27	01-01-22-001-0070	f
13493211	Ivan	Izan	Salcedo	Arroyo	m	1966-05-14	01-01-22-001-0070	f
7801841	Marta	Luna	Caldera	Molina	f	1966-03-02	01-01-22-001-0070	f
9283533	Clara	Carla	Zambrano	Portillo	f	1965-09-05	01-01-22-001-0070	f
7710839	Dario	Leonardo	Nevárez	Abad	m	1966-12-17	01-01-22-001-0070	f
12921635	Marc	Yorman	Rincón	Cerda	m	1958-08-21	01-01-22-001-0070	f
12639394	Hector	Alejandro	Gaytán	Valero	m	1966-09-12	01-01-22-001-0070	f
13895190	Valeria	Iria	Zayas	Valladares	f	1968-08-21	01-01-22-001-0070	f
8065167	Teresa	Sandra	Barela	Zarate	f	1960-08-22	01-01-22-001-0070	f
14701764	Beatriz	Mireia	Sevilla	Tamez	f	1966-11-17	01-01-22-001-0070	f
12421762	Jesus	Andres	Cotto	Téllez	m	1960-05-21	01-01-22-001-0070	f
7794582	Ana	Nahia	Heredia	Solorio	f	1963-04-10	01-01-22-001-0070	f
6687958	Laura	Naia	Montañez	Gómez	f	1956-04-02	01-01-22-001-0070	f
12666090	Pau	Cristina	Muñoz	Espinal	f	1966-06-06	01-01-22-001-0070	f
6321814	Mar	Noa	Ontiveros	Aguilar	f	1961-05-25	01-01-22-001-0070	f
12258212	Luis	Leo	Martín	Rodarte	m	1963-03-09	01-01-22-001-0070	f
10961291	Valentina	Luna	Font	Soliz	f	1955-10-19	01-01-22-001-0070	f
6861414	Alicia	Manuela	Puente	Armijo	f	1963-07-18	01-01-22-001-0070	f
8360538	Nerea	Nayara	Roldan	Villanueva	f	1966-08-18	01-01-22-001-0070	f
9065075	Maria	Carlota	Moral	Matías	f	1957-10-02	01-01-22-001-0070	f
12480863	Dario	Adria	Valverde	Roybal	m	1966-08-08	01-01-22-001-0070	f
9519994	Eva	Naiara	Muñoz	Alfaro	f	1951-12-26	01-01-22-001-0070	f
12870128	Sara	Lola	Saldivar	Colón	f	1969-11-12	01-01-22-001-0070	f
9001366	Isaac	Guillermo	Córdova	Mares	m	1955-12-14	01-01-22-001-0070	f
8017390	Bruno	Enrique	Menéndez	Casárez	m	1960-01-07	01-01-22-001-0070	f
7660422	Alexia	Sandra	Vela	Garrido	f	1962-04-03	01-01-22-001-0070	f
9094103	Gabriela	Nahia	Santamaría	León	f	1954-10-17	01-01-22-001-0070	f
14246537	Carolina	Miriam	Navarrete	Alonso	f	1963-09-27	01-01-22-001-0070	f
5393205	Alberto	Andres	Acuña	Garza	m	1969-04-23	01-01-22-001-0070	f
12714022	Jose	Mateo	Jiménez	Godoy	m	1959-03-06	01-01-22-001-0070	f
9290362	Domingo	Carlos	Suárez	Véliz	m	1957-10-11	01-01-22-001-0070	f
9142201	Laia	Marta	Escalante	Reynoso	f	1955-11-20	01-01-22-001-0070	f
10867026	Alba	Sandra	Galarza	Curiel	f	1964-08-17	01-01-22-001-0070	f
6420756	Irene	Alicia	Zamora	Chapa	f	1966-05-06	01-01-22-001-0070	f
11880710	Pol	Eric	Rojas	Esquivel	m	1954-05-06	01-01-22-001-0070	f
8677880	Bruno	Aleix	Riera	Pastor	m	1967-08-24	01-01-22-001-0070	f
12661303	Rocio	Erika	Parra	Viera	f	1969-07-01	01-01-22-001-0070	f
11844214	Guillem	Fernando	Alcaraz	Valverde	m	1957-12-13	01-01-22-001-0070	f
5962778	Adriana	Ariadna	Castellanos	Barrientos	f	1966-08-31	01-01-22-001-0070	f
8112948	Andres	Marcos	Pedroza	Cardenas	m	1966-10-03	01-01-22-001-0070	f
8806889	Adrian	Eduardo	Sáenz	Ochoa	m	1960-09-25	01-01-22-001-0070	f
13419060	Sebastian	Victor	Nava	Abreu	m	1960-04-24	01-01-22-001-0070	f
6221692	Nerea	Adriana	Soriano	Esquivel	f	1966-07-31	01-01-22-001-0070	f
12109441	Enrique	Jesus	Meza	Venegas	m	1960-09-15	01-01-22-001-0070	f
6119405	Adam	Adrian	Garibay	Arroyo	m	1958-01-01	01-01-22-001-0070	f
5344756	Manuela	Nerea	Blázquez	Nieves	f	1958-07-26	01-01-22-0001-0027	f
5736577	Daniela	Aroa	Arreola	Monroy	f	1959-02-12	01-01-22-0001-0027	f
10547812	Mario	Lucas	Miranda	Valero	m	1952-02-10	01-01-22-0001-0027	f
10681385	Dario	Daniel	Águilar	Solorzano	m	1959-09-18	01-01-22-0001-0027	f
11177546	Olivia	Lara	Mojica	Plaza	f	1969-01-28	01-01-22-0001-0027	f
6982848	Cesar	Ismael	Villagómez	Apodaca	m	1963-11-21	01-01-22-0001-0027	f
10951678	Antonia	Andrea	Rentería	Chacón	f	1964-04-28	01-01-22-0001-0027	f
5402333	Miguel	Alejandro	Benítez	Lomeli	m	1963-05-12	01-01-22-0001-0027	f
6031601	Ane	Ane	Zavala	Jáquez	f	1956-09-10	01-01-22-0001-0027	f
7149263	Nicolas	Rayan	Orozco	Bustos	m	1963-12-07	01-01-22-0001-0027	f
6941281	Saul	Yorman	Sanz	Anaya	m	1966-08-27	01-01-22-0001-0027	f
7676447	Jaime	Guillermo	Rodríguez	Olivares	m	1968-06-27	01-01-22-0001-0027	f
11971714	Valentina	Abril	Becerra	Valladares	f	1955-04-27	01-01-22-0001-0027	f
10556441	Lara	Sara	Manzanares	Páez	f	1966-02-20	01-01-22-0001-0027	f
12115029	Ainara	Valeria	Cervantes	Santamaría	f	1958-11-12	01-01-22-0001-0027	f
12663066	Carolina	Aroa	Valles	Colunga	f	1957-10-09	01-01-22-0001-0027	f
12090672	Noelia	Gabriela	Antón	Portillo	f	1966-12-21	01-01-22-0001-0027	f
11775508	Mar	Leyre	Navarro	Espinoza	f	1954-03-02	01-01-22-0001-0027	f
8005247	Beatriz	Alma	Riera	Olivárez	f	1953-06-03	01-01-22-0001-0027	f
11216859	Alexandra	Nahia	Urbina	Botello	f	1958-05-26	01-01-22-0001-0027	f
10079554	Rodrigo	Marc	Ocampo	Loya	m	1964-07-08	01-01-22-0001-0027	f
11900986	Guillermo	Eduardo	Arguello	Caldera	m	1952-08-16	01-01-22-0001-0027	f
5006092	Maria	Iria	Sanz	Solorzano	f	1954-08-26	01-01-22-0001-0027	f
11995543	Pol	Domingo	Meza	Bernal	m	1960-03-23	01-01-22-0001-0027	f
9131777	Blanca	Manuela	Segovia	Hinojosa	f	1956-01-08	01-01-22-0001-0027	f
12523000	Manuela	Berta	Padrón	Pabón	f	1965-05-25	01-01-22-0001-0027	f
13740088	Patricia	Yaiza	Tapia	Portillo	f	1958-08-18	01-01-22-0001-0027	f
13099283	Luis	Juan	Calero	Macias	m	1953-05-14	01-01-22-0001-0027	f
8032197	Francisco Javier	Alejandro	Godoy	Cuesta	m	1960-06-02	01-01-22-0001-0027	f
8013885	Ana	Natalia	Molina	Sarabia	f	1958-07-30	01-01-22-0001-0027	f
13684433	Luna	Andrea	Lomeli	Santamaría	f	1966-03-30	01-01-22-0001-0027	f
14530481	Clara	Valeria	Colunga	Ibáñez	f	1964-10-27	01-01-22-0001-0027	f
7708432	Enrique	Oscar	Manzanares	Gaona	m	1962-02-27	01-01-22-0001-0027	f
7590582	Leyre	Mar	Villareal	Narváez	f	1954-09-16	01-01-22-0001-0027	f
12280017	Guillem	Eric	Romero	Sauceda	m	1965-04-01	01-01-22-0001-0027	f
9059393	Mireia	Nerea	Navas	Santiago	f	1957-11-14	01-01-22-0001-0027	f
10418603	Vega	Africa	Espinoza	López	f	1959-05-13	01-01-22-0001-0027	f
14386693	Emma	Zulay	Esparza	Ros	f	1967-04-08	01-01-22-0001-0027	f
10663583	Omar	Yorman	Vila	Sanz	m	1968-06-05	01-01-22-0001-0027	f
14775960	Aroa	Leire	Salas	Nava	f	1958-11-01	01-01-22-0001-0027	f
13095598	Alba	Irene	Ponce	Yáñez	f	1965-10-24	01-01-22-0001-0027	f
13460024	Cristian	Eduardo	Ibarra	Cortés	m	1961-10-03	01-01-22-0001-0027	f
7010100	Ismael	Guillem	Córdoba	Sevilla	m	1966-07-01	01-01-22-0001-0027	f
12673103	Helena	Alexandra	Jasso	Cuesta	f	1954-11-19	01-01-22-0001-0027	f
7630121	Mara	Nadia	Godínez	Rodrígez	f	1954-03-27	01-01-22-0001-0027	f
5713798	Carolina	Africa	Urbina	Cuellar	f	1964-08-14	01-01-22-0001-0027	f
10579142	Noelia	Yaiza	Rangel	Carbonell	f	1952-08-03	01-01-22-0001-0027	f
11580780	Erik	Pedro	Salinas	Lucero	m	1959-07-23	01-01-22-0001-0027	f
11606505	Laura	Elena	Samaniego	Araña	f	1963-08-20	01-01-22-0001-0027	f
6845145	Diana	Leyre	Varela	Avilés	f	1955-04-03	01-01-22-0001-0027	f
11813917	Hector	Rafael	Moreno	Ordóñez	m	1957-04-22	01-01-22-0001-0027	f
8995119	Pedro	Isaac	Muñoz	Galindo	m	1958-07-07	01-01-22-0001-0027	f
9320812	Martina	Valentina	Mares	Rubio	f	1955-01-03	01-01-22-0001-0027	f
10996898	Pedro	Hector	Ornelas	Flores	m	1955-12-20	01-01-22-0001-0027	f
12762111	Jon	Francisco Javier	Valencia	Ramón	m	1963-01-13	01-01-22-0001-0027	f
11790823	Cristina	Raquel	Gómez	Montoya	f	1969-11-30	01-01-22-0001-0027	f
11076753	Sandra	Valeria	Miguel	Benito	f	1959-05-29	01-01-22-0001-0027	f
7877619	Alexia	Emma	Arias	Ocasio	f	1965-04-08	01-01-22-0001-0027	f
13267712	Blanca	Marina	Valadez	Rodrigo	f	1954-10-31	01-01-22-0001-0027	f
8879055	Ariadna	Miriam	Alcántar	Rodríquez	f	1958-02-02	01-01-22-0001-0027	f
5738402	Celia	Alma	Villaseñor	Sepúlveda	f	1969-09-23	01-01-22-0001-0027	f
11675758	Alberto	Raul	Serna	Aguilera	m	1962-09-09	01-01-22-0001-0027	f
11755832	Eduardo	Manuel	Valentín	Sisneros	m	1953-06-06	01-01-22-0001-0027	f
11752098	Martina	Candela	Velasco	Ochoa	f	1958-06-05	01-01-22-0001-0027	f
7063683	Rocio	Blanca	Mas	Santamaría	f	1962-01-01	01-01-22-0001-0027	f
11149595	Marta	Ona	Meléndez	Marín	f	1962-03-14	01-01-22-0001-0027	f
11293119	Yaiza	Elsa	Torres	Gaona	f	1965-02-08	01-01-22-0001-0027	f
5038090	Victor	Erik	Carrión	Caballero	m	1961-01-17	01-01-22-0001-0027	f
9842299	Pau	Lucia	Rael	Tovar	f	1965-10-08	01-01-22-0001-0027	f
14325172	Adam	Gabriel	Rodríquez	Verduzco	m	1959-08-11	01-01-22-0001-0027	f
5642061	Noa	Olivia	Luján	Andrés	f	1962-06-12	01-01-22-0001-0027	f
9200782	Paola	Noa	Bermejo	Zambrano	f	1967-11-22	01-01-22-0001-0027	f
10570392	Irene	Cristina	Cadena	Delgado	f	1955-09-30	01-01-22-0001-0027	f
9193270	Diego	Alonso	Santana	Rivero	m	1956-04-26	01-01-22-0001-0027	f
6845356	Aaron	Cristian	Garza	Rivas	m	1969-08-04	01-01-22-0001-0027	f
6385753	Diana	Raquel	Mora	Castañeda	f	1964-02-17	01-01-22-0001-0027	f
5361408	Diana	Noa	Almonte	Carrera	f	1964-04-05	01-01-22-0001-0027	f
9505701	Leire	Jimena	Toledo	Navarro	f	1959-11-01	01-01-22-001-0070	f
9119540	Marina	Lidia	Altamirano	Lorente	f	1959-02-14	01-01-22-001-0070	f
8953144	Sofia	Naia	Pascual	Espinoza	f	1966-05-25	01-01-22-001-0070	f
5480860	Alvaro	Dario	Portillo	Torres	m	1969-05-07	01-01-22-001-0070	f
6935088	Olivia	Nadia	Holguín	Castro	f	1962-08-30	01-01-22-001-0070	f
8081923	Diego	Adrian	Montañez	Quiñónez	m	1969-02-02	01-01-22-001-0070	f
6563056	Rodrigo	Jon	Rubio	Salgado	m	1963-02-08	01-01-22-001-0070	f
9701835	Francisco Javier	Mario	Delatorre	Cobo	m	1962-09-17	01-01-22-001-0070	f
7573848	Roberto	Jordi	Oliver	Orta	m	1965-06-10	01-01-22-001-0070	f
14730175	Luna	Salma	Hinojosa	Soriano	f	1958-07-08	01-01-22-001-0070	f
5023768	Antonio	Leo	Arribas	Rangel	m	1954-03-31	01-01-22-001-0070	f
7940041	Irene	Gabriela	Valle	Valentín	f	1952-05-10	01-01-22-0001-0027	f
9925362	Omar	Leandro	Alicea	Cortes	m	1953-11-20	01-01-22-0001-0027	f
12211885	Martina	Vega	Gaytán	Montoya	f	1961-09-06	01-01-22-0001-0027	f
11929210	Jordi	Ruben	Mares	Moreno	m	1965-09-12	01-01-22-0001-0027	f
8231786	Pau	Iria	Esquibel	Guerrero	f	1968-05-06	01-01-22-0001-0027	f
8512011	Ines	Elena	Guajardo	Pichardo	f	1955-03-13	01-01-22-0001-0027	f
13113427	Miguel	Carlos	Luevano	Rentería	m	1954-09-12	01-01-22-0001-0027	f
5519607	Alex	Andres	Delgado	Concepción	m	1968-08-09	01-01-22-0001-0027	f
8162226	Africa	Raquel	Villar	Cano	f	1963-09-22	01-01-22-0001-0027	f
6134099	Lara	Paola	Ureña	Rey	f	1965-06-06	01-01-22-0001-0027	f
7708881	Joel	Juan	Badillo	Escalante	m	1968-11-24	01-01-22-0001-0027	f
7741286	Diana	Nahia	Valverde	Benavides	f	1969-08-27	01-01-22-0001-0027	f
13555679	Nadia	Aroa	Alemán	Leyva	f	1954-05-10	01-01-22-0001-0027	f
12988878	Alonso	Marcos	Parra	Fernández	m	1966-03-05	01-01-22-0001-0027	f
7402317	Eric	Aaron	Cabello	Ros	m	1966-12-25	01-01-22-0001-0027	f
11654737	Roberto	Dario	Rodrígez	Báez	m	1960-04-13	01-01-22-0001-0027	f
8009610	Emma	Rocio	Roybal	Aguilar	f	1966-07-14	01-01-22-0001-0027	f
9061331	Samuel	Gabriel	Echevarría	Blázquez	m	1959-05-18	01-01-22-0001-0027	f
7573425	Alejandra	Lucia	Galindo	Sauceda	f	1967-09-28	01-01-22-0001-0027	f
10426612	Leyre	Marti	Zepeda	Regalado	f	1954-04-14	01-01-22-0001-0027	f
10752400	Jon	Adam	Arribas	Luque	m	1957-05-18	01-01-22-0001-0027	f
14709134	Nora	Nadia	Sotelo	Lerma	f	1968-11-21	01-01-22-0001-0027	f
11881426	David	Samuel	Vaca	Collado	m	1953-02-20	01-01-22-0001-0027	f
10777208	Jordi	Oscar	Cuellar	Delrío	m	1963-10-09	01-01-22-0001-0027	f
6898393	Marc	Guillem	Cepeda	Sarabia	m	1964-04-23	01-01-22-0001-0027	f
11404244	Jan	Miguel	Valdés	Espinal	m	1954-04-30	01-01-22-0001-0027	f
12106952	Miguel	Joel	Arteaga	Carbajal	m	1954-11-11	01-01-22-0001-0027	f
6584553	Rafael	Sergio	Arias	Vélez	m	1966-03-30	01-01-22-0001-0027	f
9286050	Africa	Mara	Porras	Sanabria	f	1956-04-06	01-01-22-0001-0027	f
12032595	Jaime	Dario	Maya	Montaño	m	1968-05-04	01-01-22-0001-0027	f
7275649	Beatriz	Carla	Salgado	Del río	f	1964-08-07	01-01-22-0001-0027	f
8331532	Leyre	Erika	Vela	Gutiérrez	f	1959-06-06	01-01-22-0001-0027	f
7408422	Leandro	Leandro	Cardenas	Guerra	m	1958-06-09	01-01-22-0001-0027	f
5950567	Nicolas	Bruno	Esquivel	Urrutia	m	1969-04-01	01-01-22-0001-0027	f
6649730	Abril	Nerea	Nieves	Solano	f	1965-01-12	01-01-22-0001-0027	f
10159849	Dario	Raul	Aparicio	Molina	m	1961-12-16	01-01-22-0001-0027	f
11244197	Marta	Aroa	Marroquín	Manzanares	f	1957-05-11	01-01-22-0001-0027	f
11322364	Mohamed	Alex	Longoria	Olivares	m	1960-09-16	01-01-22-0001-0027	f
13213125	Gabriel	Domingo	De Jesús	Viera	m	1958-04-09	01-01-22-0001-0027	f
11377992	Ian	Nicolas	Santacruz	Cuellar	m	1954-11-17	2017-10-0013	f
5822541	Jan	Ruben	Delarosa	Domingo	m	1960-09-10	2017-10-0013	f
14458325	Jana	Valentina	Ledesma	Quintero	f	1964-12-27	2017-10-0013	f
12950934	Luis	Oswaldo	Tejada	Cardona	m	1968-05-25	2017-10-0013	f
6351593	Andres	Leonardo	Villar	Santacruz	m	1962-11-16	2017-10-0013	f
6219883	Miguel	Nicolas	Dávila	Cuellar	m	1953-09-05	2017-10-0013	f
14427917	Mara	Mireia	Domínguez	Barragán	f	1961-11-30	2017-10-0013	f
10384844	Miriam	Leire	Bétancourt	Carrera	f	1956-03-15	2017-10-0013	f
7365171	Lucas	Bruno	Valdez	Otero	m	1952-06-22	2017-10-0013	f
6699083	Enrique	Daniel	Venegas	Rendón	m	1960-07-02	2017-10-0013	f
7758310	Leonardo	Javier	Delrío	Castañeda	m	1952-01-03	2017-10-0013	f
7731264	Roberto	Hugo	Zúñiga	Calvillo	m	1962-07-11	2017-10-0013	f
6506024	Martin	Javier	Navarro	Trejo	m	1957-07-04	2017-10-0013	f
12836754	Victor	Mario	Hernandes	Muñóz	m	1953-02-11	2017-10-0013	f
7117544	Blanca	Fatima	Águilar	Villalba	f	1960-10-06	2017-10-0013	f
12503570	Hugo	Mario	Agosto	Corrales	m	1967-06-03	2017-10-0013	f
5277172	Antonio	Juan	Ballesteros	Gallegos	m	1966-04-30	2017-10-0013	f
8535995	Hugo	Juan	Ontiveros	Blasco	m	1960-06-11	2017-10-0013	f
14827016	Gonzalo	Carlos	Toro	Palomo	m	1969-08-17	2017-10-0013	f
11749205	Zulay	Rocio	Expósito	Candelaria	f	1963-03-30	2017-10-0013	f
13741394	Gonzalo	David	Salcido	García	m	1958-02-03	2017-10-0013	f
8632660	Lucas	Jorge	Puente	Téllez	m	1957-05-31	2017-10-0013	f
10402737	Victoria	Olivia	Bustos	Valenzuela	f	1959-08-29	2017-10-0013	f
14114878	Lidia	Alicia	Herrero	Calderón	f	1956-12-17	01-01-22-001-0070	f
14336069	Miguel	Domingo	Solano	Santiago	m	1960-08-08	01-01-22-001-0070	f
6233671	Victor	Oliver	Mata	Oquendo	m	1967-02-16	01-01-22-001-0070	f
13505970	Francisco	Alejandro	Gaona	Barrios	m	1964-07-07	01-01-22-001-0070	f
11250045	Ian	Jorge	Costa	Moya	m	1958-04-02	01-01-22-001-0070	f
5602094	Gabriela	Ana	Palomino	Franco	f	1968-01-07	01-01-22-001-0070	f
10275052	Alberto	Aleix	Batista	Rivas	m	1956-05-15	01-01-22-001-0070	f
12820521	Mario	Rayan	Alfonso	Quintero	m	1969-09-22	01-01-22-001-0070	f
10904475	Jorge	Martin	Duarte	Miranda	m	1953-09-24	01-01-22-001-0070	f
12005659	Lucas	Andres	Barajas	Aparicio	m	1960-04-22	01-01-22-001-0070	f
14526373	Helena	Erika	Sandoval	Gaona	f	1957-03-02	01-01-22-001-0070	f
12723894	Jaime	Joel	Cuesta	Bañuelos	m	1966-07-17	01-01-22-001-0070	f
6742667	Hector	Miguel	Guevara	Paredes	m	1967-04-23	01-01-22-001-0070	f
14910694	Clara	Marta	Serrato	Montes	f	1956-12-20	01-01-22-001-0064	f
10988993	Lucia	Valeria	Angulo	Delgado	f	1969-07-23	01-01-22-001-0064	f
11139687	Africa	Ane	Cabrera	Jaramillo	f	1959-08-15	01-01-22-001-0064	f
9425937	Diana	Fatima	Candelaria	Pantoja	f	1964-09-12	01-01-22-001-0064	f
13186242	Eduardo	Sebastian	Verduzco	Villar	m	1967-04-13	01-01-22-001-0064	f
13035037	Diego	Cristian	Conde	Cordero	m	1960-06-15	01-01-22-001-0064	f
7852705	Jan	Leandro	Montoya	Alicea	m	1966-11-19	01-01-22-001-0064	f
6495132	Rodrigo	Jaime	Adorno	Negrón	m	1961-12-15	01-01-22-001-0064	f
14894149	Berta	Alicia	Paz	Vega	f	1956-11-03	01-01-22-001-0064	f
14951325	Martina	Eva	Adame	Miramontes	f	1959-05-26	01-01-22-001-0064	f
5433482	Clara	Olivia	Arriaga	Orta	f	1952-04-18	01-01-22-001-0064	f
11788694	Joel	Eric	Carmona	Llorente	m	1962-12-31	01-01-22-001-0064	f
10260714	Juan	Hugo	Sisneros	Marrero	m	1969-04-12	01-01-22-001-0064	f
11984853	Nora	Rocio	Valdés	Ávalos	f	1959-05-04	01-01-22-001-0064	f
6317696	Mateo	Mohamed	Arenas	Laureano	m	1954-06-26	01-01-22-001-0064	f
5608593	Martina	Nerea	Collazo	Ortiz	f	1965-06-01	01-01-22-001-0064	f
12947684	Oscar	Roberto	Córdoba	Andrés	m	1962-07-07	01-01-22-001-0064	f
7887945	Sofia	Anna	Delapaz	Galán	f	1956-12-20	01-01-22-001-0064	f
5824271	Ana	Nahia	Acevedo	Ojeda	f	1956-08-12	01-01-22-001-0064	f
10083100	Paula	Zulay	Valdez	Nava	f	1956-07-18	01-01-22-001-0064	f
6337745	Silvia	Silvia	Lozada	Garica	f	1966-02-10	01-01-22-001-0064	f
13881699	Juan	Oscar	Valdivia	Corrales	m	1969-03-22	01-01-22-001-0064	f
11088546	Eric	Alonso	Mas	Nieves	m	1954-04-02	01-01-22-001-0064	f
10542123	Nil	Marta	Fierro	Villalpando	f	1967-12-24	01-01-22-001-0064	f
6274822	Sandra	Mara	Palomo	Valencia	f	1966-03-12	01-01-22-001-0064	f
9720982	David	Mario	Lucio	Arellano	m	1956-05-06	01-01-22-001-0064	f
13274551	Leonardo	Daniel	Delarosa	Madrigal	m	1959-12-12	01-01-22-0001-0005	f
10406269	Marco	Javier	Sisneros	Zepeda	m	1952-01-31	01-01-22-0001-0005	f
5977940	Aroa	Marta	Melgar	Rodríquez	f	1965-06-04	01-01-22-0001-0005	f
6825487	Pau	Ariadna	Covarrubias	Pastor	f	1959-01-07	01-01-22-0001-0005	f
5411713	Anna	Alexia	Cisneros	Mateo	f	1963-10-11	01-01-22-0001-0005	f
12048743	Hugo	Sebastian	Gastélum	Naranjo	m	1962-03-02	01-01-22-0001-0005	f
13692783	Alejandra	Jana	Ordóñez	Baca	f	1952-12-24	01-01-22-0001-0005	f
11047098	Iria	Miriam	Villalpando	Delgadillo	f	1952-02-22	01-01-22-0001-0005	f
10667602	Gabriela	Lidia	Tomas	Villareal	f	1959-09-16	01-01-22-0001-0005	f
10806217	Ane	Fatima	Román	Archuleta	f	1963-06-16	01-01-22-0001-0005	f
9777475	Mireia	Rocio	Contreras	Delao	f	1966-09-11	01-01-22-0001-0005	f
5103018	Fatima	Alma	Rosario	Villaseñor	f	1964-09-25	01-01-22-0001-0005	f
12064586	Nuria	Alejandra	Araña	Yáñez	f	1964-12-24	01-01-22-0001-0005	f
12492871	Luis	Ian	Curiel	Clemente	m	1955-12-25	01-01-22-0001-0005	f
8302748	Natalia	Paula	Ávila	Correa	f	1959-01-20	01-01-22-0001-0005	f
11415504	Guillermo	Rodrigo	Bustos	Romero	m	1955-04-28	01-01-22-0001-0005	f
11197108	Hugo	Cristian	Carrillo	Carballo	m	1964-08-17	CC-02-URB-0024	f
7717069	Oscar	Pablo	Banda	Valdés	m	1956-02-20	CC-02-URB-0024	f
14276334	Adrian	Samuel	Oquendo	Sisneros	m	1958-05-02	CC-02-URB-0024	f
13780999	Noa	Nadia	Pagan	Cabrera	f	1965-06-29	CC-02-URB-0024	f
8258763	Alma	Alejandra	Mayorga	Treviño	f	1952-12-08	CC-02-URB-0024	f
12842825	Mohamed	Guillermo	Gómez	Rosario	m	1960-11-09	CC-02-URB-0024	f
9803415	Adriana	Gabriela	Reina	Heredia	f	1965-12-20	CC-02-URB-0024	f
12003099	Alexia	Blanca	Mejía	Reséndez	f	1960-01-18	CC-02-URB-0024	f
10505249	Mario	Alex	Esquivel	Hernando	m	1964-01-20	CC-02-URB-0024	f
10657174	Laia	Sandra	Montero	González	f	1961-12-11	CC-02-URB-0024	f
5722993	Oliver	Raul	Rey	Benavides	m	1968-02-12	CC-02-URB-0024	f
11619882	Africa	Luisa	Mayorga	De la torre	f	1954-07-14	CC-02-URB-0024	f
7203175	Andrea	Lidia	Galván	Polo	f	1953-01-19	CC-02-URB-0024	f
12501368	Alonso	Adria	Ramírez	Granado	m	1964-08-02	CC-02-URB-0024	f
13124420	Jose	Martin	Benavídez	Marrero	m	1955-02-17	CC-02-URB-0024	f
5062378	Angela	Elena	Ulloa	Fernández	f	1960-09-22	CC-02-URB-0024	f
11446634	Diego	Samuel	Zelaya	Casas	m	1969-10-16	CC-02-URB-0024	f
14864283	Nayara	Anna	Ramos	Nájera	f	1958-10-31	01-01-22-001-0008	f
12695931	Valeria	Miriam	Montoya	Henríquez	f	1963-11-06	01-01-22-001-0008	f
12718183	Carlota	Ariadna	Candelaria	Mireles	f	1953-09-23	01-01-22-001-0008	f
14944689	Valeria	Iria	Fajardo	Guillen	f	1961-07-14	01-01-22-001-0008	f
6860967	Carlos	Francisco	Sisneros	Caballero	m	1961-05-09	01-01-22-001-0008	f
14835423	Fatima	Lola	Partida	Rivas	f	1965-09-09	01-01-22-001-0008	f
8997210	Jorge	Andres	Carballo	Valdivia	m	1958-02-24	01-01-22-001-0008	f
11463123	Vega	Marina	Trejo	Malave	f	1952-02-01	01-01-22-001-0008	f
10097516	Cristina	Sofia	Galván	Ceballos	f	1964-04-07	01-01-22-001-0008	f
13407172	Alejandra	Valentina	Rodarte	Andreu	f	1968-12-08	01-01-22-001-0008	f
5177483	Marco	Victor	Pozo	Zepeda	m	1961-02-11	01-01-22-001-0008	f
12742107	Samuel	Mateo	Ordoñez	Salcido	m	1965-04-22	01-01-22-001-0008	f
12746152	Nadia	Diana	Chapa	Águilar	f	1968-11-03	01-01-22-001-0008	f
7183735	Adrian	Francisco	Amaya	Jiménez	m	1952-09-14	01-01-22-001-0008	f
10054658	Andrea	Mireia	Flórez	Orozco	f	1963-11-01	01-01-22-001-0008	f
11605848	Jose	Alejandro	Ocasio	Berríos	m	1966-02-26	01-01-22-001-0008	f
5704199	Carolina	Adriana	Ríos	Fuentes	f	1954-03-09	01-01-22-001-0064	f
14565333	Paula	Candela	Urías	Alcala	f	1956-12-23	01-01-22-001-0064	f
8298289	Luis	Sergio	Cuesta	Soria	m	1958-04-27	01-01-22-001-0064	f
11517952	Alex	Rodrigo	Blázquez	Salcido	m	1956-05-17	01-01-22-001-0064	f
8082047	Lara	Abril	Conde	Delatorre	f	1963-06-21	01-01-22-001-0064	f
8795739	Aleix	Jorge	Pereira	Arias	m	1955-10-06	01-01-22-001-0064	f
8470304	Jose	Eric	Curiel	Duarte	m	1963-01-02	01-01-22-001-0064	f
9838025	Adria	Oswaldo	Mora	Cabrera	m	1955-01-22	01-01-22-001-0064	f
12225869	Marti	Aroa	Mejía	Quintero	f	1958-03-22	01-01-22-001-0064	f
13037222	Valentina	Lucia	Peña	Lebrón	f	1957-06-06	01-01-22-001-0064	f
9921369	Valentina	Elsa	Botello	Valdivia	f	1960-07-18	01-01-22-001-0064	f
5693378	Pedro	Erik	Chacón	Lara	m	1954-12-11	01-01-22-001-0064	f
7375821	Cesar	Andres	Herrera	Gil	m	1956-01-01	01-01-22-001-0064	f
12415831	Lara	Lidia	Suárez	Reyna	f	1964-02-13	01-01-22-001-0064	f
5562975	Guillem	Jose	Jáquez	Marrero	m	1968-11-29	01-01-22-001-0064	f
5969651	Jorge	Victor	Cepeda	Nieto	m	1954-06-08	01-01-22-001-0064	f
6626971	Rayan	Jon	Bueno	Muñoz	m	1966-05-28	01-01-22-001-0064	f
6898108	Oliver	Miguel	Izquierdo	Santacruz	m	1959-07-05	01-01-22-001-0064	f
13084720	Nahia	Carla	Cedillo	Hidalgo	f	1951-12-09	01-01-22-001-0064	f
12900566	Paula	Lara	Galán	Zelaya	f	1963-05-16	01-01-22-001-0064	f
14672596	Naia	Africa	Martos	Andreu	f	1955-01-21	01-01-22-001-0064	f
12366693	Yaiza	Alexandra	Cotto	Oquendo	f	1958-10-21	01-01-22-001-0064	f
6959140	Clara	Daniela	Rosas	Miranda	f	1969-03-10	01-01-22-001-0064	f
7526147	Alberto	Bruno	Ortiz	Granados	m	1969-05-01	01-01-22-001-0064	f
6083247	Nerea	Adriana	Chacón	Saiz	f	1954-10-29	01-01-22-001-0064	f
6956862	Alexandra	Nerea	Valdivia	Valero	f	1966-07-06	01-01-22-001-0064	f
12754865	Alberto	Roberto	Godínez	Pascual	m	1953-05-12	01-01-22-001-0064	f
14485759	Joan	Gabriela	Quezada	Carreón	f	1955-06-06	01-01-22-001-0064	f
6512398	Ines	Laura	Quesada	Navarro	f	1957-06-06	01-01-22-001-0064	f
5827056	Eva	Jana	Ochoa	Corona	f	1958-02-22	01-01-22-001-0064	f
9128749	Miguel	Guillermo	Galarza	Baeza	m	1953-12-01	01-01-22-001-0064	f
8503800	Cristina	Noelia	Roig	Gaytán	f	1968-07-09	01-01-22-001-0064	f
14439816	Mara	Angela	Laureano	Pelayo	f	1962-01-08	01-01-22-001-0064	f
12271393	David	Cristian	Melgar	Cortes	m	1967-08-27	01-01-22-001-0064	f
13911305	Carlos	Nicolas	Merino	Herrero	m	1955-11-25	01-01-22-001-0064	f
12726034	Aroa	Irene	Urías	Ibáñez	f	1961-01-14	01-01-22-001-0064	f
11868459	Jordi	Joel	Navarro	Aguayo	m	1961-11-21	01-01-22-001-0064	f
8931963	Manuela	Elsa	Molina	Cardona	f	1958-01-06	01-01-22-001-0064	f
10921813	Andres	Alonso	Enríquez	Miranda	m	1957-07-12	01-01-22-001-0064	f
12893210	Adria	Jan	Menchaca	Abeyta	m	1957-02-27	01-01-22-001-0064	f
13284242	Berta	Luna	Malave	Aponte	f	1969-09-28	01-01-22-001-0064	f
10965075	Martina	Bella	Llamas	Lorenzo	f	1959-12-31	01-01-22-001-0064	f
9631255	Jordi	Hector	Galarza	Paz	m	1968-08-03	01-01-22-001-0064	f
7574315	Sofia	Elsa	Orta	Pereira	f	1955-07-10	01-01-22-001-0064	f
11058675	Teresa	Silvia	Rey	Valladares	f	1956-11-26	01-01-22-001-0064	f
8155805	Ariadna	Carla	Guzmán	Partida	f	1963-07-04	01-01-22-001-0064	f
12415737	Carla	Andrea	Alejandro	Romero	f	1962-09-22	01-01-22-001-0064	f
9981625	Alexandra	Alejandra	Galán	Alejandro	f	1958-05-02	01-01-22-001-0064	f
9750629	Miguel	Guillermo	Torres	Esquivel	m	1956-05-04	01-01-22-001-0064	f
9781336	Domingo	Oliver	Ferrer	Hinojosa	m	1968-01-13	01-01-22-001-0064	f
8905781	Pablo	Oscar	Abad	Alaniz	m	1964-03-24	01-01-22-001-0064	f
5509692	Marti	Andrea	Araña	Enríquez	f	1968-06-27	01-01-22-001-0064	f
6175358	Nayara	Marta	Huerta	Mondragón	f	1953-10-27	01-01-22-001-0064	f
14614847	Nadia	Jimena	Urrutia	Marín	f	1963-11-21	01-01-22-001-0064	f
9190089	Adam	Cesar	Vera	Tejada	m	1961-09-25	01-01-22-001-0064	f
7423692	Pedro	Aaron	Quintana	Puente	m	1958-09-12	01-01-22-001-0064	f
11238233	Diana	Carmen	Colón	Arce	f	1961-03-26	01-01-22-001-0064	f
14111857	Pol	Diego	Mateo	Ceballos	m	1964-12-31	01-01-22-001-0064	f
6081126	Nahia	Nayara	Mireles	Campos	f	1968-10-05	01-01-22-001-0064	f
6930720	Noa	Adriana	Casado	Valdivia	f	1952-12-11	01-01-22-001-0064	f
9696835	Santiago	Rayan	Casares	Roig	m	1952-10-30	01-01-22-001-0064	f
6036757	Pau	Carolina	Loera	Torres	f	1952-07-14	01-01-22-001-0064	f
26728989	Brando	Elias	Marcano	Ortiz	M	1997-04-23	01-01-22-001-0064	f
14159314	Teresa	Julia	Casanova	Valverde	f	1955-08-06	01-01-22-001-0064	f
5065010	Ismael	Luis	Delarosa	Patiño	m	1953-07-31	01-01-22-001-0064	f
6451083	Juan	Roberto	Mejía	Rodríquez	m	1964-09-22	01-01-22-001-0064	f
13391213	Ane	Helena	Flórez	Acuña	f	1967-10-05	01-01-22-001-0064	f
11026806	Sergio	Adria	Aguayo	Osorio	m	1960-01-13	01-01-22-001-0064	f
6170419	Ismael	Marcos	Soler	Montemayor	m	1955-10-02	01-01-22-001-0064	f
5900278	Gonzalo	Rafael	Godoy	Ruelas	m	1965-11-18	01-01-22-001-0064	f
14155219	Leire	Nora	Zarate	Pichardo	f	1956-02-07	01-01-22-001-0064	f
8472913	Isaac	Jan	Benavídez	Padrón	m	1954-11-14	01-01-22-001-0064	f
7540015	Miriam	Maria	Cintrón	Zapata	f	1966-04-06	01-01-22-001-0064	f
14513711	Gonzalo	Francisco Javier	Villagómez	Guerra	m	1952-03-08	01-01-22-001-0064	f
9420641	Alejandra	Abril	Vallejo	Dueñas	f	1965-01-17	01-01-22-001-0064	f
10646007	Miguel	Andres	Ruvalcaba	Posada	m	1966-09-15	01-01-22-001-0064	f
14153922	Iria	Ines	Rendón	Bermejo	f	1957-11-03	01-01-22-001-0064	f
8826766	Cristian	Samuel	Arenas	Ávila	m	1957-01-31	01-01-22-001-0064	f
13507111	Ruben	Marco	Lerma	Almaraz	m	1969-09-15	01-01-22-001-0064	f
6028126	Aleix	Jesus	Cardona	Izquierdo	m	1963-10-13	01-01-22-001-0064	f
10415527	Ruben	Mohamed	Menéndez	Nazario	m	1957-11-13	01-01-22-001-0064	f
7528319	Victoria	Alexia	Sáenz	Candelaria	f	1966-07-14	01-01-22-001-0064	f
10380197	Marc	Pablo	Leal	Soria	m	1964-05-10	01-01-22-001-0064	f
8512510	Hector	Marco	Tovar	Romero	m	1956-01-20	01-01-22-001-0064	f
13370315	Juan	Joel	Giménez	Esteve	m	1961-06-25	01-01-22-001-0064	f
12850402	Cesar	Rodrigo	Barragán	Luna	m	1962-01-15	01-01-22-001-0064	f
7690024	Erik	Jesus	Pereira	Narváez	m	1957-04-29	01-01-22-001-0064	f
9451188	Beatriz	Valeria	Pons	Arenas	f	1957-08-26	01-01-22-001-0064	f
11946720	Paola	Blanca	Rueda	Chavarría	f	1962-05-20	01-01-22-001-0064	f
9160787	Ignacio	Gabriel	Palomino	Hernandes	m	1958-02-18	01-01-22-001-0064	f
6504688	Antonio	Daniel	Orta	Expósito	m	1965-03-23	01-01-22-001-0064	f
10686081	Nadia	Maria	Alcántar	Montez	f	1961-05-20	01-01-22-001-0064	f
11029907	Vera	Ona	Sánchez	Roldán	f	1958-02-23	01-01-22-001-0064	f
14077770	Angela	Carla	Aguilar	Villa	f	1967-07-28	01-01-22-001-0064	f
7094595	Guillem	Adrian	Escobedo	Agosto	m	1968-04-20	01-01-22-001-0064	f
13020295	Angel	Leo	Escalante	Carbajal	m	1962-05-06	01-01-22-001-0064	f
13717178	Leonardo	Gonzalo	Quiñónez	Estévez	m	1958-05-12	01-01-22-001-0064	f
14045111	Marti	Celia	Núñez	Villalpando	f	1952-02-27	01-01-22-001-0064	f
5315612	Ana	Naia	Loera	Córdova	f	1955-01-28	01-01-22-001-0064	f
7825517	Manuela	Lucia	Olmos	Cabrera	f	1968-04-11	01-01-22-001-0064	f
10036996	Angel	Cesar	Mateos	Espinosa	m	1954-04-02	01-01-22-001-0064	f
13476350	Eva	Fatima	Rodrígez	Fierro	f	1953-10-27	01-01-22-001-0064	f
13429449	Mateo	Victor	Girón	Orozco	m	1961-08-15	01-01-22-001-0064	f
8821306	Daniel	Oliver	Aparicio	Valdivia	m	1967-11-07	01-01-22-001-0064	f
5440679	Cristina	Emma	Casillas	Moreno	f	1968-10-09	01-01-22-001-0064	f
9730016	Guillem	Martin	Palomino	Adame	m	1952-06-01	01-01-22-001-0064	f
5772407	Alonso	Alonso	Delgadillo	Matías	m	1966-10-23	01-01-22-001-0064	f
12951998	Javier	Oscar	Loya	Esteve	m	1951-11-30	01-01-22-001-0064	f
13421659	Aroa	Marti	Correa	Casares	f	1959-05-22	01-01-22-001-0064	f
13720615	Gabriela	Laura	Alfonso	Cisneros	f	1956-08-03	01-01-22-001-0064	f
9511337	Jesus	Hugo	Domenech	Oliver	m	1959-06-24	01-01-22-001-0064	f
12419352	Nicolas	Jaime	Lemus	Melgar	m	1966-05-05	01-01-22-001-0064	f
6737151	Pol	Mateo	Zepeda	Armendáriz	m	1960-11-17	01-01-22-001-0064	f
12107801	Alexia	Natalia	Banda	Gallegos	f	1953-01-06	01-01-22-001-0064	f
9369607	Alicia	Martina	Quiñónez	Plaza	f	1951-11-08	01-01-22-001-0064	f
13738298	Alma	Valentina	Benavídez	Pulido	f	1953-06-18	01-01-22-001-0064	f
5977835	Alicia	Africa	Martínez	Herrera	f	1954-05-29	01-01-22-001-0064	f
7424285	Alex	Izan	Arguello	Casanova	m	1967-10-25	01-01-22-001-0064	f
12821587	Lucas	Antonio	Delagarza	Tamez	m	1969-09-20	01-01-22-001-0064	f
7038124	Jorge	Luis	Orosco	Mireles	m	1969-08-13	01-01-22-001-0064	f
5082614	Ian	Ian	Atencio	Ramón	m	1960-10-06	01-01-22-001-0064	f
5491557	Luisa	Marina	Galán	Arellano	f	1967-06-01	01-01-22-001-0064	f
10918750	Jon	Guillermo	Méndez	Segovia	m	1963-03-09	01-01-22-001-0064	f
8774874	Jesus	Ruben	Reyes	Saldaña	m	1960-10-28	01-01-22-001-0064	f
11590392	Miguel	Guillermo	Rivero	Gaytán	m	1956-11-20	01-01-22-001-0064	f
11010578	Naiara	Noelia	Vaca	Corona	f	1967-01-13	01-01-22-001-0064	f
13911690	Francisco	Marcos	Ordoñez	Peralta	m	1967-01-12	01-01-22-001-0064	f
13144133	Luna	Zulay	Medrano	Niño	f	1969-04-18	01-01-22-001-0064	f
8831019	Miguel	Rafael	Carbajal	Fuentes	m	1967-05-31	01-01-22-001-0064	f
9016202	Alvaro	Fernando	Magaña	Olivas	m	1963-01-18	01-01-22-001-0064	f
6610521	Eva	Mara	Ceja	Toro	f	1959-06-12	01-01-22-001-0064	f
5879013	Marina	Nil	Sancho	Cardenas	f	1964-06-16	01-01-22-001-0064	f
7705277	Carmen	Raquel	Altamirano	Cabrera	f	1961-08-26	01-01-22-001-0064	f
9317140	Mara	Beatriz	Matías	Antón	f	1954-06-12	01-01-22-001-0064	f
11731711	Anna	Diana	Gaytán	Posada	f	1964-01-11	01-01-22-001-0064	f
12711975	Fatima	Nayara	Perea	Delarosa	f	1966-01-16	01-01-22-001-0064	f
9422418	Francisco Javier	Diego	Elizondo	Bétancourt	m	1967-04-03	01-01-22-001-0064	f
10090804	Lola	Pau	Millán	Aranda	f	1967-04-26	01-01-22-001-0064	f
7427276	Roberto	Raul	Zaragoza	Del río	m	1960-01-04	01-01-22-001-0064	f
12150794	Zulay	Nadia	Toledo	Garrido	f	1963-12-26	01-01-22-001-0064	f
12959010	Carlota	Teresa	Colunga	Delrío	f	1966-04-11	01-01-22-001-0064	f
8729921	Bruno	Rayan	Segovia	Olmos	m	1960-05-29	01-01-22-001-0064	f
9828513	Cesar	Alvaro	Pelayo	Casillas	m	1964-12-24	01-01-22-001-0064	f
13060028	Carla	Gabriela	Benavídez	Vela	f	1961-02-23	01-01-22-001-0064	f
13589551	Alvaro	Samuel	López	Téllez	m	1965-01-28	01-01-22-001-0064	f
11472916	Teresa	Marta	Juárez	Sancho	f	1967-01-20	01-01-22-001-0064	f
14060461	Leire	Mar	Vergara	Samaniego	f	1961-05-15	01-01-22-001-0064	f
8830355	Carmen	Eva	Rentería	Peralta	f	1955-12-09	01-01-22-001-0064	f
7083136	Omar	Luis	Deleón	Cantú	m	1962-12-14	01-01-22-001-0064	f
9602071	Vera	Angela	Villanueva	Carrera	f	1969-04-24	01-01-22-001-0064	f
7910666	Dario	Eric	Dávila	Luevano	m	1967-02-08	01-01-22-001-0064	f
12990032	Miriam	Jana	Arce	Tijerina	f	1955-05-06	01-01-22-001-0064	f
13132353	Leire	Sandra	Venegas	De la fuente	f	1961-05-04	01-01-22-001-0064	f
10288038	Antonio	Isaac	Viera	Montemayor	m	1966-01-21	01-01-22-001-0064	f
10796586	Marina	Berta	Roldan	Carrillo	f	1957-10-02	01-01-22-001-0064	f
11460317	Pedro	Juan	Caraballo	Abreu	m	1969-04-10	01-01-22-001-0064	f
13297024	Rayan	Carlos	Naranjo	Paz	m	1966-10-25	01-01-22-001-0064	f
8248263	Julia	Sandra	Delarosa	Solorzano	f	1954-05-15	01-01-22-001-0064	f
11583636	Alicia	Beatriz	Villegas	Guerra	f	1964-10-03	01-01-22-001-0064	f
13333926	Juan	Domingo	Delao	Escalante	m	1960-05-23	01-01-22-001-0064	f
9264396	David	Manuel	Valadez	Izquierdo	m	1960-01-05	01-01-22-001-0064	f
13596238	Mara	Ariadna	Alemán	Solano	f	1954-05-03	01-01-22-001-0064	f
9937190	Laura	Zulay	Blanco	Esquivel	f	1956-09-20	01-01-22-001-0064	f
5858649	Marc	Erik	Patiño	Orellana	m	1965-01-24	01-01-22-001-0064	f
12957767	Laia	Leire	Angulo	Rivera	f	1963-09-20	01-01-22-001-0064	f
12749345	Alonso	Fernando	Ferrer	Miguel	m	1962-06-18	01-01-22-001-0064	f
6313740	Rocio	Marti	Archuleta	Carbonell	f	1954-09-24	01-01-22-001-0064	f
12666878	Martina	Nora	Ibáñez	Calvillo	f	1964-10-10	01-01-22-001-0064	f
13165972	Pedro	Cristian	Montez	Fajardo	m	1962-03-01	01-01-22-001-0064	f
5094092	Pol	Oswaldo	Benito	Corral	m	1964-12-28	01-01-22-001-0064	f
11690426	Francisco	Fernando	Pardo	Perea	m	1965-07-25	01-01-22-001-0064	f
7121626	Nadia	Blanca	Ordóñez	Deleón	f	1953-05-20	01-01-22-001-0064	f
7828965	Francisco	Angel	Sarabia	Tejeda	m	1962-05-13	01-01-22-001-0064	f
12182788	Marcos	Jordi	Cornejo	Polanco	m	1968-07-03	01-01-22-001-0064	f
9339691	Natalia	Gabriela	Salazar	Medrano	f	1968-06-19	01-01-22-001-0064	f
10991504	Africa	Emma	Jimínez	Delapaz	f	1963-12-18	01-01-22-001-0064	f
14186415	Bruno	Gabriel	Montañez	Laboy	m	1953-07-24	01-01-22-001-0064	f
13780375	Hugo	Diego	Ávalos	Fajardo	m	1963-01-16	01-01-22-001-0064	f
9794108	Marcos	Enrique	Olivárez	Ramírez	m	1963-01-06	01-01-22-001-0064	f
13777356	Juan	Jan	Jurado	Castaño	m	1960-07-09	01-01-22-001-0064	f
10594860	Anna	Eva	Carrion	Sánchez	f	1966-10-13	01-01-22-001-0064	f
14766906	Jose	Marc	Amaya	Cerda	m	1958-04-14	01-01-22-001-0064	f
6753571	Andres	Bruno	Puente	Villareal	m	1955-10-11	01-01-22-001-0064	f
8712561	Jaime	Victor	Abad	Báez	m	1960-02-14	01-01-22-001-0064	f
13858710	Jana	Angela	Hurtado	Villaseñor	f	1959-09-30	01-01-22-001-0064	f
8822687	Samuel	Gonzalo	Castillo	Quiñones	m	1957-11-22	01-01-22-001-0064	f
14310459	Manuel	Eduardo	Delgadillo	Perea	m	1966-04-02	01-01-22-001-0064	f
9704287	Ruben	Roberto	Nájera	Cantú	m	1969-08-05	01-01-22-001-0064	f
10307345	Bruno	Isaac	Granado	Cadena	m	1969-06-24	01-01-22-001-0064	f
10482920	Miriam	Silvia	Galindo	Rascón	f	1963-07-11	01-01-22-001-0064	f
13917482	Jon	Leo	Jaramillo	Solís	m	1963-01-28	01-01-22-001-0064	f
11530198	Domingo	Lucas	Luis	Rico	m	1962-10-06	01-01-22-001-0064	f
10428538	Sergio	Leonardo	Leal	Cardona	m	1957-06-17	01-01-22-001-0064	f
6898116	Naiara	Noa	Terrazas	Moran	f	1962-07-17	01-01-22-001-0064	f
8207147	Eric	Manuel	Cintrón	Candelaria	m	1960-02-19	01-01-22-001-0064	f
7450345	Martin	David	Fernández	Concepción	m	1959-07-20	01-01-22-001-0064	f
7713672	Mireia	Nayara	Bonilla	Cotto	f	1969-08-14	01-01-22-001-0064	f
13113579	Mireia	Laura	Arteaga	Barrientos	f	1963-11-27	01-01-22-001-0064	f
10681606	Bella	Ariadna	Hurtado	Griego	f	1968-02-17	01-01-22-001-0064	f
10214852	Alvaro	Eduardo	Ávalos	Esteve	m	1962-04-18	01-01-22-001-0064	f
10491818	Andres	Javier	Hernandes	Urrutia	m	1954-03-11	01-01-22-001-0064	f
7602035	Bella	Raquel	Ortega	Madera	f	1955-11-06	01-01-22-001-0064	f
8933131	Saul	Hector	Rolón	Lara	m	1964-05-19	01-01-22-001-0064	f
11466820	Andrea	Leyre	Galván	Pereira	f	1953-06-21	01-01-22-001-0064	f
13046211	Martina	Ane	Peralta	Gamboa	f	1968-09-26	01-01-22-001-0064	f
12610718	Hector	Lucas	Bañuelos	Yáñez	m	1959-04-21	01-01-22-001-0064	f
6891319	Nerea	Vega	Vega	Reyes	f	1960-11-17	01-01-22-001-0064	f
7369717	Hugo	Adam	Vila	Pantoja	m	1964-03-25	01-01-22-001-0064	f
11966861	Clara	Julia	Jurado	Carballo	f	1953-11-27	01-01-22-001-0064	f
5739579	Maria	Helena	Pineda	Zarate	f	1958-08-17	01-01-22-001-0064	f
9212476	Laura	Nahia	Peláez	Toledo	f	1952-04-25	01-01-22-001-0064	f
13325584	Gabriela	Cristina	Munguía	Gurule	f	1966-08-24	01-01-22-001-0064	f
12875214	Isabel	Claudia	Rocha	Dávila	f	1959-02-04	01-01-22-001-0064	f
14788486	Ariadna	Elsa	Garica	Meléndez	f	1969-03-30	01-01-22-001-0064	f
8802710	Javier	Eduardo	Palomo	Barroso	m	1968-06-25	01-01-22-001-0064	f
14038023	Paola	Raquel	Véliz	Aragón	f	1969-01-16	01-01-22-001-0064	f
5829940	Alonso	Carlos	Pagan	Quintana	m	1956-06-22	01-01-22-001-0064	f
8982238	Bella	Claudia	Saucedo	Hidalgo	f	1968-05-12	01-01-22-001-0064	f
8109646	Manuela	Lola	Garza	Montes	f	1954-05-28	01-01-22-001-0064	f
5672366	Adrian	David	Armijo	Soto	m	1957-07-22	01-01-22-001-0064	f
5778151	Francisco	Martin	Reyna	Esquivel	m	1956-02-05	01-01-22-001-0064	f
7581685	Claudia	Pau	Alcala	Prado	f	1954-04-24	01-01-22-001-0064	f
5633814	Eric	Eric	Feliciano	Reséndez	m	1956-04-23	01-01-22-001-0064	f
12639768	Naia	Lidia	Moran	Lomeli	f	1953-06-27	01-01-22-001-0064	f
10701064	Gabriel	Miguel	Elizondo	Arce	m	1968-03-25	01-01-22-001-0064	f
13646246	Ismael	Pol	Tejeda	Tirado	m	1956-01-23	01-01-22-001-0064	f
8658468	Oliver	Luis	Llorente	Ornelas	m	1958-07-14	01-01-22-001-0064	f
6785971	Daniela	Olivia	Sánchez	Roig	f	1958-02-25	01-01-22-001-0064	f
9533124	Joel	Leonardo	Tórrez	Guevara	m	1968-06-19	01-01-22-001-0064	f
11004256	Francisco Javier	Hector	Pabón	Tamayo	m	1964-05-27	01-01-22-001-0064	f
6948215	Fernando	Jesus	Almonte	Nava	m	1960-03-29	01-01-22-001-0064	f
14044986	Berta	Diana	Aguayo	Arredondo	f	1958-01-31	01-01-22-001-0064	f
6386292	Francisco	Domingo	Olivares	Córdova	M	1963-02-17	01-01-22-001-0064	f
8898690	Yorman	Yorman	Naranjo	Feliciano	m	1953-05-08	01-01-22-001-0064	f
12922376	Juan	Saul	Reynoso	Carmona	m	1954-11-21	01-01-22-001-0064	f
14111342	Nerea	Alicia	Barrera	Barroso	f	1952-07-29	01-01-22-001-0064	f
13756968	Rodrigo	David	Valverde	Botello	m	1967-09-22	01-01-22-001-0064	f
7202809	Samuel	Francisco	Solano	Barrios	m	1952-08-25	01-01-22-001-0064	f
13122613	Alvaro	Juan	Gallegos	Carrillo	m	1954-08-24	01-01-22-001-0064	f
5577813	Nayara	Sofia	Roig	Franco	f	1960-11-12	01-01-22-001-0064	f
11423911	Roberto	Jaime	Puig	Ceballos	m	1960-03-21	01-01-22-001-0064	f
6747054	Rocio	Natalia	Santiago	Alcala	f	1968-03-17	01-01-22-001-0064	f
6696194	Candela	Maria	Pereira	Armenta	f	1969-11-14	01-01-22-001-0064	f
8725375	Antonio	Joel	Briseño	Lorenzo	m	1964-07-17	01-01-22-001-0064	f
11939745	Alejandro	Raul	Corral	Ruíz	m	1967-03-06	01-01-22-001-0064	f
13669447	Maria	Alexia	Pons	Carrion	f	1954-11-17	01-01-22-001-0064	f
9686853	Ismael	Cristian	Arroyo	Franco	m	1963-12-01	01-01-22-001-0064	f
12819390	Jon	Ignacio	Girón	Rey	m	1959-02-18	01-01-22-001-0064	f
9504121	Jorge	Eric	Villareal	Cavazos	m	1957-03-10	01-01-22-001-0064	f
13510259	Cristian	Marco	Negrón	Briseño	m	1954-07-04	01-01-22-001-0064	f
12882380	Laia	Angela	Ibáñez	Medina	f	1956-08-10	01-01-22-001-0064	f
9853342	Martin	Lucas	Gamboa	Henríquez	m	1954-09-20	01-01-22-001-0064	f
14076294	Carmen	Yaiza	Vázquez	Melgar	f	1952-09-16	01-01-22-001-0064	f
9589388	Ona	Beatriz	Maya	Esparza	f	1960-09-23	01-01-22-001-0064	f
13991014	Irene	Sandra	Cepeda	Espinoza	f	1964-03-10	01-01-22-001-0064	f
5077953	Gabriel	Saul	Saldaña	Archuleta	m	1965-06-21	01-01-22-001-0064	f
12102708	Nerea	Teresa	Gamboa	Madrid	f	1952-04-26	01-01-22-001-0064	f
5424313	Mateo	Daniel	Valentín	Ruelas	m	1960-09-04	01-01-22-001-0064	f
12076856	Paola	Erika	Vela	Olivas	f	1961-10-30	01-01-22-001-0064	f
9583401	Antonia	Joan	Llamas	Enríquez	f	1955-02-24	01-01-22-001-0064	f
10278932	Mohamed	Manuel	Trujillo	Alanis	m	1961-07-01	01-01-22-001-0064	f
8607570	Bella	Lucia	Salas	Romo	f	1961-08-14	01-01-22-001-0064	f
13028824	Saul	Angel	Carrera	Nava	m	1959-10-01	01-01-22-001-0064	f
9972264	Bruno	Leo	Berríos	Vanegas	m	1957-09-05	01-01-22-001-0064	f
13987400	Javier	Marc	Sauceda	Serrato	m	1956-02-22	01-01-22-001-0064	f
8845388	Aaron	Adria	Iglesias	Andreu	m	1962-01-13	01-01-22-001-0064	f
7206764	Miguel	Juan	Mojica	Calvillo	m	1959-05-10	01-01-22-001-0064	f
7288420	David	Oscar	Guevara	Alarcón	m	1958-01-27	01-01-22-001-0064	f
11334205	Alejandra	Blanca	Chacón	Zaragoza	f	1959-01-28	01-01-22-001-0064	f
9763106	Adam	Samuel	Sarabia	Estrada	m	1969-08-22	01-01-22-001-0064	f
13429189	Marti	Angela	Castellano	Lozada	f	1966-03-26	01-01-22-001-0064	f
13803554	Guillem	Aaron	Campos	Gálvez	m	1957-04-01	01-01-22-001-0064	f
12243603	Ander	Martin	Cavazos	Cervantes	m	1954-09-23	01-01-22-001-0064	f
8040798	Jose	Angel	Madera	Garibay	m	1956-01-17	01-01-22-001-0064	f
9618966	Oswaldo	Joel	Valero	Terrazas	m	1966-06-25	01-01-22-001-0064	f
9356859	Oswaldo	Sebastian	Palomino	Delgadillo	m	1965-01-28	01-01-22-001-0064	f
12643532	Alexandra	Cristina	Meraz	Uribe	f	1956-04-08	01-01-22-001-0064	f
10658361	Francisco Javier	Lucas	Gamez	Pardo	m	1965-06-24	01-01-22-001-0064	f
7437284	Nil	Elsa	Nazario	Nevárez	f	1963-11-11	01-01-22-001-0064	f
10301187	Lara	Beatriz	Rojas	Bernal	f	1966-01-24	01-01-22-001-0064	f
12153872	Oscar	Izan	Gallegos	Santana	m	1966-05-18	01-01-22-001-0064	f
9878064	Guillem	Miguel	Pastor	Jasso	m	1963-12-18	01-01-22-001-0064	f
14492292	Alberto	Alejandro	Sierra	Cortez	m	1954-07-20	01-01-22-001-0064	f
12328601	Gabriel	Eric	Sotelo	Venegas	m	1960-08-21	01-01-22-001-0064	f
6701947	Miguel	Ivan	Mercado	Santacruz	m	1955-02-13	01-01-22-001-0064	f
8070639	Aroa	Noa	Paredes	Alfonso	f	1962-08-27	01-01-22-001-0064	f
10497428	Sergio	Cristian	León	Páez	m	1957-06-11	01-01-22-001-0064	f
10064218	Ana	Berta	Conde	Roldán	f	1964-01-25	01-01-22-001-0064	f
13616200	Nil	Irene	Ballesteros	Oliver	f	1969-09-22	01-01-22-001-0064	f
13130631	Berta	Africa	Briseño	Delagarza	f	1955-03-10	01-01-22-001-0064	f
5361118	Miriam	Alba	Saldivar	Zelaya	f	1963-11-24	01-01-22-001-0064	f
5006952	Alvaro	Omar	Abeyta	Prado	m	1952-02-19	01-01-22-001-0064	f
7959850	Lucia	Sofia	Adorno	Lozada	f	1957-10-23	01-01-22-001-0064	f
5946357	Leandro	Ruben	Alfaro	Fajardo	m	1961-03-18	01-01-22-001-0064	f
6515236	Naiara	Jana	Castro	Carretero	f	1961-10-27	01-01-22-001-0064	f
12280884	Alonso	Leonardo	Camacho	Benavides	m	1959-02-21	01-01-22-001-0064	f
11980383	Jan	Alberto	Piñeiro	Arreola	m	1956-01-20	01-01-22-001-0064	f
13715472	Ismael	Aleix	Posada	Peralta	m	1960-10-07	01-01-22-001-0064	f
13117914	Laia	Noa	Esquivel	Valencia	f	1958-02-02	01-01-22-001-0064	f
5619856	Irene	Ainara	Ruvalcaba	Rodarte	f	1966-07-26	01-01-22-001-0064	f
9690848	Aroa	Manuela	Abeyta	Méndez	f	1966-10-29	01-01-22-001-0064	f
14973664	Ander	Jose	Arteaga	Perales	m	1969-02-23	01-01-22-001-0064	f
8468273	Julia	Lola	Sáenz	Arguello	f	1952-06-08	01-01-22-001-0064	f
12581279	Anna	Elena	Carrasco	Espinal	f	1963-02-17	01-01-22-001-0064	f
14054437	Rafael	Oswaldo	Agosto	Longoria	m	1954-09-05	01-01-22-001-0064	f
8718374	Leonardo	Sebastian	Becerra	Bautista	m	1957-10-17	01-01-22-001-0064	f
7071828	Jesus	Erik	Madrigal	Cedillo	m	1951-11-12	01-01-22-001-0064	f
6239469	Francisco	Adam	Abreu	Tórrez	m	1965-12-12	01-01-22-001-0064	f
10941111	Lola	Alicia	Rey	Padrón	F	1954-02-04	01-01-22-001-0064	f
14322403	Marti	Vera	Peralta	Iglesias	f	1956-07-18	01-01-22-001-0064	f
13568384	Marco	Cristian	Henríquez	Delarosa	m	1954-09-19	01-01-22-001-0064	f
8661104	Lara	Patricia	Guajardo	Lucas	f	1961-07-25	01-01-22-001-0064	f
12394670	Guillem	Eduardo	Peña	Ruvalcaba	m	1962-10-30	01-01-22-001-0064	f
6081871	Adriana	Carlota	Montemayor	Barraza	f	1961-12-30	01-01-22-001-0064	f
12096954	Marc	Samuel	Palacios	Vaca	m	1954-04-15	01-01-22-001-0064	f
13461749	Angel	Marco	Muñoz	Vaca	m	1952-06-01	01-01-22-001-0064	f
13375184	Diana	Ines	Godoy	Ruvalcaba	f	1961-03-24	01-01-22-001-0064	f
5160065	Mateo	Sebastian	Monroy	Menéndez	m	1966-08-30	01-01-22-001-0064	f
6918309	Guillem	Aaron	Domínquez	Borrego	m	1957-10-09	01-01-22-001-0064	f
14516211	Ignacio	Sebastian	Gaitán	Balderas	m	1966-02-15	01-01-22-001-0064	f
7994736	Jan	Ignacio	Ornelas	Aguilar	m	1969-07-14	01-01-22-001-0064	f
13601293	Francisco Javier	Alberto	Montenegro	Olivares	m	1967-08-10	01-01-22-001-0064	f
11671878	Juan	Javier	Vicente	Guzmán	m	1953-01-04	01-01-22-0001-0005	f
14089711	Lidia	Irene	Luevano	Orta	f	1967-11-03	01-01-22-0001-0005	f
12828857	Luna	Leyre	Fajardo	Collado	f	1956-04-14	01-01-22-0001-0005	f
14791241	Oscar	Eric	Merino	Bueno	m	1962-12-22	01-01-22-0001-0005	f
7557686	Vera	Leire	Manzano	Ceja	f	1960-02-26	01-01-22-0001-0005	f
11476269	Africa	Ona	Mascareñas	Alcántar	f	1958-09-30	01-01-22-0001-0005	f
14278297	Jon	Adam	Sanabria	Amador	m	1958-12-29	01-01-22-0001-0005	f
12189302	Nahia	Alexia	Concepción	Luna	f	1961-03-25	01-01-22-0001-0005	f
7142921	Natalia	Leire	Escamilla	Moral	f	1969-12-02	01-01-22-0001-0005	f
13998816	Hector	Juan	Noriega	Carvajal	m	1969-07-31	01-01-22-0001-0005	f
5744098	Noa	Martina	Cabello	Alba	f	1954-03-06	01-01-22-0001-0005	f
14209698	Carlota	Emma	Alfonso	Alfonso	f	1966-05-01	01-01-22-0001-0005	f
10000856	Jorge	Jan	Galindo	Serna	m	1969-10-27	01-01-22-0001-0005	f
8091209	Miriam	Nora	Meléndez	Mayorga	f	1966-04-10	01-01-22-0001-0005	f
9259003	Hugo	Jesus	Campos	Sanches	m	1963-01-09	01-01-22-0001-0005	f
11923656	Hugo	Oliver	Carranza	Almanza	m	1966-08-01	01-01-22-0001-0005	f
10728944	Bella	Raquel	Alonso	Frías	f	1969-05-28	01-01-22-0001-0005	f
6655920	Daniela	Anna	Collazo	Parra	f	1969-03-26	01-01-22-0001-0005	f
7417553	Lara	Teresa	Duran	Nevárez	f	1969-04-06	01-01-22-0001-0005	f
6761326	Domingo	Antonio	Vega	Bermúdez	m	1960-11-11	01-01-22-0001-0005	f
8381502	Cesar	Hector	Rodríquez	Cardenas	m	1967-07-06	01-01-22-0001-0005	f
9416123	Angela	Berta	Altamirano	Aguilar	f	1957-01-27	01-01-22-0001-0005	f
7834549	Joel	Martin	Clemente	Ibarra	m	1956-03-06	01-01-22-0001-0005	f
5175945	Oswaldo	Guillermo	Mojica	Reina	m	1965-02-18	01-01-22-0001-0005	f
14371385	Gonzalo	Manuel	Ulloa	Serrano	m	1968-09-19	01-01-22-0001-0005	f
9073298	Oscar	Domingo	Lemus	Montalvo	m	1968-07-08	01-01-22-0001-0005	f
9514585	Victor	Antonio	Zúñiga	Soler	m	1962-06-27	01-01-22-0001-0005	f
14726226	Laura	Jana	Sanabria	Hernádez	f	1961-10-09	01-01-22-0001-0005	f
5039082	Berta	Noa	Casas	Venegas	f	1955-05-16	01-01-22-0001-0005	f
14363217	Silvia	Silvia	Sandoval	Simón	f	1956-02-16	01-01-22-0001-0005	f
14469307	Jaime	Fernando	Anguiano	Flores	m	1962-12-06	01-01-22-0001-0005	f
10751169	Bella	Ines	Sosa	Rodarte	f	1955-02-23	01-01-22-0001-0005	f
10362125	Andres	Alvaro	Roig	Ayala	m	1962-07-01	01-01-22-0001-0005	f
7502187	Fernando	Rodrigo	Izquierdo	Anaya	m	1952-06-22	01-01-22-0001-0005	f
14257584	Naia	Naia	Garrido	Cisneros	f	1951-12-17	01-01-22-0001-0005	f
9158855	Fernando	Pol	Melgar	Ulibarri	m	1952-11-07	01-01-22-0001-0005	f
13249442	Naia	Irene	Villar	Frías	f	1962-04-13	01-01-22-0001-0005	f
12773264	Gabriela	Elsa	Benavídez	Santos	f	1953-04-01	01-01-22-0001-0005	f
11502002	Carlota	Eva	Quiñones	Gálvez	f	1965-03-07	01-01-22-0001-0005	f
11536554	Alexia	Gabriela	Osorio	Méndez	f	1957-07-25	01-01-22-0001-0005	f
9512102	Yaiza	Nora	Pulido	Roig	f	1969-08-22	01-01-22-0001-0005	f
5739474	Manuela	Rocio	Llamas	Espinoza	f	1952-01-02	01-01-22-0001-0005	f
10191321	Izan	Marcos	Badillo	Castellanos	m	1958-08-17	01-01-22-0001-0005	f
6660076	Diego	Joel	Escalante	Lovato	m	1960-05-31	01-01-22-0001-0005	f
14127436	Oliver	Luis	Escamilla	Alicea	m	1968-08-07	01-01-22-0001-0005	f
10048326	Adam	Ignacio	Henríquez	Raya	m	1957-05-08	01-01-22-0001-0005	f
8966037	Manuel	Andres	Olmos	Ramón	m	1964-05-03	01-01-22-0001-0005	f
12582532	Fernando	Jorge	Ponce	Delgado	m	1967-04-20	01-01-22-0001-0005	f
7207891	Enrique	Erik	Pacheco	Chavarría	m	1968-05-08	01-01-22-0001-0005	f
7218269	Fatima	Blanca	Mercado	Expósito	f	1953-02-23	01-01-22-0001-0005	f
6595891	Aroa	Carla	Casas	Zúñiga	f	1958-06-11	01-01-22-0001-0005	f
13438618	Pol	Francisco Javier	Tejada	Sauceda	m	1953-04-25	01-01-22-0001-0005	f
11488339	Lucas	Angel	Vázquez	Flórez	m	1955-01-18	01-01-22-0001-0005	f
6223816	David	Gonzalo	Palomo	Paredes	m	1967-07-02	01-01-22-0001-0005	f
5586402	Bruno	Dario	Ochoa	Adame	m	1964-11-17	01-01-22-0001-0005	f
14013807	Ainara	Bella	Franco	Benavides	f	1951-12-03	01-01-22-0001-0005	f
8229998	Miguel	Guillem	Gamboa	Escamilla	m	1955-04-24	01-01-22-0001-0005	f
8649500	Berta	Noa	Romero	Bermejo	f	1969-09-29	01-01-22-0001-0005	f
14875829	Olivia	Natalia	Amador	Oquendo	f	1954-08-14	01-01-22-0001-0005	f
10170323	Luna	Rocio	Nava	Aguayo	f	1967-09-17	01-01-22-0001-0005	f
5834924	Nadia	Ariadna	González	Hidalgo	f	1961-01-25	01-01-22-0001-0005	f
14256441	Miguel	Aaron	Pozo	Moral	m	1957-09-24	01-01-22-0001-0005	f
7609699	Teresa	Alicia	Candelaria	Giménez	f	1962-07-19	01-01-22-0001-0005	f
6964389	Juan	Bruno	Venegas	Prado	m	1955-12-21	01-01-22-0001-0005	f
10471822	Jaime	Raul	Cabrera	Varela	m	1964-08-12	01-01-22-0001-0005	f
7537450	Pol	Diego	Bueno	Quintanilla	m	1965-03-21	01-01-22-0001-0005	f
11768925	Manuel	Mohamed	García	Yáñez	m	1959-03-12	01-01-22-0001-0005	f
13496082	Manuel	Eric	Ibarra	Casanova	m	1956-03-20	01-01-22-0001-0005	f
7318988	Dario	Gabriel	Deleón	Mateo	m	1956-01-10	01-01-22-0001-0005	f
9788954	Mohamed	Joel	Mesa	Gurule	m	1963-05-21	01-01-22-0001-0005	f
14264150	Oscar	Carlos	Ortega	Colunga	m	1952-06-09	01-01-22-0001-0005	f
9601428	Hector	Alberto	Piñeiro	Esquivel	m	1964-12-03	01-01-22-0001-0005	f
9656043	Ignacio	Pedro	Duarte	Calero	m	1964-12-15	01-01-22-0001-0005	f
10023120	Marcos	Ivan	Cisneros	Perea	m	1960-01-18	01-01-22-0001-0005	f
13246516	Jesus	Jaime	Montes	Sanches	m	1967-10-14	01-01-22-0001-0005	f
14511223	Nahia	Elsa	Vila	Cruz	f	1969-10-23	01-01-22-0001-0005	f
6555770	Julia	Pau	Rubio	Zarate	f	1958-03-07	01-01-22-0001-0005	f
5851547	Alba	Noa	Beltrán	Saavedra	f	1959-03-19	01-01-22-0001-0005	f
7814792	Zulay	Andrea	Verduzco	Gallegos	f	1968-07-22	01-01-22-0001-0005	f
9329120	Salma	Alejandra	Quintanilla	Cano	f	1962-09-14	01-01-22-0001-0005	f
6994148	Clara	Victoria	Viera	Barrera	f	1966-01-22	01-01-22-0001-0005	f
8934589	Ainara	Sandra	Ortega	Menchaca	f	1953-04-25	01-01-22-0001-0005	f
9483726	Pedro	Ivan	Rosa	Vanegas	m	1962-12-16	01-01-22-0001-0005	f
10273286	Laia	Beatriz	Tamez	Palomo	f	1961-11-30	01-01-22-0001-0005	f
9312539	Rocio	Raquel	Galán	Frías	f	1960-07-30	01-01-22-0001-0005	f
11151556	Elena	Eva	Melgar	Lovato	f	1967-06-17	01-01-22-0001-0005	f
8577166	Gabriela	Laura	Lucio	Ocasio	f	1967-05-21	01-01-22-0001-0005	f
6053732	Jan	Sebastian	Abeyta	Valadez	m	1959-09-23	01-01-22-0001-0005	f
14472002	Aleix	Cristian	Valles	Figueroa	m	1959-07-25	01-01-22-0001-0005	f
12155430	Ana	Vera	Hernández	Perales	f	1962-12-06	01-01-22-0001-0005	f
7529427	Carolina	Olivia	Ozuna	Uribe	f	1959-07-28	01-01-22-0001-0005	f
14408031	Martina	Sofia	Villalba	Polanco	f	1953-01-20	01-01-22-0001-0005	f
10514581	Mohamed	Sergio	Delarosa	Meléndez	m	1956-10-05	01-01-22-0001-0005	f
13824583	Ana	Leyre	Rincón	Amaya	f	1958-10-06	01-01-22-0001-0005	f
5092689	Blanca	Martina	Cavazos	Cardenas	f	1967-06-13	01-01-22-0001-0005	f
6368429	Marcos	Domingo	Córdova	Laureano	m	1968-12-28	01-01-22-0001-0005	f
13520211	Oscar	Luis	Ortega	Giménez	m	1958-02-08	01-01-22-0001-0005	f
13819950	Isaac	Jan	Alaniz	Prado	m	1961-08-16	01-01-22-0001-0005	f
7655104	Cristina	Mar	Almaraz	Colunga	f	1953-12-17	01-01-22-0001-0005	f
7900173	Sofia	Emma	Villalba	Bonilla	f	1957-04-22	01-01-22-0001-0005	f
6375865	Adriana	Nadia	Pascual	Conde	f	1966-06-02	01-01-22-0001-0005	f
14134470	Dario	Pol	Abreu	Roldán	m	1968-07-08	01-01-22-0001-0005	f
12555202	Cristina	Carla	Zaragoza	Salcido	f	1968-06-05	01-01-22-0001-0005	f
11007378	Jan	Ignacio	Alemán	Armas	m	1960-11-10	01-01-22-0001-0005	f
11909713	Noa	Celia	Pereira	Arellano	f	1955-04-15	01-01-22-0001-0005	f
7494624	Bruno	Sergio	Casado	Martos	m	1952-03-15	01-01-22-0001-0005	f
6729753	Dario	Alejandro	Marín	Luque	m	1968-03-03	01-01-22-0001-0005	f
7374677	Blanca	Joan	Molina	Olivárez	f	1965-04-06	01-01-22-0001-0005	f
6449392	Carmen	Alicia	Olivo	Rivas	f	1964-01-22	01-01-22-0001-0005	f
11076680	Fernando	Adria	Lira	Guillen	m	1964-11-09	01-01-22-0001-0005	f
6401024	Alejandra	Aroa	Romo	Perales	f	1956-11-22	01-01-22-0001-0005	f
7630294	Andres	Angel	Bustamante	Tapia	m	1969-01-03	01-01-22-0001-0005	f
8397366	Erika	Antonia	Archuleta	Cobo	f	1965-02-07	01-01-22-0001-0005	f
6740491	Irene	Berta	Sedillo	Márquez	f	1957-11-24	01-01-22-0001-0005	f
8665396	Yorman	Ian	Carrion	Castellano	m	1968-07-22	01-01-22-0001-0005	f
11843773	Naiara	Nayara	Leyva	Zambrano	f	1954-12-17	01-01-22-0001-0005	f
12260477	Yorman	Jaime	Casanova	Gonzales	m	1967-09-30	01-01-22-0001-0005	f
9594550	Olivia	Paula	Muñiz	Tafoya	f	1953-02-24	01-01-22-0001-0005	f
6272889	Andrea	Iria	Mejía	Cintrón	f	1954-08-27	01-01-22-0001-0005	f
13851128	Raquel	Marti	Delarosa	Nazario	f	1965-07-04	01-01-22-0001-0005	f
11098096	Joel	Marc	Rosas	Polo	m	1965-09-03	01-01-22-0001-0005	f
7194669	Nerea	Paola	Juan	Vera	f	1967-01-23	01-01-22-0001-0005	f
14691818	Marti	Miriam	Marrero	Delacrúz	f	1958-01-29	01-01-22-0001-0005	f
7329443	Rodrigo	Carlos	Borrego	Ponce	m	1956-08-17	01-01-22-0001-0005	f
7986111	Ines	Marta	Solano	Mendoza	f	1967-08-20	01-01-22-0001-0005	f
13946564	Irene	Sandra	Lucio	Zambrano	f	1968-03-20	01-01-22-0001-0005	f
5835853	Victoria	Adriana	Olivo	Meraz	f	1952-03-06	01-01-22-0001-0005	f
6833532	Lidia	Alma	Montes	Saiz	f	1960-04-20	01-01-22-0001-0005	f
8192055	Alejandra	Vera	Cuellar	Linares	f	1969-11-01	01-01-22-0001-0005	f
7531323	Carolina	Carlota	Guillen	Ordóñez	f	1954-10-28	01-01-22-0001-0005	f
14986927	Eric	Cesar	Loera	Burgos	m	1956-04-20	01-01-22-0001-0005	f
12659489	Ivan	Manuel	Solano	Nevárez	m	1966-11-17	01-01-22-0001-0005	f
8755980	Alma	Bella	Barroso	Alonzo	f	1958-10-08	01-01-22-0001-0005	f
7966317	Erika	Alejandra	Arevalo	Bonilla	f	1959-05-02	01-01-22-0001-0005	f
5029959	Marcos	Gabriel	Corona	Munguía	m	1968-11-29	01-01-22-0001-0005	f
14672049	Guillermo	Eric	Sáenz	Gracia	m	1953-06-20	01-01-22-0001-0005	f
10172545	Cesar	Yorman	Saiz	Bonilla	m	1964-08-20	01-01-22-0001-0005	f
9366212	Izan	Eduardo	Candelaria	Guzmán	m	1956-05-06	01-01-22-0001-0005	f
14472059	Gonzalo	Yorman	Ybarra	Rendón	m	1967-08-16	01-01-22-0001-0005	f
10900839	Ruben	Victor	Garrido	Santiago	m	1962-09-06	01-01-22-0001-0005	f
8037899	Gabriel	Angel	Manzano	Colón	m	1961-02-05	01-01-22-0001-0005	f
13953064	Pau	Nadia	Valero	Hernández	f	1965-07-17	01-01-22-0001-0005	f
11139058	Gabriela	Raquel	Reyes	Feliciano	f	1953-08-05	01-01-22-0001-0005	f
10169359	Sandra	Raquel	Santamaría	Nájera	f	1967-01-01	01-01-22-0001-0005	f
12834043	Adam	Bruno	Altamirano	Lugo	m	1960-12-27	01-01-22-0001-0005	f
6740101	Jan	Daniel	Armendáriz	Ávila	m	1955-11-15	01-01-22-0001-0005	f
10655150	Alberto	Roberto	Cortez	Barroso	m	1963-05-29	01-01-22-0001-0005	f
7651166	Jon	Jordi	Loya	Olivares	m	1967-06-17	01-01-22-0001-0005	f
14108735	Santiago	Jordi	Ocampo	Gil	m	1959-08-26	01-01-22-0001-0005	f
6779601	Jorge	Jan	Urías	Patiño	m	1964-02-22	01-01-22-0001-0005	f
10632985	Salma	Isabel	Griego	Salvador	f	1952-09-26	01-01-22-0001-0005	f
13918728	Raul	Sergio	Vázquez	Núñez	m	1959-05-12	01-01-22-0001-0005	f
13817135	Julia	Nahia	Hurtado	Ontiveros	f	1960-05-02	01-01-22-0001-0005	f
8524301	Jorge	Miguel	Sanabria	Montez	m	1962-09-19	01-01-22-0001-0005	f
6872024	Carolina	Mireia	Carballo	Gallegos	f	1960-10-17	01-01-22-0001-0005	f
11813181	Adam	Erik	Salcido	Rodrigo	m	1957-10-18	01-01-22-0001-0005	f
9243315	Marc	Miguel	Valadez	De la cruz	m	1960-07-16	01-01-22-0001-0005	f
6113783	Fernando	Saul	Expósito	Nevárez	m	1968-09-04	01-01-22-0001-0005	f
9825856	Alberto	Antonio	Oliver	Manzanares	m	1953-02-24	01-01-22-0001-0005	f
9611172	Guillem	Santiago	Gimeno	Rojo	m	1955-05-17	01-01-22-0001-0005	f
13454181	Natalia	Valeria	Hernandes	Valles	f	1955-08-15	01-01-22-0001-0005	f
11916643	Carlota	Julia	Corral	Fierro	f	1966-06-04	01-01-22-0001-0005	f
5684678	Alex	Mohamed	Casares	Alvarado	m	1966-07-17	01-01-22-0001-0005	f
12731593	Victor	Ian	Hernández	Burgos	m	1956-01-25	01-01-22-0001-0005	f
12973427	Erik	Francisco Javier	Barreto	Hernádez	m	1956-12-23	01-01-22-0001-0005	f
6764619	Antonio	Leo	Adorno	Robles	m	1963-10-28	01-01-22-0001-0005	f
10981474	Victoria	Zulay	Gómez	Zamudio	f	1968-10-26	01-01-22-0001-0005	f
11418500	David	Enrique	Ruelas	Camacho	m	1962-06-10	01-01-22-0001-0005	f
7316520	Diego	Rayan	Martí	Madrid	m	1960-12-02	01-01-22-0001-0005	f
13741956	Saul	Oscar	Narváez	Rivero	m	1966-12-13	01-01-22-0001-0005	f
10082176	Guillermo	Jorge	Lovato	Soria	m	1959-11-28	01-01-22-0001-0005	f
11574051	Francisco	Eric	Casárez	Llamas	m	1965-03-22	01-01-22-0001-0005	f
12635147	Laia	Diana	Saavedra	Jáquez	f	1954-04-04	01-01-22-0001-0005	f
10550277	Natalia	Antonia	Roque	Delagarza	f	1954-08-13	01-01-22-0001-0005	f
5153382	Luna	Iria	Estrada	Guevara	f	1952-07-23	01-01-22-0001-0005	f
12212405	Marcos	Ander	Rodrígez	Baca	m	1955-03-18	01-01-22-0001-0005	f
11738468	Salma	Valeria	Fernández	Villareal	f	1964-03-03	01-01-22-0001-0005	f
6518118	Maria	Mireia	Muñóz	Plaza	f	1957-09-30	01-01-22-0001-0005	f
5410491	Adam	Dario	Barroso	Villalba	m	1952-08-17	01-01-22-0001-0005	f
12273461	Cesar	Jaime	Tejeda	Gamez	m	1962-12-31	01-01-22-0001-0005	f
5907856	Iria	Irene	Haro	Araña	f	1956-07-14	01-01-22-0001-0005	f
14190872	Domingo	Pol	Águilar	Terrazas	m	1960-10-01	01-01-22-0001-0005	f
12599003	Clara	Raquel	Leal	Cisneros	f	1963-02-10	01-01-22-0001-0005	f
11622947	Sandra	Isabel	Ybarra	Nájera	f	1956-05-14	01-01-22-0001-0005	f
13306102	Sebastian	Adrian	Ortega	Salinas	m	1962-11-11	01-01-22-0001-0005	f
8072169	Blanca	Olivia	Delgadillo	Núñez	f	1964-04-15	01-01-22-0001-0005	f
11180007	Eric	Sebastian	Lozada	Esquibel	m	1959-07-02	01-01-22-0001-0005	f
9776339	Laia	Martina	Pagan	Tirado	f	1954-08-03	01-01-22-0001-0005	f
13598298	Ivan	Erik	Camarillo	Estévez	m	1953-01-16	01-01-22-0001-0005	f
9260271	Pol	Ismael	Lugo	Flores	m	1965-09-20	01-01-22-0001-0005	f
6207660	Eric	Juan	Simón	Tamayo	m	1953-09-11	01-01-22-0001-0005	f
7408441	Oswaldo	Juan	Oliver	Cabrera	m	1952-03-19	01-01-22-0001-0005	f
11120277	Manuela	Nayara	Cervantes	Izquierdo	f	1952-01-01	01-01-22-0001-0005	f
10812894	Miguel	Mateo	Cantú	Ruvalcaba	m	1964-03-23	01-01-22-0001-0005	f
8512925	Izan	Marcos	Bermúdez	Andrés	m	1955-08-14	01-01-22-0001-0005	f
13008504	Helena	Pau	Velásquez	Barrera	f	1963-10-27	01-01-22-0001-0005	f
10629465	Ignacio	Andres	Puente	Soler	m	1963-01-12	01-01-22-0001-0005	f
12574519	Marti	Paula	Reynoso	Millán	f	1968-07-06	01-01-22-0001-0005	f
12869570	Isabel	Diana	Barajas	Espino	f	1968-01-17	01-01-22-0001-0005	f
13535568	Manuela	Elsa	Santiago	Bernal	f	1964-07-04	01-01-22-0001-0005	f
14434001	Nayara	Mar	Rey	Andreu	f	1969-10-16	01-01-22-0001-0005	f
7702849	Cristina	Miriam	Olivares	Valle	f	1967-04-30	01-01-22-0001-0005	f
6790077	Alexia	Rocio	Alfonso	Mata	f	1959-06-21	01-01-22-0001-0005	f
12505371	Ivan	Gonzalo	Cornejo	Venegas	m	1965-12-15	01-01-22-0001-0005	f
11208740	Carla	Angela	Velásquez	Briones	f	1968-03-08	01-01-22-0001-0005	f
9255542	Hector	Pol	Curiel	Irizarry	m	1965-09-19	01-01-22-0001-0005	f
5356504	Ivan	Victor	Alonso	Gómez	m	1959-04-12	01-01-22-0001-0005	f
10964089	Ane	Salma	Olmos	Pantoja	f	1967-10-07	01-01-22-0001-0005	f
9467014	Carlota	Zulay	Quiñónez	Urbina	f	1956-12-29	01-01-22-0001-0005	f
14406786	Oswaldo	Miguel	Valle	Montoya	m	1963-06-24	01-01-22-0001-0005	f
10945113	Alexandra	Olivia	Farías	Oliva	f	1953-03-21	01-01-22-0001-0005	f
13226840	Adrian	Leonardo	Jimínez	Lomeli	m	1958-01-19	01-01-22-0001-0005	f
9847776	Miguel	Rayan	Ocampo	Benavídez	m	1965-05-15	01-01-22-0001-0005	f
11671587	Andres	Ignacio	Longoria	Delafuente	m	1957-01-18	01-01-22-0001-0005	f
6985836	Andres	Raul	Bravo	Marco	m	1955-08-07	01-01-22-0001-0005	f
5316235	Mireia	Ana	Santacruz	Nieves	f	1958-02-18	01-01-22-0001-0005	f
7677947	Jose	Cesar	Tijerina	Luque	m	1957-03-08	01-01-22-0001-0005	f
10911811	Fatima	Salma	Abrego	Vidal	f	1953-07-25	01-01-22-0001-0005	f
5914561	Gabriel	Victor	Giménez	Burgos	m	1956-05-03	01-01-22-0001-0005	f
10620474	Gabriela	Ana	Irizarry	Peres	f	1962-08-26	01-01-22-0001-0005	f
7087694	Adria	Jorge	Villalpando	Nájera	m	1966-05-24	01-01-22-0001-0005	f
7228165	Luisa	Aroa	Navarrete	Cortez	f	1954-02-13	01-01-22-0001-0005	f
9607679	Teresa	Noelia	Sosa	Rosales	f	1956-07-11	01-01-22-0001-0005	f
10723920	Paula	Daniela	Escobedo	Rolón	f	1965-06-03	01-01-22-0001-0005	f
7060226	Patricia	Silvia	Noriega	Vázquez	f	1955-03-04	01-01-22-0001-0005	f
7844388	Abril	Naiara	Echevarría	Casares	f	1957-02-08	01-01-22-0001-0005	f
14462272	David	Raul	Acuña	Arreola	m	1964-12-03	01-01-22-0001-0005	f
14951090	Marta	Carmen	Bermúdez	Moya	f	1962-04-22	01-01-22-0001-0005	f
14610939	Vera	Alexandra	Hernández	Arredondo	f	1966-01-06	01-01-22-0001-0005	f
5417487	Paula	Nadia	Aparicio	Domínquez	f	1962-11-23	01-01-22-0001-0005	f
9520900	Luis	Sergio	Cabrera	Casanova	m	1952-11-11	01-01-22-0001-0005	f
6397418	Naia	Ona	Carmona	Heredia	f	1956-08-25	01-01-22-0001-0005	f
5056601	Leandro	Yorman	Lemus	Cervántez	m	1960-12-07	01-01-22-0001-0005	f
12812192	Bella	Paula	Ozuna	Izquierdo	f	1966-04-01	01-01-22-0001-0005	f
12053095	Ona	Alma	Cobo	Giménez	f	1957-09-26	01-01-22-0001-0005	f
5306830	Noelia	Ariadna	Holguín	Simón	f	1969-04-30	01-01-22-0001-0005	f
6184243	Erik	Alberto	Arreola	Navarro	m	1956-06-15	01-01-22-0001-0005	f
11506242	Francisco	Jaime	Salinas	Olivo	m	1960-02-01	01-01-22-0001-0005	f
5194309	Elsa	Berta	Domenech	Alfaro	f	1964-03-12	01-01-22-0001-0005	f
8105251	Andres	Ignacio	Olivares	Urías	m	1953-01-20	01-01-22-0001-0005	f
13296616	Elena	Cristina	Estévez	Peralta	f	1958-02-02	01-01-22-0001-0005	f
5539501	Yaiza	Maria	Raya	Tovar	f	1968-08-17	01-01-22-0001-0005	f
11226505	Jimena	Noelia	Carrasco	Véliz	f	1956-12-11	01-01-22-0001-0005	f
5494028	Erik	Samuel	Castaño	Gurule	m	1955-05-15	01-01-22-0001-0005	f
14707144	Ian	Rodrigo	Urbina	Montoya	m	1961-08-22	01-01-22-0001-0005	f
12780953	Helena	Gabriela	Calvillo	Casárez	f	1960-03-05	01-01-22-0001-0005	f
11834662	Ainara	Lucia	Del río	Domenech	f	1955-01-04	01-01-22-0001-0005	f
8329942	Lidia	Mar	Niño	Luque	f	1963-02-27	01-01-22-0001-0005	f
6633510	Nahia	Carmen	De Jesús	Rael	f	1959-07-23	01-01-22-0001-0005	f
10665360	Jordi	Santiago	Delrío	Jimínez	m	1965-11-23	01-01-22-0001-0005	f
6674555	Oswaldo	Alejandro	Valverde	Pantoja	m	1965-08-14	01-01-22-0001-0005	f
5031262	Cristina	Emma	Salvador	Márquez	f	1965-12-29	01-01-22-0001-0005	f
7646273	Alba	Ane	Barrera	Meza	f	1953-06-27	01-01-22-0001-0005	f
10946315	Raquel	Elsa	Sanches	Roldan	f	1965-09-23	01-01-22-0001-0005	f
5639933	Clara	Ana	León	Benavides	f	1953-10-05	01-01-22-0001-0005	f
6053350	Jordi	Omar	Vélez	Lozano	m	1955-09-08	01-01-22-0001-0005	f
6938661	Zulay	Alicia	Soriano	Bermúdez	f	1952-08-14	01-01-22-0001-0005	f
6489255	Francisco Javier	Juan	Vélez	Rendón	m	1957-01-29	01-01-22-0001-0005	f
10754137	Leandro	Alex	Avilés	Quintero	m	1961-10-25	01-01-22-0001-0005	f
9849848	Saul	Adam	Pacheco	Salvador	m	1966-09-08	01-01-22-0001-0005	f
7703975	Blanca	Mar	Santillán	Delagarza	f	1960-03-14	01-01-22-0001-0005	f
6033764	Carla	Joan	Páez	Trujillo	f	1965-12-13	01-01-22-0001-0005	f
5766918	Cesar	Guillem	Gómez	Miranda	m	1967-01-28	01-01-22-0001-0005	f
11344957	Jana	Nora	Luján	Valenzuela	f	1956-07-03	01-01-22-0001-0005	f
13557020	Juan	Santiago	Duran	Barraza	m	1955-07-04	01-01-22-0001-0005	f
7655735	Marti	Eva	Collado	Sanches	f	1964-03-22	01-01-22-0001-0005	f
11047204	Domingo	Leo	Badillo	Castillo	m	1966-08-23	01-01-22-0001-0005	f
12589556	Roberto	Ivan	Trejo	Delarosa	m	1959-09-07	01-01-22-0001-0005	f
11417683	Ana	Maria	Terán	Marrero	f	1959-11-03	01-01-22-0001-0005	f
5173078	Miriam	Emma	Bravo	Báez	f	1959-09-24	01-01-22-0001-0005	f
10146313	Alonso	Martin	Covarrubias	Atencio	m	1952-07-28	01-01-22-0001-0005	f
8093985	Helena	Luisa	Gamboa	Luján	f	1962-03-12	01-01-22-0001-0005	f
7201196	Isaac	Pablo	Jiménez	Montañez	m	1953-11-18	01-01-22-0001-0005	f
8003020	Adam	Roberto	Becerra	Gálvez	m	1968-11-21	01-01-22-0001-0005	f
5925539	Nadia	Vega	Briseño	Ballesteros	f	1965-10-22	01-01-22-0001-0005	f
8824007	Oswaldo	Gonzalo	Dueñas	Arce	m	1955-03-24	01-01-22-0001-0005	f
13255446	Alejandro	Enrique	Rueda	García	m	1956-06-06	01-01-22-0001-0005	f
9671765	Ivan	Roberto	Barrera	Negrón	m	1960-03-04	01-01-22-0001-0005	f
12212957	Roberto	Sebastian	Banda	Soliz	m	1960-07-02	01-01-22-0001-0005	f
5002466	David	Raul	Rodríguez	Mendoza	m	1962-01-06	01-01-22-0001-0005	f
7848180	Laura	Candela	Herrera	Serrato	f	1958-12-20	01-01-22-0001-0005	f
9943063	Domingo	David	Delrío	Mojica	m	1955-11-30	01-01-22-0001-0005	f
10305605	Oscar	Oliver	Bueno	Griego	m	1960-10-18	01-01-22-0001-0005	f
10031066	Alejandra	Ona	Valle	Almanza	f	1954-11-30	01-01-22-0001-0005	f
8188622	Claudia	Jana	Puente	Guillen	f	1960-06-11	01-01-22-0001-0005	f
8369413	Cristian	Miguel	Abad	Castañeda	m	1964-10-24	01-01-22-0001-0005	f
5265427	Zulay	Luisa	Rolón	Banda	f	1969-12-27	01-01-22-0001-0005	f
7740422	Aroa	Luna	Pineda	Mares	f	1955-10-28	01-01-22-0001-0005	f
6180165	Daniel	Adria	González	Tejada	m	1959-12-11	CC-02-URB-0024	f
11625172	Isabel	Luna	Benito	Serra	f	1961-09-29	CC-02-URB-0024	f
9176285	Erika	Berta	Cervántez	Vidal	f	1964-05-05	CC-02-URB-0024	f
14657427	Lucas	Gabriel	Quintana	Puente	m	1959-02-08	CC-02-URB-0024	f
9863966	Mara	Leyre	Portillo	Venegas	f	1961-11-13	CC-02-URB-0024	f
8165353	Elena	Irene	Bañuelos	Griego	f	1963-05-22	CC-02-URB-0024	f
5008043	Pol	Joel	Sandoval	Briseño	m	1955-04-12	CC-02-URB-0024	f
10065198	Alba	Paola	Gutiérrez	Martínez	f	1968-01-29	CC-02-URB-0024	f
13922501	Adrian	Izan	Del río	Delapaz	m	1967-03-04	CC-02-URB-0024	f
8118545	Carlos	Victor	Castillo	Guevara	m	1957-06-16	CC-02-URB-0024	f
8103450	Roberto	Dario	Armenta	Cedillo	m	1957-11-24	CC-02-URB-0024	f
6890926	Hugo	Adam	Casado	Sancho	m	1964-01-15	CC-02-URB-0024	f
10121312	Jon	Alberto	Elizondo	Santillán	m	1967-04-30	CC-02-URB-0024	f
5399849	Javier	Adam	Ibarra	Acuña	m	1968-08-13	CC-02-URB-0024	f
9094191	Abril	Ona	Frías	Zelaya	f	1954-06-30	CC-02-URB-0024	f
14310416	Pol	Adam	Galindo	Niño	m	1969-12-13	CC-02-URB-0024	f
10653225	Eduardo	Alejandro	Tomas	Barrios	m	1953-10-01	CC-02-URB-0024	f
9802916	Mireia	Alejandra	Solorio	Avilés	f	1952-06-12	CC-02-URB-0024	f
9098413	Pol	Joel	Zapata	Pozo	m	1963-04-28	CC-02-URB-0024	f
6406292	Joel	Marcos	Fuentes	Riojas	m	1967-04-11	CC-02-URB-0024	f
5357098	Adria	Dario	Porras	Barajas	m	1966-12-24	CC-02-URB-0024	f
11341546	Nuria	Valentina	Altamirano	Ocampo	f	1964-01-17	CC-02-URB-0024	f
11206720	Irene	Alma	Lorenzo	Orozco	f	1955-04-14	CC-02-URB-0024	f
8309465	Jaime	Santiago	Solano	Alcántar	m	1953-11-06	CC-02-URB-0024	f
13099831	Oscar	Joel	Rosado	Sosa	m	1962-04-16	CC-02-URB-0024	f
6734072	Ariadna	Alexia	Banda	Pacheco	f	1966-04-10	CC-02-URB-0024	f
11383698	Paola	Ines	Estrada	Rangel	f	1968-04-16	CC-02-URB-0024	f
13865388	Salma	Candela	Zaragoza	Carballo	f	1962-09-07	CC-02-URB-0024	f
6130548	Adam	Aaron	Porras	Valdez	m	1965-12-26	CC-02-URB-0024	f
11666627	David	Alejandro	Elizondo	Román	m	1957-05-08	CC-02-URB-0024	f
7033786	Guillermo	Alex	Ulloa	Méndez	m	1954-12-02	CC-02-URB-0024	f
12772408	Joel	Francisco	De la torre	Bermejo	m	1964-04-12	CC-02-URB-0024	f
9687916	Berta	Natalia	Gimeno	Vega	f	1954-04-23	CC-02-URB-0024	f
8681055	Carolina	Martina	Lebrón	Aguayo	f	1967-10-10	CC-02-URB-0024	f
7831085	Izan	Saul	Esteban	Ybarra	m	1960-12-24	CC-02-URB-0024	f
6092204	Ruben	Alex	Duran	Velásquez	m	1954-01-02	CC-02-URB-0024	f
12233531	Yaiza	Claudia	Tello	Ponce	f	1966-03-21	CC-02-URB-0024	f
6576387	Guillem	Adam	Sánchez	Font	m	1959-07-24	CC-02-URB-0024	f
10794543	Nayara	Martina	Soria	Alejandro	f	1958-05-21	CC-02-URB-0024	f
13550008	Berta	Marti	Lucero	Araña	f	1962-05-24	CC-02-URB-0024	f
8077817	Nayara	Leire	Sepúlveda	Esquibel	f	1964-09-12	CC-02-URB-0024	f
8960999	Marco	Lucas	Delacrúz	Carretero	m	1958-01-16	CC-02-URB-0024	f
10708353	Ines	Marina	Ibarra	Diez	f	1968-01-25	CC-02-URB-0024	f
7806104	Andrea	Iria	Báez	Cintrón	f	1954-06-09	CC-02-URB-0024	f
8814019	Natalia	Ona	Piña	Viera	f	1968-04-09	CC-02-URB-0024	f
9069069	Elena	Marti	Bañuelos	Casillas	f	1964-07-26	CC-02-URB-0024	f
10048818	Oliver	Oscar	Pastor	Giménez	m	1955-11-24	CC-02-URB-0024	f
8327414	Luna	Abril	Verduzco	Acuña	f	1954-09-26	CC-02-URB-0024	f
14006745	Marti	Irene	León	Pichardo	f	1964-08-22	CC-02-URB-0024	f
12449739	Oliver	Hugo	Reyna	Casillas	m	1955-05-22	CC-02-URB-0024	f
8639597	Julia	Mara	Rendón	Menchaca	f	1956-08-30	CC-02-URB-0024	f
7971342	Nerea	Abril	Rascón	Velasco	f	1960-12-20	CC-02-URB-0024	f
14158174	Teresa	Sandra	Terrazas	Mendoza	f	1967-05-11	CC-02-URB-0024	f
10595838	Jose	Jorge	Nevárez	Arriaga	m	1954-02-10	CC-02-URB-0024	f
9561028	Ines	Leyre	Ramírez	Jaime	f	1969-01-21	CC-02-URB-0024	f
10081639	Joan	Nuria	Moreno	Delgado	f	1955-12-14	CC-02-URB-0024	f
8014198	Fatima	Jana	Rocha	Rael	f	1966-05-18	CC-02-URB-0024	f
7939390	Adria	Omar	Macias	Alba	m	1968-07-17	CC-02-URB-0024	f
11829874	Eva	Africa	Partida	Padrón	f	1962-04-06	CC-02-URB-0024	f
11371447	Leo	Angel	Segovia	Jasso	m	1962-08-01	CC-02-URB-0024	f
10740531	Jose	Manuel	Rojo	Pulido	m	1957-10-15	CC-02-URB-0024	f
12784205	Jaime	Alejandro	Franco	Sevilla	m	1953-12-23	CC-02-URB-0024	f
9231358	Irene	Lidia	Garrido	Calero	f	1966-12-01	CC-02-URB-0024	f
12959240	Fatima	Lucia	Contreras	Abrego	f	1963-11-20	CC-02-URB-0024	f
11765169	Adam	Jorge	Velázquez	Anaya	m	1958-02-07	CC-02-URB-0024	f
14138896	Alejandra	Paola	Mejía	Oliva	f	1957-07-01	CC-02-URB-0024	f
8525123	Cristian	Ismael	Lemus	Bahena	m	1953-07-31	CC-02-URB-0024	f
9365564	Noa	Sofia	Soler	Varela	f	1952-05-05	CC-02-URB-0024	f
10740861	Claudia	Elsa	Parra	Sevilla	f	1966-10-08	CC-02-URB-0024	f
8663877	Erik	Alberto	Aguayo	Correa	m	1953-02-13	CC-02-URB-0024	f
7892380	Isaac	Aaron	Delacrúz	Cabrera	m	1953-12-15	CC-02-URB-0024	f
14748793	Paula	Carlota	Benito	Palacios	f	1969-03-03	CC-02-URB-0024	f
8320253	Africa	Nil	Soto	Muñoz	f	1958-05-26	CC-02-URB-0024	f
5783707	Lidia	Blanca	Linares	Martín	f	1968-06-29	CC-02-URB-0024	f
14120996	Daniel	Oswaldo	Escobedo	Vásquez	m	1964-09-13	CC-02-URB-0024	f
8505677	Luis	Leonardo	De Jesús	Pizarro	m	1967-03-26	CC-02-URB-0024	f
7540418	Ander	Mario	Rueda	Badillo	m	1964-09-05	CC-02-URB-0024	f
11266811	Antonia	Helena	Cardona	Rivera	f	1962-01-09	CC-02-URB-0024	f
12755388	Candela	Victoria	Carranza	Longoria	f	1954-12-14	CC-02-URB-0024	f
11841875	Oliver	Jose	Lira	Redondo	m	1957-07-14	CC-02-URB-0024	f
7973369	Andrea	Yaiza	Armenta	Ocasio	f	1964-12-28	CC-02-URB-0024	f
10436563	Jorge	Alvaro	Luna	Antón	m	1958-11-18	CC-02-URB-0024	f
9344570	Beatriz	Ines	Garza	Duarte	f	1968-04-18	CC-02-URB-0024	f
7879457	Fernando	Guillem	Alejandro	Valencia	m	1961-12-25	CC-02-URB-0024	f
5511437	Marc	Martin	Mora	Jasso	m	1956-11-28	CC-02-URB-0024	f
7378076	Anna	Mar	Santillán	Franco	f	1968-09-03	CC-02-URB-0024	f
8800415	Erik	Leandro	Barela	Zamudio	m	1957-02-17	CC-02-URB-0024	f
6912805	Carlos	Marcos	Aguirre	Luis	m	1969-01-30	CC-02-URB-0024	f
9294092	Victoria	Miriam	Román	Vaca	f	1956-05-31	CC-02-URB-0024	f
5105792	Mario	Saul	Guillen	Salcedo	m	1963-09-14	CC-02-URB-0024	f
7596342	Eduardo	Alberto	Zayas	Escobedo	m	1968-05-09	CC-02-URB-0024	f
11117387	Elsa	Nerea	Nazario	Ibáñez	f	1952-02-16	CC-02-URB-0024	f
7410241	Santiago	Jesus	Espino	Rubio	m	1969-05-01	CC-02-URB-0024	f
14278758	Daniela	Maria	Rosales	Tapia	f	1954-01-05	CC-02-URB-0024	f
8273162	Valentina	Candela	Valdez	De Jesús	f	1962-12-06	CC-02-URB-0024	f
7474126	Jaime	Ruben	Trujillo	Ybarra	m	1967-11-14	CC-02-URB-0024	f
5461870	Irene	Clara	Vélez	Martín	f	1955-11-29	CC-02-URB-0024	f
11668136	Ona	Nayara	Zepeda	Serna	f	1969-07-30	CC-02-URB-0024	f
12772966	Iria	Isabel	Orta	Alvarado	f	1966-08-22	CC-02-URB-0024	f
11085306	Rafael	Cristian	Razo	Tapia	m	1953-04-03	CC-02-URB-0024	f
7090039	Antonio	Rayan	Martín	Haro	m	1967-05-05	CC-02-URB-0024	f
5739873	Erik	Mohamed	Mateos	Delagarza	m	1962-04-04	CC-02-URB-0024	f
13256239	Teresa	Irene	Concepción	Almaraz	f	1961-10-20	CC-02-URB-0024	f
7267257	Yaiza	Claudia	Carrión	Cuellar	f	1958-06-22	CC-02-URB-0024	f
10657676	Marina	Carmen	Ruelas	Arriaga	f	1954-05-23	CC-02-URB-0024	f
6433197	Alexandra	Adriana	Cuevas	Armenta	f	1956-06-29	CC-02-URB-0024	f
9267066	Nora	Nil	Roque	Rodríquez	f	1956-04-09	CC-02-URB-0024	f
5557895	Domingo	Francisco	Cortez	Leiva	m	1960-05-09	CC-02-URB-0024	f
6583166	Ivan	Erik	Corral	Vidal	m	1968-06-27	CC-02-URB-0024	f
6241229	Oscar	Mario	Valentín	Garrido	m	1965-10-01	CC-02-URB-0024	f
13521862	Ainara	Martina	Ojeda	Haro	f	1958-08-23	CC-02-URB-0024	f
10085054	Leo	Gonzalo	Muro	Mas	m	1959-06-21	CC-02-URB-0024	f
10464944	Raul	Leo	Muñiz	Abad	m	1968-11-27	CC-02-URB-0024	f
14871271	Sebastian	Izan	Lomeli	Ureña	m	1963-10-13	CC-02-URB-0024	f
9070686	Nadia	Lola	Molina	Rodarte	f	1966-10-21	CC-02-URB-0024	f
7097625	Claudia	Victoria	Carrillo	Bermejo	f	1965-12-13	CC-02-URB-0024	f
12505598	Nerea	Claudia	Guzmán	Vásquez	f	1962-05-12	CC-02-URB-0024	f
8786653	Lara	Leyre	Medrano	Colón	f	1965-12-19	CC-02-URB-0024	f
7785770	Daniela	Jana	Sotelo	Ceja	f	1958-05-02	CC-02-URB-0024	f
9002339	Adam	Gonzalo	Casares	Reséndez	m	1962-01-10	CC-02-URB-0024	f
5013274	Angel	Leo	Delvalle	Carreón	m	1959-11-10	CC-02-URB-0024	f
8836694	Sofia	Candela	Tirado	Sáenz	f	1960-04-10	CC-02-URB-0024	f
14314674	Miguel	Jesus	Zamudio	Colón	m	1957-01-09	CC-02-URB-0024	f
10078614	Jordi	Ivan	Alanis	Marcos	m	1957-06-29	CC-02-URB-0024	f
6573749	Angela	Pau	García	Esquibel	f	1953-09-21	CC-02-URB-0024	f
14320431	Carlos	Jordi	Huerta	Mendoza	m	1967-08-07	CC-02-URB-0024	f
11480939	Jon	Rafael	Cintrón	Lozano	m	1968-08-30	CC-02-URB-0024	f
9597294	Pablo	Domingo	Contreras	Covarrubias	m	1962-02-21	CC-02-URB-0024	f
9722305	Gabriel	Pol	Menéndez	Toro	m	1955-01-16	CC-02-URB-0024	f
9864029	Isaac	Joel	Tijerina	Escalante	m	1960-08-09	CC-02-URB-0024	f
5114350	Alejandro	Oswaldo	Arreola	Prado	m	1955-12-15	CC-02-URB-0024	f
14415171	Teresa	Alicia	Fierro	Cuellar	f	1966-02-05	CC-02-URB-0024	f
8998862	Ane	Leyre	Amador	Jaimes	f	1968-12-23	CC-02-URB-0024	f
14264104	Samuel	Ander	Pichardo	Rodrigo	m	1962-09-17	CC-02-URB-0024	f
13808718	Guillem	Fernando	Garza	Escribano	m	1957-08-05	CC-02-URB-0024	f
5466042	Miguel	Jesus	Calderón	Carretero	m	1963-02-06	CC-02-URB-0024	f
11433429	Candela	Sandra	Cardona	Paredes	f	1952-09-14	CC-02-URB-0024	f
8617540	Beatriz	Alicia	Preciado	Navarro	f	1951-12-08	CC-02-URB-0024	f
6919620	Nuria	Abril	Limón	Tomas	f	1956-12-26	CC-02-URB-0024	f
11911037	Saul	Ismael	Reyes	Bahena	m	1954-09-04	CC-02-URB-0024	f
10025598	Mateo	Leonardo	Carreón	Vaca	m	1967-12-27	CC-02-URB-0024	f
8876169	Olivia	Mar	Castaño	Parra	f	1966-01-30	CC-02-URB-0024	f
14837807	Leonardo	Rafael	Gómez	Marrero	m	1967-10-25	CC-02-URB-0024	f
11871883	Berta	Silvia	Acuña	Madrid	f	1961-01-10	CC-02-URB-0024	f
12921331	Joel	Manuel	Lorente	Ramón	m	1965-05-05	CC-02-URB-0024	f
6161839	Adriana	Manuela	Diez	Meraz	f	1955-02-24	CC-02-URB-0024	f
8067019	Laia	Daniela	Sanabria	Aguado	f	1955-03-11	CC-02-URB-0024	f
9898038	Victor	Erik	Haro	Castro	m	1954-05-15	CC-02-URB-0024	f
12544387	Jaime	Mario	Maldonado	Alicea	m	1963-12-22	CC-02-URB-0024	f
12605089	Sandra	Carla	Castro	Perea	f	1951-11-22	CC-02-URB-0024	f
13376308	Jana	Marti	Acevedo	Quesada	f	1961-05-14	CC-02-URB-0024	f
6721882	Leo	Guillem	Caldera	Dávila	m	1958-11-13	CC-02-URB-0024	f
14011491	Diego	Hector	Chacón	Viera	m	1953-08-27	CC-02-URB-0024	f
10623365	Luis	Diego	Zayas	Carvajal	m	1957-07-18	CC-02-URB-0024	f
7742953	Rafael	Gonzalo	Vanegas	Molina	m	1957-02-13	CC-02-URB-0024	f
8470155	Rodrigo	Jaime	Muñiz	Plaza	m	1954-04-19	CC-02-URB-0024	f
7585709	Andrea	Mara	Munguía	Santillán	f	1962-03-24	CC-02-URB-0024	f
9335883	Alba	Carmen	Delagarza	Solorzano	f	1966-04-16	CC-02-URB-0024	f
6077722	Pablo	Sergio	Rosario	Briseño	m	1961-09-24	CC-02-URB-0024	f
9534585	Omar	Mateo	Varela	Aranda	m	1954-08-02	CC-02-URB-0024	f
7217939	Olivia	Manuela	Adorno	Bonilla	f	1959-07-16	CC-02-URB-0024	f
8610221	Rayan	Oscar	Carbajal	Almanza	m	1958-07-28	CC-02-URB-0024	f
11752268	Lola	Alejandra	Jaime	Solorio	f	1956-02-11	CC-02-URB-0024	f
7455373	Marc	Jesus	Mayorga	Barela	m	1964-09-05	CC-02-URB-0024	f
7978264	Domingo	Ismael	Calero	Negrón	m	1957-05-22	CC-02-URB-0024	f
9098840	Carolina	Martina	Tirado	Haro	f	1955-02-28	CC-02-URB-0024	f
14213419	Dario	Erik	Pedroza	Muñiz	m	1953-12-16	CC-02-URB-0024	f
13589537	Mar	Aroa	Apodaca	Zepeda	f	1959-10-24	CC-02-URB-0024	f
8522649	Lucia	Ainara	Cuevas	Carrión	f	1965-12-11	CC-02-URB-0024	f
7968322	Miguel	Jordi	Heredia	Vásquez	m	1951-12-11	CC-02-URB-0024	f
12842152	Cristian	Oscar	Del río	Cabello	m	1956-07-30	CC-02-URB-0024	f
12228214	Aleix	Nicolas	Valles	Casado	m	1962-08-24	CC-02-URB-0024	f
9369915	Mario	Leandro	Camacho	Guajardo	m	1966-04-30	CC-02-URB-0024	f
9916225	Nadia	Aroa	Valadez	Zamudio	f	1963-05-25	CC-02-URB-0024	f
11932489	Alejandra	Mireia	Tello	Monroy	f	1953-04-17	CC-02-URB-0024	f
14136726	Ivan	Bruno	Giménez	Canales	m	1967-04-15	CC-02-URB-0024	f
7106556	Diana	Leire	Ayala	Lucas	f	1964-04-18	CC-02-URB-0024	f
9308054	Isabel	Celia	Quesada	Luján	f	1955-04-03	CC-02-URB-0024	f
11622679	Nayara	Claudia	Barreto	Moreno	f	1956-08-04	CC-02-URB-0024	f
5576905	Alexia	Lucia	Bañuelos	Jaime	f	1958-08-06	CC-02-URB-0024	f
11325327	Pablo	Rayan	Cavazos	Rojo	m	1966-11-21	CC-02-URB-0024	f
8838079	Jaime	Gonzalo	Lerma	Tamayo	m	1967-10-15	CC-02-URB-0024	f
7128322	Saul	Francisco	Gimeno	Valenzuela	m	1953-11-21	CC-02-URB-0024	f
9771424	Yorman	Victor	Gallego	Quintanilla	m	1956-11-26	CC-02-URB-0024	f
6330597	Eduardo	Rayan	Candelaria	Moreno	m	1959-10-26	CC-02-URB-0024	f
9028060	Marta	Jimena	Vila	Berríos	f	1968-08-28	CC-02-URB-0024	f
8333484	Diana	Ona	Alcala	Malave	f	1964-12-12	CC-02-URB-0024	f
9589156	Helena	Laia	Pacheco	Quintana	f	1953-09-03	CC-02-URB-0024	f
6602453	Pablo	Alonso	Lozada	Casillas	m	1952-11-21	CC-02-URB-0024	f
9004179	Ariadna	Raquel	Mireles	Martí	f	1968-02-08	CC-02-URB-0024	f
11944721	Eric	Jose	Villegas	Lemus	m	1959-11-27	CC-02-URB-0024	f
11025186	Saul	Erik	Casárez	Urrutia	m	1964-08-11	CC-02-URB-0024	f
7397836	Martin	Ignacio	Padrón	Oropeza	m	1966-06-14	CC-02-URB-0024	f
14017547	Ivan	Joel	Sevilla	Barroso	m	1969-03-19	CC-02-URB-0024	f
8535837	Mireia	Ines	Alvarado	Zamora	f	1962-02-28	CC-02-URB-0024	f
5520434	Vega	Paola	Caballero	Hernández	f	1954-01-20	CC-02-URB-0024	f
11952595	Clara	Silvia	Pelayo	Romo	f	1960-09-22	CC-02-URB-0024	f
11253503	Valeria	Carolina	Nieto	Gonzales	f	1964-03-15	CC-02-URB-0024	f
9255855	Gonzalo	Miguel	Valdivia	Hinojosa	m	1952-10-13	CC-02-URB-0024	f
11605757	Anna	Jana	Lucas	Robles	f	1960-07-21	CC-02-URB-0024	f
10795121	Abril	Sofia	López	Diez	f	1969-07-02	CC-02-URB-0024	f
10698523	Maria	Angela	Gallego	Ortiz	f	1959-09-03	CC-02-URB-0024	f
7957161	Eric	Francisco	Camacho	Polo	m	1958-02-13	CC-02-URB-0024	f
5736336	Pedro	Miguel	Fierro	Jimínez	m	1962-03-31	CC-02-URB-0024	f
6429773	Jana	Elsa	Carbajal	Delarosa	f	1953-08-14	CC-02-URB-0024	f
14039443	Berta	Berta	Conde	Benítez	f	1956-02-07	CC-02-URB-0024	f
6629464	Oswaldo	Oswaldo	Carrion	Loya	m	1959-06-30	CC-02-URB-0024	f
14048519	Isabel	Erika	Soler	Guerra	f	1962-01-25	CC-02-URB-0024	f
5690077	Lara	Carla	Cuenca	Villegas	f	1962-10-09	CC-02-URB-0024	f
10413022	Daniel	Roberto	Pons	Granados	m	1968-12-05	CC-02-URB-0024	f
6495606	Joel	Alvaro	Verduzco	Millán	m	1961-12-12	CC-02-URB-0024	f
11109245	Nora	Victoria	Trejo	Castellano	f	1957-01-18	CC-02-URB-0024	f
10820565	Marcos	Alex	Otero	Arce	m	1960-02-03	CC-02-URB-0024	f
13841635	Aleix	Alejandro	Reina	Navas	m	1961-12-31	CC-02-URB-0024	f
13190606	Nayara	Daniela	Benavídez	Chapa	f	1965-10-13	CC-02-URB-0024	f
5713887	Maria	Emma	Gaitán	Jaimes	f	1969-06-11	CC-02-URB-0024	f
6313054	Leyre	Zulay	Villareal	Bueno	f	1952-03-10	CC-02-URB-0024	f
10202465	Izan	Luis	Martí	Ros	m	1953-06-22	CC-02-URB-0024	f
7807034	Alba	Ona	Alvarado	Bautista	f	1964-02-21	CC-02-URB-0024	f
11213596	Luna	Naia	Varela	Rodríguez	f	1963-01-10	CC-02-URB-0024	f
5572972	Hugo	Joel	Sotelo	Cepeda	m	1952-01-22	CC-02-URB-0024	f
14558096	Gabriel	Sergio	Ocasio	Almaraz	m	1963-08-09	CC-02-URB-0024	f
5206674	Guillermo	Leandro	Loya	Lorente	m	1959-09-19	CC-02-URB-0024	f
10379866	Alonso	Aaron	Lázaro	Lorente	m	1966-02-25	CC-02-URB-0024	f
6314662	Marti	Beatriz	Carreón	Arredondo	f	1966-11-30	CC-02-URB-0024	f
6802063	Jimena	Bella	Rosa	Arellano	f	1968-07-09	CC-02-URB-0024	f
8027845	Luna	Andrea	Páez	Heredia	f	1958-01-05	CC-02-URB-0024	f
11899638	Beatriz	Carmen	Preciado	Valencia	f	1963-06-09	CC-02-URB-0024	f
9971839	Leandro	Adrian	Mendoza	Esquibel	m	1952-09-30	CC-02-URB-0024	f
14156517	Leyre	Mireia	Soriano	Casas	f	1964-11-15	CC-02-URB-0024	f
12796234	Celia	Candela	Almaraz	Valenzuela	f	1963-06-05	CC-02-URB-0024	f
7311942	Ivan	Rafael	Guillen	Carballo	m	1952-06-23	CC-02-URB-0024	f
10615249	Elsa	Olivia	Casado	Razo	f	1969-12-04	CC-02-URB-0024	f
5594904	Jesus	Victor	Altamirano	Tafoya	m	1952-09-24	CC-02-URB-0024	f
6926423	Alberto	Francisco	Esquibel	Oliva	m	1965-06-15	CC-02-URB-0024	f
11085080	Irene	Lara	Ocasio	Asensio	f	1954-07-29	CC-02-URB-0024	f
11804224	Cristian	Leo	Lugo	Martos	m	1965-03-22	CC-02-URB-0024	f
7313769	Olivia	Lidia	Dueñas	Casares	f	1958-10-08	CC-02-URB-0024	f
10265609	Hugo	Hugo	Aguayo	Briseño	m	1961-02-27	CC-02-URB-0024	f
12261410	Alejandro	Miguel	Delrío	Zayas	m	1961-11-02	CC-02-URB-0024	f
10448702	Omar	Domingo	Abreu	Juárez	m	1968-04-04	CC-02-URB-0024	f
13933809	Bruno	Miguel	García	Ortega	m	1959-01-23	CC-02-URB-0024	f
9049571	Miguel	Oliver	Valladares	Estrada	m	1963-06-12	CC-02-URB-0024	f
5704153	Alexia	Celia	Ponce	Moya	f	1967-03-01	CC-02-URB-0024	f
13774192	Gonzalo	Enrique	Del río	Rojas	m	1952-05-14	CC-02-URB-0024	f
5324926	Irene	Alejandra	Trejo	Lira	f	1959-08-02	CC-02-URB-0024	f
6447211	Mar	Ane	Arredondo	Rey	f	1957-09-30	CC-02-URB-0024	f
9472077	Lidia	Rocio	Valero	Rojas	f	1953-01-20	CC-02-URB-0024	f
9357404	Dario	Martin	Escribano	Aponte	m	1963-05-07	CC-02-URB-0024	f
13971481	Vera	Emma	Prado	Ocasio	f	1953-09-17	CC-02-URB-0024	f
11057898	Miguel	Eduardo	Parra	Candelaria	m	1966-10-22	CC-02-URB-0024	f
7477307	Anna	Nadia	Medrano	Enríquez	f	1961-09-29	CC-02-URB-0024	f
7773876	Andres	Enrique	Urbina	Andrés	m	1959-03-24	CC-02-URB-0024	f
9952635	Rodrigo	Sebastian	Lovato	Lomeli	m	1965-10-25	CC-02-URB-0024	f
9255314	Jaime	Gabriel	Villagómez	Castro	m	1968-07-23	CC-02-URB-0024	f
6094796	Ian	Gonzalo	Ibarra	Olvera	m	1957-11-06	CC-02-URB-0024	f
5450989	Roberto	Leandro	Bonilla	Feliciano	m	1953-05-11	CC-02-URB-0024	f
7030369	Enrique	Jose	Delrío	Marrero	m	1964-05-19	CC-02-URB-0024	f
13602507	Pedro	Cesar	Mas	Lerma	m	1957-01-04	CC-02-URB-0024	f
6599201	Adam	Ian	Arriaga	Báez	m	1960-05-19	CC-02-URB-0024	f
13481763	Valeria	Nadia	Redondo	Fuentes	f	1958-01-15	CC-02-URB-0024	f
7769292	Ander	Antonio	Ibarra	Tafoya	m	1968-11-27	CC-02-URB-0024	f
5217406	Alicia	Vera	Nieto	Roldán	f	1963-07-27	CC-02-URB-0024	f
9300295	Zulay	Mar	Aguayo	Alanis	f	1962-07-30	CC-02-URB-0024	f
13568433	Lola	Nil	Gómez	Alvarado	f	1961-01-16	CC-02-URB-0024	f
12461955	Alexandra	Teresa	Matos	Sanabria	f	1953-08-06	CC-02-URB-0024	f
11588962	Guillermo	Mario	Pabón	Caraballo	m	1969-11-23	CC-02-URB-0024	f
12296827	Bruno	Ivan	Magaña	Razo	m	1965-06-17	CC-02-URB-0024	f
6130938	Leandro	Dario	Orta	Rodríguez	m	1963-07-25	CC-02-URB-0024	f
8031189	Paola	Alexia	Pineda	Domenech	f	1955-06-18	CC-02-URB-0024	f
14005918	Rafael	Mohamed	Vélez	Muñiz	m	1957-02-23	CC-02-URB-0024	f
10163305	Gabriel	Saul	Caldera	Reynoso	m	1969-11-29	CC-02-URB-0024	f
9013696	Nerea	Emma	Pacheco	Zúñiga	f	1959-12-19	CC-02-URB-0024	f
11511396	Noelia	Abril	Barraza	Villar	f	1967-12-19	CC-02-URB-0024	f
14154515	Ines	Raquel	Esquibel	Briseño	f	1952-11-05	CC-02-URB-0024	f
13192158	Alejandra	Salma	Oliva	Román	f	1953-12-24	CC-02-URB-0024	f
11383277	Jordi	Gabriel	Sepúlveda	Sedillo	m	1954-08-19	CC-02-URB-0024	f
5056285	Vega	Joan	Grijalva	Abeyta	f	1965-05-14	CC-02-URB-0024	f
12671268	Sebastian	Ander	Sevilla	Ordoñez	m	1967-07-17	CC-02-URB-0024	f
14515335	Valeria	Patricia	Maestas	Ramón	f	1961-07-26	CC-02-URB-0024	f
10621535	Maria	Sofia	Ontiveros	Ayala	f	1964-07-02	CC-02-URB-0024	f
8533740	Guillermo	Antonio	Moran	Delagarza	m	1954-01-13	CC-02-URB-0024	f
7471819	Jan	Eduardo	Rivero	Garica	m	1956-02-16	CC-02-URB-0024	f
5225464	Adria	Carlos	Tello	Diez	m	1959-07-17	CC-02-URB-0024	f
9759197	Gabriel	Hugo	Arce	Cuevas	m	1953-03-19	CC-02-URB-0024	f
12117192	Ian	Domingo	Serrano	Matos	m	1952-03-20	CC-02-URB-0024	f
10413845	Nil	Emma	Escalante	Jaramillo	f	1957-02-14	CC-02-URB-0024	f
9695262	Isabel	Antonia	Gutiérrez	Franco	f	1967-10-27	CC-02-URB-0024	f
12882067	Nahia	Nahia	Aragón	Leyva	f	1954-07-30	CC-02-URB-0024	f
6729905	Irene	Elsa	Maya	Sepúlveda	f	1965-10-19	CC-02-URB-0024	f
13303167	Abril	Natalia	Nieves	Alcala	f	1966-11-21	CC-02-URB-0024	f
7040887	Miguel	Bruno	Magaña	Ochoa	m	1969-04-05	CC-02-URB-0024	f
9140820	Eduardo	Alvaro	Soriano	Alcala	m	1967-08-28	CC-02-URB-0024	f
5530989	Eva	Lucia	Coronado	Deleón	f	1965-03-26	CC-02-URB-0024	f
13079466	Antonia	Naiara	Ros	Rosas	f	1956-09-15	CC-02-URB-0024	f
6887038	Victoria	Celia	Muñiz	Barajas	f	1961-09-18	CC-02-URB-0024	f
13795567	Erik	Martin	Cervantes	Gamez	m	1959-08-27	CC-02-URB-0024	f
13261483	Jan	Pol	Sisneros	Menchaca	m	1969-10-05	CC-02-URB-0024	f
10050589	Antonio	Mohamed	Alejandro	Mascareñas	m	1956-10-16	CC-02-URB-0024	f
14108146	Bruno	Enrique	Franco	Chacón	m	1958-03-21	CC-02-URB-0024	f
12479251	Noa	Carmen	Miguel	Loya	f	1968-12-06	CC-02-URB-0024	f
6292066	Jesus	Enrique	Treviño	Girón	m	1960-06-29	CC-02-URB-0024	f
9600975	Rodrigo	Angel	Jiménez	Carranza	m	1966-01-29	CC-02-URB-0024	f
5405936	Francisco Javier	Pol	Aguilar	Verduzco	m	1968-12-23	CC-02-URB-0024	f
12400262	Nora	Anna	Limón	Vela	f	1961-02-16	01-01-22-001-0008	f
14497917	Francisco	Gabriel	Esteban	Saldaña	m	1963-10-18	01-01-22-001-0008	f
6285882	Fernando	Joel	Olivo	Leal	m	1953-03-08	01-01-22-001-0008	f
5785706	Clara	Erika	Perea	Carrión	f	1964-12-02	01-01-22-001-0008	f
10047778	Mario	Javier	Carrillo	Meléndez	m	1963-02-17	01-01-22-001-0008	f
5228005	Sandra	Martina	Roque	Araña	f	1967-09-21	01-01-22-001-0008	f
8700984	Omar	Ivan	Pereira	Oliver	m	1968-03-01	01-01-22-001-0008	f
6478719	Pau	Nora	Ferrer	Meléndez	f	1961-01-02	01-01-22-001-0008	f
7256791	Jesus	Roberto	Barajas	Méndez	m	1954-12-25	01-01-22-001-0008	f
5995200	Jorge	Jorge	Sancho	Verduzco	m	1965-12-28	01-01-22-001-0008	f
12539340	Lucia	Raquel	Rosas	Pagan	f	1967-03-24	01-01-22-001-0008	f
10415113	Alma	Carlota	Rubio	Guevara	f	1958-02-23	01-01-22-001-0008	f
10900570	Mateo	Roberto	Rubio	Alaniz	m	1952-06-13	01-01-22-001-0008	f
14700803	Iria	Laura	Rubio	Serra	f	1958-12-09	01-01-22-001-0008	f
8608046	Paula	Salma	Sáenz	Sotelo	f	1965-10-19	01-01-22-001-0008	f
11117864	Ivan	Oscar	Delatorre	Crespo	m	1965-05-18	01-01-22-001-0008	f
9063222	Miriam	Yaiza	Peres	Mena	f	1962-02-07	01-01-22-001-0008	f
10669597	Paula	Naiara	Mondragón	Negrón	f	1959-04-04	01-01-22-001-0008	f
12115247	Jan	Alberto	Lozada	Partida	m	1962-07-15	01-01-22-001-0008	f
7925261	Ignacio	Gonzalo	Lucas	Cardona	m	1959-10-26	01-01-22-001-0008	f
7291850	Mohamed	Victor	Zelaya	Delgado	m	1961-10-03	01-01-22-001-0008	f
14755454	Silvia	Abril	Adorno	Rosa	f	1952-02-08	01-01-22-001-0008	f
12654680	Raul	Antonio	Ocasio	Alejandro	m	1967-06-06	01-01-22-001-0008	f
14788480	Laia	Marta	Valdez	Delgadillo	f	1958-10-06	01-01-22-001-0008	f
10104783	Aleix	Isaac	Lira	Alarcón	m	1963-05-25	01-01-22-001-0008	f
6839024	Anna	Vera	Grijalva	Cotto	f	1955-05-24	01-01-22-001-0008	f
8478110	Rodrigo	Jon	Aponte	Cervántez	m	1952-09-18	01-01-22-001-0008	f
10387648	Jorge	Jordi	Maestas	Tórrez	m	1963-04-17	01-01-22-001-0008	f
10875835	Natalia	Nadia	Varela	Zepeda	f	1964-09-26	01-01-22-001-0008	f
12886977	Naia	Vera	Tomas	Rueda	f	1957-09-29	01-01-22-001-0008	f
7205593	Sergio	Mohamed	Malave	Cortés	m	1969-06-21	01-01-22-001-0008	f
8356432	Oliver	Hector	Ornelas	Jaimes	m	1961-12-09	01-01-22-001-0008	f
6513147	Clara	Nil	Sotelo	Vila	f	1964-06-09	01-01-22-001-0008	f
12181289	Ainara	Ane	De Anda	Sierra	f	1955-12-17	01-01-22-001-0008	f
5543744	Aleix	Domingo	Colón	Miranda	m	1954-08-12	01-01-22-001-0008	f
5510465	Ismael	Ivan	Collado	Segura	m	1962-04-24	01-01-22-001-0008	f
9171814	Joan	Teresa	Fuentes	Saucedo	f	1968-01-22	01-01-22-001-0008	f
12682919	Marta	Antonia	García	Esquibel	f	1966-05-21	01-01-22-001-0008	f
6649100	Lola	Marta	Zayas	Mena	f	1963-12-14	01-01-22-001-0008	f
12864583	Paola	Jana	Domínguez	Delarosa	f	1956-06-04	01-01-22-001-0008	f
5768075	Ismael	Jon	Villaseñor	Jasso	m	1951-11-21	01-01-22-001-0008	f
5071363	Diego	Guillem	Urrutia	Soriano	m	1956-05-10	01-01-22-001-0008	f
8540531	Irene	Natalia	Ibarra	Zapata	f	1952-07-28	01-01-22-001-0008	f
6027799	Isaac	Hector	Santiago	Estrada	m	1963-10-07	01-01-22-001-0008	f
8423714	Abril	Victoria	Meza	Peres	f	1952-06-20	01-01-22-001-0008	f
13396565	Pol	Guillermo	Barroso	Melgar	m	1966-11-20	01-01-22-001-0008	f
8239688	Joan	Ainara	Rueda	Roque	f	1963-07-10	01-01-22-001-0008	f
8324891	Hugo	Mario	Cazares	Vicente	m	1956-08-16	01-01-22-001-0008	f
11728213	Victoria	Berta	Aranda	Aparicio	f	1956-06-19	01-01-22-001-0008	f
13214454	Santiago	Rayan	Saavedra	Lara	m	1952-02-05	01-01-22-001-0008	f
8613108	Andrea	Daniela	Abrego	Regalado	f	1958-01-22	01-01-22-001-0008	f
12287799	Anna	Blanca	Sedillo	De Anda	f	1957-04-13	01-01-22-001-0008	f
7874588	Alex	Guillem	Pedraza	Carbonell	m	1955-10-08	01-01-22-001-0008	f
7995664	Santiago	Mateo	Angulo	Murillo	m	1965-10-17	01-01-22-001-0008	f
7936878	Isaac	Pedro	Esteban	Villareal	m	1963-11-02	01-01-22-001-0008	f
13171089	Nora	Alexandra	Marrero	Esparza	f	1959-01-02	01-01-22-001-0008	f
6121981	Ines	Marta	Pabón	Medina	f	1963-01-16	01-01-22-001-0008	f
11954849	Naiara	Ona	Monroy	Rivas	f	1967-09-18	01-01-22-001-0008	f
5082271	Aleix	Alejandro	Luevano	Vanegas	m	1969-11-05	01-01-22-001-0008	f
10802792	Carlos	Hector	Linares	Martos	m	1962-04-15	01-01-22-001-0008	f
11828126	Saul	Diego	Jaimes	Valdez	m	1969-02-17	01-01-22-001-0008	f
7895753	Nerea	Miriam	Delagarza	Bermejo	f	1966-10-26	01-01-22-001-0008	f
6998334	Fatima	Daniela	Ojeda	Alvarado	f	1959-02-22	01-01-22-001-0008	f
5330485	Pau	Martina	Granados	Olivo	f	1954-05-14	01-01-22-001-0008	f
12110079	Beatriz	Vega	Valenzuela	Cerda	f	1962-06-24	01-01-22-001-0008	f
7509867	Aleix	Cristian	Camarillo	Esteban	m	1967-09-14	01-01-22-001-0008	f
5557385	Rodrigo	Alberto	Rey	Puig	m	1965-02-24	01-01-22-001-0008	f
6981338	Cristian	Pol	Cedillo	Fernández	m	1961-05-20	01-01-22-001-0008	f
14480699	Jaime	Ian	Villaseñor	Bueno	m	1955-12-08	01-01-22-001-0008	f
14313795	Laia	Lola	Aponte	Gastélum	f	1957-10-29	01-01-22-001-0008	f
7319490	Jimena	Beatriz	Serrato	Baeza	f	1958-11-09	01-01-22-001-0008	f
8365073	Alicia	Carmen	Mas	Montaño	f	1966-08-01	01-01-22-001-0008	f
9650229	Raquel	Natalia	Riera	Altamirano	f	1958-05-28	01-01-22-001-0008	f
14197863	Hugo	Roberto	Madrid	Bermejo	m	1969-08-14	01-01-22-001-0008	f
14254930	Ismael	Lucas	Ruiz	Badillo	m	1963-02-25	01-01-22-001-0008	f
9635868	Erik	Pol	Padrón	Corrales	m	1962-05-20	01-01-22-001-0008	f
8957230	Rodrigo	Pol	Arenas	Montez	m	1965-05-29	01-01-22-001-0008	f
8691061	Lidia	Pau	Polanco	Muñóz	f	1968-12-02	01-01-22-001-0008	f
5859551	Alex	Rayan	Orosco	Monroy	m	1961-07-20	01-01-22-001-0008	f
11693821	Adrian	Jordi	Costa	Corona	m	1964-06-01	01-01-22-001-0008	f
11299236	Eduardo	Alberto	Delatorre	Amaya	m	1966-05-01	01-01-22-001-0008	f
12749849	David	Pablo	Razo	Bermejo	m	1957-09-14	01-01-22-001-0008	f
11098120	Oswaldo	Leo	Botello	Meza	m	1959-07-23	01-01-22-001-0008	f
7188778	Raquel	Carla	Tirado	Medrano	f	1956-10-31	01-01-22-001-0008	f
7663033	Oscar	Ignacio	Valle	Baca	m	1964-03-11	01-01-22-001-0008	f
8369033	Eric	Saul	Sola	Mota	m	1963-02-06	01-01-22-001-0008	f
9244913	Vera	Helena	Bueno	Valencia	f	1958-11-06	01-01-22-001-0008	f
13624121	Alicia	Leyre	Partida	Madrigal	f	1969-07-29	01-01-22-001-0008	f
13254231	Claudia	Lara	Hurtado	Urrutia	f	1952-06-18	01-01-22-001-0008	f
8366953	Gabriela	Sara	Sandoval	Santos	f	1966-01-29	01-01-22-001-0008	f
10734784	Yorman	Guillermo	Araña	Varela	m	1953-04-03	01-01-22-001-0008	f
7776751	Domingo	Joel	Mojica	Matos	m	1966-10-03	01-01-22-001-0008	f
13778179	Alexia	Zulay	Gallegos	Carbonell	f	1953-01-31	01-01-22-001-0008	f
14638782	Oliver	Antonio	Domínquez	Bahena	m	1952-09-27	01-01-22-001-0008	f
10400817	Roberto	Ian	Ulibarri	Colón	m	1965-06-24	01-01-22-001-0008	f
9924724	Sandra	Carla	Suárez	Tórrez	f	1959-04-29	01-01-22-001-0008	f
13345911	Jordi	Alonso	Haro	Luján	m	1957-10-28	01-01-22-001-0008	f
10127521	Ian	Mateo	Garay	Crespo	m	1953-10-14	01-01-22-001-0008	f
14746313	Ian	Adria	Reyna	Solís	m	1953-10-28	01-01-22-001-0008	f
9568079	Dario	Marc	Aparicio	Lira	m	1960-02-18	01-01-22-001-0008	f
13868769	Helena	Nerea	Juan	Vélez	f	1967-09-08	01-01-22-001-0008	f
7300278	Nahia	Emma	Alfaro	Granado	f	1955-07-14	01-01-22-001-0008	f
11669151	Alvaro	Aaron	Carretero	Candelaria	m	1960-04-26	01-01-22-001-0008	f
5215006	Andres	Andres	Apodaca	Prieto	m	1954-01-13	01-01-22-001-0008	f
5091532	Jan	Hugo	Mateos	Magaña	m	1963-10-15	01-01-22-001-0008	f
5138672	Zulay	Carolina	Ruiz	Urrutia	f	1952-12-21	01-01-22-001-0008	f
9784003	Dario	Antonio	Bustos	Rivera	m	1965-03-11	01-01-22-001-0008	f
11242800	Mario	Mohamed	Saldivar	Luna	m	1963-01-14	01-01-22-001-0008	f
11229671	Raquel	Ainara	Serrato	Casares	f	1956-01-25	01-01-22-001-0008	f
14811374	Samuel	Joel	Terán	Mondragón	m	1953-06-14	01-01-22-001-0008	f
7832150	Nuria	Sofia	Tafoya	Ojeda	f	1952-02-18	01-01-22-001-0008	f
5532736	Luna	Leire	Véliz	Domingo	f	1957-10-31	01-01-22-001-0008	f
13378777	Abril	Blanca	Miranda	Quintanilla	f	1963-12-11	01-01-22-001-0008	f
10846193	Oswaldo	Aleix	Lozada	Ruiz	m	1958-01-29	01-01-22-001-0008	f
6341254	Nayara	Diana	Almanza	Delvalle	f	1962-07-25	01-01-22-001-0008	f
11134414	Carolina	Alexandra	Alanis	Lovato	f	1960-05-23	01-01-22-001-0008	f
12447571	Marcos	Miguel	Ramos	Moya	m	1968-09-12	01-01-22-001-0008	f
8332421	Naia	Rocio	Vásquez	Zambrano	f	1964-03-14	01-01-22-001-0008	f
13578216	Rafael	David	Chavarría	Solorio	m	1967-03-25	01-01-22-001-0008	f
6285564	Mario	Fernando	Roldan	Tomas	m	1957-09-06	01-01-22-001-0008	f
11120848	Pedro	Isaac	Villagómez	Lorente	m	1955-12-04	01-01-22-001-0008	f
5743168	Bella	Natalia	Valles	Rivas	f	1963-12-23	01-01-22-001-0008	f
11821912	Alex	Pablo	Palomino	Águilar	m	1954-09-20	01-01-22-001-0008	f
12537749	Adam	Daniel	Niño	Loera	m	1969-08-09	01-01-22-001-0008	f
5843064	Marc	Adria	Mayorga	Salgado	m	1955-01-23	01-01-22-001-0008	f
8152707	Eduardo	Adam	Navas	Casas	m	1953-09-09	01-01-22-001-0008	f
12340164	Rodrigo	Ian	Rentería	Hernádez	m	1958-07-11	01-01-22-001-0008	f
7956101	Alex	Marcos	Bueno	Soliz	m	1961-01-24	01-01-22-001-0008	f
12658154	Africa	Emma	Redondo	Villagómez	f	1964-01-14	01-01-22-001-0008	f
7598300	Claudia	Candela	Juárez	Gurule	f	1962-08-14	01-01-22-001-0008	f
13810305	Ane	Teresa	Crespo	Valdés	f	1955-03-01	01-01-22-001-0008	f
10788909	Ian	Lucas	Paz	Ureña	m	1963-04-25	01-01-22-001-0008	f
5795245	Leo	Andres	Naranjo	Muro	m	1965-08-21	01-01-22-001-0008	f
7589642	Adam	Oswaldo	Lucero	Olmos	m	1961-07-06	01-01-22-001-0008	f
8542756	Patricia	Africa	Collado	Vargas	f	1955-11-08	01-01-22-001-0008	f
8695237	Alejandro	Rodrigo	Ramón	Roca	m	1954-11-09	01-01-22-001-0008	f
5550731	Hugo	Hector	Rendón	De Anda	m	1959-04-19	01-01-22-001-0008	f
10152073	Pablo	Yorman	Valdés	Concepción	m	1956-02-10	01-01-22-001-0008	f
12837848	Ismael	Izan	Rueda	Sánchez	m	1962-01-06	01-01-22-001-0008	f
9543836	Raquel	Carlota	Pozo	Verdugo	f	1954-06-07	01-01-22-001-0008	f
10146780	Ana	Patricia	Muñoz	Cordero	f	1955-02-26	01-01-22-001-0008	f
8386447	Nadia	Patricia	Ibáñez	Polo	f	1968-06-16	01-01-22-001-0008	f
13345244	Fatima	Silvia	Mora	Salvador	f	1959-05-28	01-01-22-001-0008	f
8520966	Sara	Patricia	Raya	Rendón	f	1957-10-16	01-01-22-001-0008	f
10570289	Ona	Nerea	Rosa	Mora	f	1952-10-29	01-01-22-001-0008	f
14469372	Nicolas	Angel	Costa	Collazo	m	1951-12-23	01-01-22-001-0008	f
10766888	Eric	Omar	Alvarado	Gaona	m	1959-02-18	01-01-22-001-0008	f
14384755	Rayan	Aleix	Soto	Pantoja	m	1964-06-14	01-01-22-001-0008	f
12999474	Anna	Naia	Benito	Gallardo	f	1964-04-28	01-01-22-001-0008	f
12097516	Sebastian	Adam	Corrales	Laboy	m	1956-06-10	01-01-22-001-0008	f
14675419	Laura	Joan	Rivas	Magaña	f	1962-02-28	01-01-22-001-0008	f
6493746	Sara	Victoria	Arribas	Calderón	f	1960-06-24	01-01-22-001-0008	f
9450333	Ariadna	Erika	Baeza	Estévez	f	1965-04-24	01-01-22-001-0008	f
10686433	Alexandra	Nil	Sepúlveda	Blázquez	f	1953-02-12	01-01-22-001-0008	f
14230000	Jaime	Oscar	Figueroa	Bustos	m	1955-06-06	01-01-22-001-0008	f
9707520	Leo	Mateo	Prado	Borrego	m	1965-09-23	01-01-22-001-0008	f
7070552	Nadia	Mar	Amaya	Vaca	f	1968-10-11	01-01-22-001-0008	f
13246794	Miriam	Iria	Lira	Rey	f	1958-09-25	01-01-22-001-0008	f
8791801	Marco	Saul	Puig	Guajardo	m	1959-12-28	01-01-22-001-0008	f
6728835	Naia	Alexandra	Tello	Juan	f	1969-07-22	01-01-22-001-0008	f
12234010	Nerea	Jimena	Salcedo	Raya	f	1955-12-01	01-01-22-001-0008	f
7240015	Gabriel	Diego	Paz	Cavazos	m	1953-01-14	01-01-22-001-0008	f
12045805	Eva	Elena	Villarreal	Oliver	f	1963-12-06	01-01-22-001-0008	f
12181434	Santiago	Marco	Agosto	Quesada	m	1969-02-25	01-01-22-001-0008	f
8558592	Nicolas	Luis	Sosa	Zúñiga	m	1959-03-03	01-01-22-001-0008	f
7156352	Alvaro	Domingo	Miramontes	Carrero	m	1955-12-10	01-01-22-001-0008	f
7281746	Miguel	Alonso	Caldera	Alvarado	m	1954-03-04	01-01-22-001-0008	f
11687905	Isaac	Francisco Javier	Gálvez	Menéndez	m	1958-01-12	01-01-22-001-0008	f
13329676	Joel	Dario	Riera	Arriaga	m	1963-10-06	01-01-22-001-0008	f
10188261	Ane	Emma	Lara	Cintrón	f	1969-10-22	01-01-22-001-0008	f
7852454	Irene	Candela	Lázaro	Verdugo	f	1957-08-04	01-01-22-001-0008	f
14552770	Andres	Jan	Laureano	Cortez	m	1955-10-16	01-01-22-001-0008	f
10241560	Leire	Claudia	Negrón	Conde	f	1960-01-15	01-01-22-001-0008	f
10540605	Adrian	Javier	Macías	Luis	m	1959-01-03	01-01-22-001-0008	f
10874612	Yaiza	Vera	Vidal	Valadez	f	1962-09-17	01-01-22-001-0008	f
8371112	Yaiza	Angela	Moya	Carballo	f	1956-07-10	01-01-22-001-0008	f
7133936	Omar	Joel	Godoy	Urías	m	1954-08-06	01-01-22-001-0008	f
6105520	Lucia	Marti	Rivas	Segovia	f	1955-12-02	01-01-22-001-0008	f
10268035	Andres	Pablo	De la cruz	Briones	m	1966-07-22	01-01-22-001-0008	f
5045086	Aaron	Alex	Domínquez	Herrera	m	1965-10-05	01-01-22-001-0008	f
11697070	Francisco	Lucas	Font	Jiménez	m	1961-05-21	01-01-22-001-0008	f
6702670	Daniel	Jan	Armijo	Reyna	m	1955-07-01	01-01-22-001-0008	f
8331918	Roberto	Eric	Jaime	Segovia	m	1964-06-18	01-01-22-001-0008	f
12615970	Yorman	Ian	Fernández	Mascareñas	m	1956-04-24	01-01-22-001-0008	f
5728611	Lola	Eva	Pozo	Quiroz	f	1961-05-06	01-01-22-001-0008	f
8444663	Jesus	Javier	Villalpando	Castañeda	m	1962-07-13	01-01-22-001-0008	f
14792440	Valentina	Paula	Abeyta	Ceballos	f	1961-07-28	01-01-22-001-0008	f
12662041	Jon	Dario	Ruíz	Montoya	m	1956-03-03	01-01-22-001-0008	f
9227476	Diego	Jesus	Rivas	Valles	m	1957-05-24	01-01-22-001-0008	f
5664708	Berta	Andrea	Solorio	Alanis	f	1967-02-15	01-01-22-001-0008	f
9621453	Rayan	Leonardo	Rolón	Paredes	m	1966-03-19	01-01-22-001-0008	f
8037423	Vega	Mireia	Nieves	Roque	f	1969-02-11	01-01-22-001-0008	f
11393771	Emma	Berta	Pastor	Laureano	f	1962-07-21	01-01-22-001-0008	f
7204146	Samuel	Erik	Negrón	Ureña	m	1959-07-14	01-01-22-001-0008	f
7350282	Elena	Candela	Bahena	Briseño	f	1958-12-14	01-01-22-001-0008	f
12394413	Teresa	Jimena	Miramontes	Grijalva	f	1969-01-13	01-01-22-001-0008	f
6367770	Carolina	Marina	Reséndez	Santana	f	1961-06-06	01-01-22-001-0008	f
5920814	Paola	Mireia	Caro	Cardona	f	1957-09-09	01-01-22-001-0008	f
8754475	Vega	Elsa	Montenegro	Giménez	f	1959-01-23	01-01-22-001-0008	f
9291754	Adam	Alberto	Sosa	Sotelo	m	1964-07-25	01-01-22-001-0008	f
14333286	Jimena	Manuela	Montes	Marroquín	f	1952-04-02	01-01-22-001-0008	f
14386692	Hector	Marco	Montez	Villaseñor	m	1969-03-30	01-01-22-001-0008	f
13706992	Ainara	Paola	Domínquez	Sisneros	f	1958-11-11	01-01-22-001-0008	f
8771994	Nadia	Diana	Cazares	Quiñones	f	1952-10-17	01-01-22-001-0008	f
6708325	Miguel	Ismael	Arreola	Navarrete	m	1966-08-29	01-01-22-001-0008	f
14188079	Antonio	Leandro	Alcaraz	Rosales	m	1951-12-11	01-01-22-001-0008	f
9534548	Zulay	Alma	Rivas	Quintanilla	f	1956-04-22	01-01-22-001-0008	f
13346755	Beatriz	Elsa	Páez	Sáenz	f	1962-03-27	01-01-22-001-0008	f
10663285	Lucas	Leonardo	Almonte	Mateos	m	1967-01-10	01-01-22-001-0008	f
8839029	Nahia	Lara	Sierra	Galarza	f	1965-04-09	01-01-22-001-0008	f
10028431	Leire	Blanca	Castaño	Valentín	f	1965-08-20	01-01-22-001-0008	f
9738506	Diana	Isabel	Sandoval	Rael	f	1964-05-22	01-01-22-001-0008	f
8657342	Irene	Leyre	Robledo	Peña	f	1957-02-21	01-01-22-001-0008	f
11595562	Salma	Olivia	Rodríquez	Medrano	f	1966-10-04	01-01-22-001-0008	f
9472839	Pau	Alma	Solano	Alba	f	1955-11-15	01-01-22-001-0008	f
5686611	Alexandra	Naia	Abad	Deleón	f	1957-12-25	01-01-22-001-0008	f
10873500	Jaime	Sergio	Montez	Sancho	m	1956-04-18	01-01-22-001-0008	f
9314218	Cesar	Pol	Mejía	Ortiz	m	1952-12-06	01-01-22-001-0008	f
9205757	Irene	Alicia	Prado	Candelaria	f	1962-01-01	01-01-22-001-0008	f
12303505	Salma	Alba	Saucedo	Gimeno	f	1952-06-16	01-01-22-001-0008	f
8051958	Leandro	Ian	Domenech	Ayala	m	1958-10-16	01-01-22-001-0008	f
13926927	Ruben	Nicolas	Garibay	Lomeli	m	1966-11-03	01-01-22-001-0008	f
12178535	Carlota	Andrea	Candelaria	Esteve	f	1960-01-07	01-01-22-001-0008	f
10862612	Erik	Victor	Vidal	Delagarza	m	1952-04-26	01-01-22-001-0008	f
14682242	Jesus	Oliver	Barrientos	Mercado	m	1962-07-28	01-01-22-001-0008	f
11500198	Jesus	Jon	Huerta	Hidalgo	m	1959-07-02	01-01-22-001-0008	f
14412336	Sara	Alexia	Barraza	Calderón	f	1958-10-03	01-01-22-001-0008	f
5106868	Nicolas	Samuel	Avilés	Leiva	m	1969-01-10	01-01-22-001-0008	f
9527502	Daniel	Fernando	Sevilla	Valverde	m	1956-12-14	01-01-22-001-0008	f
9171095	Clara	Alejandra	Rolón	Báez	f	1957-09-25	01-01-22-001-0008	f
5981385	Ona	Naiara	Regalado	Pacheco	f	1965-12-08	01-01-22-001-0008	f
9009104	Naia	Vera	Estrada	Noriega	f	1959-09-14	01-01-22-001-0008	f
10009227	Carmen	Ane	Escudero	Nájera	f	1957-07-02	01-01-22-001-0008	f
5599518	David	Hugo	Anguiano	Mena	m	1955-08-21	01-01-22-001-0008	f
6855207	Candela	Sara	Tafoya	Rolón	f	1966-07-08	01-01-22-001-0008	f
13269467	Roberto	Jan	Sola	Delgadillo	m	1963-10-26	01-01-22-001-0008	f
10385284	Domingo	Enrique	Manzanares	Gallego	m	1952-01-24	01-01-22-001-0008	f
13704233	Eva	Carmen	Rosa	Venegas	f	1952-04-16	01-01-22-001-0008	f
9140884	Javier	Miguel	Batista	Limón	m	1956-01-03	01-01-22-001-0008	f
13194247	Mateo	Gonzalo	Bravo	Sarabia	m	1955-05-08	01-01-22-001-0008	f
7725922	Ruben	Oscar	Sepúlveda	Roca	m	1951-12-24	01-01-22-001-0008	f
7899931	Alvaro	Javier	Ruiz	Esparza	m	1952-04-26	01-01-22-001-0008	f
7479526	Jana	Yaiza	Álvarez	Navas	f	1952-12-22	01-01-22-001-0008	f
6046263	Alicia	Ainara	Cuenca	Romero	f	1952-05-16	01-01-22-001-0008	f
5921386	Erik	Luis	Acosta	Arias	m	1966-11-28	01-01-22-001-0008	f
5395190	Jana	Celia	Muro	Escamilla	f	1964-06-17	01-01-22-001-0008	f
5811037	Lidia	Manuela	Portillo	Espinal	f	1963-04-29	01-01-22-001-0008	f
5132292	Alexia	Natalia	Grijalva	Abrego	f	1961-08-16	01-01-22-001-0008	f
8093326	Domingo	Adam	Villarreal	Delgado	m	1958-10-10	01-01-22-001-0008	f
8655420	Angel	Eric	Puente	Castillo	m	1953-09-01	01-01-22-001-0008	f
5157108	Nayara	Yaiza	De la fuente	Andreu	f	1956-09-11	01-01-22-001-0008	f
7871244	Carlos	Erik	Suárez	Esteban	m	1953-01-22	01-01-22-001-0008	f
12118690	Clara	Luna	Jaime	Alfonso	f	1965-01-10	01-01-22-001-0008	f
8591773	Lidia	Jimena	Barreto	Anguiano	f	1958-08-27	01-01-22-001-0008	f
11388359	Jaime	Gonzalo	Valero	Reyna	m	1965-09-29	01-01-22-001-0008	f
13720452	Daniel	Pablo	Cazares	Casanova	m	1966-02-09	01-01-22-001-0008	f
14220708	Luis	Sebastian	Valencia	Lozano	m	1959-03-07	01-01-22-001-0008	f
8034850	Nora	Africa	Monroy	Velásquez	f	1965-09-25	01-01-22-001-0008	f
10997645	Paula	Alejandra	Soliz	Villagómez	f	1952-10-17	01-01-22-001-0008	f
8623314	Alexia	Candela	Salcido	Martínez	f	1955-09-19	01-01-22-001-0008	f
10767034	Mateo	Daniel	Sandoval	Molina	m	1968-10-04	01-01-22-001-0008	f
6444179	Samuel	Diego	Verduzco	Sisneros	m	1966-08-01	01-01-22-001-0008	f
7121225	Adria	Jose	Rey	Ulloa	m	1955-02-21	01-01-22-001-0008	f
14306293	Bruno	Mohamed	Loera	Vaca	m	1957-09-03	01-01-22-001-0008	f
14258149	Natalia	Ainara	Arguello	Saiz	f	1953-08-24	01-01-22-001-0008	f
13095467	Ander	Antonio	Benito	Rodrígez	m	1963-02-04	01-01-22-001-0008	f
8336373	Martin	Pol	Corona	Escamilla	m	1967-10-21	01-01-22-001-0008	f
9733241	Lucas	Hector	Trejo	Velásquez	m	1969-06-18	01-01-22-001-0008	f
14534931	Beatriz	Lola	Vicente	De Jesús	f	1968-03-11	01-01-22-001-0008	f
6351598	Luis	Aleix	Villareal	Vicente	m	1952-08-31	01-01-22-001-0008	f
9699683	Jon	Alberto	Laureano	Trejo	m	1954-05-06	01-01-22-001-0008	f
13815980	Miguel	Carlos	Tamayo	Tamez	m	1967-08-06	01-01-22-001-0008	f
11831737	Pol	Fernando	Alcala	Zelaya	m	1955-06-06	01-01-22-001-0008	f
11204289	Jose	Adam	Moral	Ballesteros	m	1951-12-30	01-01-22-001-0008	f
5210425	Francisco Javier	Sebastian	Guillen	Barroso	m	1955-03-24	01-01-22-001-0008	f
12488045	Ana	Cristina	Montes	Mayorga	f	1964-09-08	01-01-22-001-0008	f
13338407	Africa	Sandra	Peres	Araña	f	1957-07-01	01-01-22-001-0008	f
7879231	Mohamed	Luis	Cuellar	Duran	m	1958-09-26	01-01-22-001-0008	f
7270236	Oswaldo	Adria	Aguilar	Saldivar	m	1958-10-24	01-01-22-001-0008	f
14225002	Anna	Africa	Delacrúz	Zepeda	f	1952-05-04	01-01-22-001-0008	f
7274826	Marc	Carlos	Gaitán	Vásquez	m	1969-08-09	01-01-22-001-0008	f
12146401	Victor	Rayan	Leal	Camacho	m	1969-06-25	01-01-22-001-0008	f
13360224	Samuel	Raul	Carrion	Domenech	m	1959-02-27	01-01-22-001-0008	f
6021757	Ian	Erik	Moral	Cornejo	m	1952-01-12	01-01-22-001-0008	f
13380252	Irene	Paola	Valenzuela	Saavedra	f	1952-11-10	01-01-22-001-0008	f
13033415	Pedro	Javier	Reyna	Roca	m	1969-03-28	01-01-22-001-0008	f
10515587	Blanca	Mar	Padrón	Niño	f	1959-10-30	01-01-22-001-0008	f
8915175	Victoria	Blanca	Terán	Murillo	f	1968-12-21	01-01-22-001-0008	f
14465643	Vera	Angela	Iglesias	Segura	f	1964-08-26	01-01-22-001-0008	f
5100041	Ines	Emma	Rocha	Valdés	f	1960-07-01	01-01-22-001-0008	f
6994988	Pedro	Francisco Javier	Borrego	Zamudio	m	1954-03-15	01-01-22-001-0008	f
14701670	Diana	Carlota	Carrión	Mora	f	1957-11-12	01-01-22-001-0008	f
8265675	Ander	Marc	Estrada	Luis	m	1954-06-20	01-01-22-001-0008	f
5344646	Isabel	Alexandra	Solís	Sanabria	f	1961-08-01	01-01-22-001-0008	f
5011852	Mario	Jaime	Urías	Velázquez	m	1959-09-16	01-01-22-001-0008	f
14802351	Carlos	Erik	Peralta	Matías	m	1964-04-07	01-01-22-001-0008	f
9680845	Eric	Rodrigo	Altamirano	Peralta	m	1952-04-13	01-01-22-001-0008	f
6967582	Aleix	Oswaldo	Viera	Blanco	m	1953-05-21	01-01-22-001-0008	f
13563777	Adam	Ismael	Sánchez	De Jesús	m	1963-07-31	01-01-22-001-0008	f
5388321	Gabriela	Raquel	Silva	López	f	1959-05-12	01-01-22-001-0008	f
8866267	Mar	Vega	Cazares	Herrera	f	1961-10-23	01-01-22-001-0008	f
12146628	Marco	Ivan	Villar	Ávalos	m	1968-02-06	01-01-22-001-0008	f
5278047	Jon	Marc	Mireles	Escamilla	m	1961-09-11	01-01-22-001-0008	f
8240685	Oswaldo	Ander	Galarza	Feliciano	m	1969-05-22	01-01-22-001-0008	f
5470194	Antonio	Santiago	Valladares	Casado	m	1962-04-16	01-01-22-001-0008	f
8805097	Yaiza	Marina	Tamez	Armas	f	1961-02-07	01-01-22-001-0008	f
12859091	Paola	Lidia	Niño	Serrano	f	1965-10-08	01-01-22-001-0008	f
9678403	Luisa	Isabel	Gamez	Benavides	f	1962-11-02	01-01-22-001-0008	f
6060697	Gabriel	Jose	Mata	Sandoval	m	1952-06-06	01-01-22-001-0008	f
13558985	David	Adam	Álvarez	Morales	m	1963-11-15	01-01-22-001-0008	f
6382992	Alex	Alberto	Carrera	Benítez	M	1953-01-28	01-01-22-001-0064	f
14637934	Marcos	Rodrigo	Antón	Barraza	m	1962-06-03	01-01-22-001-0049	f
10597846	Erik	Alonso	Calvo	Márquez	m	1968-01-09	01-01-22-001-0049	f
\.


--
-- Data for Name: unidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unidad (id, nombre_unidad, eliminado) FROM stdin;
16	Unidad Administrativa y Financiera	f
17	Unidad Ejecutiva	f
20	Unidad De Contraloría Social	f
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id, nombre, "contraseña", cedula, tipo) FROM stdin;
1	admin	$2y$12$ALpRqa4KrqF844ITVY2Cze7JoMt6BVEACPJuiuPgs.cxN1fyVjCaa	\N	1
8	bruno	$2y$12$teTlOi59lehmBPGrVO1pwOAE4z86aZ1PMHlx1vRVAH5moITnj8H4K	14306293	2
25	diana	$2y$12$HdBBla3vAGHVYSkc17DNOOjcU9xP948TflEj9UU80DYWx94JAacea	9425937	3
26	mara	$2y$12$CGV9LGu/Q24hcDxZCF3Ux.RCBz/3vz5Y0babe/EQSoG4FeOWVSyNi	9317140	3
13	alex	$2y$12$bBjqaTUmtzgdgDThCqTnBedQw23y50eSe9EEH7lGzNeyMQAklIrxK	6382992	2
27	brando	$2y$12$VMP4tEcHICoMdi8meQ.PJem7zlNjQpCSiBI/WFjCgdbBQ/vAAlzVG	26728989	3
\.


--
-- Data for Name: usuario_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario_tipo (id, nombre) FROM stdin;
1	Administrador de Comuna
2	Administrador de Consejo Comunal
3	Administrador de Elección
\.


--
-- Data for Name: votante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.votante (ci_persona, id_eleccion) FROM stdin;
8466662	54
5482616	54
11723594	54
14247548	54
8536416	54
6152609	54
7990442	54
6386292	55
9878064	55
13780375	55
8040798	55
9504121	55
8821306	55
8512510	55
13715472	55
12639768	55
8830355	55
12366693	55
8898690	55
9264396	55
12959010	55
12922376	55
6285882	56
7204146	56
5599518	56
12394413	56
12488045	56
12303505	56
9171095	56
6998334	56
7479526	56
14682242	56
9699683	56
10127521	56
10846193	56
14197863	56
8813094	57
8671299	57
10539685	57
8353016	57
6203146	57
10308899	57
10705966	57
10615761	57
7982576	57
11124065	57
11407608	57
13460735	57
8065167	58
12639394	58
14526373	58
6279143	58
14605080	58
13718766	58
6975194	58
7954158	58
11348358	58
12163705	58
6859302	58
6604825	58
13866008	58
\.


--
-- Name: banco_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.banco_id_seq', 11, true);


--
-- Name: comite_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.comite_id_seq', 23, true);


--
-- Name: eleccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.eleccion_id_seq', 58, true);


--
-- Name: maquina_votante_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.maquina_votante_id_seq', 73, true);


--
-- Name: unidad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unidad_id_seq', 24, true);


--
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_seq', 28, true);


--
-- Name: usuario_tipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_tipo_id_seq', 3, true);


--
-- Name: banco banco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banco
    ADD CONSTRAINT banco_pkey PRIMARY KEY (id);


--
-- Name: candidato candidato_ci_persona_id_eleccion_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_ci_persona_id_eleccion_key UNIQUE (ci_persona, id_eleccion);


--
-- Name: comite comite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comite
    ADD CONSTRAINT comite_pkey PRIMARY KEY (id);


--
-- Name: comite_unidad_consejo comite_unidad_consejo_id_comite_id_unidad_cod_consejo_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comite_unidad_consejo
    ADD CONSTRAINT comite_unidad_consejo_id_comite_id_unidad_cod_consejo_key UNIQUE (id_comite, id_unidad, cod_consejo);


--
-- Name: comite_unidad_consejo comite_unidad_consejo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comite_unidad_consejo
    ADD CONSTRAINT comite_unidad_consejo_pkey PRIMARY KEY (id_comite, id_unidad, cod_consejo);


--
-- Name: consejocomunal consejocomunal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.consejocomunal
    ADD CONSTRAINT consejocomunal_pkey PRIMARY KEY (cod);


--
-- Name: consejocomunal_unidad consejocomunal_unidad_cod_consejocomunal_id_unidad_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.consejocomunal_unidad
    ADD CONSTRAINT consejocomunal_unidad_cod_consejocomunal_id_unidad_key UNIQUE (cod_consejocomunal, id_unidad);


--
-- Name: consejocomunal_unidad consejocomunal_unidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.consejocomunal_unidad
    ADD CONSTRAINT consejocomunal_unidad_pkey PRIMARY KEY (cod_consejocomunal, id_unidad);


--
-- Name: eleccion eleccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.eleccion
    ADD CONSTRAINT eleccion_pkey PRIMARY KEY (id);


--
-- Name: maquina_votante maquina_votante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.maquina_votante
    ADD CONSTRAINT maquina_votante_pkey PRIMARY KEY (id);


--
-- Name: maquina_votante_usuario maquina_votante_usuario_id_maquina_id_usuario_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.maquina_votante_usuario
    ADD CONSTRAINT maquina_votante_usuario_id_maquina_id_usuario_key UNIQUE (id_maquina, id_usuario);


--
-- Name: persona persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_pkey PRIMARY KEY (cedula);


--
-- Name: unidad unidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad
    ADD CONSTRAINT unidad_pkey PRIMARY KEY (id);


--
-- Name: usuario usuario_cedula_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_cedula_key UNIQUE (cedula);


--
-- Name: usuario usuario_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_nombre_key UNIQUE (nombre);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- Name: usuario_tipo usuario_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_tipo
    ADD CONSTRAINT usuario_tipo_pkey PRIMARY KEY (id);


--
-- Name: votante votante_ci_persona_id_eleccion_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.votante
    ADD CONSTRAINT votante_ci_persona_id_eleccion_key UNIQUE (ci_persona, id_eleccion);


--
-- Name: votante votante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.votante
    ADD CONSTRAINT votante_pkey PRIMARY KEY (ci_persona, id_eleccion);


--
-- Name: candidato candidato_ci_persona_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_ci_persona_fkey FOREIGN KEY (ci_persona) REFERENCES public.persona(cedula) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: candidato candidato_id_comite_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_id_comite_fkey FOREIGN KEY (id_comite) REFERENCES public.comite(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: candidato candidato_id_eleccion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_id_eleccion_fkey FOREIGN KEY (id_eleccion) REFERENCES public.eleccion(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: candidato candidato_id_unidad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_id_unidad_fkey FOREIGN KEY (id_unidad) REFERENCES public.unidad(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: eleccion cod_consejocomunal_consejocomunal_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.eleccion
    ADD CONSTRAINT cod_consejocomunal_consejocomunal_fkey FOREIGN KEY (cod_consejocomunal) REFERENCES public.consejocomunal(cod) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comite_unidad_consejo comite_unidad_consejo_cod_consejo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comite_unidad_consejo
    ADD CONSTRAINT comite_unidad_consejo_cod_consejo_fkey FOREIGN KEY (cod_consejo) REFERENCES public.consejocomunal(cod) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comite_unidad_consejo comite_unidad_consejo_id_comite_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comite_unidad_consejo
    ADD CONSTRAINT comite_unidad_consejo_id_comite_fkey FOREIGN KEY (id_comite) REFERENCES public.comite(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comite_unidad_consejo comite_unidad_consejo_id_unidad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comite_unidad_consejo
    ADD CONSTRAINT comite_unidad_consejo_id_unidad_fkey FOREIGN KEY (id_unidad) REFERENCES public.unidad(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: consejocomunal_unidad consejocomunal_unidad_cod_consejocomunal_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.consejocomunal_unidad
    ADD CONSTRAINT consejocomunal_unidad_cod_consejocomunal_fkey FOREIGN KEY (cod_consejocomunal) REFERENCES public.consejocomunal(cod) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: consejocomunal_unidad consejocomunal_unidad_id_unidad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.consejocomunal_unidad
    ADD CONSTRAINT consejocomunal_unidad_id_unidad_fkey FOREIGN KEY (id_unidad) REFERENCES public.unidad(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: maquina_votante maquina_votante_id_eleccion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.maquina_votante
    ADD CONSTRAINT maquina_votante_id_eleccion_fkey FOREIGN KEY (id_eleccion) REFERENCES public.eleccion(id) ON DELETE CASCADE;


--
-- Name: maquina_votante_usuario maquina_votante_usuario_id_maquina_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.maquina_votante_usuario
    ADD CONSTRAINT maquina_votante_usuario_id_maquina_fkey FOREIGN KEY (id_maquina) REFERENCES public.maquina_votante(id) ON DELETE CASCADE;


--
-- Name: maquina_votante_usuario maquina_votante_usuario_id_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.maquina_votante_usuario
    ADD CONSTRAINT maquina_votante_usuario_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.usuario(id) ON DELETE CASCADE;


--
-- Name: maquina_votante maquina_votante_votando_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.maquina_votante
    ADD CONSTRAINT maquina_votante_votando_fkey FOREIGN KEY (votando) REFERENCES public.persona(cedula) ON DELETE CASCADE;


--
-- Name: persona persona_consejocomunal_cod_consejocomunal_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_consejocomunal_cod_consejocomunal_fkey FOREIGN KEY (cod_consejocomunal) REFERENCES public.consejocomunal(cod) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: usuario usuario_cedula_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_cedula_fkey FOREIGN KEY (cedula) REFERENCES public.persona(cedula) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: usuario usuario_tipo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_tipo_fkey FOREIGN KEY (tipo) REFERENCES public.usuario_tipo(id);


--
-- Name: votante votante_ci_persona_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.votante
    ADD CONSTRAINT votante_ci_persona_fkey FOREIGN KEY (ci_persona) REFERENCES public.persona(cedula) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: votante votante_id_eleccion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.votante
    ADD CONSTRAINT votante_id_eleccion_fkey FOREIGN KEY (id_eleccion) REFERENCES public.eleccion(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

