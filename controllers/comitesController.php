<?php

	include_once('./models/comitesModel.php');
	include_once('./models/usuarioModel.php');

	class comitesController{

		private $model;

		public function __construct(){
			$usuarioModel = new Usuario();
			if(isset($_SESSION['usuario']) && $usuarioModel->getTipo($_SESSION['id']) == 1){
				$this->model = new comitesModel();
			}else{
				header('location: ' . SERVERURL. '/');
				die();
			}
		}

		public function index(){
			$comites = $this->model->getTodos();
			include_once('./views/comitesView.php');
		}

		public function agregarComite(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL. '/');
				die();
			}
			$nombre = $_POST['nombre'];
			$this->model->setNombre($nombre);
			echo $this->model->agregarComite();
		}

		public function editarComite(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL. '/');
				die();
			}
			$id = $_POST['id'];
			$nombre = $_POST['nombre'];
			$this->model->setId($id);

			if($this->model->verificarSiComiteTieneEleccionComenzada()){
				die('eleccion_comenzada');
			}

			echo $this->model->editarComite($nombre);
		}

		public function borrarComite(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL. '/');
				die();
			}
			$id = $_POST['id'];
			$this->model->setId($id);

			if($this->model->verificarSiComiteTieneEleccionComenzada()){
				die('eleccion_comenzada');
			}

			echo $this->model->borrarComite();
		}

		public function getPorConsejoUnidad(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL. '/');
				die();
			}
			$idUnidad = $_POST['idUnidad'];
			$cod = $_POST['cod'];
			echo json_encode($this->model->getPorConsejoUnidad($cod, $idUnidad));
		}

		public function getComitesNotIn(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL. '/');
				die();
			}
			$idUnidad = $_POST['idUnidad'];
			$cod = $_POST['cod'];
			echo json_encode($this->model->getComitesNotIn($cod, $idUnidad));
		}

		public function agregarComiteUnidad(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL. '/');
				die();
			}
			$idUnidad = $_POST['idUnidad'];
			$cod = $_POST['cod'];
			$comites = json_decode($_POST['comites']);
			echo $this->model->agregarComiteUnidad($cod, $idUnidad, $comites);
		}

	}


?>