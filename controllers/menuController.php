<?php

	include_once('./models/usuarioModel.php');

	class menuController{

		private $db;

		public function __construct(){
			if(isset($_SESSION['usuario'])){
				$this->db = mainModel::conectar();
			}else{
				header('location: ' . SERVERURL . '/login/');
				die();
			}
		}

		public function index(){
			$usuarioModel = new Usuario();
			$tipoUsuario = $usuarioModel->getTipo($_SESSION['id']);
			include_once('./views/menuView.php');
		}

		public function consejoscomunales(){
			$usuarioModel = new Usuario();
			if(!isset($_SESSION['usuario']) || $usuarioModel->getTipo($_SESSION['id']) != 1){
				header('location: ' . SERVERURL . '/menu/');
				die();
			}
			include_once('./views/consejoscomunalesMenuView.php');
		}

	}


?>