<?php

	include_once('./models/bancoModel.php');
	
	class bancoController{

		private $model;

		public function __construct(){
			if($_SERVER['REQUEST_METHOD'] != 'POST' || !isset($_SESSION['usuario'])){
				include_once('./views/404.php');
				die();
			}else{
				$this->model = new bancoModel();
			}
		}

		public function getTodos(){
			echo json_encode($this->model->getTodos());
		}
		

	}

?>