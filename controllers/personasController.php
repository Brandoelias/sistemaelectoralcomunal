<?php

	include_once('./models/personasModel.php');
	include_once('./models/consejocomunalModel.php');
	include_once('./models/usuarioModel.php');

	use Phppot\DataSource;
	use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

	require_once ('./vendor/autoload.php');

	class personasController{

		private $model;

		public function __construct(){
			if(isset($_SESSION['usuario'])){
				$this->model = new personasModel();
			}else{
				header('location: ' . SERVERURL . '/login/');
				die();
			}
		}

		public function index($paginaActual = 1){

			if($paginaActual < 1){
				$paginaActual = 1;
			}

			$filtrarConsejo = false;
			if(isset($_GET['consejo'])){
				$filtrarConsejo = $_GET['consejo'];
			}

			$usuarioModel = new Usuario();
			$tipoUsuario = $usuarioModel->getTipo($_SESSION['id']);
			if($tipoUsuario == 2 || $tipoUsuario == 3){
				$usuario = $usuarioModel->getUsuario($_SESSION['id']);
				$filtrarConsejo = $usuario->cod_consejocomunal;
			}

			$modelo = $this->model;
			$totalPersonas = $this->model->getCantidadTotal($filtrarConsejo);
			$limite = 25;
			if($paginaActual > ceil($totalPersonas / $limite) && $paginaActual != 1){
				include_once('./views/404.php');
				die();
			}

			//consultar consejos comunales
			include_once('./models/consejocomunalModel.php');
			$consejoscomunales = new consejocomunalModel();
			$consejoscomunales = $consejoscomunales->getTodos();

			$offset = ($paginaActual-1) * $limite;

			if(!$filtrarConsejo){
				$personas = $this->model->getTodos($limite, $offset);
			}else{
				$personas = $this->model->getTodosPorConsejo($limite, $offset, $filtrarConsejo ? $filtrarConsejo : '/');
			}

			$personas = $this->calcularEdad($personas);

			include_once('./views/personasView.php');
		}

		public function agregar(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				return;
			}

			$datos = json_decode($_POST['datos']);
			$cedula = $datos->cedula;
			$pnombre = ucfirst(trim($datos->pnombre));
			$snombre = ucfirst(trim($datos->snombre));
			$papellido = ucfirst(trim($datos->papellido));
			$sapellido = ucfirst(trim($datos->sapellido));
			$sexo = $datos->sexo;
			$datos->fecha_nacimiento = date_create_from_format('d/m/Y',$datos->fecha_nacimiento);
			if($this->model->verificarSiExiste($cedula)){
				die('cedula_existe');
			}
			if(!$datos->fecha_nacimiento){
				die('¡Fecha de nacimiento incorrecta!');
			}
			$datos->fecha_nacimiento = date_format($datos->fecha_nacimiento, 'Y-m-d');
			$fecha_nacimiento = $datos->fecha_nacimiento;
			$cod_consejocomunal = $datos->consejocomunal;

			if(isset($_FILES['file'])){
				$foto = $_FILES['file'];
				if(!move_uploaded_file($foto['tmp_name'], "./views/img/fotos_personas/$cedula.jpg")){
					echo false;
					die();
				}
			}

			$usuarioModel = new Usuario();
			$tipoUsuario = $usuarioModel->getTipo($_SESSION['id']);
			if($tipoUsuario == 2){
				$usuario = $usuarioModel->getUsuario($_SESSION['id']);
				$cod_consejocomunal = $usuario->cod_consejocomunal;
			}elseif($tipoUsuario != 1){
				// SI NO ES SUPER USUARIO NI USUARIO NO PUEDE AGREGAR
				die('no_puedes_agregar');
			}

			$this->model->setCedula($cedula);
			$this->model->setPnombre($pnombre);
			$this->model->setSnombre($snombre);
			$this->model->setPapellido($papellido);
			$this->model->setSapellido($sapellido);
			$this->model->setSexo($sexo);
			$this->model->setFechaNacimiento($fecha_nacimiento);
			$this->model->setCodConsejocomunal($cod_consejocomunal);
			echo $this->model->agregar();
		}

		public function editar(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				return;
			}

			$datos = json_decode($_POST['datos']);
			$obj = new stdClass();
			$cedulaOriginal = $datos->cedulaOriginal;

			if($this->model->participaEleccion($cedulaOriginal)){
				die('participa_eleccion');
			}

			$obj->cedula = $datos->cedulaModificar;
			$obj->pnombre = ucfirst(trim($datos->pnombre));
			$obj->snombre = ucfirst(trim($datos->snombre));
			$obj->papellido = ucfirst(trim($datos->papellido));
			$obj->sapellido = ucfirst(trim($datos->sapellido));
			$obj->sexo = $datos->sexo;
			$datos->fecha_nacimiento = date_create_from_format('d/m/Y',$datos->fecha_nacimiento);
			if($cedulaOriginal != $datos->cedulaModificar && $this->model->verificarSiExiste($datos->cedulaModificar)){
				die('cedula_existe');
			}
			if($this->model->verificarSiPerteneceEleccionSinFinalizar($cedulaOriginal, $datos->consejocomunal)){
				die('pertenece_eleccion');
			}
			if(!$datos->fecha_nacimiento){
				die('¡Fecha de nacimiento incorrecta!');
			}
			$datos->fecha_nacimiento = date_format($datos->fecha_nacimiento, 'Y-m-d');
			$obj->fecha_nacimiento = $datos->fecha_nacimiento;
			$obj->cod_consejocomunal = $datos->consejocomunal;

			$usuarioModel = new Usuario();
			$tipoUsuario = $usuarioModel->getTipo($_SESSION['id']);
			if($tipoUsuario == 2){
				$usuario = $usuarioModel->getUsuario($_SESSION['id']);
				$obj->cod_consejocomunal = $usuario->cod_consejocomunal;
				// LOS NO SUPER USUARIOS SÓLO PUEDEN EDITAR PERSONAS DE SU CONSEJO COMUNAL
				if(!$this->model->verificarSiPerteneceConsejo($cedulaOriginal, $usuario->cod_consejocomunal)){
					die('no_puedes_editar');
				}

				// UN USUARIO NO PUEDE MODIFICAR A SUPER USUARIO
				$usuarioModificar = $usuarioModel->getUsuarioPorCedula($cedulaOriginal);
				if(isset($usuarioModificar->tipo) && $usuarioModificar->tipo == 1){
					die('no_permitido');
				}
			}elseif($tipoUsuario != 1){
				// SI NO ES SUPER USUARIO NI USUARIO NO PUEDE EDITAR
				die('no_puedes_editar');
			}

			if(isset($_FILES['file'])){
				$foto = $_FILES['file'];
				if(!move_uploaded_file($foto['tmp_name'], "./views/img/fotos_personas/$obj->cedula.jpg")){
					echo false;
					die();
				}
			}

			$this->model->setCedula($cedulaOriginal);
			echo $this->model->editar($obj);
		}

		public function borrar(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				return;
			}

			$cedula = $_POST['cedula'];

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			if($usuario->tipo == 2){
				// LOS NO SUPER USUARIOS SÓLO PUEDEN BORRAR PERSONAS DE SU CONSEJO COMUNAL
				if(!$this->model->verificarSiPerteneceConsejo($cedula, $usuario->cod_consejocomunal)){
					die('no_puedes_borrar');
				}

				// UN USUARIO NO PUEDE BORRAR A SUPER USUARIO
				$usuarioBorrar = $usuarioModel->getUsuarioPorCedula($cedula);
				if(isset($usuarioBorrar->tipo) && $usuarioBorrar->tipo == 1){
					die('no_permitido');
				}
			}elseif($usuario->tipo != 1){
				// SI NO ES SUPER USUARIO NI USUARIO NO PUEDE BORRAR
				die('no_puedes_borrar');
			}

			$this->model->setCedula($cedula);
			$borrar = $this->model->borrar();
			if($borrar){
				if(is_file("./views/img/fotos_personas/$cedula.jpg") && is_writable("./views/img/fotos_personas/$cedula.jpg")){
					unlink("./views/img/fotos_personas/$cedula.jpg");
				}
			}
			echo $borrar;
		}

		public function getAjax(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				return;
			}

			$dato = trim($_POST['dato']);
			$filtrarConsejo = '';
			if(isset($_POST['filtrarConsejo'])){
				$filtrarConsejo = $_POST['filtrarConsejo'];
			}
			if($dato == ""){
				return;
			}
			$id_eleccion = null;
			if(isset($_POST['id_eleccion'])){
				$id_eleccion = $_POST['id_eleccion'];
			}
			if(is_numeric($dato)){
				if($dato < 1000000){
					return;
				}
				$personas = $this->model->getAjaxPorCedula($dato, $id_eleccion, $filtrarConsejo);
				$personas = $this->calcularEdad($personas);
				echo json_encode($personas);
			}else{
				$dato = strtolower($dato);
				$dato = explode(' ', $dato);
				if(count($dato) < 2){
					$dato[1] = '';
				}
				$personas = $this->model->getAjaxPorNombreYApellido($dato, $id_eleccion, $filtrarConsejo);
				$personas = $this->calcularEdad($personas);
				echo json_encode($personas);
			}
		}

		public function getAjaxPaginar(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				return;
			}

			$dato = trim($_POST['dato']);
			$filtrarConsejo = false;
			if(isset($_POST['filtrarConsejo']) && $_POST['filtrarConsejo'] != ''){
				$filtrarConsejo = $_POST['filtrarConsejo'];
			}
			if($dato == ""){
				return;
			}
			$id_eleccion = null;
			if(isset($_POST['id_eleccion'])){
				$id_eleccion = $_POST['id_eleccion'];
			}

			$paginaActual = 1;
			if(isset($_POST['paginaActual']) && $_POST['paginaActual'] != ''){
				$paginaActual = (int)$_POST['paginaActual'];
				if($paginaActual < 1){
					$paginaActual = 1;
				}
			}
			$limit = 25;

			$offset = ($paginaActual-1) * $limit;

			$obj = new stdClass();
			$obj->paginaInicio = $paginaActual - ($paginaActual % 10) + 1;
			$obj->paginaActual = $paginaActual;
			$obj->totalPaginas = 1;

			if(is_numeric($dato)){
				if($dato < 1000000){
					return;
				}
				$personas = $this->model->getAjaxPorCedula($dato, $id_eleccion, $filtrarConsejo);
				$personas = $this->calcularEdad($personas);
				$obj->personas = $personas;

				echo json_encode($obj);
			}else{
				$dato = strtolower($dato);
				$dato = explode(' ', $dato);
				if(count($dato) < 2){
					$dato[1] = '';
				}
				$personas = $this->model->getAjaxPorNombreYApellidoPaginar($dato, $id_eleccion, $filtrarConsejo, $limit, $offset);
				$personas[0] = $this->calcularEdad($personas[0]);
				$obj->personas = $personas[0];
				$obj->totalPaginas = ceil($personas[1] / $limit);

				echo json_encode($obj);
			}
		}

		public function getSinUsuarioAjax(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				return;
			}

			$dato = trim($_POST['dato']);
			if($dato == ""){
				return;
			}
			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			if(is_numeric($dato)){
				if($dato < 1000000){
					return;
				}
				$personas = $this->model->getAjaxPorCedulaSinUsuario($dato, $usuario);
				echo json_encode($personas);
			}else{
				$dato = strtolower($dato);
				$dato = explode(' ', $dato);
				if(count($dato) < 2){
					$dato[1] = '';
				}
				$personas = $this->model->getAjaxPorNombreYApellidoSinUsuario($dato, $usuario);
				echo json_encode($personas);
			}
		}

		public function calcularEdad($personas){
			if(!is_array($personas)){
				return;
			}
			foreach($personas as $persona){
				$tz  = new DateTimeZone('America/Caracas');
				$persona->edad = DateTime::createFromFormat('Y-m-d', $persona->fecha_nacimiento, $tz)
			    ->diff(new DateTime('now', $tz))
			    ->y;
				$persona->fechanacimiento = strftime("%d de %B de %Y", strtotime($persona->fecha_nacimiento));
				$persona->fecha_nacimiento = date("d/m/Y", strtotime($persona->fecha_nacimiento));
			}
			return $personas;
		}

		public function importarExcel(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				return;
			}

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);

			// SI NO ES SUPER USUARIO NI USUARIO NO PUEDE IMPORTAR
			if($usuario->tipo != 1 && $usuario->tipo != 2){
				die('0');
			}

			if(isset($_FILES['archivo'])){
				$allowedFileType = [
			        'application/vnd.ms-excel',
			        'text/xls',
			        'text/xlsx',
			        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			        'application/vnd.oasis.opendocument.spreadsheet'
			    ];
			    if (in_array($_FILES["archivo"]["type"], $allowedFileType)) {
			    	$archivo = $_FILES['archivo'];
			    	$tmp_name = $archivo['tmp_name'];
					if(!move_uploaded_file($archivo['tmp_name'], "./$tmp_name")){
						die('0');
					}

					/*$Reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			        $spreadSheet = $Reader->load("./$tmp_name");*/
			        $spreadSheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("./$tmp_name");
			        $excelSheet = $spreadSheet->getActiveSheet();
			        $spreadSheetAry = $excelSheet->toArray();
			        $sheetCount = count($spreadSheetAry);

			        set_time_limit(3600);

			        $actualizarDuplicados = $_POST['actualizarDuplicados'];

			        $consejocomunalModel = new consejocomunalModel();

			        $codigosComuna = $consejocomunalModel->getCodigosExistentes();

			        for ($i = 0; $i <= $sheetCount; $i ++) {
			            $cedula = 0;
			            if (isset($spreadSheetAry[$i][0])) {
			                $cedula = (int) trim($spreadSheetAry[$i][0]);
			                if(!is_numeric($cedula)){
			                	continue;
			                }
			            }else{
			            	continue;
			            }
			            $pnombre = "";
			            if (isset($spreadSheetAry[$i][1])) {
			                $pnombre = ucfirst(trim($spreadSheetAry[$i][1]));
			            }
			            $snombre = "";
			            if (isset($spreadSheetAry[$i][2])) {
			                $snombre = ucfirst(trim($spreadSheetAry[$i][2]));
			            }
			            $papellido = "";
			            if (isset($spreadSheetAry[$i][3])) {
			                $papellido = ucfirst(trim($spreadSheetAry[$i][3]));
			            }
			            $sapellido = "";
			            if (isset($spreadSheetAry[$i][4])) {
			                $sapellido = ucfirst(trim($spreadSheetAry[$i][4]));
			            }
			            $sexo = "";
			            if (isset($spreadSheetAry[$i][5])) {
			                $sexo = trim($spreadSheetAry[$i][5][0]);
			            }
			            $fecha_nacimiento = "";
			            if (isset($spreadSheetAry[$i][6])) {
			                $fecha_nacimiento = trim($spreadSheetAry[$i][6]);
			            }
			            $codcomuna = null;
			            if (isset($spreadSheetAry[$i][7])) {
			                $codcomuna = trim($spreadSheetAry[$i][7]);
			                if(!in_array($codcomuna, $codigosComuna)){
			                	$codcomuna = null;
			                }
			            }

						if($usuario->tipo == 2){
							// LOS NO SUPER USUARIOS SÓLO PUEDEN IMPORTAR PERSONAS A SU CONSEJO COMUNAL
							if($codcomuna != $usuario->cod_consejocomunal){
								continue;
							}
						}

			            $this->model->setCedula($cedula);
						$this->model->setPnombre($pnombre);
						$this->model->setSnombre($snombre);
						$this->model->setPapellido($papellido);
						$this->model->setSapellido($sapellido);
						$this->model->setSexo($sexo);
						$this->model->setFechaNacimiento($fecha_nacimiento);
						$this->model->setCodConsejocomunal($codcomuna);

						if($actualizarDuplicados == "1"){
							if($this->model->verificarSiExiste($cedula)){
								$datos = new stdClass();
								$datos->cedula = $cedula;
								$datos->pnombre = $pnombre;
								$datos->snombre = $snombre;
								$datos->papellido = $papellido;
								$datos->sapellido = $sapellido;
								$datos->sexo = $sexo;
								$datos->fecha_nacimiento = $fecha_nacimiento;
								$datos->cod_consejocomunal = $codcomuna;
								// SI NO ES SUPER USUARIO Y LA PERSONA YA EXISTE EN OTRO CONSEJO COMUNAL, NO SE MODIFICA
								if($usuario->tipo == 2 && !$this->model->verificarSiPerteneceConsejo($cedula, $usuario->cod_consejocomunal)){
									continue;
								}
								$this->model->editar($datos);
							}else{
								$this->model->agregar();
							}
						}else{
							if(!$this->model->verificarSiExiste($cedula)){
								$this->model->agregar();
							}
						}

			            
			        }

			        unlink("./$tmp_name");

			        die('1');


			    }else{
			    	die('0');
			    }

			}else{
				die('0');
			}
		}

		public function borrarTodos(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				return;
			}
			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			if($usuario->tipo == 2){
				// LOS NO SUPER USUARIOS SÓLO PUEDEN BORRAR PERSONAS DE SU CONSEJO COMUNAL
				echo $this->model->borrarTodos($usuario->cod_consejocomunal, $usuario->cedula);
				return;
			}elseif($usuario->tipo != 1){
				// SI NO ES SUPER USUARIO NI USUARIO NO PUEDE BORRAR
				die('no_puedes_borrar');
			}

			echo $this->model->borrarTodos($_POST['cod'], $usuario->cedula != '' ? $usuario->cedula : 0);
		}

		public function imprimirElectores(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				return;
			}

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);

			if($usuario->tipo == 2){
				// LOS NO SUPER USUARIOS SÓLO PUEDEN ACCEDER A PERSONAS DE SU CONSEJO COMUNAL
				echo json_encode($this->model->imprimirElectores($usuario->cod_consejocomunal));
				return;
			}elseif($usuario->tipo != 1){
				die('denegado');
			}

			echo json_encode($this->model->imprimirElectores($_POST['cod']));
		}

	}

?>