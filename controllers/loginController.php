<?php

	class loginController {

		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}
		
		public function index(){
			if(isset($_SESSION['usuario'])){
				include_once('./models/usuarioModel.php');
				$usuarioModel = new Usuario();
				$tipoUsuario = $usuarioModel->getTipo($_SESSION['id']);
				include_once('./views/menuView.php');
			}else{
				include_once('./views/loginView.php');
			}
		}

		public function check(){
			if(isset($_SESSION['maquina_votante'])){
				die('es_maquina_votante');
			}
			$usuario = $_POST['usuario'];
			$contrasena = $_POST['contrasena'];
			$captcha = $_POST['captcha'];

			if(!isset($_SESSION['captcha']) || $_SESSION['captcha'] == ''){
				die('captcha_incorrecto');
			}

			if(strtolower($captcha) != strtolower($_SESSION['captcha'])){
				$_SESSION['captcha'] = '';
				die('captcha_incorrecto');
			}

			$verificar = $this->db->prepare('SELECT * FROM usuario WHERE nombre = ?');
			$verificar->execute([ $usuario ]);

			if($verificar->rowCount() > 0){

				$datos = $verificar->fetchAll(PDO::FETCH_OBJ);
				$dato = $datos[0]->contraseña;
				
				$verificar = password_verify($contrasena, $dato);

				if($verificar){
					$_SESSION['usuario'] = $usuario;
					$_SESSION['id'] = $datos[0]->id;
					mainModel::actividad('ha iniciado sesión',null,null,$this->db);
					echo true;
				}else{
					$_SESSION['captcha'] = '';
					echo false;
				}

			}else{
				$_SESSION['captcha'] = '';
				echo false;
			}
		}

		public function getCaptcha(){
			function hextorgb ($hexstring){
				$integar = hexdec($hexstring);
				return array(
				"red" => 0xFF & ($integar >> 0x10),
				"green" => 0xFF & ($integar >> 0x8),
				"blue" => 0xFF & $integar
				);
			}
			$captcha = "";
			$captchaHeight = 60;
			$captchaWidth = 200;
			$totalCharacters = 6;
			$possibleLetters = "123456789mnbvcxzasdfghjklpoiuytrewwq";
			$captchaFont = __DIR__ . "/../views/img/font.ttf";
			$randomDots = 200;
			$randomLines = 80;
			$textColor = "2196F3";
			$noiseColor = "2196F3";
			$character = 0;
			while ($character < $totalCharacters) {
			$captcha .= substr($possibleLetters, mt_rand(0, strlen($possibleLetters)-1), 1);
			$character++;
			}
			$captchaFontSize = $captchaHeight * 0.65;
			$captchaImage = @imagecreate($captchaWidth,$captchaHeight);
			$backgroundColor = imagecolorallocate($captchaImage,255,255,255);
			$arrayTextColor = hextorgb($textColor);
			$textColor = imagecolorallocate($captchaImage,$arrayTextColor['red'],$arrayTextColor['green'],$arrayTextColor['blue']);
			$arrayNoiseColor = hextorgb($noiseColor);
			$imageNoiseColor = imagecolorallocate($captchaImage,$arrayNoiseColor['red'],$arrayNoiseColor['green'],$arrayNoiseColor['blue']);
			for( $captchaDotsCount=0; $captchaDotsCount<$randomDots; $captchaDotsCount++ ) {
			imagefilledellipse($captchaImage,mt_rand(0,$captchaWidth),mt_rand(0,$captchaHeight),2,3,$imageNoiseColor);
			}
			for( $captchaLinesCount=0; $captchaLinesCount<$randomLines; $captchaLinesCount++ ) {
			imageline($captchaImage,mt_rand(0,$captchaWidth),mt_rand(0,$captchaHeight),mt_rand(0,$captchaWidth),mt_rand(0,$captchaHeight),$imageNoiseColor);
			}
			$text_box = imagettfbbox($captchaFontSize,0,$captchaFont,$captcha);
			$x = ($captchaWidth - $text_box[4])/2;
			$y = ($captchaHeight - $text_box[5])/2;
			imagettftext($captchaImage,$captchaFontSize,0,$x,$y,$textColor,$captchaFont,$captcha);
			header('Content-Type: image/jpeg');
			imagejpeg($captchaImage);
			imagedestroy($captchaImage);
			$_SESSION['captcha'] = $captcha;
		}

	}

?>