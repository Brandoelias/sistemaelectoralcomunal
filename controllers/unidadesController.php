<?php
	
	include_once('./models/unidadesModel.php');
	include_once('./models/usuarioModel.php');

	class unidadesController{

		private $model;

		public function __construct(){
			$usuarioModel = new Usuario();
			if(isset($_SESSION['usuario']) && $usuarioModel->getTipo($_SESSION['id']) == 1){
				$this->model = new unidadesModel();
			}else{
				header('location: ' . SERVERURL . '/menu/');
				die();
			}
			
		}

		public function index(){
			$unidades = $this->model->getTodos();
			include_once('./views/unidadesView.php');
		}

		public function editarUnidad(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$datos = $_POST;
			$this->model->setId($datos['id']);

			if($this->model->verificarSiUnidadTieneEleccionComenzada()){
				die('eleccion_comenzada');
			}
			
			echo $this->model->editarUnidad($datos);
		}

		public function borrarUnidad(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$id = $_POST['id'];
			$this->model->setId($id);

			if($this->model->verificarSiUnidadTieneEleccionComenzada()){
				die('eleccion_comenzada');
			}

			echo $this->model->borrarUnidad();
		}

		public function agregarUnidad(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$nombre = $_POST['nombre'];
			$this->model->setNombre($nombre);
			echo $this->model->agregarUnidad();
		}

		public function borrarUnidadConsejo(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$cod = $_POST['codConsejo'];
			$unidades = json_decode($_POST['unidades']);
			if(!is_array($unidades) || count($unidades) < 1){
				die(false);
			}
			echo $this->model->borrarUnidadConsejo($cod, $unidades);
		}

		public function borrarComiteUnidad(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$cod = $_POST['codConsejo'];
			$idUnidad = $_POST['idUnidad'];
			$comites = json_decode($_POST['comites']);
			if(!is_array($comites) || count($comites) < 1){
				die(false);
			}
			$this->model->setId($idUnidad);
			echo $this->model->borrarComiteUnidad($cod, $comites);
		}

	}


?>