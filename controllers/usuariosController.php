<?php

include_once('./models/usuarioModel.php');
include_once('./models/personasModel.php');

class usuariosController {

	private $model;

	public function __construct(){
		$this->model = new Usuario();
		if(!isset($_SESSION['usuario'])){
			header('location: ' . SERVERURL . '/menu/');
			die();
		}
	}

	public function index($paginaActual = 1){
		// SÓLAMENTE SUPER USUARIOS Y USUARIOS PUEDEN ACCEDER
		$usuario = $this->model->getUsuario($_SESSION['id']);
		$tipoUsuario = $usuario->tipo;
		if($tipoUsuario != 1 && $tipoUsuario != 2){
			header('location: ' . SERVERURL . '/menu/');
			die();
		}

		if($paginaActual < 1){
			$paginaActual = 1;
		}

		$filtrarConsejo = false;
		if(isset($_GET['consejo'])){
			$filtrarConsejo = $_GET['consejo'];
		}

		if($tipoUsuario != 1){
			$filtrarConsejo = $usuario->cod_consejocomunal;
		}

		$totalUsuarios = $this->model->getCantidadTotal($usuario, $filtrarConsejo);
		$limite = 25;
		if($paginaActual > ceil($totalUsuarios / $limite) && $paginaActual != 1){
			include_once('./views/404.php');
			die();
		}

		//consultar consejos comunales
		include_once('./models/consejocomunalModel.php');
		$consejoscomunales = new consejocomunalModel();
		$consejoscomunales = $consejoscomunales->getTodos();

		$offset = ($paginaActual-1) * $limite;

		$usuarios = $this->model->getTodos($limite, $offset, $usuario, $filtrarConsejo ? $filtrarConsejo : '');
		

		include_once('./views/usuariosView.php');
	}

	public function cambiarClave(){
		if($_SERVER['REQUEST_METHOD'] != 'POST'){
			header('location: ' . SERVERURL . '/');
			return;
		}
		// SE VERIFICA QUE SÓLAMENTE EL USUARIO SE PUEDA MODIFICAR A SI MISMO O SI ES SUPER USUARIO O USUARIO
		$usuario = $this->model->getUsuario($_SESSION['id']);
		$tipoUsuario = $usuario->tipo;
		if((int)$_POST['id'] == $_SESSION['id'] || $tipoUsuario == 1 || $tipoUsuario == 2){
			$id = $_POST['id'];
			// SÓLO SE PUEDE MODIFICAR EL USUARIO POR DEFECTO SI ÉS SU SESIÓN
			if(($id == '1' || $id == 1) && $_SESSION['id'] != 1){
				die('no_admin');
			}

			// SI SE ESTÁ CAMBIANDO LA CLAVE A OTRO USUARIO Y NO ES SUPER USUARIO, SE VERIFICA QUE ESTÉ CAMBIÁNDOSELA A UN ADMINISTRADOR DE ELECCIÓN DE SU CONSEJO COMUNAL
			if((int)$_POST['id'] != $_SESSION['id']){
				if($tipoUsuario != 1){
					$persona = $this->model->getUsuario($id);
					if(!$persona || $usuario->cod_consejocomunal != $persona->cod_consejocomunal){
						die(false);
					}
					if($persona->tipo != 3){
						die(false);
					}
				}
			}

			// SI SE ESTÁ MODIFICANDO LA CONTRASEÑA DEL MISMO USUARIO, SE VERIFICA SI INGRESÓ CORRECTAMENTE SU CONTRASEÑA ACTUAL ANTES DE CAMBIAR
			if((int)$_POST['id'] == $_SESSION['id']){
				if($this->getTipo() == 1 && $_POST['claveActual'] == ''){
					// NO VERIFICAR CLAVE ACTUAL SI ES SUPER USUARIO Y NO LA INGRESÓ
				}else{
					$claveActual = $_POST['claveActual'];
					if(!$this->model->verificarClaveActual($id, $claveActual)){
						die('clave_actual_no');
					}
				}
			}
			$clave = $_POST['clave'];
			$clave = password_hash($clave, PASSWORD_BCRYPT, array('cost'=>12));
			echo $this->model->cambiarClave($id, $clave);
		}else{
			die(false);
		}
	}

	public function agregar(){
		if($_SERVER['REQUEST_METHOD'] != 'POST'){
			header('location: ' . SERVERURL . '/');
			return;
		}
		// SÓLAMENTE SUPER USUARIOS Y USUARIOS PUEDEN ACCEDER
		$usuario = $this->model->getUsuario($_SESSION['id']);
		$tipoUsuario = $usuario->tipo;
		if($tipoUsuario != 1 && $tipoUsuario != 2){
			header('location: ' . SERVERURL . '/menu/');
			die();
		}

		$cedula = $_POST['cedula'];
		$nombre = $_POST['usuario'];
		$clave = password_hash($_POST['clave'], PASSWORD_BCRYPT, array('cost'=>12));
		if(isset($_POST['tipo'])){
			$tipo = $_POST['tipo'];
		}

		// SI NO ES SUPER USUARIO ENTONCES EL TIPO DE USUARIO A AGREGAR SIEMPRE SERÁ ADMINISTRADOR DE ELECCIÓN, ADEMÁS DE SÓLAMENTE PODER AGREGAR USUARIOS A SU CONSEJO COMUNAL
		if($tipoUsuario != 1){
			$tipo = 3;
			$personaModel = new personasModel();
			$persona = $personaModel->getPorCedula($cedula);
			if(!$persona || $usuario->cod_consejocomunal != $persona->cod_consejocomunal){
				die(false);
			}
		}

		if($this->model->verificarSiExiste($nombre)){
			die('ya_existe');
		}

		echo $this->model->agregar($cedula, $nombre, $clave, $tipo);
	}

	public function modificar(){
		if($_SERVER['REQUEST_METHOD'] != 'POST'){
			header('location: ' . SERVERURL . '/');
			return;
		}
		// SÓLAMENTE SUPER USUARIOS Y USUARIOS PUEDEN ACCEDER
		$usuario = $this->model->getUsuario($_SESSION['id']);
		$tipoUsuario = $usuario->tipo;
		if($tipoUsuario == 1 || $tipoUsuario == 2){
			$id = $_POST['id'];
			// SÓLO SE PUEDE MODIFICAR EL USUARIO POR DEFECTO SI ÉS SU SESIÓN
			if(($id == '1' || $id == 1) && $_SESSION['id'] != 1){
				die('no_admin');
			}
			$nombre = $_POST['usuario'];
			$tipo = null;
			if(isset($_POST['tipo'])){
				$tipo = $_POST['tipo'];
				if((int)$tipo < 1 || (int)$tipo > 3 ){
					die(false);
				}
			}

			// SI NO ES SUPER USUARIO ENTONCES EL TIPO DE USUARIO A MODIFICAR SIEMPRE SERÁ ADMINISTRADOR DE ELECCIÓN, ADEMÁS DE SÓLAMENTE PODER MODIFICAR USUARIOS DE SU CONSEJO COMUNAL QUE SEAN ADMINISTRADOR DE ELECCION
			if($tipoUsuario != 1){
				$tipo = 3;
				$persona = $this->model->getUsuario($id);
				if(!$persona || $usuario->cod_consejocomunal != $persona->cod_consejocomunal){
					die(false);
				}
				if($persona->tipo != 3){
					die(false);
				}
			}

			echo $this->model->modificar($id, $nombre, $tipo);
		}else{
			die(false);
		}
	}

	public function borrar(){
		if($_SERVER['REQUEST_METHOD'] != 'POST'){
			header('location: ' . SERVERURL . '/');
			return;
		}
		// SÓLAMENTE SUPER USUARIOS Y USUARIOS PUEDEN ACCEDER
		$usuario = $this->model->getUsuario($_SESSION['id']);
		$tipoUsuario = $usuario->tipo;
		if($tipoUsuario != 1 && $tipoUsuario != 2){
			header('location: ' . SERVERURL . '/menu/');
			die();
		}

		$id = $_POST['id'];

		// SÓLAMENTE PODER BORRAR USUARIOS DE SU CONSEJO COMUNAL QUE SEAN ADMINISTRADOR DE ELECCIÓN
		if($tipoUsuario != 1){
			$persona = $this->model->getUsuario($id);
			if(!$persona || $usuario->cod_consejocomunal != $persona->cod_consejocomunal){
				die(false);
			}
			if($persona->tipo != 3){
				die(false);
			}
		}

		echo $this->model->borrar($id);
	}

	public function getTipo(){
		return $this->model->getTipo($_SESSION['id']);
	}


	public function getAjax(){
		if($_SERVER['REQUEST_METHOD'] != 'POST'){
			header('location: ' . SERVERURL . '/');
			return;
		}

		$dato = trim($_POST['dato']);
		$filtrarConsejo = '';
		if(isset($_POST['filtrarConsejo'])){
			$filtrarConsejo = $_POST['filtrarConsejo'];
		}
		if($dato == ""){
			return;
		}

		$usuario = $this->model->getUsuario($_SESSION['id']);

		if(is_numeric($dato)){
			if($dato < 1000000){
				return;
			}
			$usuarios = $this->model->getAjaxPorCedula($dato, $filtrarConsejo, $usuario);
			echo json_encode($usuarios);
		}else{
			$dato = strtolower($dato);
			$dato = explode(' ', $dato);
			if(count($dato) < 2){
				$dato[1] = '';
			}
			$usuarios = $this->model->getAjaxPorNombreYApellido($dato, $filtrarConsejo, $usuario);
			echo json_encode($usuarios);
		}
	}

}