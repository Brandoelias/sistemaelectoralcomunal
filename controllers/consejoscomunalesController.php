<?php

	include_once('./models/consejocomunalModel.php');
	include_once('./models/usuarioModel.php');

	class consejoscomunalesController{

		private $model;

		public function __construct($accion = ''){
			// ARREGLO CON RUTAS QUE SE PODRÁ ACCEDER SIN SER SUPER USUARIO
			$rutasAbiertas = ['consultartodos'];

			$usuarioModel = new Usuario();
			if(!isset($_SESSION['usuario']) || ($usuarioModel->getTipo($_SESSION['id']) != 1 && !in_array($accion, $rutasAbiertas))){
				header('location: ' . SERVERURL . '/menu/');
				die();
			}else{
				$this->model = new consejocomunalModel();
			}
		}

		public function index(){
			$consejosComunales = $this->model->getTodos();
			foreach($consejosComunales as $cc){
				$cc->habitantes = $this->model->getCantidadHabitantes($cc->cod);
			}
			include_once('./models/bancoModel.php');
			$banco = new bancoModel();
			$banco = $banco->getTodos();
			include_once('./views/consejoscomunalesView.php');
		}

		public function editarConsejoComunal(){
			$datos = $_POST;
			if($datos['cod'] != $datos['codM'] && $this->model->verificarSiExiste($datos['codM'])){
				die('codigo_existe');
			}
			$this->model->setCod($datos['cod']);
			echo $this->model->editarConsejoComunal($datos);
		}

		public function borrarConsejoComunal(){
			$cod = $_POST['cod'];
			$this->model->setCod($cod);
			echo $this->model->borrarConsejoComunal();
		}

		public function agregarConsejoComunal(){
			$datos = $_POST;
			if($this->model->verificarSiExiste($datos['cod'])){
				die('codigo_existe');
			}
			$this->model->setCod($datos['cod']);
			$this->model->setNombre($datos['nombre']);
			$this->model->setTelefono(isset($datos['telefono']) ? $datos['telefono'] : null);
			$this->model->setRif(isset($datos['rif']) ? $datos['rif'] : null);
			$this->model->setCorreo(isset($datos['correo']) ? $datos['correo'] : null);
			$this->model->setIdBanco(isset($datos['id_banco']) && $datos['id_banco'] != '' ? $datos['id_banco'] : 0);
			$this->model->setNumerodecuenta(isset($datos['numerodecuenta']) ? $datos['numerodecuenta'] : null);

			echo $this->model->agregarConsejoComunal();
		}

		public function verUnidades(){
			$cod = $_POST['cod'];
			$this->model->setCod($cod);
			echo json_encode($this->model->verUnidades());
		}

		public function getUnidadesNotIn(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$cod = $_POST['cod'];
			$this->model->setCod($cod);
			echo json_encode($this->model->getUnidadesNotIn());
		}

		public function agregarUnidadConsejo(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$unidades = json_decode($_POST['unidades']);
			$this->model->setCod($_POST['cod']);
			echo $this->model->agregarUnidadConsejo($unidades);
		}

		public function consultarTodos(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			echo json_encode($this->model->getTodos());
		}

		public function verificarSiExiste($cod){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			return $this->model->verificarSiExiste($cod);
		}

		public function getCodigosExistentes(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			return $this->model->getCodigosExistentes();
		}

		public function actividad(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die(false);
			}

			$id = $_POST['id'];

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die(false);
			}
			if($tipoUsuario != 1){
				$eleccion = $this->model->getEleccion($id);
				if($usuario->cod_consejocomunal != $id){
					die(false);
				}
			}

			$paginaActual = 1;
			if(isset($_POST['paginaActual'])){
				$paginaActual = (int)$_POST['paginaActual'];
			}
			$limit = 50;

			$offset = ($paginaActual-1) * $limit;
			
			$obj = new stdClass();
			$obj->actividad = $this->model->actividad($id, $offset, $limit);
			$obj->paginaInicio = $paginaActual - ($paginaActual % 10) + 1;
			$obj->totalPaginas = ceil($this->model->totalActividad($id) / $limit);
			$obj->paginaActual = $paginaActual;

			echo json_encode($obj);
		}


		public function cambiarVotosMaximos(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				die(false);
			}
			if(!isset($_POST['votosMaximos']) || !isset($_POST['codConsejo'])){
				die(false);
			}
			$votosMaximos = json_decode($_POST['votosMaximos']);
			if(!is_array($votosMaximos)){
				die(false);
			}

			include_once('./models/eleccionesModel.php');
			$eleccionesModel = new eleccionesModel();

			if($eleccionesModel->verificarSiConsejoTieneEleccionComenzada($_POST['codConsejo'])){
				die('eleccion_comenzada');
			}

			$this->model->setCod($_POST['codConsejo']);
			echo $this->model->cambiarVotosMaximos($votosMaximos);
		}

	}

?>