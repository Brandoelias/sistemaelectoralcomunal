<?php

	include_once('./models/eleccionesModel.php');
	include_once('./models/personasModel.php');
	include_once('./models/usuarioModel.php');

	class eleccionesController{

		private $model;
		private $ip;

		public function __construct(){
			$this->ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
		}

		public function autenticado(){
			if(!isset($_SESSION['usuario'])){
				header('location: ' . SERVERURL . '/');
				die();
			}else{
				$this->model = new eleccionesModel();
			}
		}

		public function index(){
			$this->autenticado();
			$usuarioModel = new Usuario();
			$tipoUsuario = $usuarioModel->getTipo($_SESSION['id']);
			include_once('./views/eleccionesView.php');
		}

		public function crear(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die(false);
			}
			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die('¡No tienes permitido crear Elecciones!');
			}

			if(isset($_POST['consejocomunal'])){
				$cod_consejocomunal = $_POST['consejocomunal'];
			}
			if($tipoUsuario == 2){
				$cod_consejocomunal = $usuario->cod_consejocomunal;
			}
			if(!isset($cod_consejocomunal)){
				die('!Consejo comunal incorrecto!');
			}
			$_POST['fecha'] = date_create_from_format('d/m/Y',$_POST['fecha']);
			if(!$_POST['fecha']){
				die('¡Fecha de nacimiento incorrecta!');
			}
			$_POST['fecha'] = date_format($_POST['fecha'], 'Y-m-d');
			$fecha = $_POST['fecha'];

			$this->model->setCodConsejocomunal($cod_consejocomunal);
			$this->model->setFecha($fecha);
			echo $this->model->crear();
		}

		public function activas(){
			$this->autenticado();
			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			if($tipoUsuario == 1){
				$elecciones = $this->model->getActivas();
			}elseif($tipoUsuario == 2 || $tipoUsuario == 3){
				$elecciones = $this->model->getActivas($usuario->cod_consejocomunal);
			}else{
				header('location: ' . SERVERURL . '/');
				die();
			}
			foreach ($elecciones as $eleccion) {
				$eleccion->fecha_formato = strftime("%d de %B de %Y", strtotime($eleccion->fecha));
			}
			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			include_once('./views/eleccionesActivasView.php');
		}

		public function finalizadas(){
			$this->autenticado();
			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			if($tipoUsuario == 1){
				$elecciones = $this->model->getFinalizadas();
			}elseif($tipoUsuario == 2 || $tipoUsuario == 3){
				$elecciones = $this->model->getFinalizadas($usuario->cod_consejocomunal);
			}else{
				header('location: ' . SERVERURL . '/');
				die();
			}
			foreach ($elecciones as $eleccion) {
				$eleccion->fecha_formato = strftime("%d de %B de %Y", strtotime($eleccion->fecha));
			}
			include_once('./views/eleccionesFinalizadasView.php');
		}

		public function ver($url){
			$this->autenticado();
			$id = $url[2];
			$this->model->setId($id);
			$eleccion = $this->model->consultarPorId();
			if(!$eleccion){
				include_once('./views/404.php');
				die();
			}

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ ACCEDIENDO A UNA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					include_once('./views/404.php');
					die();
				}
			}

			$eleccion->fecha_formato = strftime("%d de %B de %Y", strtotime($eleccion->fecha));

			if(Date($eleccion->fecha) <= Date('Y-m-d')){
				$iniciarVotacion = true;
			}else{
				$iniciarVotacion = false;
			}

			$finalizado = $this->model->verificarSiYAFinalizo();
			$comenzado = $this->model->verificarSiYaComenzo();

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			include_once('./views/eleccionesVerView.php');
		}

		public function candidatos($url){
			$this->autenticado();

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			$id = $url[2];

			if($tipoUsuario != 1 && $tipoUsuario != 2){
				header('location: ' . SERVERURL . '/elecciones/ver/' . $id . '/');
				die();
			}

			$this->model->setId($id);
			$eleccion = $this->model->consultarPorId();
			if(!$eleccion){
				include_once('./views/404.php');
				die();
			}

			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					include_once('./views/404.php');
					die();
				}
			}

			$finalizado = $this->model->verificarSiYAFinalizo();
			$comenzado = $this->model->verificarSiYaComenzo();
			if($finalizado || $comenzado){
				header('location: ' . SERVERURL . '/elecciones/ver/' . $id . '/');
				die();
			}
			$eleccion->fecha_formato = strftime("%d de %B de %Y", strtotime($eleccion->fecha));
			include_once('./models/unidadesModel.php');
			$unidades = new unidadesModel();
			$unidades = $unidades->getPorConsejocomunal($eleccion->cod_consejocomunal);
			foreach ($unidades as $unidad) {
				$unidad->candidatos = $this->model->getCantidadDeCandidatosPorUnidad($unidad->id_unidad, $eleccion->cod_consejocomunal);
			}

			include_once('./views/candidatosView.php');
		}

		public function verCandidatosUnidad(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}

			$idEleccion = $_POST['idEleccion'];

			$eleccion = $this->model->getEleccion($idEleccion);

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die(false);
			}
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ ACCEDIENDO A UNA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					die(false);
				}
			}

			$idUnidad = $_POST['idUnidad'];
			$cod_consejocomunal = $_POST['cod_consejocomunal'];
			$this->model->setId($idEleccion);
			$res = new \stdClass();

			$cantComites = $this->model->getCantidadDeComitesPorUnidad($idUnidad, $cod_consejocomunal);
			if($cantComites > 0){
				$comites = $this->model->getComitesPorUnidad($idUnidad, $cod_consejocomunal);
				$res->comites = true;
				foreach ($comites as $comite) {
					$comite->candidatos = $this->model->getCantidadDeCandidatosPorComite($idUnidad, $comite->id);
				}
				$res->comites = $comites;	
			}else{
				$candidatos = $this->model->verCandidatosUnidad($idUnidad);
				$res->comites = false;
				$res->candidatos = $candidatos;
			}

			echo json_encode($res);

		}

		public function verCandidatosComite(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}

			$idEleccion = $_POST['idEleccion'];

			$eleccion = $this->model->getEleccion($idEleccion);

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die(false);
			}
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ ACCEDIENDO A UNA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					die(false);
				}
			}

			$idUnidad = $_POST['idUnidad'];
			$idComite = $_POST['idComite'];
			$this->model->setId($idEleccion);
			$res = new \stdClass();

			echo json_encode($this->model->verCandidatosComite($idUnidad, $idComite));

		}

		public function getPersonasAjax(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}

			$idEleccion = $_POST['idEleccion'];

			$eleccion = $this->model->getEleccion($idEleccion);

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die(false);
			}
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ ACCEDIENDO A UNA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					die(false);
				}
			}

			$idUnidad = $_POST['idUnidad'];
			$idComite = $_POST['idComite'];
			$dato = explode(' ', $_POST['dato']);

			foreach($dato as $i=>$d){
				$dato[$i] = strtolower($d);
			}

			if(is_numeric($dato[0])){
				if($dato[0] < 1000000){
					return;
				}
			}

			for($i = 1; $i <= 3; $i++){
				if(!isset($dato[$i])){
					$dato[$i] = '';
				}
			}

			$this->model->setId($idEleccion);

			if(is_numeric($dato[0]) && $idComite != 'undefined'){
				echo json_encode($this->model->getPersonasComiteCedulaAjax($idUnidad, $idComite, $dato[0]));
			}elseif(is_numeric($dato[0])){
				echo json_encode($this->model->getPersonasUnidadCedulaAjax($idUnidad, $dato[0]));
			}elseif(isset($idComite) && $idComite != 'undefined' && !is_int($dato[0])){
				echo json_encode($this->model->getPersonasComiteNombreAjax($idUnidad, $idComite, $dato));
			}else{
				echo json_encode($this->model->getPersonasUnidadNombreAjax($idUnidad, $dato));
			}

		}

		public function guardarCandidatoPostulado(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}

			$idEleccion = $_POST['idEleccion'];

			$eleccion = $this->model->getEleccion($idEleccion);

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die('{"err" : "1", "msj" : "¡No tienes permitido postular candidatos en esta elección!"}');
			}
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ ACCEDIENDO A UNA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					die('{"err" : "1", "msj" : "¡No tienes permitido postular candidatos en esta elección!"}');
				}
			}

			$listaPersonas = json_decode($_POST['listaPersonas']);
			$idUnidad = $_POST['idUnidad'];

			$this->model->setId($idEleccion);

			$finalizado = $this->model->verificarSiYAFinalizo();
			$comenzado = $this->model->verificarSiYaComenzo();
			if($finalizado || $comenzado){
				die('{"err" : "1", "msj" : "¡No se pueden postular candidatos a esta Elección!"}');
				return false;
			}

			$this->model->setId($idEleccion);

			if(isset($_POST['idComite']) && $_POST['idComite'] != 'undefined'){
				$idComite = $_POST['idComite'];
				echo $this->model->guardarCandidatoPostuladoUnidad($idUnidad, $idComite, $listaPersonas);
			}else{
				echo $this->model->guardarCandidatoPostuladoComite($idUnidad, $listaPersonas);
			}

		}

		public function borrarCandidato(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}

			$idEleccion = $_POST['idEleccion'];

			$eleccion = $this->model->getEleccion($idEleccion);

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			// SI NO ES SUPER USUARIO NI USUARIO NO CONTINUA
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die(false);
			}
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ BORRANDO UN CANDIDATO DE LA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					die(false);
				}
			}

			$cedulas = json_decode($_POST['cedulas']);
			if(!is_array($cedulas) || count($cedulas) < 1){
				die(false);
			}
			$idUnidad = $_POST['idUnidad'];

			$this->model->setId($idEleccion);

			$finalizado = $this->model->verificarSiYAFinalizo();
			$comenzado = $this->model->verificarSiYaComenzo();
			if($finalizado || $comenzado){
				die(false);
				return false;
			}

			if(isset($_POST['idComite'])){
				$idComite = $_POST['idComite'];
			}else{
				$idComite = false;
			}

			$this->model->setId($idEleccion);

			echo $this->model->borrarCandidato($cedulas, $idUnidad, $idComite);

		}

		public function boletaelectoral($url){
			$this->autenticado();
			$id = $url[2];
			$this->model->setId($id);
			$eleccion = $this->model->consultarPorId();
			if(!$eleccion){
				include_once('./views/404.php');
				die();
			}

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ ACCEDIENDO A UNA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					include_once('./views/404.php');
					die();
				}
			}

			$eleccion->fecha_formato = strftime("%d de %B de %Y", strtotime($eleccion->fecha));

			/*
			include_once('./models/unidadesModel.php');
			$unidades = new unidadesModel();
			$unidades = $unidades->getPorConsejocomunal($eleccion->cod_consejocomunal);
			*/

			$unidades = $this->model->getUnidadesResultados();

			include_once('./models/comitesModel.php');
			$comites = new comitesModel();

			foreach ($unidades as $unidad) {
				//$unidad->comites = $comites->getPorConsejoUnidad($eleccion->cod_consejocomunal, $unidad->id);
				$unidad->comites = $this->model->getComitesUnidadResultados($unidad->id);
				foreach ($unidad->comites as $unidadComites) {
					$unidadComites->candidatos = $this->model->verCandidatosComite($unidad->id, $unidadComites->id);
				}
				$unidad->candidatos = $this->model->verCandidatosUnidad($unidad->id);
			}

			//Ordenar el arreglo de unidades para mostrar primero los que no tienen comites
			$unidadesSinComites = [];
			$unidadesConComites = [];

			foreach($unidades as $unidad){
				if(count($unidad->comites) < 1){
					array_push($unidadesSinComites, $unidad);
				}else{
					array_push($unidadesConComites, $unidad);
				}
			}

			$unidades = array_merge($unidadesSinComites, $unidadesConComites);

			include_once('./views/boletaElectoralView.php');
		}

		public function votar($url){
			$this->model = new eleccionesModel();
			$id = $url[2];
			$this->model->setId($id);
			$eleccion = $this->model->consultarPorId();
			if(!$eleccion){
				include_once('./views/404.php');
				die();
			}

			if(Date($eleccion->fecha) > Date('Y-m-d') || !isset($_SESSION['maquina_votante'])){
				error_log('SE INTENTO VOTAR EN UNA FECHA O SESION INCORRECTA');
				$_SESSION['maquina_votante'] = null;
				$_SESSION['maquina_votante_ideleccion'] = null;
				$_SESSION['maquina_votante_id'] = null;
				header('location: ' . SERVERURL . '/elecciones/autenticar/' . $id);
				die();
			}

			$verificar = $this->model->verificarMaquinaVotanteActivo($this->ip, $_SESSION['maquina_votante_id']);
			if(!$verificar || count($verificar) <= 0 || !$verificar[0]->activado){
				header('location: ' . SERVERURL . '/elecciones/esperar/' . $eleccion->id . '/');
				die();
			}else{
				$personasModel = new personasModel();
				$votando = $personasModel->getPorCedula($verificar[0]->votando);
			}
			
			/*
			include_once('./models/unidadesModel.php');
			$unidades = new unidadesModel();
			$unidades = $unidades->getPorConsejocomunal($eleccion->cod_consejocomunal);
			*/

			$unidades = $this->model->getUnidadesResultados();

			include_once('./models/comitesModel.php');
			$comites = new comitesModel();

			foreach ($unidades as $unidad) {
				//$unidad->comites = $comites->getPorConsejoUnidad($eleccion->cod_consejocomunal, $unidad->id);
				$unidad->comites = $this->model->getComitesUnidadResultados($unidad->id);
				foreach ($unidad->comites as $unidadComites) {
					$unidadComites->candidatos = $this->model->verCandidatosComite($unidad->id, $unidadComites->id);
				}
				$unidad->candidatos = $this->model->verCandidatosUnidad($unidad->id);
			}

			//Ordenar el arreglo de unidades para mostrar primero los que no tienen comites
			$unidadesSinComites = [];
			$unidadesConComites = [];

			foreach($unidades as $unidad){
				if(count($unidad->comites) < 1){
					array_push($unidadesSinComites, $unidad);
				}else{
					array_push($unidadesConComites, $unidad);
				}
			}

			$unidades = array_merge($unidadesSinComites, $unidadesConComites);

			include_once('./views/votar.php');

		}

		public function guardarVotacion(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			if(!isset($_SESSION['maquina_votante']) || $_SESSION['maquina_votante_ideleccion'] != $_POST['idEleccion']){
				die(false);
			}
			$this->model = new eleccionesModel();
			$id = $_POST['idEleccion'];
			$this->model->setId($id);
			$verificar = $this->model->verificarMaquinaVotante($this->ip, $_SESSION['maquina_votante_id']);
			if(!$verificar){
				$this->model->cancelarVotante($_SESSION['maquina_votante_id'], $this->ip, $id);
				$_SESSION['maquina_votante'] = null;
				$_SESSION['maquina_votante_ideleccion'] = null;
				$_SESSION['maquina_votante_id'] = null;
				die(false);
			}
			$votos = json_decode($_POST['votos']);

			echo $this->model->guardarVotacion($votos, $this->ip);
		}

		public function cambiarFecha(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			if($tipoUsuario != 1 && $tipoUsuario != 2){
				echo false;
				die();
			}

			$idEleccion = $_POST['idEleccion'];

			$this->model->setId($idEleccion);

			$finalizado = $this->model->verificarSiYAFinalizo();
			$comenzado = $this->model->verificarSiYaComenzo();
			if($finalizado || $comenzado){
				return false;
			}

			$fecha = $_POST['fecha'];

			$fecha1 = Date($fecha);
			$fecha2 = Date('Y-m-d');

			if($fecha1 < $fecha2){
				echo false;
				die();
			}

			$this->model->setId($idEleccion);
			echo $this->model->cambiarFecha($fecha);
		}

		public function resultados($url){
			$this->autenticado();

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			$idEleccion = $url[2];
			$this->model->setId($idEleccion);
			$eleccion = $this->model->consultarPorId();
			if(!$eleccion){
				include_once('./views/404.php');
				die();
			}

			if($tipoUsuario != 1 && $tipoUsuario != 2){
				header('location: ' . SERVERURL . '/elecciones/ver/' . $eleccion->id . '/');
				die();
			}

			$eleccion->fecha_formato = strftime("%d de %B de %Y", strtotime($eleccion->fecha));

			if(!$eleccion->finalizado){
				header('location: ' . SERVERURL . '/elecciones/ver/' . $eleccion->id . '/');
				die();
			}

			/*
			include_once('./models/unidadesModel.php');
			$unidades = new unidadesModel();
			$unidades = $unidades->getPorConsejocomunal($eleccion->cod_consejocomunal);
			*/

			$unidades = $this->model->getUnidadesResultados();

			/*
			include_once('./models/comitesModel.php');
			$comites = new comitesModel();
			*/

			foreach ($unidades as $unidad) {
				//$unidad->comites = $comites->getPorConsejoUnidad($eleccion->cod_consejocomunal, $unidad->id);
				$unidad->comites = $this->model->getComitesUnidadResultados($unidad->id);
				foreach ($unidad->comites as $unidadComites) {
					$unidadComites->candidatos = $this->model->verCandidatosComite($unidad->id, $unidadComites->id, true);
				}
				$unidad->candidatos = $this->model->verCandidatosUnidad($unidad->id, true);
			}

			//Ordenar el arreglo de unidades para mostrar primero los que no tienen comites
			$unidadesSinComites = [];
			$unidadesConComites = [];

			foreach($unidades as $unidad){
				if(count($unidad->comites) < 1){
					array_push($unidadesSinComites, $unidad);
				}else{
					array_push($unidadesConComites, $unidad);
				}
			}

			$unidades = array_merge($unidadesSinComites, $unidadesConComites);

			$cantParticipantes = $this->model->cantidadDeParticipantes();

			include_once('./views/eleccionResultadosView.php');
		}

		public function administrar($url){
			$this->autenticado();
			$idEleccion = $url[2];
			$this->model->setId($idEleccion);
			$eleccion = $this->model->consultarPorId();
			if(!$eleccion){
				include_once('./views/404.php');
				die();
			}

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			// SI NO ES SUPER USUARIO SE VERIFICA QUE LA ELECCIÓN PERTENECE A SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					header('location: ' . SERVERURL . '/elecciones/ver/' . $eleccion->id . '/');
					die();
				}
			}

			$eleccion->fecha_formato = strftime("%d de %B de %Y", strtotime($eleccion->fecha));

			$fecha1 = Date($eleccion->fecha);
			$fecha2 = Date('Y-m-d');

			if($eleccion->finalizado || $fecha1 > $fecha2){
				header('location: ' . SERVERURL . '/elecciones/ver/' . $eleccion->id . '/');
				die();
			}
			include_once('./views/eleccionesAdministrarView.php');
		}

		public function getMaquinasVotantes(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}

			$idEleccion = $_POST['idEleccion'];
			$this->model->setId($idEleccion);

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			$eleccion = $this->model->getEleccion($idEleccion);

			if(!isset($eleccion->cod_consejocomunal)){
				die(false);
			}

			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ ACCEDIENDO A UNA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					die(false);
				}
			}

			set_time_limit(60);
			ignore_user_abort(false);

			echo json_encode($this->model->getMaquinasVotantes($usuario));
		}

		public function getAdministradoresEleccion(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}

			$id_maquina = $_POST['idMaquina'];

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			// SI NO ES SUPER USUARIO NI USUARIO NO CONTINUA
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die(false);
			}

			// VERIFICAR QUE LA MAQUINA DE VOTACION EXISTE
			if(!$this->model->getMaquinaVotantePorId($id_maquina)){
				die('no_maquina');
			}

			echo json_encode($this->model->getAdministradoresEleccion($usuario, $id_maquina));
		}

		public function esperar($url){
			if(isset($_SESSION['usuario'])){
				header('location: ' . SERVERURL . '/elecciones/administrar/' .  $url[2] . '/');
				die();
			}
			$this->model = new eleccionesModel();
			$idEleccion = $url[2];
			$this->model->setId($idEleccion);
			$eleccion = $this->model->consultarPorId();
			if(!$eleccion){
				include_once('./views/404.php');
				die();
			}
			if(!isset($_SESSION['maquina_votante'])){
				header('location: ' . SERVERURL . '/elecciones/autenticar/' . $idEleccion . '/');
				die();
			}

			$verificar = $this->model->verificarMaquinaVotante($this->ip, $_SESSION['maquina_votante_id']);
			if(!$verificar){
				$_SESSION['maquina_votante'] = null;
				$_SESSION['maquina_votante_ideleccion'] = null;
				$_SESSION['maquina_votante_id'] = null;
				header('location: ' . SERVERURL . '/elecciones/autenticar/' . $idEleccion . '/');
				die();
			}

			include_once('./views/esperarView.php');

		}

		public function autenticar($url){
			if(isset($_SESSION['usuario'])){
				header('location: ' . SERVERURL . '/elecciones/administrar/' .  $url[2] . '/');
				die();
			}
			$this->model = new eleccionesModel();
			$idEleccion = $url[2];
			$this->model->setId($idEleccion);
			$eleccion = $this->model->consultarPorId();
			if(!$eleccion){
				include_once('./views/404.php');
				die();
			}

			if(Date($eleccion->fecha) > Date('Y-m-d')){
				include_once('./views/404.php');
				die();
			}

			if($eleccion->finalizado){
				include_once('./views/404.php');
				die();
			}
			if(isset($_SESSION['maquina_votante'])){
				header('location: ' . SERVERURL . '/elecciones/esperar/' . $idEleccion . '/');
				die();
			}

			include_once('./views/eleccionAutenticarView.php');
			
		}

		public function autenticarse(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			if(isset($_SESSION['usuario'])){
				header('location: ' . SERVERURL . '/elecciones/administrar/' .  $_POST['idEleccion'] . '/');
				die();
				return;
			}

			$captcha = $_POST['captcha'];

			if(!isset($_SESSION['captcha']) || $_SESSION['captcha'] == ''){
				die('captcha_incorrecto');
			}

			if(strtolower($captcha) != strtolower($_SESSION['captcha'])){
				$_SESSION['captcha'] = '';
				die('captcha_incorrecto');
			}

			$this->model = new eleccionesModel();
			$idEleccion = $_POST['idEleccion'];
			$usuario = $_POST['usuario'];
			$pass = $_POST['pass'];
			$this->model->setId($idEleccion);

			$eleccion = $this->model->getEleccion($idEleccion);
			if(Date($eleccion->fecha) > Date('Y-m-d')){
				die(false);
			}

			$verificarIp = $this->model->VerificarSiExisteLaIp($this->ip, $idEleccion);
			if($verificarIp){
				$_SESSION['captcha'] = '';
				echo 'ip_existente';
				die();
				return;
			}
			$hash = $this->model->getHashPass();
			if(count($hash) < 1){
				$_SESSION['captcha'] = '';
				die('false');
				return;
			}else{
				$hash = $hash[0]->contraseña;
				$verificar = password_verify($pass, $hash);
			}
			if($verificar){
				$id_maquina = $this->model->insertarMaquinaVotante($this->ip);
				if(isset($id_maquina)){
					$_SESSION['maquina_votante'] = $this->ip;
					$_SESSION['maquina_votante_ideleccion'] = $idEleccion;
					$_SESSION['maquina_votante_id'] = $id_maquina;
					echo 'true';
				}else{
					$_SESSION['captcha'] = '';
					echo 'false';
				}
			}else{
				$_SESSION['captcha'] = '';
				echo 'false';
			}
		}

		public function esperando(){
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			if(!isset($_SESSION['maquina_votante'])){
				die(false);
			}
			$this->model = new eleccionesModel();
			$idEleccion = $_POST['idEleccion'];
			$this->model->setId($idEleccion);

			echo $this->model->esperando($_SESSION['maquina_votante']);
		}

		public function activarMaquinaVotante(){
			try{
				$this->autenticado();
				if($_SERVER['REQUEST_METHOD'] != 'POST'){
					header('location: ' . SERVERURL . '/');
					die();
				}
				$this->model = new eleccionesModel();
				$id = $_POST['id'];
				$idEleccion = $_POST['idEleccion'];
				$cedula = $_POST['cedula'];
				$ip = $_POST['ip'];
				$this->model->setId($idEleccion);

				$eleccion = $this->model->getEleccion($idEleccion);

				$usuarioModel = new Usuario();
				$usuario = $usuarioModel->getUsuario($_SESSION['id']);
				$tipoUsuario = $usuario->tipo;

				// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ ACTIVANDO UNA MAQUINA DE VOTACION QUE PERTENECE A UNA ELECCIÓN DE SU CONSEJO COMUNAL
				if($tipoUsuario != 1){
					if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
						die(false);
					}
					// SI ES ADMINISTRADOR DE ELECCIÓN SE VERIFICA QUE ESTÉ HABILITADO PARA ACTIVAR ESA MÁQUINA DE VOTACIÓN
					if($tipoUsuario == 3){
						if(!$this->model->usuarioHabilitadoMaquina($usuario->id, $id)){
							die(false);
						}
					}
				}

				// SE VERIFICA QUE LA PERSONA PERTENEZCA AL CONSEJO COMUNAL QUE ESTÁ REALIZANDO LA VOTACIÓN
				if(!$this->model->votantePerteneceConsejo($cedula)){
					error_log('SE INTENTO ACTIVAR MAQUINA DE VOTACION A PERSONA QUE NO PERTECENE AL CONSEJO COMUNAL: ' . $cedula);
					echo false;
					return false;
				}

				// SE VERIFICA QUE EL VOTANTE SEA MAYOR DE 15 AÑOS
				$personasModel = new personasModel();
				if(!$personasModel->verificarEdadVotante($cedula)){
					error_log('SE INTENTO ACTIVAR MAQUINA DE VOTACION A PERSONA MENOR DE 15 ANOS: ' . $cedula);
					echo false;
					return false;
				}

				// SE VERIFICA QUE LA PERSONA NO HA VOTADO
				if(!$this->model->verificarQueNoHaVotado($cedula)){
					error_log('SE INTENTO ACTIVAR MAQUINA DE VOTACION A PERSONA QUE YA VOTO: ' . $cedula);
					echo false;
					return false;
				}

				// SE VERIFICA QUE LA MAQUINA VOTANTE NO ESTÉ ACTIVA
				$verificar = $this->model->verificarMaquinaVotanteActivo($ip, $id);
				if(count($verificar) > 0 && $verificar[0]->activado){
					error_log('SE INTENTO ACTIVAR MAQUINA DE VOTACION QUE YA ESTABA ACTIVA: ' . $ip);
					echo false;
					return false;
				}

				echo $this->model->activarMaquinaVotante($id, $cedula, $idEleccion);
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function quitarMaquinaVotante(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}

			$idEleccion = $_POST['idEleccion'];

			$eleccion = $this->model->getEleccion($idEleccion);

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			// SOLO PUEDEN QUITAR MAQUINAS DE VOTACION LOS SUPER USUARIOS Y USUARIOS
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die(false);
			}
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ QUITANDO UNA MAQUINA DE VOTACION QUE PERTENECE A UNA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					die(false);
				}
			}

			$this->model = new eleccionesModel();
			$id = $_POST['id'];
			$this->model->setId($idEleccion);

			echo $this->model->quitarMaquinaVotante($id);
		}

		public function finalizar(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$idEleccion = $_POST['idEleccion'];

			$eleccion = $this->model->getEleccion($idEleccion);
			if($eleccion->finalizado){
				die(true);
			}

			$clave = $_POST['clave'];

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			if(!$usuarioModel->verificarClaveActual($_SESSION['id'], $clave)){
				die('clave_incorrecta');
			}

			// SOLO PUEDEN FINALIZAR ELECCIÓN LOS SUPER USUARIOS Y USUARIOS
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die(false);
			}
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ FINALIZANDO UNA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					die(false);
				}
			}

			$this->model->setId($idEleccion);
			echo $this->model->finalizar();
		}

		public function cambiarPassVotacion(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			$this->model = new eleccionesModel();
			$id = $_POST['id'];

			$eleccion = $this->model->getEleccion($id);

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			// SOLO PUEDEN CAMBIAR CONTRASEÑA DE ELECCIÓN LOS SUPER USUARIOS Y USUARIOS
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die(false);
			}
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÁ CAMBIANDO CONTRASEÑA A UNA ELECCIÓN DE SU CONSEJO COMUNAL
			if($tipoUsuario != 1){
				if($usuario->cod_consejocomunal != $eleccion->cod_consejocomunal){
					die(false);
				}
			}

			$pass = $_POST['pass'];
			$pass = password_hash($pass, PASSWORD_BCRYPT, array('cost'=>12));
			$this->model->setId($id);
			echo $this->model->cambiarPassVotacion($pass);
		}

		public function eliminar(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}
			// ID DE ELECCIÓN A ELIMINAR
			$id = $_POST['id'];
			$this->model->setId($id);

			$finalizado = $this->model->verificarSiYAFinalizo();
			$comenzado = $this->model->verificarSiYaComenzo();

			// SI LA ELECCIÓN ESTÁ EN CURSO (COMENZÓ Y NO HA FINALIZADO) NO SE PUEDE ELIMINAR
			if(!$finalizado && $comenzado){
				die('en_curso');
			}

			$this->model = new eleccionesModel();

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;
			
			// SI NO ES SUPER USUARIO SE VERIFICA QUE ESTÉ ELIMINANDO UNA ELECCIÓN QUE PERTENECE A SU CONSEJO COMUNAL
			if($tipoUsuario == 2){
				$eleccion = $this->model->getEleccion($id);
				// LOS NO SUPER USUARIOS NO PUEDEN ELIMINAR ELECCIONES QUE YA FINALIZARON O ESTÁN EN CURSO
				if($finalizado || $comenzado){
					die('no_permitido_finalizado');
				}

				if($eleccion->cod_consejocomunal != $usuario->cod_consejocomunal){
					die('0');
				}
			}elseif($tipoUsuario != 1){
				die('0');
			}

			echo $this->model->eliminar($id);
		}


		public function habilitarAdministradoresEleccion(){
			$this->autenticado();
			if($_SERVER['REQUEST_METHOD'] != 'POST'){
				header('location: ' . SERVERURL . '/');
				die();
			}

			$usuarioModel = new Usuario();
			$usuario = $usuarioModel->getUsuario($_SESSION['id']);
			$tipoUsuario = $usuario->tipo;

			// SOLO SUPER USUARIOS Y USUARIOS PUEDEN ACCEDER
			if($tipoUsuario != 1 && $tipoUsuario != 2){
				die(false);
			}

			$idsUsuarios = json_decode($_POST['idsUsuarios']);
			if(!is_array($idsUsuarios)){
				die(false);
			}
			$idMaquina = $_POST['idMaquina'];

			$this->model->iniciarTransaccion();
			// SE ELIMINAN LOS ADMINISTRADORES DE ELECCIÓN DE LA MÁQUINA DE VOTACIÓN QUE NO SE RECIBIERON EN EL ARREGLO DE USUARIOS
			$this->model->eliminarAdministradoresDeEleccion($idMaquina, $idsUsuarios);

			if($tipoUsuario != 1){
				// VERIFICAR QUE LA MÁQUINA DE VOTACIÓN PERTENECE A UNA ELECCIÓN DE SU CONSEJO COMUNAL
				if(!$this->model->maquinaPerteneceConsejo($idMaquina, $usuario->cod_consejocomunal)){
					$this->model->rollBack();
					die(false);
				}

				// SE RECORRE CADA USUARIO RECIBIDO
				foreach ($idsUsuarios as $idUsuario) {
					// VERIFICAR QUE EL USUARIO SEA ADMINISTRADOR DE ELECCION ADEMÁS DE PERTENECER AL CONSEJO COMUNAL VERIFICADO ANTERIORMENTE
					if(!$this->model->verificarUsuarioAdministradorDeEleccionConsejo($idUsuario, $usuario->cod_consejocomunal)){
						continue;
					}

					// SE HABILITA LA MÁQUINA DE VOTACIÓN AL USUARIO
					if(!$this->model->habilitarMaquinaUsuario($idMaquina, $idUsuario)){
						$this->model->rollBack();
						die(false);
					}
				}
			}else{
				// SE RECORRE CADA USUARIO RECIBIDO
				foreach ($idsUsuarios as $idUsuario) {
					// SE HABILITA LA MÁQUINA DE VOTACIÓN AL USUARIO
					if(!$this->model->habilitarMaquinaUsuario($idMaquina, $idUsuario)){
						$this->model->rollBack();
						die(false);
					}
				}
			}

			$this->model->commit();

			echo '1';

		}
		

	}

?>