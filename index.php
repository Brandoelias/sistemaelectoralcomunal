<?php

include_once('./config/general.php');
include_once('./config/basededatos.php');
include_once('./models/mainModel.php');

session_start();

if (isset($_GET['view'])) {
	$url = explode('/', $_GET['view']);

	//Eliminar el último '/' al final de la url si existe
	if (strlen($url[count($url) - 1]) < 1) {
		array_pop($url);
	}

		$link = './controllers/';
		$count = count($url);

		if (is_file($link . $url[0] . 'Controller.php')){
			include_once($link . $url[0] . 'Controller.php');
			$controlador = "$url[0]Controller";
			if (isset($url[1])){
				$action = strtolower($url[1]);
			}else{
				$action = 'index';
			}
			$controlador = new $controlador($action);
			if(method_exists($controlador, $action)){
				if(isset($url[2])){
					$controlador->$action($url);
				}else{
					try{
						$controlador->$action();
					}catch(ArgumentCountError $e){
						http_response_code(404);
						include_once('./views/404.php');
					}
				}
				
			}elseif(is_numeric($url[1])){
				$controlador->index($url[1]);
			}else{
				http_response_code(404);
				include_once('./views/404.php');
			}
		}else{
			http_response_code(404);
			include_once('./views/404.php');
		}


}
else {
	include_once('./controllers/loginController.php');
	$controlador = new loginController();
	$controlador->index();
}

?>