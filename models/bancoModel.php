<?php

	class bancoModel{

		private $id;
		private $nombre;
		private $db;

		public function __construct(){
			if(isset($_SESSION['usuario'])){
				$this->db = mainModel::conectar();
			}else{
				die();
			}
		}

		public function getTodos(){
			try{
				$res = $this->db->prepare('SELECT * FROM banco ORDER BY id ASC');
				$res->execute();
			}catch(Exception $e){
				return false;
			}

			return $res->fetchAll(PDO::FETCH_OBJ);
		}

	}

?>