<?php


	class consejocomunalModel{

		private $cod;
		private $nombre;
		private $telefono;
		private $rif;
		private $correo;
		private $id_banco;
		private $numerodecuenta;
		private $db;

		public function __construct(){
			if(isset($_SESSION['usuario'])){
				$this->db = mainModel::conectar();
			}else{
				header('location: ' . SERVERURL . '/login/');
				die();
			}
		}

		public function setCod($cod){
			$this->cod = $cod;
		}
		public function getCod(){
			return $this->cod;
		}
		public function setNombre($nombre){
			$this->nombre = $nombre;
		}
		public function getNombre(){
			return $this->nombre;
		}
		public function setTelefono($telefono){
			$this->telefono = $telefono;
		}
		public function getTelefono(){
			return $this->telefono;
		}
		public function setRif($rif){
			$this->rif = $rif;
		}
		public function getRif(){
			return $this->rif;
		}
		public function setCorreo($correo){
			$this->correo = $correo;
		}
		public function getCorreo(){
			return $this->correo;
		}
		public function setIdBanco($id_banco){
			$this->id_banco = $id_banco;
		}
		public function getIdBanco(){
			return $this->id_banco;
		}
		public function setNumerodecuenta($numerodecuenta){
			$this->numerodecuenta = $numerodecuenta;
		}
		public function getNumerodecuenta(){
			return $this->numerodecuenta;
		}


	 	public function getTodos(){
			$res = $this->db->prepare('SELECT * FROM consejocomunal,banco WHERE consejocomunal.id_banco = banco.id ORDER BY nombre ASC');
			$res->execute();
			$res = $res->fetchAll(PDO::FETCH_OBJ);

			foreach ($res as $r) {
				$query = $this->db->prepare('SELECT DISTINCT COUNT(id_unidad) FROM consejocomunal_unidad WHERE cod_consejocomunal = ?');
				$query->execute([ $r->cod ]);

				$query = $query->fetchAll(PDO::FETCH_OBJ);

				$r->unidades = $query[0]->count;

			}

			return $res;
		}

		public function editarConsejoComunal($datos){
			$cod = $datos['codM'];
			$nombre = $datos['nombre'];
			$telefono = $datos['telefono'];
			$rif = $datos['rif'];
			$correo = $datos['correo'];
			$numerodecuenta = $datos['numerodecuenta'];
			$id_banco = $datos['id_banco'];

			try{
				// VERIFICAR QUE NO SE ESTÉ REALIZANDO UNA ELECCIÓN ACTUALMENTE QUE YA COMENZÓ Y NO HA FINALIZADO
				$res = $this->db->prepare('SELECT * FROM consejocomunal WHERE cod = ? AND cod IN (SELECT cod_consejocomunal FROM eleccion WHERE finalizado = false AND id IN (SELECT id_eleccion FROM votante))');
				$res->execute([ $this->cod ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					return 'eleccion_comenzada';
				}
				
				$res = $this->db->prepare('UPDATE consejocomunal SET cod = ?, nombre = ?, telefono = ?, rif = ?, correo = ?, numerodecuenta = ?, id_banco = ? WHERE cod = ?');
				$res->execute([ $cod, $nombre, $telefono, $rif, $correo, $numerodecuenta, $id_banco , $this->cod ]);
			}catch(Exception $e){
				return false;
			}

			if($res->rowCount() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function borrarConsejoComunal(){
			try{
				// VERIFICAR QUE NO SE ESTÉ REALIZANDO UNA ELECCIÓN ACTUALMENTE QUE YA COMENZÓ Y NO HA FINALIZADO
				$res = $this->db->prepare('SELECT * FROM consejocomunal WHERE cod = ? AND cod IN (SELECT cod_consejocomunal FROM eleccion WHERE finalizado = false AND id IN (SELECT id_eleccion FROM votante))');
				$res->execute([ $this->cod ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					return 'eleccion_comenzada';
				}

				$res = $this->db->prepare('DELETE FROM consejocomunal WHERE cod = ?');
				$res->execute([ $this->cod ]);
			}catch(Exception $e){
				error_log($e);
				return false;
			}

			if($res->rowCount() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function agregarConsejoComunal(){
			try{
				$res = $this->db->prepare('INSERT INTO consejocomunal (cod, nombre, telefono, rif, correo, numerodecuenta, id_banco) VALUES (?, ?, ?, ?, ?, ?, ?)');
				$res->execute([ $this->cod, $this->nombre, $this->telefono, $this->rif, $this->correo, $this->numerodecuenta, $this->id_banco ]);
			}catch(Exception $e){
				error_log($e);
				return false;
			}

			if($res->rowCount() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function verUnidades(){
			try{
				$res = $this->db->prepare('SELECT * FROM consejocomunal_unidad,unidad WHERE cod_consejocomunal = ? AND consejocomunal_unidad.id_unidad = unidad.id');
				$res->execute([ $this->cod ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);

				foreach ($res as $r) {
					$q = $this->db->prepare('SELECT COUNT(id_comite) FROM comite_unidad_consejo WHERE id_unidad = ? AND cod_consejo = ?');
					$q->execute([ $r->id_unidad, $this->cod ]);
					$q = $q->fetchAll(PDO::FETCH_OBJ);

					if($q[0]->count > 0){
						$r->comites = 'Ver (' . $q[0]->count . ')';
					}else{
						$r->comites = 'Ninguno';
					}
					
				}

			}catch(Exception $e){
				return false;
			}

			return $res;
		}

		public function getUnidadesNotIn(){
			try{
				$res = $this->db->prepare('SELECT * FROM unidad WHERE eliminado = false AND id NOT IN (SELECT id_unidad FROM consejocomunal_unidad WHERE cod_consejocomunal = ?)');
				$res->execute([ $this->cod ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res;
		}

		public function agregarUnidadConsejo($unidades){
			$cont = 0;
			$err = 0;

			foreach ($unidades as $unidad) {
				try{
				$res = $this->db->prepare('INSERT INTO consejocomunal_unidad (cod_consejocomunal, id_unidad) VALUES (?, ?)');
				$res->execute([ $this->cod, $unidad ]);
				$cont++;
				}catch(Exception $e){
					$err++;
					continue;
				}
			}
			
			if($err == 0){
				return true;
			}else{
				return false;
			}
		}

		public function verificarSiExiste($cod){
			try{
				$sql = 'SELECT * FROM consejocomunal WHERE cod = ?';
				$res = $this->db->prepare($sql);
				$res->execute([$cod]);
				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				return false;
			}

			return false;
		}

		public function getCodigosExistentes(){
			try{
				$sql = 'SELECT cod FROM consejocomunal';
				$res = $this->db->query($sql);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				$res2 = array();
				foreach($res as $r){
					array_push($res2, $r->cod);
				}
			}catch(Exception $e){
				return array();
			}

			return $res2;
		}

		public function getCantidadHabitantes($cod){
			try{
				$sql = 'SELECT COUNT(*) AS count FROM persona WHERE eliminado = false AND cod_consejocomunal = ?';
				$res = $this->db->prepare($sql);
				$res->execute([ $cod ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 0;
			}
			return $res[0]->count;
		}

		public function cambiarVotosMaximos($votosMaximos){
			try{
				$this->db->beginTransaction();
				foreach($votosMaximos as $votoMaximo){
					if($votoMaximo->votosMaximos < 1 || $votoMaximo->votosMaximos > 99){
						$this->db->rollBack();
						return 0;
					}
					if(isset($votoMaximo->idComite)){
						$res = $this->db->prepare('UPDATE comite_unidad_consejo SET votos_maximos = ? WHERE id_unidad = ? AND cod_consejo = ? AND id_comite = ?');
						$res->execute([ $votoMaximo->votosMaximos, $votoMaximo->idUnidad, $this->cod, $votoMaximo->idComite ]);
					}else{
						$res = $this->db->prepare('UPDATE consejocomunal_unidad SET votos_maximos = ? WHERE id_unidad = ? AND cod_consejocomunal = ?');
						$res->execute([ $votoMaximo->votosMaximos, $votoMaximo->idUnidad, $this->cod ]);
					}
				}
			}catch(Exception $e){
				$this->db->rollBack();
				return 0;
			}
			$this->db->commit();
			return 1;
		}


	}


?>