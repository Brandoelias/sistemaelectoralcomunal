<?php


class mainModel{


	public static function getFechaActualFormateada(){

		$d = Date('d');
		$m = intval(Date('m')) - 1;
		$a = Date('Y');

		$h = Date('h');
		$mi = Date('i');
		$s = Date('s');
		$me = Date('a');

		$meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

		return "$d de $meses[$m] del $a - $h:$mi:$s $me"; 

	}

	public static function conectar() {

		try {

			$host = HOST_DB;
			$port = PORT_DB;
			$dbname = NOMBRE_DB;
			$driver = DRIVER_DB;

    		$conexion = new PDO("$driver:host=$host;port=$port;dbname=$dbname", USUARIO_DB, CLAVE_DB);
    		$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    		if(isset($_SESSION['usuario'])){
    			$id = $_SESSION['id'];
    			$usuario = $conexion->query("SELECT * FROM usuario WHERE id = $id");
				$usuario = $usuario->fetchAll(PDO::FETCH_OBJ);
				if(count($usuario) < 1){
					$_SESSION['usuario'] = null;
					$_SESSION['id'] = null;
				}else{
					$_SESSION['usuario'] = $usuario[0]->nombre;
				}
    		}

		} catch (Exception $e) {

    		die("Ocurrió un error con la base de datos: " . $e->getMessage());

		}

		return $conexion;

	}

	public static function getPersonaUsuario(){
		try{
			$db = mainModel::conectar();
			$id = $_SESSION['id'];
			$persona = $db->query("SELECT pnombre,papellido FROM persona INNER JOIN usuario ON persona.cedula = usuario.cedula WHERE usuario.id = $id");
			$persona = $persona->fetchAll(PDO::FETCH_OBJ);
			return $persona[0]->pnombre . ' ' . $persona[0]->papellido;
		}catch(Exception $e){
			error_log($e);
			return '';
		}
	}

	public static function actividad($descripcion, $idTabla = null, $tabla = null, $conexion = null){
		return true;
	}


}


?>