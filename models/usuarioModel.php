<?php


	class Usuario{
		private $id;
		private $nombre;
		private $contrasena;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function getTodos($limit = null, $offset = 0, $usuario, $filtrarConsejo){
			try{
				if($usuario->tipo == 1){
					$sql = 'SELECT usuario.id,persona.cedula,pnombre,papellido,usuario.nombre AS nombre_usuario,usuario_tipo.nombre AS tipo,usuario.tipo AS tipo_id,consejocomunal.nombre AS nombre_consejo FROM usuario LEFT JOIN persona ON usuario.cedula = persona.cedula INNER JOIN usuario_tipo ON usuario.tipo = usuario_tipo.id INNER JOIN consejocomunal ON persona.cod_consejocomunal = consejocomunal.cod';
				}else{
					$sql = "SELECT usuario.id,persona.cedula,pnombre,papellido,usuario.nombre AS nombre_usuario,usuario_tipo.nombre AS tipo,usuario.tipo AS tipo_id,consejocomunal.nombre AS nombre_consejo FROM usuario LEFT JOIN persona ON usuario.cedula = persona.cedula INNER JOIN usuario_tipo ON usuario.tipo = usuario_tipo.id INNER JOIN consejocomunal ON persona.cod_consejocomunal = consejocomunal.cod WHERE persona.cod_consejocomunal = ? AND usuario.tipo = 3";
				}
				if($filtrarConsejo != ''){
					$sql .= ' AND persona.cod_consejocomunal = ?';
				}
				$sql .= ' ORDER BY id ASC';
				if($limit != null){
					$sql .= " LIMIT $limit OFFSET $offset";
				}
				$res = $this->db->prepare($sql);
				if($usuario->tipo == 1){
					if($filtrarConsejo == ''){
						$res->execute();
					}else{
						$res->execute([$filtrarConsejo]);
					}
				}else{
					if($filtrarConsejo == ''){
						$res->execute([$usuario->cod_consejocomunal]);
					}else{
						$res->execute([$usuario->cod_consejocomunal, $filtrarConsejo]);
					}
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return false;
			}

			return $res;
		}

		public function getCantidadTotal($usuario, $filtrarConsejo){
			try{
				if($usuario->tipo == 2){
					$sql = 'SELECT COUNT(*) AS count FROM usuario INNER JOIN persona ON usuario.cedula = persona.cedula WHERE persona.cod_consejocomunal = ? AND usuario.tipo = 3';
					if($filtrarConsejo){
						$sql .= ' AND persona.cod_consejocomunal = ?';
					}
					$res = $this->db->prepare($sql);
					if(!$filtrarConsejo){
						$res->execute([ $usuario->cod_consejocomunal ]);
					}else{
						$res->execute([ $usuario->cod_consejocomunal, $filtrarConsejo ]);
					}
				}else{
					$sql = 'SELECT COUNT(*) AS count FROM usuario INNER JOIN persona ON usuario.cedula = persona.cedula';
					if($filtrarConsejo){
						$sql .= ' WHERE persona.cod_consejocomunal = ?';
					}
					$res = $this->db->prepare($sql);
					if(!$filtrarConsejo){
						$res->execute();
					}else{
						$res->execute([ $filtrarConsejo ]);
					}
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 0;
			}
			return $res[0]->count;
		}

		public function paginar($paginaActual, $totalElementos, $limit, $controlador, $parametros = '/'){
			$totalPaginas = ceil($totalElementos / $limit);
			$paginaInicio = $paginaActual - ($paginaActual % 10) + 1;

			if($parametros == ''){
				$parametros = '/';
			}
			
			if($totalPaginas > 1){
				include_once('./views/partials/paginacion.php');
			}
		}

		public function cambiarClave($id, $clave){
			try{
				$res = $this->db->prepare('UPDATE usuario SET contraseña = ? WHERE id = ?');
				$res->execute([ $clave, $id ]);

				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function verificarSiExiste($nombre){
			try{
				$res = $this->db->prepare('SELECT * FROM usuario WHERE nombre = ?');
				$res->execute([ $nombre ]);

				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function agregar($cedula, $nombre, $clave, $tipo){
			try{
				$res = $this->db->prepare('INSERT INTO usuario(nombre, contraseña, cedula, tipo) VALUES (?, ?, ?, ?) RETURNING id');
				$res->execute([ $nombre, $clave, $cedula, $tipo ]);
				$idUsuario = $res->fetchAll(PDO::FETCH_OBJ);
				$idUsuario = $idUsuario[0]->id;
				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function modificar($id, $nombre, $tipo){
			try{
				// SI NO ES EL ID 1 (SUPER USUARIO POR DEFECTO) SE PUEDE CAMBIAR NOMBRE Y TIPO, SINO, SÓLO SE LE PUEDE CAMBIAR EL NOMBRE
				if($id != 1 && $id != '1'){
					$res = $this->db->prepare("UPDATE usuario SET nombre = ?, tipo = ? WHERE id = ?");
					$res->execute([ $nombre, $tipo, $id ]);
				}else{
					$res = $this->db->prepare("UPDATE usuario SET nombre = ? WHERE id = 1");
					$res->execute([ $nombre ]);
				}
				
				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function getTipo($id){
			try{
				if(!isset($id) || $id == ''){
					return 0;
				}
				$res = $this->db->query("SELECT tipo FROM usuario WHERE id = $id");
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				return $res = $res[0]->tipo;
			}catch(Exception $e){
				error_log($e);
				return 0;
			}
		}

		public function verificarClaveActual($id, $claveActual){
			try{
				$res = $this->db->prepare('SELECT * FROM usuario WHERE id = ?');
				$res->execute([ $id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				return password_verify($claveActual, $res[0]->contraseña);
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function borrar($id){
			try{
				// SI ES EL ID 1 (SUPER USUARIO POR DEFECTO) NO SE PODRÁ BORRAR
				if($id != 1 && $id != '1'){
					$res = $this->db->prepare("DELETE FROM usuario WHERE id = ?");
					$res->execute([ $id]);
				}else{
					return false;
				}
				
				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function getUsuario($id){
			try{

				$res = $this->db->prepare("SELECT * FROM usuario LEFT JOIN persona ON usuario.cedula = persona.cedula WHERE id = ?");
				$res->execute([ $id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				return $res[0];
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function getUsuarioPorCedula($cedula){
			try{

				$res = $this->db->prepare("SELECT * FROM usuario LEFT JOIN persona ON usuario.cedula = persona.cedula WHERE persona.cedula = ?");
				$res->execute([ $cedula ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					$res = $res[0];
				}else{
					$res = [];
				}
				return $res;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function getAjaxPorCedula($dato, $filtrarConsejo, $usuario){
			try{
				if($usuario->tipo == 1){
					$sql = 'SELECT usuario.id,persona.cedula,pnombre,papellido,usuario.nombre AS nombre_usuario,usuario_tipo.nombre AS tipo,usuario.tipo AS tipo_id,consejocomunal.nombre AS nombre_consejo FROM usuario LEFT JOIN persona ON usuario.cedula = persona.cedula INNER JOIN usuario_tipo ON usuario.tipo = usuario_tipo.id LEFT JOIN consejocomunal ON persona.cod_consejocomunal = consejocomunal.cod WHERE persona.cedula = ?';
				}else{
					$sql = "SELECT usuario.id,persona.cedula,pnombre,papellido,usuario.nombre AS nombre_usuario,usuario_tipo.nombre AS tipo,usuario.tipo AS tipo_id,consejocomunal.nombre AS nombre_consejo FROM usuario LEFT JOIN persona ON usuario.cedula = persona.cedula INNER JOIN usuario_tipo ON usuario.tipo = usuario_tipo.id LEFT JOIN consejocomunal ON persona.cod_consejocomunal = consejocomunal.cod WHERE persona.cod_consejocomunal = ? AND usuario.tipo = 3 WHERE persona.cedula = ?";
				}
				if($filtrarConsejo != ''){
					$sql .= ' AND cod_consejocomunal = ?';
				}
				$sql .= ' ORDER BY id ASC';
				$res = $this->db->prepare($sql);
				if($usuario->tipo == 1){
					if($filtrarConsejo == ''){
						$res->execute([$dato]);
					}else{
						$res->execute([$dato, $filtrarConsejo]);
					}
				}else{
					if($filtrarConsejo == ''){
						$res->execute([$usuario->cod_consejocomunal, $dato]);
					}else{
						$res->execute([$usuario->cod_consejocomunal, $dato, $filtrarConsejo]);
					}
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'false';
			}

			return $res;
		}

		public function getAjaxPorNombreYApellido($dato, $filtrarConsejo, $usuario){
			try{
				if($usuario->tipo == 1){
					$sql = 'SELECT usuario.id,persona.cedula,pnombre,papellido,usuario.nombre AS nombre_usuario,usuario_tipo.nombre AS tipo,usuario.tipo AS tipo_id,consejocomunal.nombre AS nombre_consejo FROM usuario LEFT JOIN persona ON usuario.cedula = persona.cedula INNER JOIN usuario_tipo ON usuario.tipo = usuario_tipo.id LEFT JOIN consejocomunal ON persona.cod_consejocomunal = consejocomunal.cod WHERE LOWER(pnombre) LIKE ? AND LOWER(papellido) LIKE ? OR LOWER(papellido) LIKE ?';
				}else{
					$sql = "SELECT usuario.id,persona.cedula,pnombre,papellido,usuario.nombre AS nombre_usuario,usuario_tipo.nombre AS tipo,usuario.tipo AS tipo_id,consejocomunal.nombre AS nombre_consejo FROM usuario LEFT JOIN persona ON usuario.cedula = persona.cedula INNER JOIN usuario_tipo ON usuario.tipo = usuario_tipo.id LEFT JOIN consejocomunal ON persona.cod_consejocomunal = consejocomunal.cod WHERE persona.cod_consejocomunal = ? AND usuario.tipo = 3 WHERE LOWER(pnombre) LIKE ? AND LOWER(papellido) LIKE ? OR LOWER(papellido) LIKE ?";
				}
				if($filtrarConsejo != ''){
					$sql .= ' AND cod_consejocomunal = ?';
				}
				$sql .= ' ORDER BY id ASC';
				$res = $this->db->prepare($sql);
				if($usuario->tipo == 1){
					if($filtrarConsejo == ''){
						$res->execute([$dato[0].'%', $dato[1].'%', $dato[0].'%']);
					}else{
						$res->execute([$dato[0].'%', $dato[1].'%', $dato[0].'%', $filtrarConsejo]);
					}
				}else{
					if($filtrarConsejo == ''){
						$res->execute([$usuario->cod_consejocomunal, $dato[0].'%', $dato[1].'%', $dato[0].'%']);
					}else{
						$res->execute([$usuario->cod_consejocomunal, $dato[0].'%', $dato[1].'%', $dato[0].'%', $filtrarConsejo]);
					}
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'false';
			}

			return $res;
		}

	}


?>