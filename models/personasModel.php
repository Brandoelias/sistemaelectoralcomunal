<?php

	class personasModel{

		private $cedula;
		private $pnombre;
		private $snombre;
		private $papellido;
		private $sapellido;
		private $sexo;
		private $fecha_nacimiento;
		private $cod_consejocomunal;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function setCedula($cedula){
			$this->cedula = $cedula;
		}
		public function getCedula(){
			return $this->cedula;
		}
		public function setPnombre($pnombre){
			$this->pnombre = $pnombre;
		}
		public function getPnombre(){
			return $this->pnombre;
		}
		public function setSnombre($snombre){
			$this->snombre = $snombre;
		}
		public function getSnombre(){
			return $this->snombre;
		}
		public function setPapellido($papellido){
			$this->papellido = $papellido;
		}
		public function getPapellido(){
			return $this->papellido;
		}
		public function setSapellido($sapellido){
			$this->sapellido = $sapellido;
		}
		public function getSapellido(){
			return $this->sapellido;
		}
		public function setSexo($sexo){
			$this->sexo = $sexo;
		}
		public function getSexo(){
			return $this->sexo;
		}
		public function setFechaNacimiento($fecha_nacimiento){
			$this->fecha_nacimiento = $fecha_nacimiento;
		}
		public function getFechaNacimiento(){
			return $this->fecha_nacimiento;
		}
		public function setCodConsejocomunal($cod){
			$this->cod_consejocomunal = $cod;
		}
		public function getCodConsejocomunal(){
			return $this->cod_consejocomunal;
		}


		public function getTodos($limit = null, $offset = 0){
			try{
				$sql = 'SELECT * FROM persona LEFT JOIN consejocomunal ON cod_consejocomunal = cod WHERE eliminado = false ORDER BY cedula DESC';
				if($limit != null){
					$sql .= " LIMIT $limit OFFSET $offset";
				}
				$res = $this->db->query($sql);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res;
		}

		public function getTodosPorConsejo($limit = null, $offset = 0, $cod){
			try{
				$sql = 'SELECT * FROM persona LEFT JOIN consejocomunal ON cod_consejocomunal = cod WHERE eliminado = false AND cod_consejocomunal = ? ORDER BY cedula DESC';
				if($limit != null){
					$sql .= " LIMIT $limit OFFSET $offset";
				}
				$res = $this->db->prepare($sql);
				$res->execute([ $cod ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}
			return $res;
		}

		public function agregar(){
			try{
				// VERIFICAR SI LA PERSONA QUE SE ESTÁ INTENTANDO AGREGAR HA SIDO SOFT DELETEADA, SI ES ASÍ, SE ACTUALIZA CON LOS DATOS INGRESADOS QUITANDOSE EL SOFT DELETE
				$verificar = $this->db->prepare('SELECT cedula FROM persona WHERE cedula = ? AND eliminado = true');
				$verificar->execute([ $this->cedula ]);
				$verificar = $verificar->fetchAll(PDO::FETCH_OBJ);
				if(count($verificar) > 0 && $verificar[0]->cedula == $this->cedula){
					// SI HA PARTICIPADO EN UNA ELECCIÓN FINALIZADA O COMENZADA, SUS DATOS YA NO SE PUEDEN MODIFICAR, SÓLAMENTE SE LE QUITA EL SOFT DELETE
					if($this->participaEleccion($this->cedula)){
						$res = $this->db->prepare('UPDATE persona SET eliminado = false WHERE cedula = ? ');
						$res->execute([ $this->cedula ]);
						return true;
					}
					return $this->editar($this);
				}


				if($this->cod_consejocomunal != null){
					$res = $this->db->prepare('INSERT INTO persona(cedula, pnombre, snombre, papellido, sapellido, sexo, fecha_nacimiento, cod_consejocomunal) VALUES(?, ?, ?, ?, ?, ?, ?, ?)');
					$res->execute([ $this->cedula, $this->pnombre, $this->snombre, $this->papellido, $this->sapellido, $this->sexo, $this->fecha_nacimiento, $this->cod_consejocomunal ]);
				}/*else{
					$res = $this->db->prepare('INSERT INTO persona(cedula, pnombre, snombre, papellido, sapellido, sexo, fecha_nacimiento) VALUES(?, ?, ?, ?, ?, ?, ?)');
					$res->execute([ $this->cedula, $this->pnombre, $this->snombre, $this->papellido, $this->sapellido, $this->sexo, $this->fecha_nacimiento ]);
				}*/
				else{
					return false;
				}
			}catch(Exception $e){
				error_log($e);
				return false;
			}

			if($res->rowCount() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function editar($datos){
			try{
				$res = $this->db->prepare('UPDATE persona SET cedula = ?, pnombre = ?, snombre = ?, papellido = ?, sapellido = ?, sexo = ?, fecha_nacimiento = ?, cod_consejocomunal = ?, eliminado = false WHERE cedula = ? ');
				$res->execute([ $datos->cedula, $datos->pnombre, $datos->snombre, $datos->papellido, $datos->sapellido, $datos->sexo, $datos->fecha_nacimiento, $datos->cod_consejocomunal, $this->cedula ]);
			}catch(Exception $e){
				error_log($e);
				return false;
			}

			if($res->rowCount() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function borrar(){
			try{
				$this->db->beginTransaction();
				// VERIFICAR QUE LA PERSONA NO SEA CANDIDATO EN UNA ELECCIÓN QUE SE ESTÉ REALIZANDO ACTUALMENTE
				$verificar = $this->db->prepare('SELECT DISTINCT ci_persona FROM candidato WHERE ci_persona = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = false) AND id_eleccion IN (SELECT DISTINCT id_eleccion FROM votante)');
				$verificar->execute([ $this->cedula ]);
				$verificar = $verificar->fetchAll(PDO::FETCH_OBJ);

				if(count($verificar) > 0){
					return 'candidato_participando';
				}

				// VERIFICAR SI LA PERSONA FUÉ CANDIDATO EN ALGUNA ELECCIÓN YA FINALIZADA, SI ES ASÍ, SE HACE UN SOFT DELETE EN LUGAR DE UN HARD DELETE, PARA QUE LA INFORMACIÓN DE LA ELECCIÓN FINALIZADA DE ESE CANDIDATO NO SE PIERDA
				$verificar = $this->db->prepare('SELECT DISTINCT finalizado FROM eleccion WHERE id IN (SELECT id_eleccion FROM candidato WHERE ci_persona = ?) AND finalizado = true');
				$verificar->execute([ $this->cedula ]);
				$verificar = $verificar->fetchAll(PDO::FETCH_OBJ);

				// VERIFICAR SI LA PERSONA VOTÓ EN ALGUNA ELECCIÓN YA FINALIZADA, SI ES ASÍ, SE HACE UN SOFT DELETE EN LUGAR DE UN HARD DELETE, PARA NO PERDER LA INFORMACIÓN DEL VOTANTE
				$verificar2 = $this->db->prepare('SELECT * FROM votante WHERE ci_persona = ?');
				$verificar2->execute([ $this->cedula ]);
				$verificar2 = $verificar2->fetchAll(PDO::FETCH_OBJ);

				if((count($verificar) > 0 && $verificar[0]->finalizado) || count($verificar2) > 0){
					$res = $this->db->prepare('UPDATE persona SET eliminado = true WHERE cedula = ?');
					$res->execute([ $this->cedula ]);
				}else{
					$res = $this->db->prepare('DELETE FROM persona WHERE cedula = ?');
					$res->execute([ $this->cedula ]);
				}
				if(!$this->eliminarUsuariosSinPersonas()){
					$this->db->rollBack();
					return false;
				}
			}catch(Exception $e){
				error_log($e);
				$this->db->rollBack();
				return false;
			}

			if($res->rowCount() > 0){
				$this->db->commit();
				return true;
			}else{
				$this->db->rollBack();
				return false;
			}
		}

		public function getAjaxPorCedula($dato, $id_eleccion, $filtrarConsejo){
			try{
				$sql = 'SELECT * FROM persona LEFT JOIN consejocomunal ON cod_consejocomunal = cod WHERE cedula = ? AND eliminado = false';
				if($filtrarConsejo != ''){
					$sql .= ' AND cod_consejocomunal = ?';
				}
				if(isset($id_eleccion)  && $id_eleccion != null){
					// SOLO MOSTRAR VOTANTES MAYORES DE 15 AÑOS
					$sql = "SELECT * FROM (SELECT *,DATE_PART('year',AGE(fecha_nacimiento)) as edad FROM persona) as persona LEFT JOIN consejocomunal ON cod_consejocomunal = cod WHERE cedula = ? AND eliminado = false AND edad > 15";
					$sql .= ' AND cedula NOT IN(SELECT ci_persona FROM votante WHERE id_eleccion = ?) AND cod_consejocomunal IN (SELECT cod_consejocomunal FROM eleccion WHERE id = ?)';
				}
				$res = $this->db->prepare($sql);
				if(isset($id_eleccion) && $id_eleccion != null){
					$res->execute([$dato, $id_eleccion, $id_eleccion]);
				}else{
					if($filtrarConsejo == ''){
						$res->execute([$dato]);
					}else{
						$res->execute([$dato, $filtrarConsejo]);
					}
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'false';
			}

			return $res;
		}

		public function getAjaxPorNombreYApellido($dato, $id_eleccion, $filtrarConsejo){
			try{
				$sql = 'SELECT * FROM persona LEFT JOIN consejocomunal ON cod_consejocomunal = cod WHERE LOWER(pnombre) LIKE ? AND LOWER(papellido) LIKE ? AND eliminado = false';
				if($filtrarConsejo != ''){
					$sql .= ' AND cod_consejocomunal = ?';
				}
				if(isset($id_eleccion) && $id_eleccion != null){
					// SOLO MOSTRAR VOTANTES MAYORES DE 15 AÑOS
					$sql = "SELECT * FROM (SELECT *,DATE_PART('year',AGE(fecha_nacimiento)) as edad FROM persona) as persona LEFT JOIN consejocomunal ON cod_consejocomunal = cod WHERE LOWER(pnombre) LIKE ? AND LOWER(papellido) LIKE ? AND eliminado = false AND edad > 15";
					$sql .= ' AND cedula NOT IN(SELECT ci_persona FROM votante WHERE id_eleccion = ?) AND cod_consejocomunal IN (SELECT cod_consejocomunal FROM eleccion WHERE id = ?)';
				}
				$res = $this->db->prepare($sql);
				if(isset($id_eleccion) && $id_eleccion != null){
					$res->execute([$dato[0].'%', $dato[1].'%', $id_eleccion, $id_eleccion]);
				}else{
					if($filtrarConsejo == ''){
						$res->execute([$dato[0].'%', $dato[1].'%']);
					}else{
						$res->execute([$dato[0].'%', $dato[1].'%', $filtrarConsejo]);
					}
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'false';
			}

			return $res;
		}

		public function getAjaxPorNombreYApellidoPaginar($dato, $id_eleccion, $filtrarConsejo, $limit, $offset){
			try{
				$sql = 'SELECT * FROM persona LEFT JOIN consejocomunal ON cod_consejocomunal = cod WHERE (LOWER(pnombre) LIKE ? AND LOWER(papellido) LIKE ?) OR (LOWER(papellido) LIKE ?) AND eliminado = false';
				$cantidad = 'SELECT COUNT(cedula) AS count FROM persona LEFT JOIN consejocomunal ON cod_consejocomunal = cod WHERE (LOWER(pnombre) LIKE ? AND LOWER(papellido) LIKE ?) OR (LOWER(papellido) LIKE ?) AND eliminado = false';
				if($filtrarConsejo != ''){
					$sql .= ' AND cod_consejocomunal = ? LIMIT ? OFFSET ?';
					$cantidad .= ' AND cod_consejocomunal = ?';
				}else{
					$sql .= ' LIMIT ? OFFSET ?';
				}
				if(isset($id_eleccion) && $id_eleccion != null){
					// SOLO MOSTRAR VOTANTES MAYORES DE 15 AÑOS
					$sql = "SELECT * FROM (SELECT *,DATE_PART('year',AGE(fecha_nacimiento)) as edad FROM persona) as persona LEFT JOIN consejocomunal ON cod_consejocomunal = cod WHERE LOWER(pnombre) LIKE ? AND LOWER(papellido) LIKE ? AND eliminado = false AND edad > 15";
					$sql .= ' AND cedula NOT IN(SELECT ci_persona FROM votante WHERE id_eleccion = ?) AND cod_consejocomunal IN (SELECT cod_consejocomunal FROM eleccion WHERE id = ?)';
					$cantidad = 'SELECT COUNT(cedula) AS count FROM persona LEFT JOIN consejocomunal ON cod_consejocomunal = cod WHERE LOWER(pnombre) LIKE ? AND LOWER(papellido) LIKE ? AND eliminado = false';
				}
				$res = $this->db->prepare($sql);
				$res2 = $this->db->prepare($cantidad);
				if(isset($id_eleccion) && $id_eleccion != null){
					$res->execute([$dato[0].'%', $dato[1].'%', $id_eleccion, $id_eleccion]);
				}else{
					if($filtrarConsejo == ''){
						$res->execute([$dato[0].'%', $dato[1].'%', $dato[0].'%', $limit, $offset]);
						$res2->execute([ $dato[0].'%', $dato[1].'%', $dato[0].'%' ]);
					}else{
						$res->execute([$dato[0].'%', $dato[1].'%', $dato[0].'%', $filtrarConsejo, $limit, $offset]);
						$res2->execute([ $dato[0].'%', $dato[1].'%',  $dato[0].'%', $filtrarConsejo ]);
					}
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				$res2 = $res2->fetchAll(PDO::FETCH_OBJ);
				$res = [$res, $res2[0]->count];
			}catch(Exception $e){
				error_log($e);
				return [[], 0];
			}

			return $res;
		}

		public function getCantidadTotal($cod){
			try{
				if(!$cod){
					$sql = 'SELECT COUNT(*) AS count FROM persona WHERE eliminado = false';
					$res = $this->db->prepare($sql);
					$res->execute();
				}else{
					$sql = 'SELECT COUNT(*) AS count FROM persona WHERE eliminado = false AND cod_consejocomunal = ?';
					$res = $this->db->prepare($sql);
					$res->execute([ $cod ]);
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 0;
			}
			return $res[0]->count;
		}

		public function paginar($paginaActual, $totalElementos, $limit, $controlador, $parametros = '/'){
			$totalPaginas = ceil($totalElementos / $limit);
			$paginaInicio = $paginaActual - ($paginaActual % 10) + 1;

			if($parametros == ''){
				$parametros = '/';
			}
			
			if($totalPaginas > 1){
				include_once('./views/partials/paginacion.php');
			}
		}

		public function verificarSiExiste($cedula){
			try{
				$sql = 'SELECT * FROM persona WHERE cedula = ? AND eliminado = false';
				$res = $this->db->prepare($sql);
				$res->execute([$cedula]);
				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				return false;
			}

			return false;
		}

		public function getPorCedula($cedula){
			try{
				$sql = 'SELECT * FROM persona WHERE cedula = ? AND eliminado = false';
				$res = $this->db->prepare($sql);
				$res->execute([$cedula]);
				if($res->rowCount() > 0){
					$res = $res->fetchAll(PDO::FETCH_OBJ);
					return $res[0];
				}else{
					return false;
				}
			}catch(Exception $e){
				return false;
			}

			return false;
		}

		public function borrarTodos($cod = null, $cedulaExcluir = 0){
			try{
				$this->db->beginTransaction();
				//if(isset($cod) && isset($cedulaExcluir)){
					$res = $this->db->prepare('UPDATE persona SET eliminado = true WHERE cod_consejocomunal = ? AND cedula != ? AND cedula NOT IN (SELECT cedula FROM usuario WHERE cedula IS NOT NULL) AND cedula IN (SELECT ci_persona FROM candidato WHERE id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = true)) OR cedula IN (SELECT DISTINCT ci_persona FROM votante)');
					$res->execute([ $cod, $cedulaExcluir ]);
					$res = $this->db->prepare('DELETE FROM persona WHERE cod_consejocomunal = ? AND cedula != ? AND eliminado = false AND cedula NOT IN (SELECT cedula FROM usuario WHERE cedula IS NOT NULL) AND cedula NOT IN (SELECT ci_persona FROM candidato WHERE id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = false) AND id_eleccion IN (SELECT DISTINCT id_eleccion FROM votante))');
					$res->execute([ $cod, $cedulaExcluir ]);
				/*}else{
					$res = $this->db->prepare('UPDATE persona SET eliminado = true WHERE cedula IN (SELECT ci_persona FROM candidato WHERE id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = true)) OR cedula IN (SELECT DISTINCT ci_persona FROM votante)');
					$res->execute();
					$res = $this->db->prepare('DELETE FROM persona WHERE eliminado = false AND cedula NOT IN (SELECT ci_persona FROM candidato WHERE id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = false) AND id_eleccion IN (SELECT DISTINCT id_eleccion FROM votante))');
					$res->execute();
				}*/
				if(!$this->eliminarUsuariosSinPersonas()){
					$this->db->rollBack();
					return false;
				}
			}catch(Exception $e){
				error_log($e);
				$this->db->rollBack();
				return false;
			}

			$this->db->commit();
			return true;
		}

		private function eliminarUsuariosSinPersonas(){
			try{
				$res = $this->db->query('DELETE FROM usuario WHERE cedula IN (SELECT cedula FROM persona WHERE eliminado = true)');
				$res->execute();
				return true;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function verificarEdadVotante($cedula){
			try{
				$res = $this->db->prepare("SELECT cedula FROM (SELECT *,DATE_PART('year',AGE(fecha_nacimiento)) as edad FROM persona) as persona WHERE CAST(cedula AS TEXT) LIKE ? AND eliminado = false AND edad > 15");
				$res->execute([ $cedula ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function verificarSiPerteneceEleccionSinFinalizar($cedula, $cod){
			try{
				$res = $this->db->prepare('SELECT * FROM persona WHERE cedula = ? AND cod_consejocomunal IN (SELECT cod FROM consejocomunal WHERE cod IN (SELECT cod_consejocomunal FROM eleccion WHERE finalizado = false AND cod_consejocomunal != ? AND id IN (SELECT id_eleccion FROM candidato WHERE ci_persona = ?)))');
				$res->execute([ $cedula, $cod, $cedula ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function getAjaxPorCedulaSinUsuario($dato, $usuario){
			try{
				if($usuario->tipo == 1){
					$sql = 'SELECT * FROM persona WHERE cedula = ? AND eliminado = false AND cedula NOT IN (SELECT cedula FROM usuario WHERE cedula IS NOT NULL)';
					$res = $this->db->prepare($sql);
					$res->execute([$dato]);
				}else{
					$sql = 'SELECT * FROM persona WHERE cod_consejocomunal = ? AND cedula = ? AND eliminado = false AND cedula NOT IN (SELECT cedula FROM usuario WHERE cedula IS NOT NULL)';
					$res = $this->db->prepare($sql);
					$res->execute([$usuario->cod_consejocomunal, $dato]);
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return 'false';
			}
			return $res;
		}

		public function getAjaxPorNombreYApellidoSinUsuario($dato, $usuario){
			try{
				if($usuario->tipo == 1){
					$sql = 'SELECT * FROM persona WHERE LOWER(pnombre) LIKE ? AND LOWER(papellido) LIKE ? AND eliminado = false AND cedula NOT IN (SELECT cedula FROM usuario WHERE cedula IS NOT NULL)';
					$res = $this->db->prepare($sql);
					$res->execute([$dato[0].'%', $dato[1].'%']);
				}else{
					$sql = 'SELECT * FROM persona WHERE cod_consejocomunal = ? AND LOWER(pnombre) LIKE ? AND LOWER(papellido) LIKE ? AND eliminado = false AND cedula NOT IN (SELECT cedula FROM usuario WHERE cedula IS NOT NULL)';
					$res = $this->db->prepare($sql);
					$res->execute([$usuario->cod_consejocomunal, $dato[0].'%', $dato[1].'%']);
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return 'false';
			}
			return $res;
		}

		public function verificarSiPerteneceConsejo($cedula, $cod){
			try{
				$res = $this->db->prepare('SELECT * FROM persona WHERE cedula = ? AND cod_consejocomunal = ?');
				$res->execute([ $cedula, $cod ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function participaEleccion($cedula){
			try{
				$res = $this->db->prepare('SELECT * FROM persona WHERE cedula = ? AND cedula IN (SELECT ci_persona FROM candidato INNER JOIN eleccion ON candidato.id_eleccion = eleccion.id AND (finalizado = true OR eleccion.id IN (SELECT id_eleccion FROM votante)))');
				$res->execute([ $cedula ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function imprimirElectores($cod){
			try{
				$res = $this->db->prepare("SELECT cedula,pnombre,snombre,papellido,sapellido FROM (SELECT cedula,pnombre,snombre,papellido,sapellido,eliminado,cod_consejocomunal,DATE_PART('year',AGE(fecha_nacimiento)) as edad FROM persona) as persona WHERE eliminado = false AND cod_consejocomunal = ? AND edad > 15");
				$res->execute([ $cod ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				return $res;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

	}

?>