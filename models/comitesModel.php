<?php

	class comitesModel{

		private $id;
		private $nombre;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function setId($id){
			$this->id = $id;
		}
		public function getId(){
			return $this->id;
		}
		public function setNombre($nombre){
			$this->nombre = $nombre;
		}
		public function getNombre(){
			return $this->nombre;
		}

		public function getTodos(){
			try{
				$res = $this->db->prepare('SELECT * FROM comite WHERE eliminado = false ORDER BY nombre_comite ASC');
				$res->execute();
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res;
		}

		public function agregarComite(){
			try{

				$res = $this->db->prepare('INSERT INTO comite(nombre_comite) VALUES (?) RETURNING id');
				$res->execute([ $this->nombre ]);
			}catch(Exception $e){
				return false;
			}

			if($res->rowCount() > 0){
				$id = $res->fetchAll(PDO::FETCH_OBJ);
				$id = $id[0]->id;
				return true;
			}else{
				return false;
			}
		}

		public function editarComite($nombre){
			try{
				$this->db->beginTransaction();
				//SI EL COMITE A EDITAR SE UTILIZÓ EN ALGUNA ELECCIÓN FINALIZADA, SE CREA UN NUEVO COMITE CON ESE MISMO NOMBRE, SOFT DELETEADO, Y SE ACTUALIZA LA RELACIÓN, PARA QUE DE ESA MANERA NO SE MODIFIQUE EL COMITE EN ELECCIONES FINALIZADAS
				if($this->verificarSiComiteTieneEleccionFinalizada()){
					//SE OBTIENE EL NOMBRE ACTUAL DEL COMITE SIN EDITAR
					$nombreViejo = $this->db->prepare('SELECT nombre_comite FROM comite WHERE id = ?');
					$nombreViejo->execute([ $this->id ]);
					$nombreViejo = $nombreViejo->fetchAll(PDO::FETCH_OBJ);
					$nombreViejo = $nombreViejo[0]->nombre_comite;

					//SE CREA UN NUEVO COMITE CON EL CAMPO ELIMINADO EN TRUE Y EL NOMBRE DEL COMITE SIN EDITAR, RETORNANDO EL ID DE ESE COMITE RECIEN CREADO
					$idComiteNuevo = $this->db->prepare('INSERT INTO comite(nombre_comite, eliminado) VALUES (?, true) RETURNING id');
					$idComiteNuevo->execute([ $nombreViejo ]);
					$idComiteNuevo = $idComiteNuevo->fetchAll(PDO::FETCH_OBJ);
					$idComiteNuevo = $idComiteNuevo[0]->id;

					//SE ACTUALIZA LA RELACION DE LOS CANDIDATOS POSTULADOS AL COMITE EN ELECCIONES FINALIZADAS CON EL NUEVO COMITE RECIEN CREADO
					$res = $this->db->prepare('UPDATE candidato SET id_comite = ? WHERE id_comite = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = true)');
					$res->execute([ $idComiteNuevo, $this->id ]);
				}
				//SE ACTUALIZA EL NOMBRE DEL COMITE
				$res = $this->db->prepare('UPDATE comite SET nombre_comite = ? WHERE id = ?');
				$res->execute([ $nombre, $this->id ]);
			}catch(Exception $e){
				error_log($e);
				$this->db->rollBack();
				return false;
			}

			if($res->rowCount() > 0){
				$this->db->commit();
				return true;
			}else{
				$this->db->rollBack();
				return false;
			}
		}

		public function borrarComite(){
			try{
				$this->db->beginTransaction();

				// SE VERIFICA SI EL COMITE EXISTE EN ALGUNA ELECCIÓN YA FINALIZADA O COMENZADA, SI ES ASÍ, SE REALIZA UN SOFT DELETE EN LUGAR DE HARD DELETE
				$res = $this->db->prepare('SELECT DISTINCT id_comite FROM candidato WHERE id_comite = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = true) OR id_eleccion IN (SELECT DISTINCT id_eleccion FROM votante) AND id_comite = ?');
				$res->execute([ $this->id, $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					// SOFT DELETE AL COMITE
					$res = $this->db->prepare('UPDATE comite SET eliminado = true WHERE id = ?');
					$res->execute([ $this->id ]);
					// SE BORRAN LOS CANDIDATOS QUE ESTABAN POSTULADOS PARA ESE COMITE EN LAS ELECCIONES QUE NO HAN FINALIZADO NI COMENZADO
					$res = $this->db->prepare('DELETE FROM candidato WHERE id_comite = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = false) AND id_eleccion NOT IN (SELECT DISTINCT id_eleccion FROM votante)');
					$res->execute([ $this->id ]);
					// SE BORRA EL COMITE EN LOS CONSEJOS COMUNALES QUE LO TENGAN
					$res = $this->db->prepare('DELETE FROM comite_unidad_consejo WHERE id_comite = ?');
					$res->execute([ $this->id ]);
					$this->db->commit();
					return true;
				}

				// HARD DELETE AL COMITE SI NO ESTÁ EN UNA ELECCIÓN FINALIZADA
				$res = $this->db->prepare('DELETE FROM comite WHERE id = ?');
				$res->execute([ $this->id ]);
			}catch(Exception $e){
				$this->db->rollBack();
				return false;
			}

			if($res->rowCount() > 0){
				$this->db->commit();
				return true;
			}else{
				$this->db->rollBack();
				return false;
			}
		}

		public function getPorConsejoUnidad($cod, $idUnidad){
			try{
				$res = $this->db->prepare('SELECT * FROM comite_unidad_consejo,comite WHERE comite_unidad_consejo.id_comite = comite.id AND id_unidad = ? AND cod_consejo = ? ORDER BY nombre_comite ASC');
				$res->execute([ $idUnidad, $cod ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res;

		}

		public function getComitesNotIn($cod, $idUnidad){
			try{
				$res = $this->db->prepare('SELECT * FROM comite WHERE eliminado = false AND comite.id NOT IN (SELECT id_comite FROM comite_unidad_consejo WHERE id_unidad = ? AND cod_consejo = ?)');
				$res->execute([ $idUnidad, $cod ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res;

		}

		public function agregarComiteUnidad($cod, $idUnidad, $comites){
			$cont = 0;
			$err = 0;

			// ELIMINAR CANDIDATOS DE LA UNIDAD QUE SE POSTULARON ANTES DE QUE TUVIERA COMITES
			$res = $this->db->prepare('DELETE FROM candidato WHERE id_unidad = ? AND id_comite IS NULL AND id_eleccion IN (SELECT id FROM eleccion WHERE cod_consejocomunal = ? AND finalizado = false AND id NOT IN (SELECT DISTINCT id_eleccion FROM votante))');
			$res->execute([ $idUnidad, $cod ]);

			foreach ($comites as $comite) {
				try{
					$res = $this->db->prepare('INSERT INTO comite_unidad_consejo (id_comite, id_unidad, cod_consejo) VALUES (?, ?, ?)');
					$res->execute([ $comite, $idUnidad, $cod ]);
					$cont++;
				}catch(Exception $e){
					$err++;
					continue;
				}
			}
			
			if($err == 0){
				return true;
			}else{
				return false;
			}

		}


		public function verificarSiComiteTieneEleccionComenzada(){
			try{
				$res = $this->db->prepare('SELECT DISTINCT 1 FROM candidato WHERE id_comite = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = false) AND id_eleccion IN (SELECT id_eleccion FROM votante)');
				$res->execute([ $this->id ]);
				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				die(false);
				return true;
			}
		}

		public function verificarSiComiteTieneEleccionFinalizada(){
			try{
				$res = $this->db->prepare('SELECT DISTINCT 1 FROM candidato WHERE id_comite = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = true)');
				$res->execute([ $this->id ]);
				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				error_log($e);
				die(false);
				return true;
			}
		}

	}

?>