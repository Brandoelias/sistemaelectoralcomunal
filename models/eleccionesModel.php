<?php

	class eleccionesModel{
		private $id;
		private $fecha;
		private $cod_consejocomunal;
		private $finalizado;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function setId($id){
			$this->id = $id;
		}
		public function getId(){
			return $this->id;
		}
		public function setFecha($fecha){
			$this->fecha = $fecha;
		}
		public function getFecha(){
			return $this->fecha;
		}
		public function setCodConsejocomunal($cod){
			$this->cod_consejocomunal = $cod;
		}
		public function getCodConsejocomunal(){
			return $this->cod_consejocomunal;
		}
		public function setFinalizado($finalizado){
			$this->finalizado = $finalizado;
		}
		public function getFinalizado(){
			return $this->finalizado;
		}

		public function crear(){
			try{
				$res = $this->db->prepare('INSERT INTO eleccion (fecha, cod_consejocomunal, finalizado) VALUES (?, ?, ?) RETURNING id');
				$res->execute([ $this->fecha, $this->cod_consejocomunal, 'false' ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				$idEleccion = $res[0]->id;
				if(count($res) > 0){
					return $res[0]->id;
				}else{
					return false;
				}
			}catch(Exception $e){
				return false;
			}
		}

		public function getActivas($cod = null){
			try{
				if(!isset($cod)){
					$res = $this->db->prepare('SELECT * FROM eleccion,consejocomunal WHERE eleccion.cod_consejocomunal = consejocomunal.cod AND finalizado != true ORDER BY id DESC');
					$res->execute();
				}else{
					$res = $this->db->prepare('SELECT * FROM eleccion,consejocomunal WHERE eleccion.cod_consejocomunal = consejocomunal.cod AND eleccion.cod_consejocomunal = ? AND finalizado != true ORDER BY id DESC');
					$res->execute([ $cod ]);
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return false;
			}

			return $res;
		}

		public function getFinalizadas($cod = null){
			try{
				if(!isset($cod)){
					$res = $this->db->prepare('SELECT * FROM eleccion,consejocomunal WHERE eleccion.cod_consejocomunal = consejocomunal.cod AND finalizado = true ORDER BY eleccion.fecha DESC');
					$res->execute();
				}else{
					$res = $this->db->prepare('SELECT * FROM eleccion,consejocomunal WHERE eleccion.cod_consejocomunal = consejocomunal.cod AND eleccion.cod_consejocomunal = ? AND finalizado = true ORDER BY eleccion.fecha DESC');
					$res->execute([ $cod ]);
				}
				
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}
			return $res;
		}

		public function consultarPorId(){
			try{
				$res = $this->db->prepare('SELECT * FROM eleccion,consejocomunal WHERE eleccion.cod_consejocomunal = consejocomunal.cod AND id = ?');
				$res->execute([ $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			if(count($res) > 0){
				return $res[0];
			}else{
				return false;
			}
		}

		public function getCantidadDeCandidatosPorUnidad($idUnidad, $cod_consejocomunal){
			try{
				$res = null;
				if($this->getCantidadDeComitesPorUnidad($idUnidad, $cod_consejocomunal) > 0){
					$res = $this->db->prepare('SELECT count(ci_persona) FROM candidato WHERE id_unidad = ? AND id_comite > 0 AND id_eleccion = ?');
					$res->execute([ $idUnidad, $this->id ]);
				}else{
					$res = $this->db->prepare('SELECT count(ci_persona) FROM candidato WHERE id_unidad = ? AND id_eleccion = ?');
					$res->execute([ $idUnidad, $this->id ]);
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			if(count($res) > 0){
				return $res[0]->count;
			}else{
				return 0;
			}

		}

		public function getCantidadDeComitesPorUnidad($idUnidad, $cod_consejocomunal){
			try{
				$res = $this->db->prepare('SELECT count(id_comite) FROM comite_unidad_consejo WHERE id_unidad = ? AND cod_consejo = ?');
				$res->execute([ $idUnidad, $cod_consejocomunal ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res[0]->count;
			
		}

		public function verCandidatosUnidad($idUnidad, $orderByVotos = false){
			if($orderByVotos){
				$sql = 'SELECT * FROM candidato,persona WHERE candidato.ci_persona = persona.cedula AND id_eleccion = ? AND id_unidad = ? ORDER BY candidato.votos DESC';
				$res = $this->db->prepare($sql);
				$res->execute([ $this->id, $idUnidad ]);
			}else{
				$sql = 'SELECT * FROM candidato,persona WHERE candidato.ci_persona = persona.cedula AND id_eleccion = ? AND id_unidad = ? ORDER BY pnombre ASC';
				$res = $this->db->prepare($sql);
				$res->execute([ $this->id, $idUnidad ]);
			}

			try{
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return false;
			}

			return $res;
		}

		public function getComitesPorUnidad($idUnidad, $cod_consejocomunal){
			try{
				$res = $this->db->prepare('SELECT * FROM comite_unidad_consejo,comite WHERE comite_unidad_consejo.id_comite = comite.id AND id_unidad = ? AND cod_consejo = ?');
				$res->execute([ $idUnidad, $cod_consejocomunal ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res;
		}

		public function getCantidadDeCandidatosPorComite($idUnidad, $idComite){
			try{
				$res = $this->db->prepare('SELECT count(ci_persona) FROM candidato WHERE id_eleccion = ? AND id_unidad = ? AND id_comite = ?');
				$res->execute([ $this->id, $idUnidad, $idComite ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res[0]->count;
		}

		public function verCandidatosComite($idUnidad, $idComite, $orderByVotos = false){
			if($orderByVotos){
				$sql = 'SELECT * FROM candidato,persona WHERE candidato.ci_persona = persona.cedula AND id_eleccion = ? AND id_unidad = ? AND id_comite = ? ORDER BY candidato.votos DESC';
			}else{
				$sql = 'SELECT * FROM candidato,persona WHERE candidato.ci_persona = persona.cedula AND id_eleccion = ? AND id_unidad = ? AND id_comite = ? ORDER BY pnombre ASC';
			}
			try{
				$res = $this->db->prepare($sql);
				$res->execute([ $this->id, $idUnidad, $idComite ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res;
		}

		public function getPersonasComiteNombreAjax($idUnidad, $idComite, $dato){

			try{
				$res = $this->db->prepare("SELECT * FROM (SELECT *,DATE_PART('year',AGE(fecha_nacimiento)) as edad FROM persona) as persona WHERE edad > 18 AND eliminado = false AND cedula NOT IN (SELECT ci_persona FROM candidato WHERE id_eleccion = ?) AND cod_consejocomunal IN (SELECT cod FROM consejocomunal WHERE cod = (SELECT cod_consejocomunal FROM eleccion WHERE id = ?)) AND LOWER(pnombre) LIKE ? AND (LOWER(snombre) LIKE ? OR LOWER(papellido) LIKE ?) AND LOWER(papellido) LIKE ? AND LOWER(sapellido) LIKE ?");
				$res->execute([ $this->id, $this->id, $dato[0].'%', $dato[1].'%', $dato[1].'%', $dato[2].'%', $dato[3].'%' ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return $e;
			}

			return $res;

		}

		public function getPersonasUnidadNombreAjax($idUnidad, $dato){

			try{
				$res = $this->db->prepare("SELECT * FROM (SELECT *,DATE_PART('year',AGE(fecha_nacimiento)) as edad FROM persona) as persona WHERE edad > 18 AND eliminado = false AND cedula NOT IN (SELECT ci_persona FROM candidato WHERE id_eleccion = ?) AND cod_consejocomunal IN (SELECT cod FROM consejocomunal WHERE cod = (SELECT cod_consejocomunal FROM eleccion WHERE id = ?)) AND LOWER(pnombre) LIKE ? AND (LOWER(snombre) LIKE ? OR LOWER(papellido) LIKE ?) AND LOWER(papellido) LIKE ? AND LOWER(sapellido) LIKE ?");
				$res->execute([ $this->id, $this->id, $dato[0].'%', $dato[1].'%', $dato[1].'%', $dato[2].'%', $dato[3].'%' ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return $e;
			}

			return $res;

		}

		public function getPersonasComiteCedulaAjax($idUnidad, $idComite, $dato){

			try{
				$res = $this->db->prepare("SELECT * FROM (SELECT *,DATE_PART('year',AGE(fecha_nacimiento)) as edad FROM persona) as persona WHERE edad > 18 AND eliminado = false AND cedula NOT IN (SELECT ci_persona FROM candidato WHERE id_eleccion = ?) AND cod_consejocomunal IN (SELECT cod FROM consejocomunal WHERE cod = (SELECT cod_consejocomunal FROM eleccion WHERE id = ?)) AND CAST(cedula AS TEXT) LIKE ?");
				$res->execute([ $this->id, $this->id, $dato.'%' ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return $e;
			}

			return $res;

		}

		public function getPersonasUnidadCedulaAjax($idUnidad, $dato){

			try{
				$res = $this->db->prepare("SELECT * FROM (SELECT *,DATE_PART('year',AGE(fecha_nacimiento)) as edad FROM persona) as persona WHERE edad > 18 AND eliminado = false AND cedula NOT IN (SELECT ci_persona FROM candidato WHERE id_eleccion = ?) AND cod_consejocomunal IN (SELECT cod FROM consejocomunal WHERE cod = (SELECT cod_consejocomunal FROM eleccion WHERE id = ?)) AND CAST(cedula AS TEXT) LIKE ?");
				$res->execute([ $this->id, $this->id, $dato.'%' ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return $e;
			}

			return $res;

		}

		public function guardarCandidatoPostuladoUnidad($idUnidad, $idComite, $listaPersonas){

			$nombreComite = $this->db->prepare('SELECT nombre_comite FROM comite WHERE id = ?');
			$nombreComite->execute([ $idComite ]);
			$nombreComite = $nombreComite->fetchAll(PDO::FETCH_OBJ);
			if(count($nombreComite) < 1){
				return '{"err" : "1", "msj" : "¡No se pudo agregar lo/s postulado/s!"}';
			}
			$nombreComite = $nombreComite[0]->nombre_comite;

			$err = 0;
			$bn = 0;
			for($i = 0; $i < count($listaPersonas); $i++){
				try{
					// VERIFICAR QUE LA PERSONA SE ESTÉ POSTULANDO A UNA ELECCIÓN DEL CONSEJO COMUNAL AL QUE PERTENECE
					$personaPostular = $this->db->prepare('SELECT * FROM persona WHERE cedula = ? AND cod_consejocomunal IN (SELECT cod FROM consejocomunal WHERE cod IN (SELECT cod_consejocomunal FROM eleccion WHERE id = ?))');
					$personaPostular->execute([ $listaPersonas[$i], $this->id ]);
					$personaPostular = $personaPostular->fetchAll(PDO::FETCH_OBJ);
					if(count($personaPostular) < 1){
						$err++;
						continue;
					}

					// SE VERIFICA QUE LA PERSONA NO ESTÉ POSTULADA EN ESA ELECCIÓN
					$res = $this->db->prepare('SELECT * FROM candidato WHERE ci_persona = ? AND id_eleccion = ?');
					$res->execute([ $listaPersonas[$i], $this->id ]);
					$res = $res->fetchAll(PDO::FETCH_OBJ);
					if(count($res) > 0){
						$err++;
						continue;
					}

					$res = $this->db->prepare('INSERT INTO candidato(ci_persona, id_eleccion, id_unidad, id_comite) VALUES(?, ?, ?, ?)');
					$res->execute([ $listaPersonas[$i], $this->id, $idUnidad, $idComite ]);
					$personaPostular = $personaPostular[0];
				}catch(Exception $e){
					error_log($e);
					$err++;
					continue;
				}
				$bn++;
			}

			if($bn > $err && $err > 0){
				return '{"err" : "1", "msj" : "¡Algunos postulados no se pudieron agregar!"}';
			}elseif($err >= $bn){
				return '{"err" : "1", "msj" : "¡No se pudo agregar lo/s postulado/s!"}';
			}else{
				return '{"err" : "0", "msj" : "¡Candidato/s agregado/s!"}';
			}

		}

		public function guardarCandidatoPostuladoComite($idUnidad, $listaPersonas){
			$err = 0;
			$bn = 0;
			for($i = 0; $i < count($listaPersonas); $i++){
				try{
					// VERIFICAR QUE LA PERSONA SE ESTÉ POSTULANDO A UNA ELECCIÓN DEL CONSEJO COMUNAL AL QUE PERTENECE
					$personaPostular = $this->db->prepare('SELECT * FROM persona WHERE cedula = ? AND cod_consejocomunal IN (SELECT cod FROM consejocomunal WHERE cod IN (SELECT cod_consejocomunal FROM eleccion WHERE id = ?))');
					$personaPostular->execute([ $listaPersonas[$i], $this->id ]);
					$personaPostular = $personaPostular->fetchAll(PDO::FETCH_OBJ);
					if(count($personaPostular) < 1){
						$err++;
						continue;
					}

					// SE VERIFICA QUE LA PERSONA NO ESTÉ POSTULADA EN ESA ELECCIÓN
					$res = $this->db->prepare('SELECT * FROM candidato WHERE ci_persona = ? AND id_eleccion = ?');
					$res->execute([ $listaPersonas[$i], $this->id ]);
					$res = $res->fetchAll(PDO::FETCH_OBJ);
					if(count($res) > 0){
						$err++;
						continue;
					}

					$res = $this->db->prepare('INSERT INTO candidato(ci_persona, id_eleccion, id_unidad) VALUES(?, ?, ?)');
					$res->execute([ $listaPersonas[$i], $this->id, $idUnidad ]);
					$personaPostular = $personaPostular[0];
				}catch(Exception $e){
					$err++;
					continue;
				}
				$bn++;
			}

			if($bn > $err && $err > 0){
				return '{"err" : "1", "msj" : "¡Algunos postulados no se pudieron agregar!"}';
			}elseif($err >= $bn){
				return '{"err" : "1", "msj" : "¡No se pudo agregar lo/s postulado/s!"}';
			}else{
				return '{"err" : "0", "msj" : "¡Candidato/s agregado/s!"}';
			}

		}


		public function borrarCandidato($cedulas, $idUnidad, $idComite){
			$this->db->beginTransaction();

			foreach($cedulas as $cedula){
				if(!$idComite){
					try{
						$res = $this->db->prepare('DELETE FROM candidato WHERE ci_persona = ? AND id_eleccion = ? AND id_unidad = ?');
						$res->execute([ $cedula, $this->id, $idUnidad ]);
					}catch(Exception $e){
						error_log($e);
						$this->db->rollBack();
						return false;
					}

				}else{
					try{
						$res = $this->db->prepare('DELETE FROM candidato WHERE ci_persona = ? AND id_eleccion = ? AND id_unidad = ? AND id_comite = ?');
						$res->execute([ $cedula, $this->id, $idUnidad, $idComite ]);
					}catch(Exception $e){
						error_log($e);
						$this->db->rollBack();
						return false;
					}
				}
			}

			if($res->rowCount() > 0){
				$this->db->commit();
				return true;
			}else{
				$this->db->rollBack();
				return false;
			}

		}

		public function guardarVotacion($votos, $ip){
			$this->db->beginTransaction();

			// SE VERIFICA QUE LA MÁQUINA ESTÉ ACTIVA ANTES DE GUARDAR LA VOTACIÓN, PARA ASEGURARSE DE QUE LOS VOTOS SÓLO SE CUENTEN UNA SÓLA VEZ SI SE LLEGARA A RECIBIR MÁS DE UNA VEZ LA MISMA SOLICITUD DE PARTE DEL MISMO VOTANTE
			$maquinaActivada = $this->verificarMaquinaVotanteActivo($ip, $_SESSION['maquina_votante_id']);
			if(!$maquinaActivada || count($maquinaActivada) <= 0 || !$maquinaActivada[0]->activado){
				error_log('SE INTENTO VOTAR EN UNA MAQUINA NO ACTIVA');
				$this->db->rollBack();
				return false;
			}

			// SE DESACTIVA LA MÁQUINA VOTANTE PARA QUE NO SE PUEDA VOTAR HASTA QUE SEA ACTIVADA DE NUEVO PARA OTRO VOTANTE
			$prepare = $this->db->prepare('UPDATE maquina_votante SET activado = false AND votando = NULL WHERE id = ? AND ip = ? AND id_eleccion = ?');
			$prepare->execute([ $_SESSION['maquina_votante_id'], $ip, $this->id ]);

			// ARREGLO PARA GUARDAR LAS UNIDADES SIN COMITÉS QUE YA SE REALIZÓ UNA VOTACIÓN, PARA LUEGO VERIFICAR QUE SE HAYA RECIBIDO SÓLAMENTE UN VOTO POR CADA UNIDAD SIN COMITÉ
			$UnidadesSinComitesYaVotados = [];

			// ARREGLO PARA GUARDAR UNIDADES CON COMITÉS QUE YA SE REALIZÓ UNA VOTACIÓN, PARA LUEGO VERIFICAR QUE NO PASE LOS 5 VOTOS MÁXIMOS
			$UnidadesConComitesYaVotados = [];

			try{
				$cod_consejocomunal = $this->db->prepare('SELECT cod_consejocomunal FROM eleccion WHERE id = ?');
				$cod_consejocomunal->execute([ $this->id ]);
				$cod_consejocomunal = $cod_consejocomunal->fetchAll(PDO::FETCH_OBJ);
				if(count($cod_consejocomunal) < 1){
					error_log('NO SE PUDO OBTENER EL CONSEJO COMUNAL DE LA ELECCIÓN');
					return false;
				}
				$cod_consejocomunal = $cod_consejocomunal[0]->cod_consejocomunal;
			}catch(Exception $e){
				error_log('NO SE PUDO OBTENER EL CONSEJO COMUNAL DE LA ELECCIÓN');
				return false;
			}

			// SE GUARDA CADA VOTO QUE HAYA REALIZADO EL VOTANTE
			foreach ($votos as $voto) {
				// SI EL VOTO NO TIENE IDCOMITE ENTONCES ES UN VOTO PARA UNIDAD
				if(!isset($voto->idComite) || $voto->idComite == '' || !is_numeric($voto->idComite)){

					try{
						$votosMaximos = $this->db->prepare('SELECT votos_maximos FROM consejocomunal_unidad WHERE cod_consejocomunal = ? AND id_unidad = ?');
						$votosMaximos->execute([ $cod_consejocomunal, $voto->idUnidad ]);
						$votosMaximos = $votosMaximos->fetchAll(PDO::FETCH_OBJ);
						if(count($votosMaximos) < 1){
							$this->db->rollBack();
							error_log('NO SE PUDO OBTENER LOS VOTOS MÁXIMOS DE UNIDAD');
							return false;
						}
						$votosMaximos = $votosMaximos[0]->votos_maximos;
					}catch(Exception $e){
						error_log('NO SE PUDO OBTENER LOS VOTOS MÁXIMOS DE UNIDAD');
						return false;
					}

					/* VERIFICAR QUE SÓLAMENTE SE HAYA VOTADO POR UNA UNIDAD SIN COMITÉS UNA SÓLA VEZ
					foreach ($UnidadesSinComitesYaVotados as $u) {
						if($u == $voto->idUnidad){
							error_log('SE INTENTO VOTAR MAS DE UNA VEZ EN UNA UNIDAD.');
							$this->db->rollBack();
							return false;
						}
					}*/

					// VERIFICAR QUE NO SE PASE DE LOS VOTOS MÁXIMOS
					array_push($UnidadesSinComitesYaVotados, $voto->idUnidad);
					$contar = 0;
					foreach ($UnidadesSinComitesYaVotados as $u) {
						if($u == $voto->idUnidad){
							$contar++;
							if($contar > $votosMaximos){
								error_log('SE INTENTO SOBREPASAR EL LIMITE DE VOTOS EN UNA UNIDAD.');
								$this->db->rollBack();
								return false;
							}
						}
					}

					$sql = 'UPDATE candidato SET votos = votos + 1 WHERE ci_persona = ? AND id_eleccion = ? AND id_unidad = ? AND id_comite IS NULL';
					$execute = [ $voto->ciPersona, $this->id, $voto->idUnidad ];
				}else{

					try{
						$votosMaximos = $this->db->prepare('SELECT votos_maximos FROM comite_unidad_consejo WHERE cod_consejo = ? AND id_unidad = ? AND id_comite = ?');
						$votosMaximos->execute([ $cod_consejocomunal, $voto->idUnidad, $voto->idComite ]);
						$votosMaximos = $votosMaximos->fetchAll(PDO::FETCH_OBJ);
						if(count($votosMaximos) < 1){
							$this->db->rollBack();
							error_log('NO SE PUDO OBTENER LOS VOTOS MÁXIMOS DE COMITE');
							return false;
						}
						$votosMaximos = $votosMaximos[0]->votos_maximos;
					}catch(Exception $e){
						error_log('NO SE PUDO OBTENER LOS VOTOS MÁXIMOS DE COMITE');
						return false;
					}

					// VERIFICAR QUE NO SE PASE DE 5 VOTOS EN COMITÉ
					array_push($UnidadesConComitesYaVotados, [$voto->idUnidad, $voto->idComite]);
					$contar = 0;
					foreach ($UnidadesConComitesYaVotados as $u) {
						if($u[0] == $voto->idUnidad && $u[1] == $voto->idComite){
							$contar++;
							if($contar > $votosMaximos){
								error_log('SE INTENTO SOBREPASAR EL LIMITE DE VOTOS EN UN COMITE');
								$this->db->rollBack();
								return false;
							}
						}
					}

					$sql = 'UPDATE candidato SET votos = votos + 1 WHERE ci_persona = ? AND id_eleccion = ? AND id_unidad = ? AND id_comite = ?';
					$execute = [ $voto->ciPersona, $this->id, $voto->idUnidad, $voto->idComite ];
				}
				try{
					$res = $this->db->prepare($sql);
					$res->execute($execute);
				}catch(Exception $e){
					error_log($e);
					$this->db->rollBack();
					return false;
				}
			}
			$this->db->commit();
			return true;
		}

		public function cambiarFecha($fecha){
			try{
				$res = $this->db->prepare('UPDATE eleccion SET fecha = ? WHERE id = ?');
				$res->execute([ $fecha, $this->id ]);
			}catch(Exception $e){
				return false;
			}
			return true;
		}

		public function getMaquinasVotantes($usuario){
			try{
				if($usuario->tipo == 1 || $usuario->tipo == 2){
					$res = $this->db->prepare('SELECT * FROM maquina_votante LEFT JOIN persona ON maquina_votante.votando = persona.cedula WHERE id_eleccion = ? ORDER BY id ASC');
					$res->execute([ $this->id ]);
				}else{
					$res = $this->db->prepare('SELECT * FROM maquina_votante LEFT JOIN persona ON maquina_votante.votando = persona.cedula WHERE id_eleccion = ? AND id IN (SELECT id_maquina FROM maquina_votante_usuario WHERE id_usuario = ?) ORDER BY id ASC');
					$res->execute([ $this->id, $usuario->id ]);
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}
			return $res;
		}

		public function getMaquinaVotantePorId($id){
			try{
				$res = $this->db->prepare('SELECT * FROM maquina_votante WHERE id = ?');
					$res->execute([ $id]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) < 1){
					return false;
				}
			}catch(Exception $e){
				return false;
			}
			return $res;
		}

		public function getHashPass(){
			try{
				$res = $this->db->prepare('SELECT contraseña FROM eleccion WHERE id = ?');
				$res->execute([ $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				error_log($e);
				return false;
			}
			return $res;
		}

		public function autenticarse($usuario, $pass){
			try{
				$res = $this->db->prepare('SELECT count(id_eleccion) FROM eleccion WHERE id = ? AND contraseña = ?');
				$res->execute([ $this->id, $pass ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			if($res[0]->count > 0){
				return true;
			}else{
				return false;
			}
		}

		public function insertarMaquinaVotante($ip){
			try{
				$res = $this->db->prepare('INSERT INTO maquina_votante (ip, activado, id_eleccion) VALUES(?, ?, ?) RETURNING id');
				$res->execute([ $ip, 'false', $this->id ]);
			}catch(Exception $e){
				return false;
			}

			if($res->rowCount() > 0){
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				return $res[0]->id;
			}else{
				return false;
			}
		}

		public function verificarMaquinaVotante($ip, $id){
			try{
				$res = $this->db->prepare('SELECT count(id) FROM maquina_votante WHERE ip = ? AND id_eleccion = ? AND id = ?');
				$res->execute([ $ip, $this->id, $id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			if($res[0]->count > 0){
				return true;
			}else{
				return false;
			}
		}

		public function verificarMaquinaVotanteActivo($ip, $id){
			try{
				$res = $this->db->prepare('SELECT * FROM maquina_votante WHERE ip = ? AND id_eleccion = ? AND id = ?');
				$res->execute([ $ip, $this->id, $id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res;
		}

		public function esperando($ip){
			try{
				$res = $this->db->prepare('SELECT activado FROM maquina_votante WHERE ip = ? AND id = ? AND id_eleccion = ?');
				$res->execute([ $ip, $_SESSION['maquina_votante_id'], $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			if(count($res) > 0 && $res[0]->activado == 'true'){
				return true;
			}elseif(count($res) < 1){
				return 'quitar';
			}
			else{
				return false;
			}
		}

		public function activarMaquinaVotante($id, $cedula, $id_eleccion){
			try{
				$this->db->beginTransaction();
				$res = $this->db->prepare('UPDATE maquina_votante SET activado = true, votando = ? WHERE id = ?');
				$res2 = $this->db->prepare('INSERT INTO votante(ci_persona, id_eleccion) VALUES(?, ?)');
				$res2->execute([ $cedula, $id_eleccion ]);
			}catch(Exception $e){
				$this->db->rollBack();
				error_log($e);
				return false;
			}

			if($res2->rowCount() > 0){
				try{
					$res->execute([ $cedula, $id ]);
					$this->db->commit();
				}catch(Exception $e){
					$this->db->rollBack();
					return false;
				}
				return 'ok';
			}else{
				$this->db->rollBack();
				return false;
			}
		}

		public function quitarMaquinaVotante($id){
			try{
				$this->db->beginTransaction();
				$res = $this->db->prepare('DELETE FROM votante WHERE id_eleccion = ? AND ci_persona = (SELECT votando FROM maquina_votante WHERE id = ? AND id_eleccion = ? AND activado = true)');
				$res->execute([ $this->id, $id, $this->id ]);

				$res = $this->db->prepare('DELETE FROM maquina_votante WHERE id = ?');
				$res->execute([ $id ]);
			}catch(Exception $e){
				error_log($e);
				$this->db->rollBack();
				return 0;
			}

			if($res->rowCount() > 0){
				$this->db->commit();
				return 1;
			}else{
				$this->db->rollBack();
				return 0;
			}
		}

		public function finalizar(){
			try{
				$this->db->beginTransaction();
				$res = $this->db->prepare('UPDATE eleccion SET finalizado = true WHERE id = ?');
				$res->execute([ $this->id ]);
				$res = $this->db->prepare('DELETE FROM maquina_votante WHERE id_eleccion = ?');
				$res->execute([ $this->id ]);
				$this->db->commit();
			}catch(Exception $e){
				$this->db->rollBack();
				return false;
			}
			return true;

		}

		public function VerificarSiExisteLaIp($ip, $id_eleccion){
			try{
				$res = $this->db->prepare('SELECT COUNT(ip) FROM maquina_votante WHERE ip = ? AND id_eleccion = ?');
				$res->execute([ $ip, $id_eleccion ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			if($res[0]->count > 0){
				return true;
			}else{
				return false;
			}
		}

		public function cambiarPassVotacion($pass){
			try{
				$res = $this->db->prepare('UPDATE eleccion SET contraseña = ? WHERE id = ?');
				$res->execute([ $pass, $this->id ]);
			}catch(Exception $e){
				error_log($e);
				return false;
			}

			if($res->rowCount() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function eliminar($id){
			try{
				$this->db->beginTransaction();

				$res = $this->db->prepare('DELETE FROM eleccion WHERE id = ?');
				$res->execute([$id]);

				// VERIFICAR UNIDADES, COMITES Y PERSONAS SOFT DELETEADAS PARA HACERLES HARD DELETE SI YA NINGUNA ELECCIÓN NECESITA SUS DATOS
				$r = $this->db->prepare('DELETE FROM unidad WHERE eliminado = true AND id NOT IN (SELECT DISTINCT id_unidad FROM candidato)');
				$r->execute();
				$r = $this->db->prepare('DELETE FROM comite WHERE eliminado = true AND id NOT IN (SELECT DISTINCT id_comite FROM candidato)');
				$r->execute();
				$r = $this->db->prepare('DELETE FROM persona WHERE eliminado = true AND cedula NOT IN (SELECT DISTINCT ci_persona FROM candidato)');
				$r->execute();
			}catch(Exception $e){
				$this->db->rollBack();
				error_log($e);
				return '0';
			}

			if($res->rowCount() > 0){
				$this->db->commit();
				return '1';
			}else{
				$this->db->rollBack();
				return '0';
			}
		}

		public function cantidadDeParticipantes(){
			try{
				$res = $this->db->prepare('SELECT count(ci_persona) FROM votante WHERE id_eleccion = ?');
				$res->execute([ $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			if(count($res) > 0){
				return $res[0]->count;
			}else{
				return 0;
			}
		}

		public function getUnidadesResultados(){
			try{
				$res = $this->db->prepare('SELECT *,(SELECT votos_maximos FROM consejocomunal_unidad WHERE id_unidad = id AND cod_consejocomunal = (SELECT cod_consejocomunal FROM eleccion WHERE id = ?)) AS votos_maximos FROM unidad WHERE id IN (SELECT id_unidad FROM candidato WHERE id_eleccion = ?) ORDER BY nombre_unidad ASC');
				$res->execute([ $this->id, $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				return $res;
			}catch(Exception $e){
				error_log($e);
				return [];
			}
		}

		public function getComitesUnidadResultados($id_unidad){
			try{
				$res = $this->db->prepare('SELECT *,(SELECT votos_maximos FROM comite_unidad_consejo WHERE id_unidad = ? AND id_comite = id AND cod_consejo = (SELECT cod_consejocomunal FROM eleccion WHERE id = ?)) AS votos_maximos FROM comite WHERE id IN (SELECT id_comite FROM candidato WHERE id_unidad = ? AND id_eleccion = ?) ORDER BY nombre_comite ASC');
				$res->execute([ $id_unidad, $this->id, $id_unidad, $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				return $res;
			}catch(Exception $e){
				error_log($e);
				return [];
			}
		}

		public function verificarQueNoHaVotado($cedula){
			try{
				$res = $this->db->prepare('SELECT * FROM votante WHERE ci_persona = ? AND id_eleccion = ?');
				$res->execute([ $cedula, $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) < 1){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function verificarSiYaComenzo(){
			try{
				$res = $this->db->prepare('SELECT * FROM votante WHERE id_eleccion = ?');
				$res->execute([ $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return true;
			}
		}

		public function verificarSiYaFinalizo(){
			try{
				$res = $this->db->prepare('SELECT * FROM eleccion WHERE finalizado = true AND id = ?');
				$res->execute([ $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return true;
			}
		}
		// VERIFICAR SI EL CONSEJO COMUNAL TIENE ALGUNA ELECCION EN CURSO Y QUE NO HA FINALIZADO
		public function verificarSiConsejoTieneEleccionComenzada($cod){
			try{
				$res = $this->db->prepare('SELECT 1 FROM votante INNER JOIN eleccion ON votante.id_eleccion = eleccion.id WHERE eleccion.cod_consejocomunal = ? AND eleccion.finalizado = false');
				$res->execute([ $cod ]);
				if($res->rowCount() > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return true;
			}
		}

		public function votantePerteneceConsejo($cedula){
			try{
				$res = $this->db->prepare('SELECT * FROM persona WHERE cedula = ? AND cod_consejocomunal = (SELECT cod_consejocomunal FROM eleccion WHERE id = ?)');
				$res->execute([ $cedula, $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function cancelarVotante($idMaquina, $ip, $idEleccion){
			try{
				$this->db->beginTransaction();
				$res = $this->db->prepare('DELETE FROM votante WHERE id_eleccion = ? AND ci_persona = (SELECT votando FROM maquina_votante WHERE id = ? AND ip = ? AND id_eleccion = ?)');
				$res->execute([ $idEleccion, $idMaquina, $ip, $idEleccion ]);
			}catch(Exception $e){
				$this->db->rollBack();
				error_log($e);
				return false;
			}
			return true;
		}

		public function getEleccion($id){
			try{
				$res = $this->db->prepare('SELECT * FROM eleccion WHERE id = ?');
				$res->execute([ $id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(isset($res[0])){
					$res = $res[0];
				}else{
					$res = [];
				}
				return $res;
			}catch(Exception $e){
				error_log($e);
				return [];
			}
		}

		public function getAdministradoresEleccion($usuario, $id_maquina){
			try{
				if($usuario->tipo != 1){
					$res = $this->db->prepare('SELECT id,persona.cedula,pnombre,papellido,(SELECT id_maquina FROM maquina_votante_usuario WHERE id_maquina = ? AND id_usuario = id) AS habilitado FROM usuario INNER JOIN persona ON usuario.cedula = persona.cedula WHERE cod_consejocomunal = ? AND usuario.tipo = 3');
					$res->execute([ $id_maquina, $usuario->cod_consejocomunal ]);
				}else{
					$res = $this->db->prepare('SELECT id,persona.cedula,pnombre,papellido,(SELECT id_maquina FROM maquina_votante_usuario WHERE id_maquina = ? AND id_usuario = id) AS habilitado FROM usuario INNER JOIN persona ON usuario.cedula = persona.cedula WHERE cod_consejocomunal = (SELECT cod_consejocomunal FROM eleccion WHERE id = (SELECT id_eleccion FROM maquina_votante WHERE id = ?)) AND usuario.tipo = 3');
					$res->execute([ $id_maquina, $id_maquina ]);
				}
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				return $res;
			}catch(Exception $e){
				error_log($e);
				return [];
			}
		}

		public function maquinaPerteneceConsejo($idMaquina, $cod){
			try{
				$res = $this->db->prepare('SELECT * FROM maquina_votante INNER JOIN eleccion ON maquina_votante.id_eleccion = eleccion.id WHERE eleccion.cod_consejocomunal = ? AND maquina_votante.id = ?');
				$res->execute([ $cod, $idMaquina ]);
				if($res->rowCount() > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function verificarUsuarioAdministradorDeEleccionConsejo($idUsuario, $cod){
			try{
				$res = $this->db->prepare('SELECT * FROM usuario INNER JOIN persona ON usuario.cedula = persona.cedula WHERE usuario.id = ? AND usuario.tipo = 3 AND persona.cod_consejocomunal = ?');
				$res->execute([ $idUsuario, $cod ]);
				if($res->rowCount() > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function habilitarMaquinaUsuario($idMaquina, $idUsuario){
			try{
				$res = $this->db->prepare('INSERT INTO maquina_votante_usuario VALUES(?, ?) ON CONFLICT (id_maquina, id_usuario) DO NOTHING');
				$res->execute([ $idMaquina, $idUsuario ]);
				return true;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function eliminarAdministradoresDeEleccion($idMaquina, $idsUsuarios){
			try{
				$sql = '(';
				foreach($idsUsuarios as $i => $idUsuario){
					$sql .= $idUsuario;
					if($i != count($idsUsuarios) - 1){
						$sql .= ',';
					}
				}
				$sql .= ')';
				if(count($idsUsuarios) < 1){
					$sql = 'DELETE FROM maquina_votante_usuario WHERE id_maquina = ?';
				}else{
					$sql = 'DELETE FROM maquina_votante_usuario WHERE id_maquina = ? AND id_usuario NOT IN ' . $sql;
				}
				$res = $this->db->prepare($sql);
				$res->execute([ $idMaquina ]);
				if($res->rowCount() > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function usuarioHabilitadoMaquina($idUsuario, $idMaquina){
			try{
				$res = $this->db->prepare('SELECT * FROM maquina_votante_usuario WHERE id_maquina = ? AND id_usuario = ?');
				$res->execute([ $idMaquina, $idUsuario ]);
				if($res->rowCount() > 0){
					return true;
				}
				return false;
			}catch(Exception $e){
				error_log($e);
				return false;
			}
		}

		public function iniciarTransaccion(){
			$this->db->beginTransaction();
		}
		public function rollBack(){
			$this->db->rollBack();
		}
		public function commit(){
			$this->db->commit();
		}
	
	}

?>