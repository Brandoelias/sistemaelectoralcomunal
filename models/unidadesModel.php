<?php

	class unidadesModel{

		private $id;
		private $nombre;
		private $db;

		public function __construct(){
			$this->db = mainModel::conectar();
		}

		public function setId($id){
			$this->id = $id;
		}
		public function getId(){
			return $this->id;
		}
		public function setNombre($nombre){
			$this->nombre = $nombre;
		}
		public function getNombre(){
			return $this->nombre;
		}

		public function getTodos(){
			$res = $this->db->prepare('SELECT * FROM unidad WHERE eliminado = false ORDER BY nombre_unidad ASC');
			$res->execute();
			$res = $res->fetchAll(PDO::FETCH_OBJ);

			return $res;
		}

		public function editarUnidad($datos){
			try{
				$this->db->beginTransaction();
				//SI LA UNIDAD A EDITAR SE UTILIZÓ EN ALGUNA ELECCIÓN FINALIZADA, SE CREA UNA NUEVA UNIDAD CON ESE MISMO NOMBRE, SOFT DELETEADA, Y SE ACTUALIZA LA RELACIÓN, PARA QUE DE ESA MANERA NO SE MODIFIQUE LA UNIDAD EN ELECCIONES FINALIZADAS
				if($this->verificarSiUnidadTieneEleccionFinalizada()){
					//SE OBTIENE EL NOMBRE ACTUAL DE LA UNIDAD SIN EDITAR
					$nombreViejo = $this->db->prepare('SELECT nombre_unidad FROM unidad WHERE id = ?');
					$nombreViejo->execute([ $this->id ]);
					$nombreViejo = $nombreViejo->fetchAll(PDO::FETCH_OBJ);
					$nombreViejo = $nombreViejo[0]->nombre_unidad;

					//SE CREA UNA NUEVA UNIDAD CON EL CAMPO ELIMINADO EN TRUE Y EL NOMBRE DE LA UNIDAD SIN EDITAR, RETORNANDO EL ID DE ESA UNIDAD RECIEN CREADA
					$idUnidadNueva = $this->db->prepare('INSERT INTO unidad(nombre_unidad, eliminado) VALUES (?, true) RETURNING id');
					$idUnidadNueva->execute([ $nombreViejo ]);
					$idUnidadNueva = $idUnidadNueva->fetchAll(PDO::FETCH_OBJ);
					$idUnidadNueva = $idUnidadNueva[0]->id;

					//SE ACTUALIZA LA RELACION DE LOS CANDIDATOS POSTULADOS A LA UNIDAD EN ELECCIONES FINALIZADAS CON LA NUEVA UNIDAD RECIEN CREADA
					$res = $this->db->prepare('UPDATE candidato SET id_unidad = ? WHERE id_unidad = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = true)');
					$res->execute([ $idUnidadNueva, $this->id ]);
				}
				//SE ACTUALIZA EL NOMBRE DE LA UNIDAD
				$res = $this->db->prepare('UPDATE unidad SET nombre_unidad = ? WHERE id = ?');
				$res->execute([ $datos['nombre'], $this->id ]);
			}catch(Exception $e){
				error_log($e);
				$this->db->rollBack();
				return false;
			}

			if($res->rowCount() > 0){
				$nombre = $datos['nombre'];
				$this->db->commit();
				return true;
			}else{
				$this->db->rollBack();
				return false;
			}
		}

		public function borrarUnidad(){
			try{
				$this->db->beginTransaction();
				// SE VERIFICA SI LA UNIDAD EXISTE EN ALGUNA ELECCIÓN YA FINALIZADA O COMENZADA, SI ES ASÍ, SE REALIZA UN SOFT DELETE EN LUGAR DE HARD DELETE
				$res = $this->db->prepare('SELECT DISTINCT id_unidad FROM candidato WHERE id_unidad = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = true) OR id_eleccion IN (SELECT DISTINCT id_eleccion FROM votante) AND id_unidad = ?');
				$res->execute([ $this->id, $this->id ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
				if(count($res) > 0){
					// SOFT DELETE A LA UNIDAD
					$res = $this->db->prepare('UPDATE unidad SET eliminado = true WHERE id = ?');
					$res->execute([ $this->id ]);
					// SE BORRAN LOS CANDIDATOS QUE ESTABAN POSTULADOS A ESA UNIDAD EN LAS ELECCIONES QUE NO HAN FINALIZADO Y QUE NO HAN COMENZADO
					$res = $this->db->prepare('DELETE FROM candidato WHERE id_unidad = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = false) AND id_eleccion NOT IN (SELECT DISTINCT id_eleccion FROM votante)');
					$res->execute([ $this->id ]);
					// SE BORRAN LOS COMITES EN LOS CONSEJOS COMUNALES QUE DEPENDIAN DE ESA UNIDAD
					$res = $this->db->prepare('DELETE FROM comite_unidad_consejo WHERE id_unidad = ?');
					$res->execute([ $this->id ]);
					// SE BORRA LA UNIDAD EN LOS CONSEJOS COMUNALES QUE TENIAN ESE COMITE
					$res = $this->db->prepare('DELETE FROM consejocomunal_unidad WHERE id_unidad = ?');
					$res->execute([ $this->id ]);
					$this->db->commit();
					return true;
				}

				// HARD DELETE SI LA UNIDAD NO ESTÁ EN UNA ELECCIÓN FINALIZADA
				$res = $this->db->prepare('DELETE FROM unidad WHERE id = ?');
				$res->execute([ $this->id ]);
			}catch(Exception $e){
				error_log($e);
				$this->db->rollBack();
				return false;
			}

			if($res->rowCount() > 0){
				$this->db->commit();
				return true;
			}else{
				$this->db->rollBack();
				return false;
			}
		}

		public function agregarUnidad(){
			try{
				$res = $this->db->prepare('INSERT INTO unidad(nombre_unidad) VALUES (?) RETURNING id');
				$res->execute([ $this->nombre ]);
			}catch(Exception $e){
				return false;
			}

			if($res->rowCount() > 0){
				$id = $res->fetchAll(PDO::FETCH_OBJ);
				$id = $id[0]->id;
				return true;
			}else{
				return false;
			}
		}

		public function borrarUnidadConsejo($cod, $unidades){
			try{
				require_once('./models/eleccionesModel.php');
				$eleccionesModel = new eleccionesModel();

				if($eleccionesModel->verificarSiConsejoTieneEleccionComenzada($cod)){
					die('eleccion_comenzada');
				}

				$this->db->beginTransaction();
				foreach($unidades as $idUnidad){
					$this->setId($idUnidad);
					//BORRAR TODOS LOS CANDIDATOS POSTULADOS A ESA UNIDAD Y LUEGO BORRARLA
					$res = $this->db->prepare('DELETE FROM consejocomunal_unidad WHERE cod_consejocomunal = ? AND id_unidad = ?');
					$res2 = $this->db->prepare('DELETE FROM comite_unidad_consejo WHERE cod_consejo = ? AND id_unidad = ?');

					$id_eleccion = $this->db->prepare('SELECT id FROM eleccion WHERE finalizado = false AND cod_consejocomunal = ? AND id NOT IN (SELECT DISTINCT id_eleccion FROM votante)');
					$id_eleccion->execute([$cod]);
					$id_eleccion = $id_eleccion->fetchAll(PDO::FETCH_OBJ);
					foreach($id_eleccion as $id){
						$id = $id->id;
						$res3 = $this->db->prepare('DELETE FROM candidato WHERE id_unidad = ? AND id_eleccion = ?');
						$res3->execute([ $this->id, $id ]);
					}
					
					$res->execute([ $cod, $this->id ]);
					$res2->execute([ $cod, $this->id ]);
				}
			}catch(Exception $e){
				error_log($e);
				$this->db->rollBack();
				return false;
			}

			if($res->rowCount() > 0){
				$this->db->commit();
				return true;
			}else{
				$this->db->rollBack();
				return false;
			}
		}

		public function borrarComiteUnidad($cod, $comites){
			try{
				require_once('./models/eleccionesModel.php');
				$eleccionesModel = new eleccionesModel();

				if($eleccionesModel->verificarSiConsejoTieneEleccionComenzada($cod)){
					die('eleccion_comenzada');
				}

				$this->db->beginTransaction();
				foreach($comites as $idComite){
					//BORRAR TODOS LOS CANDIDATOS POSTULADOS AL COMITE A BORRAR Y LUEGO BORRAR ESE COMITE
					$id_eleccion = $this->db->prepare('SELECT id FROM eleccion WHERE finalizado = false AND cod_consejocomunal = ? AND id NOT IN (SELECT DISTINCT id_eleccion FROM votante)');
					$id_eleccion->execute([$cod]);
					$id_eleccion = $id_eleccion->fetchAll(PDO::FETCH_OBJ);
					foreach($id_eleccion as $id){
						$id = $id->id;
						$res = $this->db->prepare('DELETE FROM candidato WHERE id_comite = ? AND id_unidad = ? AND id_eleccion = ?');
						$res->execute([ $idComite, $this->id, $id ]);
					}
					$res = $this->db->prepare('DELETE FROM comite_unidad_consejo WHERE id_comite = ? AND id_unidad = ? AND cod_consejo = ?');
					$res->execute([ $idComite, $this->id, $cod ]);
				}
			}catch(Exception $e){
				error_log($e);
				$this->db->rollBack();
				return false;
			}

			if($res->rowCount() > 0){
				$this->db->commit();
				return true;
			}else{
				$this->db->rollBack();
				return false;
			}
		}

		public function getPorConsejocomunal($codConsejocomunal){
			try{
				$res = $this->db->prepare('SELECT *,(SELECT COUNT(id_comite) FROM comite_unidad_consejo WHERE id_unidad = consejocomunal_unidad.id_unidad AND cod_consejo = consejocomunal_unidad.cod_consejocomunal) AS comites FROM consejocomunal_unidad,unidad WHERE id_unidad = id AND cod_consejocomunal = ? ORDER BY nombre_unidad ASC');
				$res->execute([ $codConsejocomunal ]);
				$res = $res->fetchAll(PDO::FETCH_OBJ);
			}catch(Exception $e){
				return false;
			}

			return $res;
		}

		public function verificarSiUnidadTieneEleccionComenzada(){
			try{
				$res = $this->db->prepare('SELECT DISTINCT 1 FROM candidato WHERE id_unidad = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = false) AND id_eleccion IN (SELECT id_eleccion FROM votante)');
				$res->execute([ $this->id ]);
				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				die(false);
				return true;
			}
		}

		public function verificarSiUnidadTieneEleccionFinalizada(){
			try{
				$res = $this->db->prepare('SELECT DISTINCT 1 FROM candidato WHERE id_unidad = ? AND id_eleccion IN (SELECT id FROM eleccion WHERE finalizado = true)');
				$res->execute([ $this->id ]);
				if($res->rowCount() > 0){
					return true;
				}else{
					return false;
				}
			}catch(Exception $e){
				error_log($e);
				die(false);
				return true;
			}
		}

	}

?>